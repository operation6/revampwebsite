import "./personalDetails.css";
import { useEffect, useState } from "react";

const PersonalDetails = () => {
  const [user, setUser] = useState({});

  useEffect(() => {
    const userData = JSON.parse(localStorage.getItem("oceanAllUserData"));
    // console.log(userData);
    setUser(userData);
  }, []);

  return (
    <>
      <div className="myProfileRightpart">
        <div class="personaldetailDiv">
          <div class="container">
            <p class="detailSpan">Personal Details</p>

            <p class="personaldetailItems">First Name</p>
            <p class="personaldetailItems">{user?.first_name || "N/A"}</p>
            <div class="hrdiv"></div>

            <p class="personaldetailItems">Last Name</p>
            <p class="personaldetailItems">{user?.last_name || "N/A"}</p>
            <p class="personaldetailItems">Registered Email</p>
            <p class="personaldetailItems">{user?.email || "N/A"}</p>
            <div class="hrdiv"></div>
            {/* <p class="personaldetailItems">Password</p> */}
            {/* <p class="personaldetailItems" style={{display: 'block', width: '99%', whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis'}}>{user?.password || "N/A"}</p> */}
            <p class="personaldetailItems">Contact</p>
            <p class="personaldetailItems">{user?.phone_number || "N/A"}</p>
            {/* <div class="hrdiv"></div> */}
            {/* <p class="personaldetailItemslast"></p>
            <p class="personaldetailItemslast"></p>
            <div class="hrdiv"></div> */}

          </div>
        </div>
        <div class="personaldetailDiv">
          <div class="container">
            <p class="detailSpan">Company Information</p>
            <p class="personaldetailItems">Company Name</p>
            <p class="personaldetailItems profile_company_name">{user?.company_name || "N/A"}</p>
            <div class="hrdiv"></div>
            <p class="personaldetailItems">Company Domain</p>
            <p class="personaldetailItems">{user?.company_domain || "N/A"}</p>
            <p class="personaldetailItems">Country</p>
            <p class="personaldetailItems">{user?.country  || "N/A"}</p>
            {/* <div class="hrdiv"></div>
            <p class="personaldetailItems">Vat No</p>
            <p class="personaldetailItems">{user?.vat}</p> */}
            {/* <p class="personaldetailItems">Company Registration no</p>
            <p class="personaldetailItems">3894739873945</p> */}
            <div class="hrdiv"></div>
            <p class="personaldetailItems">Company Type</p>
            <p class="personaldetailItems">{user?.company_type?.value || "N/A"}</p>
            <p class="personaldetailItems">Company Main Email</p>
            <p class="personaldetailItems">{user?.email || "N/A"}</p>
            <div class="hrdiv"></div>
            
            <p class="personaldetailItems">Company Address</p>
            <p class="personaldetailItems">{user?.company_address || "N/A"}</p>
            {/* <div class="hrdiv"></div> */} 
            <p class="personaldetailItemslast"></p>
            <p class="personaldetailItemslast"></p>
            
          </div>
        </div>
      </div>
    </>
  );
};

export default PersonalDetails;
