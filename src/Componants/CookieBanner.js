import React, { useState } from "react";
import { Box, Typography, Button, Link, IconButton } from "@mui/material";
import { CloseOutlined } from "@mui/icons-material";

const CookieBanner = () => {
  const [showBanner, setShowBanner] = useState(true);

  return (
    <Box
      display="flex"
      flexDirection={{ xs: "column", md: "row" }}
      p={1}
      border="1px solid #ccc"
      borderRadius="3px"
      position="fixed"
      right="0px"
      bottom={showBanner ? "0px" : "-200px"} // Adjust the -200px based on your banner height
      zIndex={99}
      style={{
        backgroundColor: "#efefef",
        color: "#0c1a2e",
        width: "100%",
        transition: "bottom 0.5s ease",
      }}
    >
      <IconButton
        variant="outlined"
        size="small"
        onClick={() => setShowBanner(false)}
        style={{
          position: "absolute",
          right: "0px",
          top: "-16px",
          backgroundColor: "#fff",
          borderRadius: "50%",
          width: "35px",
          height: "35px",
          minWidth: "auto",
          border: "1px solid #ccc",
        }}
      >
        <CloseOutlined />
      </IconButton>
      <Box flex={4} p={2}>
        <Typography variant="h7" fontWeight="bold">
          Enhance Your Browsing Experience
        </Typography>
        <Typography variant="body2" id="onetrust-policy-text">
          We use cookies and similar technologies to ensure the basic
          functionality of the site and to enhance your experience. Some cookies
          are essential, while others help us to personalize content and ads,
          analyze our traffic, and provide social media features. By clicking
          “Accept All Cookies,” you consent to the use of all cookies. You can
          change your cookie preferences or withdraw your consent at any time.{" "}
          <Link
            href="https://theoceann.ai/cookies"
            target="_blank"
            className="ot-cookie-policy-link"
          >
            Learn more about our cookie policy.
          </Link>
        </Typography>
      </Box>
      <Box
        flex={1}
        display="flex"
        flexDirection="column"
        justifyContent="space-around"
        p={2}
      >
        <Button
          size="small"
          style={{
            backgroundColor: "#39b1ff",
            borderColor: "#39b1ff",
            color: "#FFFFFF",
            marginBottom: "5px",
          }}
        >
          Accept All Cookies
        </Button>
        <Button
          size="small"
          style={{
            backgroundColor: "#39b1ff",
            borderColor: "#39b1ff",
            color: "#FFFFFF",
            marginBottom: "5px",
          }}
        >
          Reject Optional Cookies
        </Button>
        <Button
          variant="outlined"
          size="small"
          style={{
            backgroundColor: "#f39c12",
            borderColor: "#f39c12",
            color: "#fff",
          }}
        >
          Manage Cookie Settings
        </Button>
      </Box>
    </Box>
  );
};

export default CookieBanner;
