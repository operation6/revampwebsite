import React, { useEffect, useRef, useState } from "react";
import {
  ArrowsAltOutlined,
  CloseOutlined,
  //   FullscreenOutlined,
  LeftOutlined,
  MailOutlined,
  //   MessageOutlined,
  //   MessageTwoTone,
  MinusOutlined,
  QuestionCircleOutlined,
  ReloadOutlined,
  RightOutlined,
  SendOutlined,
  SyncOutlined,
  UserOutlined,
} from "@ant-design/icons";
import {
  Button,
  Card,
  //   Descriptions,
  Form,
  //   Grid,
  Input,
  Popover,
  //   Table,
  Tooltip,
  Typography,
  Tag,
} from "antd";
import "./chatbot.css";
// import Icon from "@ant-design/icons/lib/components/Icon";
// import URL_WITH_VERSION, { IMAGE_PATH, postAPICallService } from "../../shared";
import { useLocation, useNavigate } from "react-router";
// import { useDispatch } from "react-redux";
// import { setChatData } from "../../actions/chatActions";
import { Link } from "react-router-dom";
// import { FaUserCircle } from "react-icons/fa";
// import Draggable from "react-draggable";
// import moment from "moment";
// import ChatPortCall from "./ChatPortCall";
// import ChatVesselInfo from "./ChatVesselInfo";
// import ChatVesselPosition from "./ChatVesselPosition";
// import ChatLiveBunkerPrice from "./ChatLiveBunkerPrice";
// import ChatPortDistance from "./ChatPortDistance";
// import { chatAlertMessages, questionList } from "./chatData";
// import ChatInfo from "./ChatInfo";
// import Slider from "react-slick";
// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";
// import DefaultQuestionsSlider from "./DefaultQuestionsSlider";
// import ChatVesselLineUp from "./ChatVesselLineUp";
// import ChatWeather from "./ChatWeather";
// import ChatPortInfo from "./ChatPortInfo";

// import { setAllEmails, setShowAiMails } from "../../actions/emailActions";
// import { color } from "echarts";
// import { setAllEmails } from "../../actions/emailActions";
export const IMAGE_PATH = process.env.REACT_APP_IMAGE_PATH;
const ShadowDomComponent = ({ html }) => {
  const containerRef = useRef(null);

  useEffect(() => {
    const shadowRoot = containerRef.current.attachShadow({ mode: "open" });
    const div = document.createElement("div");
    div.innerHTML = html;

    // Append the content to the shadow root
    shadowRoot.appendChild(div);

    // Optionally, add styles to the shadow DOM
    const style = document.createElement("style");
    style.textContent = `
            /* Add your shadow DOM styles here */
            div {
                background-color: white; /* Example style */
                color: black;
            }
        `;
    shadowRoot.appendChild(style);

    return () => {
      // Cleanup on unmount
      while (shadowRoot.firstChild) {
        shadowRoot.removeChild(shadowRoot.firstChild);
      }
    };
  }, [html]);

  return <div className="shadow" ref={containerRef}></div>;
};
export const Chatbot = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [closeMin, setCloseMin] = useState(false);
  const [isMinimized, setMinimized] = useState(false);
  const [chatHistory, setChatHistory] = useState([]);
  const [sendQuery, setSendQuery] = useState("");
  const [chatload, setchatload] = useState(false);
  const [page, setPage] = useState(1);
  const [MessageButton, setMessageButton] = useState("");
  const [gotoredirectPage, setgotoredirectPage] = useState("");
  const [questions, setQuestions] = useState([
    {
      question: "OceannAI, What is oceann mail?",
      icon: "tabler:mail",

      background: "rgb(255, 250, 250)" /* Snow */,
      color: "rgb(255, 105, 180)",
    },
    {
      question: "OceannAI, What is Oceann VM?",
      icon: "tabler:filter",
      background: "rgb(245, 255, 250)" /* Mint Cream */,
      color: "rgb(102, 205, 170)",
    },
    {
      question: "OceannAI, How to reset my password in Oceann VM?",
      icon: "tabler:filter",
      background: "rgb(250, 255, 240)" /* Ivory */,
      color: "rgb(90 136 90)",
    },
    {
      question: "OceannAI, How can I Contact Support?",
      icon: "tabler:calendar",
      background: "rgb(255, 250, 240)" /* Floral White */,
      color: "rgb(255, 160, 122)",
    },
    {
      question: "OceannAI, What pricing plans are available?",
      icon: "tabler:invoice",
      background: "rgb(255, 250, 250)" /* Snow */,
      color: "rgb(255, 105, 180)",
    },
    {
      question: "OceannAI, How to track your voyages in Oceann Voyage Manager?",
      icon: "tabler:report",
      background: "rgb(245, 255, 250)" /* Mint Cream */,
      color: "rgb(132 107 235)",
    },
    {
      question: "OceannAI, How does Oceann Mail handle email security?",
      icon: "tabler:filter",
      background: "rgb(250, 240, 230)" /* Linen */,
      color: "rgb(235 149 72)",
    },
    {
      question: "OceannAI, Can I schedule emails in Oceann Mail?",
      icon: "tabler:report-money",
      background: "rgb(255, 240, 245)" /* Lavender Blush */,
      color: "rgb(199 103 199)",
    },
    {
      question:
        "OceannAI, Can Oceann Voyage Manager optimize my route for fuel efficiency?",
      icon: "tabler:calendar-event",
      background: "rgb(245, 255, 250)" /* Mint Cream */,
      color: "rgb(102, 205, 170)",
    },
    {
      question:
        "OceannAI, How do I manage crew communication through Oceann Mail?",
      icon: "tabler:map-pin",
      background: "rgb(255, 250, 250)" /* Snow */,
      color: "rgb(255, 105, 180)",
    },
  ]);
  const [currentItem, setCurrentItem] = useState([]);

  const location = useLocation();
  //   const dispatch = useDispatch();
  const navigate = useNavigate();

  const [startPosition, setStartPosition] = useState({ x: 0, y: 0 });
  const [popoverVisible, setPopoverVisible] = useState(false);
  const [showMessage, setShowMessage] = useState(false);

  const showMessageBox = () => {
    setShowMessage(true);
    setTimeout(() => {
      setShowMessage(false);
    }, 15 * 1000); // Hide message box after 15 seconds
  };

  useEffect(() => {
    // Show the message box initially and then every 20 seconds
    showMessageBox();
    const interval = setInterval(() => {
      // Pick a random item from the array
      //   const randomItem =
      //     chatAlertMessages[
      //     Math.floor(Math.random() * chatAlertMessages?.length)
      //     ];
      //   setCurrentItem(randomItem);
      showMessageBox();
    }, 5 * 60 * 1000); // Show message box every 5 minutes

    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    const open = props.isOpen;
    if (open) {
      handleOpen();
    }
  }, [props]);

  const handleVisibleChange = (visible) => {
    setPopoverVisible(visible);
  };

  const handleDragStart = (e, data) => {
    setStartPosition({ x: data.x, y: data.y });
  };

  const handleDragStop = (e, data) => {
    // Check if it's a click (not a drag)
    if (
      Math.abs(data.x - startPosition.x) < 2 &&
      Math.abs(data.y - startPosition.y) < 2
    ) {
      //TODO: while dragging stop if cursor remains on top of chaticon then poup gets open.
      // handleOpen();
      setShowMessage(false);
    }
  };

  const handleOpen = () => {
    // if (!isOpen) {

    setIsOpen(true);
    setPage(1);
    setSendQuery("");
    setgotoredirectPage("");
    setMessageButton("");
    setMinimized(false);
    setCloseMin(false);

    // }
    // else {
    //     setIsOpen(false);
    //     // dispatch(setShowAiMails(false))
    //     setChatHistory([]);
    //     setPage(1);
    //     setSendQuery("");
    //     setgotoredirectPage("");
    //     setMessageButton("");
    //     setCloseMin(true);
    //     localStorage.setItem("isChatWindow", false); // to close the chat window
    // }
  };

  const handleClose = () => {
    props.toggleChatbot(false)
    setIsOpen(false);
    // dispatch(setShowAiMails(false))
    setChatHistory([]);
    setPage(1);
    setSendQuery("");
    setgotoredirectPage("");
    setMessageButton("");
    setCloseMin(true);
    localStorage.setItem("isChatWindow", false); // to close the chat window
  };

  const handleNewChat = () => {
    // setIsOpen(false)
    // dispatch(setShowAiMails(false));
    setChatHistory([]);
    setPage(1);
    setSendQuery("");
    setgotoredirectPage("");
    setMessageButton("");
  };

  const handleMinimize = () => {
    setMinimized(true);
  };

  const handleMaximize = () => {
    setMinimized(false);
  };

  const handleKeyPress = (event) => {
    if (event.key === "Enter" && !event.shiftKey) {
      event.preventDefault();
      sendChatApi(sendQuery, null);
    }
  };

  // const sendChatApi = async (query, page) => {
  //     // console.log("query",query,"page",page)
  //     if (query?.trim() === "") return;
  //     setchatload(true);
  //     try {
  //         setSendQuery("");
  //         if (query.trim()) {
  //             if (!page) {
  //                 setChatHistory((prev) => [...prev, { type: "send", message: query }]);
  //             }
  //             // Scroll to bottom after user message is added
  //             setTimeout(() => {
  //                 if (lastMessageRef.current) {
  //                     lastMessageRef.current.scrollIntoView({
  //                         behavior: "smooth",
  //                         block: "end",
  //                     });
  //                 }
  //             }, 100);

  //             setChatHistory((prev) => [...prev, { type: "response", message: "" }]);
  //             // let url = `${URL_WITH_VERSION}/oceann_ai/chat_bot?page=${page}`;
  //             // if (page) {
  //             //   url = `${URL_WITH_VERSION}/marine/chat-query?page=${page}`
  //             // }
  //             // const url = "http://192.168.18.175:5000/api/v1/oceann_ai/chat_bot";
  //             const isToken = true;
  //             let payload = {
  //                 query: query,
  //                 token: localStorage.getItem("oceanToken")
  //                     ? localStorage.getItem("oceanToken")
  //                     : "",
  //             };
  //             // const response = await postAPICallService({
  //             //   url,
  //             //   isToken,
  //             //   payload,
  //             //   showMessage: false,
  //             // });

  //             // console.log("chatresponse", response)
  //             //         if (response.status) {
  //             //           setchatload(false);
  //             //           if (response.key === "voyage") {
  //             //             if (location.pathname.includes("voyage-manager-list")) {
  //             //               setChatHistory((prev) => [
  //             //                 ...prev,
  //             //                 {
  //             //                   type: "response",
  //             //                   message: response.response,
  //             //                   data: response.data,
  //             //                   key: response.key,
  //             //                 },
  //             //               ]);
  //             //             //   dispatch(setChatData({ data: response.data, key: response.key }));
  //             //             } else {
  //             //               setChatHistory((prev) => [
  //             //                 ...prev,
  //             //                 {
  //             //                   type: "response",
  //             //                   message: response.response,
  //             //                   data: response.data,
  //             //                   key: response.key,
  //             //                 },
  //             //               ]);
  //             //             //   dispatch(setChatData({ data: response.data, key: response.key }));
  //             //               setgotoredirectPage("/voyage-manager-list");
  //             //             }
  //             //           } else if (response.key === "filter_bunker_price") {
  //             //             if (location.pathname.includes("spot-price")) {
  //             //               setChatHistory((prev) => [
  //             //                 ...prev,
  //             //                 {
  //             //                   type: "response",
  //             //                   message: response.response ?? "Data fetched successfully.",
  //             //                   data: response.data,
  //             //                   key: response.key,
  //             //                 },
  //             //               ]);
  //             //             //   dispatch(setChatData({ data: response.data, key: response.key }));
  //             //             } else {
  //             //               setChatHistory((prev) => [
  //             //                 ...prev,
  //             //                 {
  //             //                   type: "response",
  //             //                   message: response.response ?? "Data fetched successfully.",
  //             //                   data: response.data,
  //             //                   key: response.key,
  //             //                 },
  //             //               ]);
  //             //             //   dispatch(setChatData({ data: response.data, key: response.key }));
  //             //               setgotoredirectPage("/spot-price");
  //             //             }
  //             //           } else if (response.key === "mail") {
  //             //             if (location.pathname.includes("mails")) {
  //             //               setChatHistory((prev) => [
  //             //                 ...prev,
  //             //                 {
  //             //                   type: "response",
  //             //                   message: response.response ?? "Data fetched successfully.",
  //             //                   data: response.data,
  //             //                   key: response.key,
  //             //                   query: response?.query

  //             //                 },
  //             //               ]);
  //             //               // setMessageButton(response?.paginate)
  //             //               setPage(page + 1)
  //             //             //   dispatch(setChatData({ data: response.data, key: response.key }));
  //             //             //   dispatch(setAllEmails({ "emails": response?.data }))
  //             //               setMessageButton(response?.paginate)

  //             //             } else {
  //             //               setChatHistory((prev) => [
  //             //                 ...prev,
  //             //                 {
  //             //                   type: "response",
  //             //                   message: response.response ?? "Data fetched successfully.",
  //             //                   data: response.data,
  //             //                   key: response.key,
  //             //                   query: response?.query
  //             //                 },
  //             //               ]);
  //             //               setPage(page + 1)
  //             //               setMessageButton(response?.paginate)
  //             //             //   dispatch(setChatData({ data: response.data, key: response.key }));

  //             //             }
  //             //           } else if (response.key == "live_bunker_price") {
  //             //             setChatHistory((prev) => [
  //             //               ...prev,
  //             //               {
  //             //                 type: "response",
  //             //                 message: response.response,
  //             //                 data: response.data,
  //             //                 key: response.key,
  //             //               },
  //             //             ]);
  //             //             // dispatch(setChatData({ data: response.data, key: response.key }));
  //             //           } else if (response.key == "vessel_position") {
  //             //             setChatHistory((prev) => [
  //             //               ...prev,
  //             //               {
  //             //                 type: "response",
  //             //                 message: response.response,
  //             //                 data: response.data,
  //             //                 key: response.key,
  //             //               },
  //             //             ]);
  //             //             // dispatch(setChatData({ data: response.data, key: response.key }));
  //             //           } else if (response.key == "vessel_info") {
  //             //             setChatHistory((prev) => [
  //             //               ...prev,
  //             //               {
  //             //                 type: "response",
  //             //                 message: response.response,
  //             //                 data: response.data,
  //             //                 key: response.key,
  //             //               },
  //             //             ]);
  //             //             // dispatch(setChatData({ data: response.data, key: response.key }));
  //             //           } else if (response.key == "port_call") {
  //             //             setChatHistory((prev) => [
  //             //               ...prev,
  //             //               {
  //             //                 type: "response",
  //             //                 message: response.response,
  //             //                 data: response.data,
  //             //                 key: response.key,
  //             //               },
  //             //             ]);
  //             //             // dispatch(setChatData({ data: response.data, key: response.key }));
  //             //           } else if (response.key == "port_distance") {
  //             //             setChatHistory((prev) => [
  //             //               ...prev,
  //             //               {
  //             //                 type: "response",
  //             //                 message: response.response,
  //             //                 data: response.data,
  //             //                 key: response.key,
  //             //               },
  //             //             ]);
  //             //             // dispatch(setChatData({ data: response.data, key: response.key }));
  //             //           } else if (response.key == "info") {
  //             //             setChatHistory((prev) => [
  //             //               ...prev,
  //             //               {
  //             //                 type: "response",
  //             //                 message: response.response,
  //             //                 data: response.data,
  //             //                 key: response.key,
  //             //               },
  //             //             ]);
  //             //             // dispatch(setChatData({ data: response.data, key: response.key }));
  //             //           } else if (response.key == "vessel_lineup") {
  //             //             setChatHistory((prev) => [
  //             //               ...prev,
  //             //               {
  //             //                 type: "response",
  //             //                 message: response.response,
  //             //                 data: response.data,
  //             //                 key: response.key,
  //             //               },
  //             //             ]);
  //             //             // dispatch(setChatData({ data: response.data, key: response.key }));
  //             //           } else if (response.key == "weather") {
  //             //             setChatHistory((prev) => [
  //             //               ...prev,
  //             //               {
  //             //                 type: "response",
  //             //                 message: response.response,
  //             //                 data: response.data,
  //             //                 key: response.key,
  //             //               },
  //             //             ]);
  //             //             // dispatch(setChatData({ data: response.data, key: response.key }));
  //             //           } else if (response.key == "port_info") {
  //             //             setChatHistory((prev) => [
  //             //               ...prev,
  //             //               {
  //             //                 type: "response",
  //             //                 message: response.response,
  //             //                 data: response.data,
  //             //                 key: response.key,
  //             //               },
  //             //             ]);
  //             //             // dispatch(setChatData({ data: response.data, key: response.key }));
  //             //           }
  //             //           else if (response.key == "other") {
  //             //             setChatHistory((prev) => [
  //             //               ...prev,
  //             //               {
  //             //                 type: "response",
  //             //                 message: response.response,
  //             //                 data: response.data,
  //             //                 key: response.key,
  //             //               },
  //             //             ]);
  //             //           } else {
  //             //             setChatHistory((prev) => [
  //             //               ...prev,
  //             //               { type: "response", message: response.message },
  //             //             ]);
  //             //           }
  //             //         } else {
  //             //           setchatload(false);
  //             //           setChatHistory((prev) => [
  //             //             ...prev,
  //             //             {
  //             //               type: "response",
  //             //               message:
  //             //                 "We apologise, but the information we have on your query is limited, thus we are unable to assist you.",
  //             //             },
  //             //           ]);
  //             //         }
  //                   }
  //                 } catch (error) {
  //                   setchatload(false);
  //                   setChatHistory((prev) => [
  //                     ...prev,
  //                     {
  //                       type: "response",
  //                       message:
  //                         "We apologise, but the information we have on your query is limited, thus we are unable to assist you.",
  //                     },
  //                   ]);
  //                   console.error("Error sending chat:", error);
  //                 }
  //               };
  const sendChatApi = async (query, page) => {
    // console.log("query", query, "page", page);
    if (query?.trim() === "") return;
    setchatload(true);

    try {
      setSendQuery("");

      if (query.trim()) {
        if (!page) {
          setChatHistory((prev) => [...prev, { type: "send", message: query }]);
        }
        // Scroll to bottom after user message is added
        setTimeout(() => {
          if (lastMessageRef.current) {
            lastMessageRef.current.scrollIntoView({
              behavior: "smooth",
              block: "end",
            });
          }
        }, 100);

        setChatHistory((prev) => [...prev, { type: "response", message: "" }]);

        const isToken = true;
        let payload = {
          message: query,
          // token: localStorage.getItem("oceanToken") || "",
        };

        const response = await fetch("https://aiml.azurewebsites.net/chat", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(payload),
        });

        // console.log("chatresponse", response);
        if (response.ok) {
          const data = await response.json();
          // Check if the response has the expected structure
          if (data.response) {
            setchatload(false);

            // Handle HTML response
            const parser = new DOMParser();
            const htmlDocument = parser.parseFromString(
              data.response,
              "text/html"
            );
            const content = htmlDocument.body.innerHTML;

            setChatHistory((prev) => [
              ...prev,
              { type: "response", message: content },
            ]);
          } else {
            setchatload(false);
            setChatHistory((prev) => [
              ...prev,
              {
                type: "response",
                message:
                  "We apologize, but the information we have on your query is limited, thus we are unable to assist you.",
              },
            ]);
          }
        } else {
          setchatload(false);
          setChatHistory((prev) => [
            ...prev,
            {
              type: "response",
              message:
                "We apologize, but the information we have on your query is limited, thus we are unable to assist you.",
            },
          ]);
        }
      }
    } catch (error) {
      setchatload(false);
      setChatHistory((prev) => [
        ...prev,
        {
          type: "response",
          message:
            "We apologize, but the information we have on your query is limited, thus we are unable to assist you.",
        },
      ]);
      console.error("Error sending chat:", error);
    }
  };

  const lastMessageRef = useRef(null);
  useEffect(() => {
    if (lastMessageRef.current) {
      lastMessageRef.current.scrollIntoView({
        behavior: "smooth",
        block: "end",
      });
    }
  }, [chatHistory]);

  // Chatbot open from popup when click on d
  const getLocalStorage = localStorage.getItem("isChatWindow");

  useEffect(() => {
    if (getLocalStorage === "true") {
      setIsOpen(true);
      setMinimized(false);
      setCloseMin(false);
    } else {
      setIsOpen(false);
    }
  }, [getLocalStorage]);

  const NextArrow = (props) => {
    const { onClick } = props;
    return (
      <div
        style={{
          right: "10px",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          width: "30px",
          height: "30px",
          backgroundColor: "#000",
          color: "#fff",
          borderRadius: "50%",
          position: "absolute",
          zIndex: "1",
          top: "50%",
          transform: "translateY(-50%)",
          cursor: "pointer",
        }}
        onClick={onClick}
      >
        <RightOutlined />
      </div>
    );
  };

  const PrevArrow = (props) => {
    const { onClick } = props;
    return (
      <div style={{ left: "10px" }} onClick={onClick}>
        <LeftOutlined />
      </div>
    );
  };

  const goToMails = (data) => {
    // console.log("dataaaaaa",data)
    // dispatch(setShowAiMails(true))
    // navigate("/mails")
    // dispatch(setAllEmails({ emails: data }))
  };

  return (
    <>
      <div
        style={{
          position: "fixed",
          bottom: "60px",
          right: "10px",
          zIndex: "1000",
          backgroundColor: "#003e78",
          borderRadius: "50%",
          padding: "8px",
          boxShadow: " 0px 0px 20px #003e78",
          transition: "box-shadow 0.3s ease",
        }}
      >
        {/* <MessageOutlined style={{ fontSize: "30px", color: "white" }} /> */}
        <Tooltip placement="topLeft" title={"Open OceannAI Chatbot"}>
          <div style={{ cursor: "pointer" }} onClick={handleOpen}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              aria-hidden="true"
              role="img"
              fontSize="30"
              className="iconify iconify--ri"
              height="1.1em"
              width="1.1em"
              // width="2em"
              // height="2em"
              viewBox="0 0 24 24"
              style={{ color: "white" }}
            >
              <path
                fill="currentColor"
                d="M7.291 20.824L2 22l1.176-5.291A9.96 9.96 0 0 1 2 12C2 6.477 6.477 2 12 2s10 4.477 10 10s-4.477 10-10 10a9.96 9.96 0 0 1-4.709-1.176m.29-2.113l.653.35A7.96 7.96 0 0 0 12 20a8 8 0 1 0-8-8c0 1.335.325 2.617.94 3.766l.349.653l-.655 2.947zM7 12h2a3 3 0 1 0 6 0h2a5 5 0 0 1-10 0"
              />
              <text
                x="50%"
                y="45%"
                dominantBaseline="middle"
                textAnchor="middle"
                fontSize="6"
                fontWeight="bold"
                fill="white"
              >
                AI
              </text>
            </svg>
          </div>
        </Tooltip>
        {showMessage && (
          <div
            style={{
              position: "fixed",
              bottom: "-30px",
              right: "60px",
              minWidth: "330px",
              maxWidth: "350px",
              backgroundColor: "lightblue",
              border: "1px solid #ccc",
              borderRadius: "10px",
              padding: "0 10px 10px",
              boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.1)",
              zIndex: "1000",
              display: "flex",
              flexDirection: "column",
              fontSize: "14px",
            }}
          >
            <div style={{ display: "flex", justifyContent: "flex-end" }}>
              <div
                style={{
                  textAlign: "end",
                  pointerEvents: "auto",
                  display: "inline-block",
                  cursor: "pointer",
                }}
                onClick={(e) => {
                  e.stopPropagation();
                  setShowMessage(false);
                }}
              >
                x
              </div>
            </div>
            <div
              style={{
                position: "absolute",
                right: "-10px",
                top: "50%",
                width: "0",
                height: "0",
                borderLeft: "10px solid #ccc",
                borderTop: "10px solid transparent",
                borderBottom: "10px solid transparent",
                transform: "translateY(-50%)",
              }}
            />
            <div
              style={{
                position: "absolute",
                right: "-8px",
                top: "50%",
                width: "0",
                height: "0",
                borderLeft: "10px solid lightblue",
                borderTop: "10px solid transparent",
                borderBottom: "10px solid transparent",
                transform: "translateY(-50%)",
              }}
            />
            <strong>{currentItem?.header}</strong>
            <div>{currentItem?.text}</div>
          </div>
        )}
      </div>

      {isMinimized && !closeMin ? (
        <div
          className="minimized-header"
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            backgroundColor: "#003e78",
            padding: "0.5rem",
            borderRadius: "0.75rem 0.75rem 0rem 0rem",
            zIndex: "1000",
            gap: "15px",
          }}
        >
          <div className="header-box-img  ">
            <div className="bot-header">
              <img src="/images/girl-with-laptop.png" className="bot-img" />
            </div>
            <div style={{ display: "flex", flexDirection: "column" }}>
              <span style={{ fontWeight: "500", color: "white" }}>
                Oceann AI
              </span>
              <span style={{ color: "white", fontSize: "12px" }}>Online</span>
            </div>
          </div>
          {/* <Tooltip title='New Chat'>
        <IconButton onClick={handleNewChat}>
          <Icon icon='tabler:refresh' fontSize={20} style={{ color: 'white' }} />
        </IconButton>
      </Tooltip> */}
          <Tooltip title="Maximize">
            <div onClick={handleMaximize}>
              {/* <FullscreenOutlined style={{ fontSize: "15px", color: "white" }}/> */}
              <ArrowsAltOutlined
                style={{ fontSize: "15px", color: "white", cursor: "pointer" }}
              />
            </div>
          </Tooltip>
          <div onClick={handleClose}>
            <CloseOutlined
              style={{ fontSize: "15px", color: "white", cursor: "pointer" }}
            />
          </div>
        </div>
      ) : (
        <div
          className="chat-box max-xxxs:w-[90%] max-xxxs:h-[70%] max-xxxs:right-4 max-xxs:h-[65%] max-xxs:w-[90%] max-xxs:right-4 max-xs:h-[65%] max-xs:w-[75%]  w-[400px] h-[80%] right-[5rem]"
          style={{
            transform: `translateY(${isOpen ? 0 : "100%"}))`,
            backgroundImage: `url(../images/chat_bg_white.png)`,
            visibility: isOpen ? "" : "hidden",
          }}
        >
          <div className=""
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              backgroundColor: "#003e78",
              // background: 'linear-gradient(45deg, #39B1FF 30%, #39B1FF 100%)',
              padding: "0.5rem",
              borderRadius: "0.75rem 0.75rem 0rem 0rem",
            }}
          >
            <div className="header-box-img">
              <div className="bot-header">
                <img src="/images/girl-with-laptop.png" className="bot-img" />
              </div>
              <div style={{ display: "flex", flexDirection: "column" }}>
                <span style={{ fontWeight: "500", color: "white" }}>
                  Oceann AI 🌊
                </span>
                <span style={{ color: "white", fontSize: "12px" }}>Online</span>
              </div>
            </div>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                columnGap: "15px",
              }}
            >
              <Tooltip title="New Chat">
                <div onClick={handleNewChat}>
                  <SyncOutlined
                    style={{
                      fontSize: "20px",
                      color: "white",
                      cursor: "pointer",
                    }}
                  />
                </div>
              </Tooltip>
              <Tooltip title="Minimize">
                <div onClick={handleMinimize}>
                  <MinusOutlined
                    style={{
                      fontSize: "20px",
                      color: "white",
                      cursor: "pointer",
                    }}
                  />
                </div>
              </Tooltip>
              <div onClick={handleClose}>
                <CloseOutlined
                  style={{
                    fontSize: "20px",
                    color: "white",
                    cursor: "pointer",
                  }}
                />
              </div>
            </div>
          </div>
          {chatHistory && (
            <>
              {chatHistory.length == 0 ? (
                <>
                  <div
                    style={{ overflowY: "auto", height: "100%" }}
                    className="custom-scroll"
                  >
                    <div
                      style={{
                        marginTop: "1rem",
                        paddingLeft: "0.5rem",
                        paddingRight: "0.5rem",
                        backgroundImage: `url('../images/chat_bg_white.png') `,
                      }}
                    >
                      <Card className="default_card2">
                        <div
                          style={{ display: "flex", flexDirection: "column" }}
                        >
                          <div className="image-container">
                            <img src={IMAGE_PATH + "video/chartbot_icon.gif"} />
                            {/* <FaUserCircle /> */}
                          </div>
                          <div
                            style={{
                              backgroundColor: "rgb(102, 205, 170)",
                              fontWeight: "500",
                              fontSize: "12px",
                            }}
                          >
                            Are you looking for maritime information? 10X
                            faster! 🚀
                          </div>
                        </div>
                      </Card>

                      <Card
                        sx={{
                          borderRadius: "15px 15px 15px 0px", // top-left, top-right, bottom-right, bottom-left
                          maxWidth: "75%",
                          padding: "2px",
                          marginBottom: "2rem",
                          boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.1)",
                          // backgroundColor: "red",
                          // borderRadius:"8px 8px 0px 0"
                        }}
                        className="default_card"
                      >
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                            borderRadius: "15px 15px 0 0",
                          }}
                          className="main-card"
                        >
                          <div
                            className="bot-header"
                            style={{ marginRight: "10px" }}
                          >
                            <img
                              src="/images/girl-with-laptop.png"
                              className="bot-img"
                            />
                          </div>
                          <Typography
                            sx={{
                              fontSize: "12px",
                              color: "#333",
                            }}
                            style={{
                              fontWeight: "500",
                              padding: "5px",
                              fontSize: "12px",
                            }}
                          >
                            Hi there! I'm here to assist you with any questions
                            you might have.
                          </Typography>
                        </div>
                      </Card>
                    </div>

                    <div
                      className="chat-slider"
                      style={{ maxWidth: "25rem", margin: "auto" }}
                    >
                      {/* <DefaultQuestionsSlider sendChatApi={sendChatApi} /> */}
                    </div>
                  </div>
                </>
              ) : (
                <>
                  <div
                    className="msg-container"
                    style={{
                      padding: "0.5rem",
                      overflowY: "auto",
                    }}
                  >
                    {chatHistory.map((chat, index) => (
                      <>
                        <div
                          key={index}
                          ref={
                            index === chatHistory.length - 1
                              ? lastMessageRef
                              : null
                          }
                          style={{
                            display: "flex",
                            flexDirection: "column",
                            alignItems:
                              chat.type === "send" ? "flex-end" : "flex-start",
                          }}
                        >
                          {chat.type === "send" ? (
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "column",
                                alignItems: "flex-end",
                                width: "100%",
                              }}
                            >
                              <div
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                  gap: "5px",
                                  marginBottom: "5px",
                                }}
                              >
                                <p
                                  style={{
                                    fontSize: "0.75rem",
                                    margin: 0,
                                    padding: 0,
                                  }}
                                >
                                  User
                                </p>
                                <div
                                  style={{
                                    borderRadius: "50%",
                                    width: "25px",
                                    height: "25px",
                                    display: "flex",
                                    justifyContent: "center",
                                    backgroundColor: "lightgrey",
                                  }}
                                >
                                  <UserOutlined />
                                </div>
                              </div>
                              <pre
                                className={
                                  chat.type === "send"
                                    ? "user-message"
                                    : "bot-message"
                                }
                                style={{
                                  textAlign: "left",
                                  color: "black",
                                  margin: 0,
                                  backgroundColor: "rgb(102, 205, 170)",
                                  fontWeight: "500",
                                }}
                              >
                                {chat.message}
                              </pre>
                            </div>
                          ) : (
                            //Bot Message
                            <div
                              style={{
                                display: "flex",
                                gap: "0.25rem",
                                width: "100%",
                              }}
                            >
                              {chatload == true &&
                                index == chatHistory.length - 1 ? (
                                <>
                                  <div className="bot-photo-div">
                                    <img
                                      src="/images/girl-with-laptop.png"
                                      className="bot-img"
                                    />
                                  </div>

                                  <div
                                    className="typing"
                                    style={{
                                      width: "100%",
                                      alignItems: "center",
                                      display: "flex",
                                    }}
                                  >
                                    <div className="dot"></div>
                                    <div className="dot"></div>
                                    <div className="dot"></div>
                                  </div>
                                </>
                              ) : (
                                <>
                                  {chat?.message?.length !== 0 ? (
                                    <>
                                      <div className="bot-photo-div">
                                        <img
                                          src="/images/girl-with-laptop.png"
                                          className="bot-img"
                                        />
                                      </div>
                                      <div
                                        style={{
                                          display: "flex",
                                          flexDirection: "column",
                                          gap: "0.5rem",
                                          width: "80%",
                                        }}
                                      >
                                        <p
                                          style={{
                                            fontSize: "1rem",
                                            margin: 0,
                                            padding: 0,
                                            fontWeight: "500",
                                          }}
                                        >
                                          Oceann Bot
                                        </p>
                                        <span
                                          className={
                                            chat.type === "response"
                                              ? "bot-message"
                                              : "user-message"
                                          }
                                          style={{
                                            color: "#000",
                                            textWrap: "wrap",
                                            minWidth: "5rem",
                                            wordBreak: "break-word",
                                            backgroundColor: "rgb(178,223,219)",
                                            color: "rgb(47,79,79)",
                                            fontWeight: "500",
                                          }}
                                        >
                                          {/* {chat.message} */}
                                          {/* <div
                                                                                    ref={divRef}
                                                                                key={index}
                                                                                        dangerouslySetInnerHTML={{
                                                                                            __html: chat.message,
                                                                                        }}
                                                                                    ></div> */}
                                          <ShadowDomComponent
                                            html={chat.message}
                                          />
                                        </span>
                                        {chat?.key === "mail" &&
                                          !location.pathname.includes("mails") ? (
                                          <Tag
                                            onClick={() => goToMails(chat.data)}
                                            color="blue"
                                            style={{
                                              cursor: "pointer",
                                              width: "100px",
                                              border: "1px solid",
                                              padding: "5px",
                                              fontWeight: "bold",
                                            }}
                                          >
                                            <MailOutlined
                                              style={{ marginRight: "8px" }}
                                            />
                                            Go to Mails
                                          </Tag>
                                        ) : null}
                                        {chat?.key === "mail" &&
                                          MessageButton ? (
                                          <Tag
                                            onClick={() => {
                                              sendChatApi(chat?.query, page);
                                            }}
                                            color="blue"
                                            style={{
                                              cursor: "pointer",
                                              display: "inline-flex",
                                              alignItems: "center",
                                              border: "1px solid",
                                              padding: "5px",
                                              fontWeight: "bold",
                                              width: "130px",
                                            }}
                                          >
                                            <ReloadOutlined
                                              style={{ marginRight: "8px" }}
                                            />
                                            Fetch more data
                                          </Tag>
                                        ) : null}

                                        {chat?.key && (
                                          <div
                                            className="vesselinfo-chat-response"
                                            ref={
                                              index === chatHistory.length - 1
                                                ? lastMessageRef
                                                : null
                                            }
                                          >
                                            {/* {chat.key === "port_call" &&
                                              chat?.data?.length > 0 && (
                                                <ChatPortCall chat={chat} />
                                              )}
                                            {chat.key === "live_bunker_price" &&
                                              chat?.data?.length > 0 && (
                                                <ChatLiveBunkerPrice
                                                  chat={chat}
                                                />
                                              )} */}
                                            {/* {chat.key === "vessel_info" &&
                                              chat?.data?.length > 0 &&
                                              chat?.data?.map((item, index) => (
                                                <ChatVesselInfo item={item} />
                                              ))} */}
                                            {/* {chat.key === "info" &&
                                              chat?.data?.length > 0 &&
                                              chat?.data?.map((item, index) => (
                                                <ChatInfo item={item}/>
                                              ))}
                                            {chat.key === "vessel_position" && (
                                              <ChatVesselPosition
                                                item={chat?.data}
                                              />
                                            )} */}
                                            {/* {chat.key === "port_distance" && (
                                              <ChatPortDistance
                                                item={chat?.data}
                                              />
                                            )}
                                            {chat.key === "vessel_lineup" && (
                                              <ChatVesselLineUp
                                                item={chat?.data}
                                              />
                                            )} */}
                                            {/* {chat.key === "weather" && (
                                              <ChatWeather item={chat?.data} />
                                            )}
                                            {chat.key === "port_info" && (
                                              <ChatPortInfo item={chat?.data} />
                                            )} */}

                                            {![
                                              "other",
                                              "info",
                                              "live_bunker_price",
                                              "vessel_info",
                                              "port_call",
                                              "vessel_position",
                                              "port_distance",
                                              "vessel_lineup",
                                            ].includes(chat.key) &&
                                              !location.pathname.includes(
                                                gotoredirectPage
                                              ) &&
                                              gotoredirectPage.length > 0 && (
                                                <Link to={gotoredirectPage}>
                                                  <Tag
                                                    color="pink"
                                                    style={{
                                                      cursor: "pointer",
                                                    }}
                                                  >
                                                    Click to view data
                                                  </Tag>
                                                </Link>
                                              )}
                                          </div>
                                        )}
                                      </div>
                                    </>
                                  ) : (
                                    ""
                                  )}
                                </>
                              )}
                            </div>
                          )}
                        </div>
                      </>
                    ))}
                  </div>
                </>
              )}

              <div style={{ position: "relative", bottom: 0 }}>
                <Form.Item style={{ width: "100%", padding: "0 5px" }}>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      position: "relative",
                      borderRadius: "10px",
                      overflow: "hidden",
                    }}
                    className="chat-input-wrapper"
                  >
                    <Input.TextArea
                      className="custom-input"
                      autoSize={{ minRows: 1, maxRows: 3 }}
                      value={sendQuery}
                      placeholder="Chat with us..."
                      onChange={(e) => setSendQuery(e.target.value)}
                      onKeyDown={(e) => handleKeyPress(e)}
                      style={{
                        width: "100%",
                        padding: "0.5rem",
                        paddingRight: "2.5rem",
                        // boxSizing: "border-box",
                        // border: "1px solid blue",
                        border: "none",
                        resize: "none",
                        maxHeight: "60px",
                        overflowY: "auto",
                        backgroundColor: "rgb(245, 255, 250)",
                        border: "2px solid rgb(102, 205, 170)",
                      }}
                    />
                    <div id="madhu" className="chatbootcss">
                      <Popover
                        placement="topLeft"
                        // title={
                        //     <div
                        //     id="madhu"></div>
                        // }
                        // id="madhu"
                        content={
                          <div
                            // id="madhu"
                            //  className="chatbootcss"
                            className="custom-scrollbar"
                            style={{
                              display: "flex",
                              flexDirection: "column",
                              justifyContent: "space-between",
                              padding: "5px",
                              // width: "400px",
                              width: "100%",
                              maxHeight: "350px",
                              overflow: "auto",
                            }}
                          >
                            {questions?.length > 0 &&
                              questions.map((item, index) => (
                                <div
                                  key={index}
                                  style={{ marginBottom: "8px" }}
                                >
                                  <div
                                    style={{
                                      border: `3px solid ${item.color}`,
                                      borderRadius: "10px",
                                      width: "fit-content",
                                      padding: "5px",
                                      cursor: "pointer",
                                      backgroundColor: item.background,
                                    }}
                                    onClick={() => {
                                      sendChatApi(item.question, null);
                                      setPopoverVisible(false);
                                    }}
                                  //   icon={
                                  //     <Icon
                                  //       style={{ color: item?.color }}
                                  //       component={item?.icon}
                                  //     />
                                  //   }
                                  >
                                    <Typography.Text
                                      style={{
                                        color: "black",
                                        fontWeight: "500",
                                      }}
                                    >
                                      {item.question}
                                    </Typography.Text>
                                  </div>
                                </div>
                              ))}
                          </div>
                        }
                        open={popoverVisible}
                        onClick={() => setPopoverVisible(!popoverVisible)}
                        onOpenChange={handleVisibleChange}
                        trigger="click"
                      >
                        {/* {chatHistory.length > 0 && ( */}
                        <Tooltip title="FAQs" placement="top">
                          <Button
                            shape="circle"
                            icon={<QuestionCircleOutlined />}
                            style={{
                              position: "absolute",
                              right: "2.4rem",
                              top: "50%",
                              transform: "translateY(-50%)",
                              // backgroundColor: "#2E92FD",
                              backgroundColor: "rgb(102, 205, 170)",
                              color: "white",
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          />
                        </Tooltip>
                        {/* )} */}
                      </Popover>
                    </div>
                    <Button
                      shape="circle"
                      icon={<SendOutlined style={{ marginLeft: "4px" }} />}
                      style={{
                        position: "absolute",
                        right: "0.2rem",
                        top: "50%",
                        transform: "translateY(-50%)",
                        backgroundColor: "#2E92FD",
                        color: "white",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                      onClick={() => {
                        sendChatApi(sendQuery, null);
                      }}
                    />
                  </div>
                </Form.Item>
                <Typography
                  style={{
                    fontSize: "12px",
                    textAlign: "center",
                    fontWeight: "400",
                    backgroundColor: "rgb(250, 240, 230)", // Adjust the opacity here
                    border: "2px solid rgb(235 149 72)",
                  }}
                >
                  Oceann Bot may respond with inaccurate or insensetive
                  information.This feature is powered by ChatGPT from OpenAI and
                  Microsoft.
                </Typography>
              </div>
            </>
          )}
        </div>
      )}
    </>
  );
};

export default Chatbot;
