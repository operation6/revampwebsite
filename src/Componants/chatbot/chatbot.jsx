import React from 'react'

const chatbot = () => {
    return (
        <div>
            <div
                style={{
                    position: "fixed",
                    bottom: "100px",
                    right: "10px",
                    width: "300px",
                    height: "400px",
                    backgroundColor: "white",
                    border: "1px solid #ccc",
                    borderRadius: "8px",
                    boxShadow: "0 4px 8px rgba(0, 0, 0, 0.2)",
                    zIndex: "99",
                    padding: "10px",
                    overflow: "auto",
                }}
            >
                {/* Your chatbot content goes here */}
                <h3>Chatbot</h3>
                <p>How can I help you today?</p>
            </div>
        </div>
    )
}

export default chatbot