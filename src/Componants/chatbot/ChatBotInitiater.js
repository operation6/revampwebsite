import React from "react";
import { Images } from "../../assests";

const ChatBotInitiater = (props) => {
  return (
    <div className="chatbot-container">
      <div className="chatbot-header">
        <img
          src={`${Images}logo-without-title.png`}
          alt="Support"
          className="chatbot-avatar"
        />
        <div className="chatbot-title">
          Discover the Future of Maritime Technology!
        </div>
        <button
          onClick={() => props.setShowChatBotInitiater(false)}
          className="chatbot-close"
        >
          ×
        </button>
      </div>
      <div className="chatbot-body">
        <p className="chatbot-message">
          Let us help you navigate your maritime challenges effortlessly. Click
          'Let's Start' to begin a conversation with Oceann AI, your intelligent
          maritime assistant!
        </p>
        <div className="chatbot-input-group">
          <button
            onClick={() => {
              props.toggleChatbot(true);
              props.setShowChatBotInitiater(false);
            }}
            className="chatbot-submit"
          >
            Let's Start
          </button>
        </div>
        {/* <div>
            <p style={{fontSize:"10px", margin:"5px"}}>Connect with Ocean AI and explore how we can assist you in real-time.</p>
        </div> */}
      </div>
      <div className="chatbot-footer">
        Trusted by maritime professionals worldwide
      </div>
    </div>
  );
};

export default ChatBotInitiater;
