import React, { useState } from "react";
import { FaAngleDown } from "react-icons/fa";
import { Link } from "react-router-dom";

export const MobileDropDown = ({ page, list, setNavOpen }) => {
  const [showDropDown, setShowDropdown] = useState(false);

  return (
    <div
      role="button"
      className="rounded-md "
      onClick={() => setShowDropdown(!showDropDown)}
    // onMouseLeave={() => setShowDropdown(false)}
    >
      <button className="flex px-4 pb-2 capitalize items-center w-full justify-between items-center whitespace-nowrap font-semibold">
        {page == "about-us" ? page : page + "s"}
        <FaAngleDown
          className={showDropDown ? "rotate-180" : "rotate-0"}
          size={20}
        />
      </button>
      {showDropDown && (
        <ul className="flex flex-col max-h-fit pt-2 bg-white  gap-2 pb-2 ">
          {/* <h3 className="text-black font-semibold underline ml-4 text-[1rem]">
            {page + "s"}
          </h3> */}
          {page !== "solution" &&
            <>
              {list.map(({ route, title, logo }) => {
                return (
                  <li className="flex p-1  text-[black] gap-3">
                    <img src="" alt="" />
                    <Link onClick={() => setNavOpen(false)} to={route}>
                      {title}
                    </Link>
                  </li>
                );
              })}
            </>}

          {page === "solution" && (
            <>
              <div className="flex gap-2 pb-2 capitalize flex-col">
                {list.map(({ route, title, logo, list }, index) => (
                  <div key={index}>
                    <h3 className="text-black font-semibold  ml-4 text-md  p-1">
                      {title}
                    </h3>
                    <ul className="flex gap-2 pb-2 capitalize flex-col  ml-4">
                      {list.map((subList, subIndex) => (
                        <Link key={subIndex} to={subList?.link || "#"}>
                          <li className="flex p-1  text-[black] gap-3 ">
                            {subList?.des || "No description available"}
                          </li>
                        </Link>
                      ))}
                    </ul>
                  </div>
                ))}
              </div>
            </>
          )}




          {/* <div className="flex gap-2  pb-2 capitalize flex-col">
                <h3 className="text-black font-semibold underline ml-4 text-[1rem]">
                  Roles
                </h3>
                <li className="flex p-1  text-[black] gap-3">
                  <img src="" alt="" />
                  <Link to={"#"}>Dry</Link>
                </li>
                <li className="flex p-1  text-[black] gap-3">
                  <img src="" alt="" />
                  <Link to={"#"}>Project</Link>
                </li>
                <li className="flex p-1  text-[black] gap-3">
                  <img src="" alt="" />
                  <Link to={"#"}>Parceling</Link>
                </li>
                <li className="flex p-1  text-[black] gap-3">
                  <img src="" alt="" />
                  <Link to={"#"}>Tankers</Link>
                </li>
                <li className="flex p-1  text-[black] gap-3">
                  <img src="" alt="" />
                  <Link to={"#"}>Pooling</Link>
                </li>
              </div>
              <div className="flex gap-2  pb-2 capitalize flex-col">
                <h3 className="text-black font-semibold underline ml-4 text-[1rem]">
                  Industries
                </h3>
                <li className="flex p-1  text-[black] gap-3">
                  <img src="" alt="" />
                  <Link to={"#"}>Shiping Owner</Link>
                </li>
                <li className="flex p-1  text-[black] gap-3">
                  <img src="" alt="" />
                  <Link to={"#"}>Charter-Operator</Link>
                </li>
                <li className="flex p-1  text-[black] gap-3">
                  <img src="" alt="" />
                  <Link to={"#"}>Commodities Trader</Link>
                </li>
                <li className="flex p-1  text-[black] gap-3">
                  <img src="" alt="" />
                  <Link to={"#"}>Port Agent</Link>
                </li>
                <li className="flex p-1  text-[black] gap-3">
                  <img src="" alt="" />
                  <Link to={"#"}>ship Broker</Link>
                </li>
              </div> */}

        </ul>
      )
      }
    </div >
  );
};
