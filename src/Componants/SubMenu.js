import React, { useState } from "react";
import { AiOutlineUser } from "react-icons/ai";
import { Link } from "react-router-dom";
import { BiSolidDownArrow } from "react-icons/bi";

export const SubMenu = ({ subMenuProductContent, page, logo }) => {
  // const [isMenuVisible, setMenuVisible] = useState(false);
  // const handleMouseEnter = () => {
  //   setMenuVisible(true);
  // };
  // const handleMouseLeave = () => {
  //   setMenuVisible(false);
  // };
  // console.log('logo', logo);

  const [isMenuVisible, setMenuVisible] = useState(false);
  const [isSub_menu_visible, setIsSubMenu_visible] = useState(null);
  const handleMouseEnter = () => {

    setMenuVisible(true);
  };

  const handleMouseLeave = (index) => {
    setMenuVisible(false);
    setSelectMenu(false)
    // handleMouseLeaveSubmenu(index)
  };
  const handleMouseEnterSubmenu = (index) => {
    setIsSubMenu_visible(index);
  };

  // const handleMouseLeaveSubmenu = () => {
  //   setIsSubMenu_visible(null);
  // };

  if (page === "Knowledege-hub") {
    page = "#";
  }
  const [selectMenu, setSelectMenu] = useState(false)
  const subMenu = [
    [
      {
        title: 'Data & Analytics',
        list: [
          { title: 'Oceann BI', des: 'Turn complex maritime data into actionable business intelligence for smarter operations' },
          { title: 'Market Insight', des: 'Stay ahead of global trade trends with precise and real-time market insights' },
          { title: 'Mail Analytic', des: 'Revolutionize maritime communication with AI-powered email tracking and analytics.' },
          { title: 'Report & Analytics', des: 'Gain a competitive edge with detailed shipping reports and predictive analytics.' },
        ],
      },
    ],
    [
      {
        title: 'Our Incorporation',
        list: [
          { title: 'Kpler', des: 'Marine Traffic API offering live vessel monitoring and comprehensive details' },
          { title: 'Bunker Ex', des: 'Bunker-ex API providing live fuel prices at over 400 global ports' },
          { title: 'Netpas', des: 'Netpas API enabling precise route distance calculations' },
          { title: 'Bimco', des: 'BIMCO membership and partnership for standardized charter party agreements' },
        ],
      },
    ],
  ];

  const img = [
    '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="currentColor" d="M17.05 20.28c-.98.95-2.05.8-3.08.35c-1.09-.46-2.09-.48-3.24 0c-1.44.62-2.2.44-3.06-.35C2.79 15.25 3.51 7.59 9.05 7.31c1.35.07 2.29.74 3.08.8c1.18-.24 2.31-.93 3.57-.84c1.51.12 2.65.72 3.4 1.8c-3.12 1.87-2.38 5.98.48 7.13c-.57 1.5-1.31 2.99-2.54 4.09zM12.03 7.25c-.15-2.23 1.66-4.07 3.74-4.25c.29 2.58-2.34 4.5-3.74 4.25"/></svg>'
    ,
    '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="currentColor" d="M18.06 9.33a.94.94 0 0 0-.94.94v4.26a.94.94 0 0 0 1.88 0v-4.26a.94.94 0 0 0-.94-.94m-12.12 0a.94.94 0 0 0-.94.94v4.26a.94.94 0 0 0 1.88 0v-4.26a.94.94 0 0 0-.94-.94m1.62 0v6.4a1.07 1.07 0 0 0 1.07 1.07h.68v2.27a.94.94 0 0 0 1.88 0V16.8h1.62v2.27a.94.94 0 0 0 1.88 0V16.8h.68a1.07 1.07 0 0 0 1.07-1.07v-6.4Z"/><circle cx="9.98" cy="7.07" r=".4" fill="none"/><circle cx="14.02" cy="7.07" r=".4" fill="none"/><path fill="currentColor" d="M16.32 8a4.13 4.13 0 0 0-1.83-2.36l-.15-.09l-.16-.08l.18-.31l.53-1a.14.14 0 0 0-.05-.16h-.07a.17.17 0 0 0-.13.07L14.1 5l-.17.31l-.16-.07l-.17-.06a4.88 4.88 0 0 0-3.2 0l-.16.06l-.17.07L9.9 5l-.54-1a.14.14 0 0 0-.25.14l.53 1l.18.31l-.16.08l-.15.09A4.07 4.07 0 0 0 7.69 8a3 3 0 0 0-.13.8h8.88a3.6 3.6 0 0 0-.12-.8M10 7.47a.4.4 0 1 1 .4-.4a.4.4 0 0 1-.4.4m4 0a.4.4 0 1 1 .4-.4a.4.4 0 0 1-.4.4"/></svg>',

    '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="currentColor" fill-rule="evenodd" d="M3.005 12L3 6.408l6.8-.923v6.517H3.005ZM11 5.32L19.997 4v8H11zM20.067 13l-.069 8l-9.065-1.275L11 13zM9.8 19.58l-6.795-.931V13H9.8z" clip-rule="evenodd"/></svg>'
  ]














  const [hoveredMenu, setHoveredMenu] = useState(subMenuProductContent[0]);

  return (
    <div>
      <div
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        className="flex gap-2 items-center text-[1.1rem] menuOpen solutions"
      >
        <Link to={`/product`} className="hover:text-blue-400 text-[1.1rem] custom-dropdown-icon cust-font-size">
          {page === "#" ? "Knowledege-hub" : page}
          <svg
            stroke="currentColor"
            fill="currentColor"
            stroke-width="0"
            viewBox="0 0 320 512"
            class="rotate-0"
            height="20"
            width="20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M143 352.3L7 216.3c-9.4-9.4-9.4-24.6 0-33.9l22.6-22.6c9.4-9.4 24.6-9.4 33.9 0l96.4 96.4 96.4-96.4c9.4-9.4 24.6-9.4 33.9 0l22.6 22.6c9.4 9.4 9.4 24.6 0 33.9l-136 136c-9.2 9.4-24.4 9.4-33.8 0z"></path>
          </svg>
        </Link>
        {/* <BiSolidDownArrow size={10} /> */}
        {isMenuVisible && (
          // <div className="menuOpen productsMenu top-[1.2rem] min  -w-[768px]  flex justify-center items-center absolute left-[-12rem]">
          <div className=" menuOpen productsMenu top-[2rem] w-[1280px]  absolute  sm:absolute sm:top-[1.2rem] sm:l-[120px] -left-[270px]">
            <div className="bg-white rounded w-full mt-[1.5rem] hover:bg-white  flex flex-col menuWrap  font-[poppins] h-[70vh]">
              <div className="relative w-full  flex top-[11px]  pb-[1%]  text-slate-700">
                {/* Left Menu */}
                <div className="w-1/4 bg-white pl-[1%]"
                  onMouseLeave={() => setSelectMenu(false)}>
                  {subMenuProductContent?.map((item, index) => (
                    <div
                      key={index}
                      className={`rounded-l-lg p-4 cursor-pointer hover:bg-[#0070C7] group transition-all `}
                      onMouseEnter={() => { setHoveredMenu(item); setSelectMenu(true) }}

                    >
                      <Link
                        to={item?.route}
                        className="text-black group-hover:text-white "
                        style={{
                          display: "flex",
                          alignItems: "center",
                          width: "100%",
                          gap: "0px",
                        }}
                      >
                        <img
                          src={item?.logo}
                          alt=""
                          className="w-[20px] h-[20px] mr-[6px]"
                        />
                        <h3 className="text-md font-semibold text-black group-hover:text-white">{item.title}</h3>
                      </Link>
                      <p className="text-[0.8375rem] group-hover:text-white">{item?.description}</p>

                      {/* <h3 className="text-lg font-semibold">{item.title}</h3>
                      <p className="text-sm">{item?.description}</p> */}
                    </div>
                  ))}
                </div>
                {/* Right Menu */}
                {hoveredMenu && (
                  <div
                    className={` rounded left-[50%] top-0 w-1/4 h-full p-[2%] 
                    ${selectMenu ? 'bg-[#0070C7] text-white' : 'bg-gray-100 text-black'} transition-all`}
                  >
                    <h2 className="text-md font-semibold">{hoveredMenu.title}</h2>
                    <p className="text-[0.8375rem] mb-6">{hoveredMenu.description}</p>
                    {hoveredMenu.list && hoveredMenu.list.length > 0 ? (
                      <ul className="space-y-3">
                        {hoveredMenu?.title === 'Oceann X' ? <>
                          {hoveredMenu.list.map((submenuItem, index) => (
                            <li key={index} className="hover:underline flex ">
                              <div className='h-[0.8375rem]' key={index} dangerouslySetInnerHTML={{ __html: img[index] }} />
                              <a href={submenuItem.link || '#'} target="_blank" rel="noopener noreferrer" >
                                <p className="text-[0.8375rem]">  {submenuItem.des}</p>
                              </a>

                            </li>
                          ))}
                        </> : <> {hoveredMenu.list.map((submenuItem, index) => (
                          <li key={index} className="hover:underline">


                            <p className="text-[0.8375rem]">  {submenuItem.des}</p>

                          </li>
                        ))}</>}

                      </ul>
                    ) : (
                      ''
                    )}
                  </div>
                )}


                <div className="w-1/4 bg-white pl-[1%] pb-[1%] ">
                  {subMenu[0]?.map((item, index) => (
                    <div
                      key={index}
                      className={`p-4 cursor-pointer  transition-all `}

                    // onMouseLeave={() => setHoveredMenu(null)}
                    >
                      {/* <Link
                        to={item?.route}
                        style={{
                          display: "flex",
                          alignItems: "center",
                          width: "100%",
                          gap: "0px",
                        }}
                      > */}

                      <h3 className="text-[20px] font-semibold text-[#0070C7] pb-[5px]">{item.title}</h3>
                      {/* </Link> */}
                      <Link
                        to={item?.route}
                        style={{
                          display: "flex",
                          alignItems: "center",
                          width: "100%",
                          gap: "0px",
                        }}
                      >
                        <ul className="space-y-3">
                          {item.list.map((submenuItem, index) => (
                            <li className="group rounded-lg text-[0.8375rem] pl-[6px] px-[4px] py-[6px] text-slate-700 hover:bg-[#0070c7] hover:text-[#ffffff]">
                              <p className="text-md  font-semibold group-hover:text-white text-black">  {submenuItem?.title}</p>
                              <p className="text-[0.8375rem] group-hover:text-white">  {submenuItem?.des}</p>

                            </li>
                          ))}
                        </ul>
                      </Link>

                    </div>
                  ))}
                </div>
                <div className="w-1/4 bg-white pl-[1%]  pb-[1%] ">
                  {subMenu[1]?.map((item, index) => (
                    <div
                      key={index}
                      className={`p-4  transition-all `}

                    // onMouseLeave={() => setHoveredMenu(null)}
                    >
                      {/* <Link
                        to={item?.route}
                        style={{
                          display: "flex",
                          alignItems: "center",
                          width: "100%",
                          gap: "0px",
                        }}
                      > */}

                      <h3 className="text-[20px] font-semibold text-[#0070C7] pb-[5px]">{item.title}</h3>
                      {/* </Link> */}
                      <Link
                        to={item?.route}
                        style={{
                          display: "flex",
                          alignItems: "center",
                          width: "100%",
                          gap: "0px",
                        }}
                      >
                        <ul className="space-y-3">
                          {item.list.map((submenuItem, index) => (
                            <li className=" rounded-lg text-[0.8375rem] pl-[6px] px-[4px] py-[6px] text-slate-700 hover:bg-[#0070c7] hover:text-[#ffffff] group">
                              <p className="text-md font-semibold text-blackgroup-hover:text-white">  {submenuItem.title}</p>
                              <p className="text-[0.8375rem] group-hover:text-white">  {submenuItem?.des}</p>

                            </li>
                          ))}
                        </ul>
                      </Link>


                    </div>
                  ))}
                </div>

              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};