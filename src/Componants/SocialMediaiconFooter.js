import React from "react";
// import indeedicon from "../Componants/Assets/footer-icon/linkedin-icon.svg";
import googlePlay from "../Componants/Assets/footer-icon/google_play.png";
import appStore from "../Componants/Assets/footer-icon/app_store.png";
import windowApp from "../Componants/Assets/footer-icon/window_app.png";
import "../App.css";
import { Images, s3Image } from "../assests";
let indeedicon = Images + "linkedin-icon.svg";
let twitericon = Images + "twitter-icon.svg";
let facebookicon = Images + "facebook-icon.svg";
let NVIDIA_MICROSOFT = "https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/upscalemedia-transformed.jpeg";

const SocialMediaiconFooter = () => {
  return (
    <>
      <div className="grid grid-cols-2 gap-8 max-md:gap-4  px-4 w-[90%] mx-auto pt-[5%] md-px-2 max-sm:grid-cols-2 max-md:grid-cols-2 max-lg:grid-cols-2 bottom-footer pb-2">
        <div className="text-white first">
          <h3 className="text-sm sm:text-md md:text-lg w-[94%] text-left mb-2">
            DOWNLOAD APP
          </h3>
          <ul className="text-white font-light cust-foot-links_1">
            <li className=" max-sm:mb-0">
              <a href="https://play.google.com/store/apps/details?id=com.theoceann.app.oceannapp&hl=en" target="_blank" rel="noopener noreferrer">
                <img
                  className="download-img-foot"
                  src={googlePlay}
                  alt="googlePlay"
                />
              </a>
            </li>

            <li className=" max-sm:mb-0">
              <a href="https://apps.apple.com/us/app/oceann-x/id6737811559" target="_blank" rel="noopener noreferrer">
                <img
                  className="download-img-foot"
                  src={appStore}
                  alt="appStore"
                />
              </a>
            </li>
            <li className=" max-sm:mb-0">
              <a href="https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/oceannX.exe" target="_blank" rel="noopener noreferrer">
                <img
                  className="download-img-foot wapp"
                  src={windowApp}
                  alt="windowApp"
                />
              </a>
            </li>
          </ul>
        </div>
        <div className="text-white second flex flex-row  justify-end gap-4 items-end">
          <div className="flex items-center h-12 overflow-hidden">
            <img
              src={NVIDIA_MICROSOFT}
              alt="MicrosoftAndNvidia"
              className=" h-28 xxxxs:hidden md:flex social-img-foot object-contain"
            />
          </div>
          <div>
          <h3 className="text-sm sm:text-md md:text-lg w-[94%] text-left mb-2">
            GET IN TOUCH
          </h3>
          <ul className="text-white font-light w-[94%] cust-foot-links_2">
            <li className=" max-sm:mb-0">
              <a
                href="https://www.linkedin.com/company/theoceann/"
                target="_blank"
                rel="noopener noreferrer"
              >
                <div className="p-2 bg-white rounded-lg shadow-lg border-[#e0e0e0] border-[1px]">
                  <img
                    src={indeedicon}
                    alt="indeedicon"
                    className="w-8 social-img-foot"
                  />
                </div>
              </a>
            </li>

            <li className=" max-sm:mb-0">
              <a
                href="https://twitter.com"
                target="_blank"
                rel="noopener noreferrer"
              >
                <div className="p-2 bg-white rounded-lg shadow-lg border-[#e0e0e0] border-[1px]">
                  <img
                    src={twitericon}
                    alt="twitericon"
                    className="w-8 social-img-foot"
                  />
                </div>
              </a>
            </li>
            <li className=" max-sm:mb-0">
              <a
                href="https://www.facebook.com"
                target="_blank"
                rel="noopener noreferrer"
              >
                <div className="p-2 bg-white rounded-lg shadow-lg border-[#e0e0e0] border-[1px]">
                  <img
                    src={facebookicon}
                    alt="facebookicon"
                    className="w-8 social-img-foot"
                  />
                </div>
              </a>
            </li>
          </ul>
          </div>
        </div>
      </div>

      {/* <div className="download-app-foot grid grid-cols-2 gap-2 max-md:gap-2 px-4 w-[90%] mx-auto pt-[5%] md-px-2 max-sm:grid-cols-2 max-md:grid-cols-2 max-lg:grid-cols-2 pb-1">
        <div className="flex justify-start justify-items-start gap-3 mr-[3%] ">
          <p className="font-100 download-text-foot">Download App</p>
        </div>
        <div className="flex justify-end justify-items-end gap-3 ml-[3%] ">
          <a
            href="https://www.linkedin.com/company/theoceann/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <div className="p-2 bg-white rounded-lg shadow-lg border-[#e0e0e0] border-[1px]">
              <img
                src={indeedicon}
                alt="indeedicon"
                className="w-8 social-img-foot"
              />
            </div>
          </a>
          <a
            href="https://twitter.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            <div className="p-2 bg-white rounded-lg shadow-lg border-[#e0e0e0] border-[1px]">
              <img
                src={twitericon}
                alt="twitericon"
                className="w-8 social-img-foot"
              />
            </div>
          </a>
          <a
            href="https://www.facebook.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            <div className="p-2 bg-white rounded-lg shadow-lg border-[#e0e0e0] border-[1px]">
              <img
                src={facebookicon}
                alt="facebookicon"
                className="w-8 social-img-foot"
              />
            </div>
          </a>
        </div>
      </div> */}
    </>
  );
};

export default SocialMediaiconFooter;
