import styles from "./subscriptions.module.css";
import DataTable from "react-data-table-component";
import { useEffect, useState } from "react";
import { subscriptionListService } from "../../services/all";

const Subscriptions = () => {
  const [data1, setData] = useState([]);

  useEffect(() => {
    const subscriptionFunction = async () => {
      const data = await subscriptionListService();
      setData(data.data);
      // console.log(data, "ggggggg");
    };
    subscriptionFunction();
  }, []);

  const columns = [
    {
      name: "Product Name",
      selector: (row) => row.product_name,
    },
    {
      name: "Status",
      selector: (row) => row.status,
      cell: (row) => {
        return row.status ? "Active" : "Expired";
      },
    },
    {
      name: "Validity",
      selector: (row) => row.subScription_type,
    },
    {
      name: "Expiry Date",
      selector: (row) => row.Expire_date,
      cell: (row) => {
        // Convert the Expire_date to the desired format
        const expiryDate = new Date(row.Expire_date);
        const options = { year: "numeric", month: "short", day: "2-digit" };
        return expiryDate.toLocaleDateString("en-IN", options);
      },
    },
    {
      name: "Users",
      selector: (row) => row.number_of_users,
    },
  ];

  const dateOptions = {
    year: "numeric",
    month: "short",
    day: "2-digit",
  };

  const customStyles = {
    headRow: {
      style: {
        background: "#003E78", // Set the background color for the header
        color: "white",
        fontSize: "15px",
        borderRightStyle: "solid",
        borderRightWidth: "1px",
      },
    },
    rows: {
      style: {
        fontSize: "13px",
        fontWeight: 600,
        backgroundColor: "white",
        borderBottomStyle: "solid",
        borderBottomWidth: "1px",
        borderBottomColor: "grey",
      },
    },
    cells: {
      style: {
        fontSize: "13px",
        fontWeight: 600,
        backgroundColor: "white",
      },
    },
  };
  return (
    <div className={styles.topcontent1}>
      <div className={styles.personalDetailTable}>
        <div className={styles.about}>
          <p className={styles.personalHeading}>Subscription Details</p>
          <div className={styles.edit_icon}></div>
        </div>

        <div className={styles.rightTable}>
          <div className={styles.firstline}>
            <DataTable
              columns={columns}
              data={data1}
              customStyles={customStyles}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Subscriptions;
