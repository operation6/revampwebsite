// import React from 'react';
// import {motion,useAnimation} from "framer-motion";

// const cardScroll = () => {

//     const control=useAnimation();
//     const handleScroll=()=>{
//         const scrollY=window.scrollY;
//         control.start({y:scrollY*0.2})
//     }


// React.useEffect(()=>{
//     window.addEventListener(scroll,handleScroll);
//     return()=>{
//         window.removeEventListener(scroll,handleScroll);
//     };},[])

//   return (
//     <>
   
    
//     </>
//   )
// }

// export default cardScroll







import React from 'react';
import { motion, useAnimation } from 'framer-motion';
// import { Card } from 'reactstrap';

const CardScroll = ({content}) => {
  const controls = useAnimation();

  const handleScroll = () => {
    const scrollY = window.scrollY;
    controls.start({ y: scrollY * 1 }); // Adjust the multiplier for the desired effect
  };

  // Attach scroll event listener
  React.useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  return (
    <motion.div
      initial={{ y: 0 }}
      animate={controls}
    >
      {content}
      
    </motion.div>
  );
};

export default CardScroll;

// CardVariant.js
// import React from 'react';
// import { motion, useAnimation } from 'framer-motion';

// const CardVariant = ({ content }) => {
//   const controls = useAnimation();

//   const handleScroll = () => {
//     const scrollY = window.scrollY;
//     controls.start({ y: scrollY * 0.2 });
//   };

//   React.useEffect(() => {
//     window.addEventListener('scroll', handleScroll);

//     return () => {
//       window.removeEventListener('scroll', handleScroll);
//     };
//   }, []);

//   return (
//     <motion.div
//       initial={{ y: 0 }}
//       animate={controls}
//       style={{
//         width: '100%',
//         height: '200px',
//         background: '#ffffff',
//         boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.1)',
//       }}
//     >
//       {content}
//     </motion.div>
//   );
// };

// export default CardVariant;






// CardStack.js
// CardStack.js
// import React from 'react';
// import { motion, useAnimation } from 'framer-motion';

// const CardScroll = ({ content }) => {
//   const controls = useAnimation();

//   const handleScroll = () => {
//     const scrollY = window.scrollY;
//     controls.start({ y: scrollY * 0.5 });
//   };

//   React.useEffect(() => {
//     const scrollListener = () => handleScroll();
//     window.addEventListener('scroll', scrollListener);

//     return () => {
//       window.removeEventListener('scroll', scrollListener);
//     };
//   }, [controls]);

//   return (
//     <motion.div
//       initial={{ y: 0 }}
//       animate={controls}
//       style={{
//         position: 'relative', // Use relative positioning for stacking
//         width: '100%',
//         height: '200px',
//         background: '#ffffff',
//         boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.1)',
//         zIndex: 1,
//         marginBottom: '20px', // Add some margin to separate the cards
//       }}
//     >
//       {content}
//     </motion.div>
//   );
// };

// export default CardScroll;



// Photos from https://citizenofnowhe.re/lines-of-the-city
// import "./styles.css";
// import { useRef } from "react";
// import {
//   motion,
//   useScroll,
//   useSpring,
//   useTransform,
//   MotionValue
// } from "framer-motion";

// function useParallax(value: MotionValue<number>, distance: number) {
//   return useTransform(value, [0, 1], [-distance, distance]);
// }

// function Image({ id }: { id: number }) {
//   const ref = useRef(null);
//   const { scrollYProgress } = useScroll({ target: ref });
//   const y = useParallax(scrollYProgress, 300);

//   return (
//     <section>
//       <div ref={ref}>
//         <img src={`/${id}.jpg`} alt="A London skyscraper" />
//       </div>
//       <motion.h2 style={{ y }}>{`#00${id}`}</motion.h2>
//     </section>
//   );
// }

// export default function App() {
//   const { scrollYProgress } = useScroll();
//   const scaleX = useSpring(scrollYProgress, {
//     stiffness: 100,
//     damping: 30,
//     restDelta: 0.001
//   });

//   return (
//     <>
//       {[1, 2, 3, 4, 5].map((image) => (
//         <Image id={image} />
//       ))}
//       <motion.div className="progress" style={{ scaleX }} />
//     </>
//   );
// }




// document.addEventListener('DOMContentLoaded', function () {
//     const imageContainer = document.querySelector('.image-container');
//     const images = document.querySelectorAll('.image-container img');

//     window.addEventListener('scroll', function () {
//         const scrollY = window.scrollY;

//         images.forEach(function (image) {
//             const imageOffsetTop = image.offsetTop;

//             if (scrollY > imageOffsetTop) {
//                 image.classList.add('sticky');
//             } else {
//                 image.classList.remove('sticky');
//             }
//         });
//     });
// });
