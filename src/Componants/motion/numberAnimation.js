// File: NumberAnimation.js
import React, { useEffect, useRef, useState } from 'react';
import { useSpring, animated } from 'react-spring';

const NumberAnimation = ({ value, duration = 1000, fontSize = '2em', fontWeight = 'bold', textColor = '#000', startAnimationOnScroll = true }) => {
  const [animateNumbers, setAnimateNumbers] = useState(false);
  const numberAnimationRef = useRef();

  const handleScroll = () => {
    const element = numberAnimationRef.current;
    if (!element) return;

    const elementTop = element.getBoundingClientRect().top;
    const windowHeight = window.innerHeight;

    if (elementTop < windowHeight) {
      setAnimateNumbers(true);
      window.removeEventListener('scroll', handleScroll);
    }
  };

  useEffect(() => {
    if (startAnimationOnScroll) {
      window.addEventListener('scroll', handleScroll);
      return () => {
        window.removeEventListener('scroll', handleScroll);
      };
    }
  }, [startAnimationOnScroll]);

  const { number } = useSpring({
    from: { number: 0 },
    to: { number: animateNumbers ? value : 0 },
    config: { duration },
  });

  return (
    <animated.div
      ref={numberAnimationRef}
      className="number-animation"
      style={{
        fontSize,
        fontWeight,
        color: textColor,
      }}
    >
      {number.to((val) => Math.floor(val))}
    </animated.div>
  );
};

export default NumberAnimation;
