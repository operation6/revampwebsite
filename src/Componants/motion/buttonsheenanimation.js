import React from 'react';
import { motion } from 'framer-motion';

const SheenButton = () => {
  return (
    <motion.button
      whileTap={{ scale: 0.9, backgroundColor: '#EDE358' }}
      className="sheen-button"
    >
      Get started
      <div className="sheen"></div>
    </motion.button>
  );
};

export default SheenButton;
