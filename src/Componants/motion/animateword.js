import React from 'react';
import { motion } from "framer-motion";


const Animatedword = ({ text }) => {
  const words = text.split("\n")

  const container = {
    hidden: { opacity: 0 },

    visible: (i = 1) => ({
      opacity: 1,
      transition: { staggerChildren: 0.12, delayChildren: 0.04 * i },
    }),
  };

  const child = {
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        type: "spring",
        damping: 12,
        stiffness: 150,
      },
    },


    hidden: {
      opacity: 0,
      y: 20,
      transition: {
        type: "spring",
        damping: 12,
        stiffness: 150,


      },

    },

  };

  return (
    <motion.div
      style={{ display: "flex" }}
      variants={container} initial="hidden" animate="visible">
      {words.map((word, index) => (
        <div className='flex flex-col'>
          <motion.span
            variants={child}
            key={index}
            style={{width:"100%"}}
          >
            {word}
          </motion.span>
                <div className="w-[inherit] h-[2px] bg-white rounded-md my-3"></div>
        </div>

      ))}
    </motion.div>
  )
}

export default Animatedword;