import React, { useEffect, useState } from 'react';
import { motion, useAnimation } from 'framer-motion';

const Bubbles = () => {
  const randomPosition = () => ({
    x: Math.random() * (window.innerWidth - 50),
    y: Math.random() * (window.innerHeight - 50),
  });

  return (
    <motion.div
      initial={{ opacity: 0, scale: 0, ...randomPosition() }}
      animate={{ opacity: 1, scale: 1 }}
      transition={{ duration: 1 }}
      style={{
        width: '50px',
        height: '50px',
        borderRadius: '50%',
        background: 'blue',
        position: 'absolute',
        zIndex:"2"
      }}
    />
  );
};

const BubbleAnimation = () => {
  const [showPoppedImage, setShowPoppedImage] = useState(false);
  const controls = useAnimation();
  const numberOfBubbles = 5; // Change this value to control the number of bubbles

  useEffect(() => {
    const timer = setTimeout(() => {
      setShowPoppedImage(true);
      controls.start({ opacity: 0, scale: 0 });
    }, 3000);

    return () => clearTimeout(timer);
  }, [controls]);

  return (
    <div style={{ position: 'relative', width: '100vw', height: '100vh', overflow: 'hidden' }}>
      {/* Render multiple instances of the Bubbles component */}
      {[...Array(numberOfBubbles)].map((_, index) => (
        <Bubbles key={index} />
      ))}

      {showPoppedImage && (
        // Render the PoppedImage component
        <div style={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', zIndex: 2 }}>
          {/* Replace the placeholder image with your actual image */}
          {/* <img src="https://placekitten.com/200/200" alt="Popped Image" /> */}
          {/* <p>Some text above the popped image</p> */}
        </div>
      )}
    </div>
  );
};

export default BubbleAnimation;
