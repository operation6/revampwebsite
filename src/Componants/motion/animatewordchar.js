// import React from 'react';
// import {motion} from "framer-motion";


// const Animatedwordchar = ({ text }) => {
//   const letters=Array.from(text);
//   const lines = text.split('\n');


//   const container = {
//     hidden: { opacity: 0 },

//     visible: (i = 1) => ({
//       opacity: 1,
//       transition: { staggerChildren: 0.03, delayChildren: 0.04 * i },
//     }),
//   };

//   const child = {
//     visible: {
//       opacity: 1,
//       x:0,
//       transition: {
//         type: "spring",
//         damping: 12,
//         stiffness: 100,
//       },
//     },


//     hidden: {
//       opacity: 0,
//       x:20, 
//       transition: {
//         type: "spring",
//         damping: 12,
//         stiffness: 100,
//       },

//     },

//   };

//   return (
//     <motion.div 
//     style={{overflow:"hidden",display:"flex", width:"40%"}}
//     variants={container} initial="hidden" animate="visible">
//       {letters.map((letter, index) => (
//         <motion.span
//           variants={child}
//           key={index}
          
//         >
//           {letter===" "?"\u00A0":letter}
//         </motion.span>
//       ))}
//     </motion.div>
//   )
// }

// export default Animatedwordchar;


import React from 'react';
import { motion } from 'framer-motion';

const Animatedwordchar = ({ text }) => {
  const lines = text.split('\n');

  const container = {
    hidden: { opacity: 0 },
    visible: (i = 1) => ({
      opacity: 1,
      transition: { staggerChildren: 0.03, delayChildren: 0.04 * i },
    }),
  };

  const child = {
    visible: {
      opacity: 1,
      x: 0,
      transition: {
        type: 'spring',
        damping: 12,
        stiffness: 100,
      },
    },
    hidden: {
      opacity: 0,
      x: 20,
      transition: {
        type: 'spring',
        damping: 12,
        stiffness: 100,
      },
    },
  };

  return (
    <motion.div style={{display: 'flex', whiteSpace: 'pre-line' }} variants={container} initial="hidden" animate="visible">
      {lines.map((line, lineIndex) => (
        <div key={lineIndex}>
          {Array.from(line).map((letter, index) => (
            <motion.span variants={child} key={index} style={{ width: '80%' }}>
              {letter === '' ? '\u00A0' : letter}
            </motion.span>
          ))}
          {lineIndex < lines.length - 1 && <br />} {/* Add <br /> between lines */}
        </div>
      ))}
    </motion.div>
  );
};

export default Animatedwordchar;
