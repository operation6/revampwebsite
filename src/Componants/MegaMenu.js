import { Button, Card, Col, Row, Tabs } from "antd";
import React, { useState } from "react";
import "../styles/megaMenu.css";

const { TabPane } = Tabs;

const MegaMenu = () => {
  const [activeMenu, setActiveMenu] = useState(null);

  const handleMouseEnter = (menu) => {
    setActiveMenu(menu);
  };

  const handleMouseLeave = () => {
    setActiveMenu(null);
  };

  const menuItems = [
    { name: "Products", content: ["Product 1", "Product 2", "Product 3"] },
    { name: "Solutions", content: ["Solution 1", "Solution 2", "Solution 3"] },
    { name: "The Platform", content: ["Feature 1", "Feature 2", "Feature 3"] },
    { name: "Knowledge Hub", content: ["Article 1", "Article 2", "Article 3"] },
    { name: "About Us", content: ["Team", "History", "Careers"] },
    { name: "Plans", content: ["Plan 1", "Plan 2", "Plan 3"] },
  ];

  // subitem of menu
  const models = [
    {
      name: "AI-Powered Shipping Email",
      img: "./vessel-pic.png",
    },
    {
      name: "Trade Tonnage Tracker",
      img: "./vessel-pic.png",
    },
    {
      name: "Analytics for Market Dynamics",
      img: "./vessel-pic.png",
    },
    {
      name: "AI-Driven Map Estimate",
      img: "./vessel-pic.png",
    },
    {
      name: "Live:Tracking, Bunker, Port data",
      img: "./vessel-pic.png",
    },
    {
      name: "Predictive Modeling",
      img: "./vessel-pic.png",
    },
  ];

  //  enums and keys
  const enumsSubMenu = {
    TRADE_INTELLIGENCE: "Trade Intelligence",
    DECARBONISATION: "Decarbonisation",
    CHARTERING: "Chartering",
    OPERATION: "Operation",
    FINANCE: "Finance",
    ANALYTICS_AND_REPORTING: "Analytics & Reporting",
  };

  const subMenuItems = [
    enumsSubMenu.TRADE_INTELLIGENCE,
    enumsSubMenu.DECARBONISATION,
    enumsSubMenu.CHARTERING,
    enumsSubMenu.OPERATION,
    enumsSubMenu.FINANCE,
    enumsSubMenu.ANALYTICS_AND_REPORTING,
  ];
  //  enums and keys

  return (
    <div className="fixed z-10 w-full left-0 top-0 mainMenu">
      <nav className="flex justify-around bg-white py-4 border-b border-gray-200">
        {menuItems.map((menuItem, index) => (
          <div
            key={index}
            className="relative"
            onMouseEnter={() => handleMouseEnter(menuItem.name)}
            onMouseLeave={handleMouseLeave}
          >
            <button className="flex items-center px-4 py-2 space-x-2">
              <span>{menuItem.name}</span>
            </button>
            <div
              className={`fixed left-0 w-full bg-white shadow-md p-6 transition-opacity duration-300 ease-in-out z-50 ${
                activeMenu === menuItem.name
                  ? "opacity-100"
                  : "opacity-0 pointer-events-none"
              }`}
            >
              {activeMenu === menuItem.name && menuItem.name === "Products" ? (
                <>
                  <Tabs
                    tabPosition="right"
                    className="w-full flex customMenuTabs"
                    animated
                  >
                    {subMenuItems.map((item, index) => (
                      <TabPane tab={item} key={index} className="columns-3">
                        {console.log("itemsdafdasd", item, enumsSubMenu)}
                        {item === enumsSubMenu.TRADE_INTELLIGENCE ? (
                          <div className="flex flex-wrap justify-center w-full">
                            {models.map((model) => (
                              <div
                                key={model.name}
                                className="text-sm text-center m-4"
                              >
                                <Card
                                  cover={
                                    <img alt={model.name} src={model.img} />
                                  }
                                  className="w-60 border-none"
                                >
                                  <div className="text-sm">{model.name}</div>
                                </Card>
                              </div>
                            ))}
                          </div>
                        ) : item === enumsSubMenu.DECARBONISATION ? (
                          <div className="flex flex-wrap justify-center w-full">
                            {models.map((model) => (
                              <div
                                key={model.name}
                                className="text-sm text-center m-4"
                              >
                                adsfalfjdaofojo
                              </div>
                            ))}
                          </div>
                        ) : (
                          ""
                        )}
                      </TabPane>
                    ))}
                  </Tabs>
                </>
              ) : activeMenu === menuItem.name &&
                menuItem.name === "Solutions" ? (
                // <div className="flex w-full justify-between divide-x">
                //   <div className="flex flex-wrap justify-center w-3/4">
                //     {models.map((model) => (
                //       <div key={model.name} className="text-sm text-center m-4">
                //         <Card
                //           // hoverable
                //           cover={<img alt={model.name} src={model.img} />}
                //           className="w-60 border-none"
                //         >
                //           <div className="text-sm">{model.name}</div>
                //           {/* <Card.Meta title={model.name} /> */}
                //           {/* <div className="mt-2">
                //             <Button type="link">Learn</Button>
                //             <Button type="link">Order</Button>
                //           </div> */}
                //         </Card>
                //       </div>
                //     ))}
                //   </div>

                //   <div className="flex flex-col justify-center w-1/4 pl-2 md:pl-[3rem]">
                //     {subMenuItems.map((item) => (
                //       <a
                //         href="#"
                //         key={item}
                //         className="text-black no-underline my-2"
                //       >
                //         {item}
                //       </a>
                //     ))}
                //   </div>
                // </div>
                menuItem.content.map((item, idx) => (
                  <div key={idx}>
                    <h4 className="font-bold">Section {idx + 1}</h4>
                    <ul>
                      <li>
                        <a href="#">{item}</a>
                      </li>
                    </ul>
                  </div>
                ))
              ) : (
                ""
              )}
            </div>
          </div>
        ))}
      </nav>
    </div>
  );
};

export default MegaMenu;
