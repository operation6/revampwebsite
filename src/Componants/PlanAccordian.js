import { Accordion, AccordionActions, AccordionDetails, AccordionSummary, Button, Icon, Typography } from '@mui/material'
import React, { useState } from 'react'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import RemoveIcon from '@mui/icons-material/Remove';
import AddIcon from '@mui/icons-material/Add';
import { AiFillCheckCircle } from "react-icons/ai";

const PlanAccordian = ({ name, description, minUsers, price, id }) => {

  const [expanded, setExpanded] = useState(false);

  const handleToggle = () => {
    setExpanded(prev => !prev); //merge
  };
  const colors = [
    { backgroundColor: '#0000880', wholeBackGroundColor: '#faf3e0' },
    { backgroundColor: '#FFC300', wholeBackGroundColor: '#ffe6e0' },
    { backgroundColor: '#4CAF50', wholeBackGroundColor: '#e8f5e9' },
    { backgroundColor: '#DBA400', wholeBackGroundColor: '#e3f2fd' },
    { backgroundColor: '#9C27B0', wholeBackGroundColor: '#f3e5f5' },
  ]
  const colorPalette = [
    { wholeBackGroundColor: 'rgba(24, 184, 66, 0.2)', backgroundColor: 'rgb(24, 184, 66)' },
    { wholeBackGroundColor: 'rgba(3, 120, 198, 0.2)', backgroundColor: 'rgb(3, 120, 198)' },
    { wholeBackGroundColor: 'rgba(245, 56, 0, 0.2)', backgroundColor: 'rgb(245, 56, 0)' },
    { wholeBackGroundColor: 'rgba(250, 173, 20, 0.2)', backgroundColor: 'rgb(250, 173, 20)' },
  ];
  const currentColor = colorPalette[id % colorPalette?.length] || {
    backgroundColor: "#000000",
    wholeBackGroundColor: "#ffffff",
  };

  return (
    <Accordion className="mt-1 mb-1" style={{ borderRadius: '10px' }} expanded={expanded} onChange={handleToggle}
      defaultExpanded={name === 'Oceann X Free'}>
      <AccordionSummary
        expandIcon={expanded ? (
          <RemoveIcon sx={{ color: 'white' }} />
        ) : (
          <AddIcon sx={{ color: 'white' }} />
        )}
        // expandLessIcon={<RemoveIcon sx={{color:'white'}}/>}
        aria-controls={`panel${id}bh-content`}
        id="panel3-header"
        style={{ backgroundColor: `${currentColor.backgroundColor}`, display: 'flex', alignItems: 'center', justifyContent: 'space-between', color: `#ffffff`, borderRadius: '10px' }}
      >
        <div className='font-bold flex items-center text-md'>{name}</div>
        <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'start', paddingLeft: '24px', paddingRight: '24px', }}>
          <div className='flex flex-col text-center items-start gap-1 '>
            <div className='flex flex-row items-center gap-2 text-center'>
              <div className='flex flex-row items-center gap-[1px]'>
                <div className='text-lg font-bold items-start'>$</div>
                <div className='text-lg font-bold items-start'>{price}</div>
              </div>
              {name === 'Oceann X Free' ? (<div className='text-sm font-medium italic'>(Free For 1 Week)</div>) : (<div className='text-sm font-medium italic'> Per User/Month (Annual Plan)</div>)}
            </div>
            {minUsers > 5 ? <div className='text-sm opacity-95'> More Than 5 Users</div> : <div className='text-sm opacity-95'>Minimum Users: {minUsers}</div>}
          </div>
        </div>
      </AccordionSummary>
      <AccordionDetails style={{ backgroundColor: `${currentColor.wholeBackGroundColor}` }}>

        {description.map((planDesc, index) => {
          return <div key={index} className='m-1 text-md flex items-center justify-start gap-2' ><AiFillCheckCircle color={currentColor.backgroundColor} fontSize={18} />{planDesc}</div>
        })}
      </AccordionDetails>

    </Accordion>
  )
}

export default PlanAccordian