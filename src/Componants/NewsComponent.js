import React from "react";
import {
  Card,
  CardContent,
  CardHeader,
  CardMedia,
  Grid,
  Typography,
  Button,
  Divider,
} from "@mui/material";
import { styled } from "@mui/system";

const NewsComponent = () => {
  const newsItems = [
    {
      title:
        "Shantha Rangaswamy, Indian cricket’s first woman, finally gets her due",
      time: "2 hours ago",
      image:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQzHMDlwRCHOHZP_tX7jRYNxV8W8MpNEog45w&s",
    },
    {
      title:
        "In trolling Gurmehar Kaur, Sehwag is merely following the establishment line. And he is not alone",
      time: "7 hours ago",
      image:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQzHMDlwRCHOHZP_tX7jRYNxV8W8MpNEog45w&s",
    },
    {
      title:
        "From Mr Gay India to Dalit beauty queens: The changing face of beauty pageants in India",
      time: "10 hours ago",
      image:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQzHMDlwRCHOHZP_tX7jRYNxV8W8MpNEog45w&s",
    },
  ];

  const Title = styled(Typography)(({ theme }) => ({
    fontWeight: "bold",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  }));

  return (
    <Card
      className="newsSectionComp"
      style={{
        width: "350px",
        height: "300px",
        border: "1px solid #ccc",
        position: "fixed",
        right: "5px",
        bottom: "5px",
      }}
    >
      <CardHeader
        style={{
          borderBottom: "1px solid #ccc",
          paddingTop: "10px",
          paddingBottom: "10px",
        }}
        title={
          <Title variant="h6" style={{ fontSize: "1rem" }}>
            <span>Editor's Picks</span>
            <Button
              // variant="outlined"
              size="small"
              style={{ backgroundColor: "#f39c12", color: "#fff" }}
            >
              See all
            </Button>
          </Title>
        }
        subheaderTypographyProps={{ fontSize: "0.8rem" }} // Adjust the subheader font size here
        subheader="Top stories of the day"
      />
      <CardContent
        style={{ overflowY: "auto", height: "300px", paddingBottom: "80px" }}
      >
        <Grid container direction="column" spacing={2}>
          {newsItems.map((news, index) => (
            <Grid item key={index}>
              <Grid container spacing={2}>
                <Grid item>
                  <CardMedia
                    component="img"
                    style={{ width: "70px" }}
                    // height="60"
                    image={news.image}
                    alt={news.title}
                  />
                </Grid>
                <Grid item xs>
                  <Typography variant="body2">{news.title}</Typography>
                  <Typography variant="caption" color="textSecondary">
                    {news.time}
                  </Typography>
                </Grid>
              </Grid>
              {index !== newsItems.length - 1 && (
                <Divider style={{ marginTop: "5px" }} />
              )}
            </Grid>
            // {newsItems.length > index.length === '-1' ? <Divider/> : ''}
          ))}
        </Grid>
      </CardContent>
    </Card>
  );
};

export default NewsComponent;
