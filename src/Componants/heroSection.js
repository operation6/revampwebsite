import React from "react";
import backgroundImage2 from "./Assets/image/home_img_2.png";
import { useNavigate } from "react-router-dom";
import { motion } from "framer-motion";
// import Parallax from "./motion/parallex"
import { ScrollAnimation, animationVariants } from "./motion/scrollanimation"




const HeroSection = () => {
  const navigator = useNavigate()
  const divStyle = {
    backgroundImage: `url(${backgroundImage2})`,
    backgroundSize: "cover",
    backgroundPosition: "center",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "90%",
    margin: "auto",
    borderRadius: "16px"

  };

  return (
    <>
      <ScrollAnimation>

        <div
          style={divStyle}
          className="mx-[3rem] max-sm:max-[2rem] text-white  max-lg:m-[3rem] mt-[3rem] mb-[3rem] max-md:m-[4rem]"
        >

          <div className="flex flex-col gap-3 max-sm:gap-3 text-center h-[100%] py-[5rem]">
            <h2 className="text-[40px] max-md:[30px] max-sm:text-[25px] font-semibold">
              The
              {/* <span className="text-[#00C8FF]">O</span> */}
              Oceann
            </h2>
            <h3 className="font-medium text-[25px] mx-[1rem] max-sm:mx-[1rem] max-md:text-[22px] max-sm:text-[15px]">
              Welcome to our thriving Bulk Trading community!
            </h3>
            <p className="font-extralight text-[18px] max-md:[15px] max-sm:text-[12px] mx-[1rem] max-sm:[1rem]">
              Celebrate the forefront of maritime digitization with The Oceann. Our
              tailored, state-of-the-art solutions <br /> empower industry leaders,
              promoting sustainability and enhancing operational profitability
            </p>
            <div>
              <motion.button onClick={() => navigator("/demo")} whileHover={{ scale: 1.04, backgroundColor: '#003e78'}} className="max-sm:p-2 max-sm:mt-3 mt-4 px-6 py-2 rounded-[2rem] text-xs sm:text-sm md:text-md text-white bg-[#ec9107]">Learn More</motion.button>

            </div>
          </div>
        </div>
      </ScrollAnimation>

    </>
  );
};

export default HeroSection;
