// // import React, { useEffect, useState } from "react";
// // import { AiOutlineGlobal } from "react-icons/ai"; // Icon for translation
// // import { toast } from "react-toastify";

// // const Translation = ({ pageLanguage = "en", includedLanguages = "en,zh-CN,ar,fr,de,it,es,pt" }) => {
// //     const [showDropdown, setShowDropdown] = useState(false);
// //     const languages = [
// //         { code: "en", name: "English" },
// //         { code: "zh-CN", name: "Chinese" },
// //         { code: "ar", name: "Arabic" },
// //         { code: "fr", name: "French" },
// //         { code: "de", name: "German" },
// //         { code: "it", name: "Italian" },
// //         { code: "es", name: "Spanish" },
// //         { code: "pt", name: "Portuguese" },
// //     ];
// //     const toggleDropdown = () => setShowDropdown((prev) => !prev);


// //     const selectLanguage = (code) => {
// //         console.log(code)
// //         // const iframe = document.querySelector("iframe.goog-te-menu-frame");
// //         // if (iframe) {
// //         //     const innerDoc = iframe.contentDocument || iframe.contentWindow.document;
// //         //     const languageLink = innerDoc.querySelector(`a[lang="${code}"]`);
// //         //     if (languageLink) {
// //         //         languageLink.click();
// //         //     } else {
// //         //         toast.error("Language selection failed.");
// //         //     }
// //         // } else {
// //         //     toast.error("Translation menu is not loaded.");
// //         // }
// //         const selectElement = document.querySelector("#google_translate_element select");
// //         if (selectElement) {
// //             selectElement.value = code; // Set the selected language
// //             selectElement.dispatchEvent(new Event("change")); // Trigger the language change
// //         }

// //         setShowDropdown(false);
// //     };

// //     const googleTranslateElementInit = () => {
// //         if (window.google && window.google.translate) {
// //             new window.google.translate.TranslateElement(
// //                 {
// //                     pageLanguage,
// //                     includedLanguages,
// //                     autoDisplay: false,
// //                 },
// //                 "google_translate_element"
// //             );
// //         }
// //     };

// //     useEffect(() => {
// //         const scriptId = "google-translate-script";
// //         if (!document.getElementById(scriptId)) {
// //             window.googleTranslateElementInit = googleTranslateElementInit;
// //             const script = document.createElement("script");
// //             script.id = scriptId;
// //             script.src = "https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit";
// //             script.async = true;
// //             script.onerror = () => toast.error("Failed to load translation service.");
// //             document.body.appendChild(script);
// //             return () => {
// //                 document.body.removeChild(script);
// //                 delete window.googleTranslateElementInit;
// //             };
// //         } else {
// //             googleTranslateElementInit();
// //         }
// //     }, [pageLanguage, includedLanguages]);

// //     return (
// //         <div>
// //             <AiOutlineGlobal
// //                 onClick={toggleDropdown}
// //                 style={{ cursor: "pointer", fontSize: "24px" }}
// //             />
// //             {
// //                 showDropdown && (<>
// //                     {/* <div id="google_translate_element"></div>
// //                     <style>{`
// //                         .goog-te-combo option[value="en"],
// //                         .goog-te-combo option[value="zh-CN"],
// //                         .goog-te-combo option[value="ar"],
// //                         .goog-te-combo option[value="fr"],
// //                         .goog-te-combo option[value="de"],
// //                         .goog-te-combo option[value="it"],
// //                         .goog-te-combo option[value="es"],
// //                         .goog-te-combo option[value="pt"],
// //                         .goog-te-combo option[value="reset"] {
// //                         display: block;
// //                         }
// //                         .skiptranslate > iframe {
// //                         height: 0 !important;
// //                         border-style: none;
// //                         box-shadow: none;
// //                         overflow: hidden;
// //                         display: none !important;
// //                         }
// //                         .VIpgJd-ZVi9od-ORHb-OEVmcd {
// //                         height: 0px !important;
// //                         }
// //                         iframe {
// //                         display: none !important;
// //                         }
// //                         body {
// //                         top: 0px !important;
// //                         }
// //                         .skiptranslate {
// //                         color: transparent !important;
// //                         }
// //                         .skiptranslate + .skiptranslate {
// //                         display: none;
// //                         }
// //                         .skiptranslate select {
// //                         outline: 0;
// //                         border: 0;
// //                         height: 48px;
// //                         margin: 0 !important;
// //                         position:relative;
// //                         z-index: 999;
// //                         color: white;
// //                         background-color: #2B344C;
// //                         border-left: 1px solid #414E91;
// //                         border-right: 1px solid #414E91;
// //                         }
// //                         #google_translate_element a {
// //                         display: none;
// //                         }
// //                         #goog-gt-tt {
// //                         display: none !important;
// //                         top: 0px !important;
// //                         }
// //                         .goog-tooltip.skiptranslate {
// //                         display: none !important;
// //                         top: 0px !important;
// //                         }
// //                         .activity-root {
// //                         display: none !important;
// //                         }
// //                         .status-message {
// //                         display: none !important;
// //                         }
// //                         .started-activity-container {
// //                         display: none !important;
// //                         }
// //                         .goog-te-gadget .goog-te-spinner {
// //                         display: hidden !important;
// //                         z-index: -1;
// //                         }
// //                     `}</style> */}
// //                     <div
// //                         style={{
// //                             position: "absolute",
// //                             backgroundColor: "#fff",
// //                             border: "1px solid #ccc",
// //                             boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
// //                             borderRadius: "4px",
// //                             padding: "5px",
// //                             zIndex: 1000,
// //                             minWidth: "160px",
// //                         }}
// //                     >
// //                         {languages.map((lang) => (
// //                             <div
// //                                 key={lang.code}
// //                                 onClick={() => selectLanguage(lang.code)}
// //                                 style={{
// //                                     padding: "8px",
// //                                     cursor: "pointer",
// //                                     whiteSpace: "nowrap",
// //                                     borderBottom: "1px solid #eee",
// //                                 }}
// //                             >
// //                                 {lang.name}
// //                             </div>
// //                         ))}
// //                     </div>
// //                 </>
// //                 )
// //             }
// //         </div >
// //     );
// // };

// // export default Translation;

// import { useEffect, useState } from "react";
// import { GlobalOutlined } from "@ant-design/icons"; // Using Ant Design's globe icon
// import { AiOutlineGlobal } from "react-icons/ai";

// const Translation = () => {
//     const [languageListVisible, setLanguageListVisible] = useState(false);

//     const googleTranslateElementInit = () => {
//         new window.google.translate.TranslateElement(
//             {
//                 pageLanguage: "en",
//                 includedLanguages: "en,zh-CN,ar,fr,de,it,es,pt",
//                 autoDisplay: false,
//             },
//             "google_translate_element"
//         );

//         // Hide the default dropdown
//         setTimeout(() => {
//             const selectElement = document.querySelector("#google_translate_element select");
//             if (selectElement) {
//                 selectElement.style.display = "none";
//             }
//         }, 1000);
//     };

//     useEffect(() => {
//         const addScript = document.createElement("script");
//         addScript.setAttribute(
//             "src",
//             "//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"
//         );
//         document.body.appendChild(addScript);
//         window.googleTranslateElementInit = googleTranslateElementInit;
//     }, []);

//     const handleLanguageSelect = (languageCode) => {
//         const selectElement = document.querySelector("#google_translate_element select");
//         if (selectElement) {
//             selectElement.value = languageCode; // Set the selected language
//             selectElement.dispatchEvent(new Event("change")); // Trigger the language change
//         }
//         setLanguageListVisible(false); // Hide the language list after selection
//     };

//     const languages = [
//         { code: "en", name: "English" },
//         { code: "zh-CN", name: "Chinese" },
//         { code: "ar", name: "Arabic" },
//         { code: "fr", name: "French" },
//         { code: "de", name: "German" },
//         { code: "it", name: "Italian" },
//         { code: "es", name: "Spanish" },
//         { code: "pt", name: "Portuguese" },
//     ];

//     return (
//         // <div style={{ position: "relative", display: "inline-block" }}>
//         //     <GlobalOutlined
//         //         onClick={() => setLanguageListVisible((prev) => !prev)}
//         //         style={{
//         //             fontSize: "24px",
//         //             cursor: "pointer",
//         //         }}
//         //         title="Select Language"
//         //     />
//         //     {languageListVisible && (
//         //         <div
//         //             style={{
//         //                 position: "absolute",
//         //                 top: "30px",
//         //                 right: "0",
//         //                 background: "#fff",
//         //                 border: "1px solid #ccc",
//         //                 borderRadius: "4px",
//         //                 boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
//         //                 zIndex: 1000,
//         //             }}
//         //         >
//         //             {languages.map((lang) => (
//         //                 <div
//         //                     key={lang.code}
//         //                     onClick={() => handleLanguageSelect(lang.code)}
//         //                     style={{
//         //                         padding: "8px 12px",
//         //                         cursor: "pointer",
//         //                         whiteSpace: "nowrap",
//         //                     }}
//         //                     onMouseEnter={(e) => (e.target.style.background = "#f0f0f0")}
//         //                     onMouseLeave={(e) => (e.target.style.background = "transparent")}
//         //                 >
//         //                     {lang.name}
//         //                 </div>
//         //             ))}
//         //         </div>
//         //     )}
//         //     <div id="google_translate_element" style={{ display: "none" }}></div>
//         //     <style>{`
//         //                 .goog-te-combo option[value="en"],
//         //                 .goog-te-combo option[value="zh-CN"],
//         //                 .goog-te-combo option[value="ar"],
//         //                 .goog-te-combo option[value="fr"],
//         //                 .goog-te-combo option[value="de"],
//         //                 .goog-te-combo option[value="it"],
//         //                 .goog-te-combo option[value="es"],
//         //                 .goog-te-combo option[value="pt"],
//         //                 .goog-te-combo option[value="reset"] {
//         //                 display: block;
//         //                 }
//         //                 .skiptranslate > iframe {
//         //                 height: 0 !important;
//         //                 border-style: none;
//         //                 box-shadow: none;
//         //                 overflow: hidden;
//         //                 display: none !important;
//         //                 }
//         //                 .VIpgJd-ZVi9od-ORHb-OEVmcd {
//         //                 height: 0px !important;
//         //                 }
//         //                 iframe {
//         //                 display: none !important;
//         //                 }
//         //                 body {
//         //                 top: 0px !important;
//         //                 }
//         //                 .skiptranslate {
//         //                 color: transparent !important;
//         //                 }
//         //                 .skiptranslate + .skiptranslate {
//         //                 display: none;
//         //                 }
//         //                 .skiptranslate select {
//         //                 outline: 0;
//         //                 border: 0;
//         //                 height: 48px;
//         //                 margin: 0 !important;
//         //                 position:relative;
//         //                 z-index: 999;
//         //                 color: white;
//         //                 background-color: #2B344C;
//         //                 border-left: 1px solid #414E91;
//         //                 border-right: 1px solid #414E91;
//         //                 }
//         //                 #google_translate_element a {
//         //                 display: none;
//         //                 }
//         //                 #goog-gt-tt {
//         //                 display: none !important;
//         //                 top: 0px !important;
//         //                 }
//         //                 .goog-tooltip.skiptranslate {
//         //                 display: none !important;
//         //                 top: 0px !important;
//         //                 }
//         //                 .activity-root {
//         //                 display: none !important;
//         //                 }
//         //                 .status-message {
//         //                 display: none !important;
//         //                 }
//         //                 .started-activity-container {
//         //                 display: none !important;
//         //                 }
//         //                 .goog-te-gadget .goog-te-spinner {
//         //                 display: hidden !important;
//         //                 z-index: -1;
//         //                 }
//         //             `}</style>
//         // </div>
//         <div
//             onMouseLeave={() => setLanguageListVisible(false)}
//         >
//             <AiOutlineGlobal
//                 onClick={() => setLanguageListVisible((prev) => !prev)}
//                 style={{ cursor: "pointer", fontSize: "24px" }}
//                 onMouseEnter={() => setLanguageListVisible(true)}
//             />
//             <div id="google_translate_element"></div>
//             <style>{`
//                                 #google_translate_element {
//                                     display:hidden;
//                                 }
//                                     .goog-te-combo option[value="en"],
//                                     .goog-te-combo option[value="zh-CN"],
//                                     .goog-te-combo option[value="ar"],
//                                     .goog-te-combo option[value="fr"],
//                                     .goog-te-combo option[value="de"],
//                                     .goog-te-combo option[value="it"],
//                                     .goog-te-combo option[value="es"],
//                                     .goog-te-combo option[value="pt"],
//                                     .goog-te-combo option[value="reset"] {
//                                     display: block;
//                                     }
//                                     .skiptranslate > iframe {
//                                     height: 0 !important;
//                                     border-style: none;
//                                     box-shadow: none;
//                                     overflow: hidden;
//                                     display: none !important;
//                                     }
//                                     .VIpgJd-ZVi9od-ORHb-OEVmcd {
//                                     height: 0px !important;
//                                     }
//                                     .skiptranslate iframe {
//                                     display: none !important;
//                                     }
//                                     body {
//                                     top: 0px !important;
//                                     }
//                                     .skiptranslate {
//                                     color: transparent !important;
//                                     }
//                                     .goog-te-gadget {
//                                         display:none;
//                                     }
//                                     .skiptranslate + .skiptranslate {
//                                     display: none;
//                                     }
//                                     .skiptranslate select {
//                                     outline: 0;
//                                     border: 0;
//                                     height: 48px;
//                                     margin: 0 !important;
//                                     position:relative;
//                                     z-index: 999;
//                                     color: white;
//                                     background-color: #2B344C;
//                                     border-left: 1px solid #414E91;
//                                     border-right: 1px solid #414E91;
//                                     }
//                                     #google_translate_element a {
//                                     display: none;
//                                     }
//                                     #goog-gt-tt {
//                                     display: none !important;
//                                     top: 0px !important;
//                                     }
//                                     .VIpgJd-ZVi9od-aZ2wEe-wOHMyf{
//                                         display:none;
//                                     }
//                                     .goog-tooltip.skiptranslate {
//                                     display: none !important;
//                                     top: 0px !important;
//                                     }
//                                     .activity-root {
//                                     display: none !important;
//                                     }
//                                     .status-message {
//                                     display: none !important;
//                                     }
//                                     .started-activity-container {
//                                     display: none !important;
//                                     }
//                                     .goog-te-gadget .goog-te-spinner {
//                                     display: hidden !important;
//                                     z-index: -1;
//                                     }
//                                 `}</style>
//             {
//                 languageListVisible && (<>
//                     <div className="languages"
//                         onMouseLeave={() => setLanguageListVisible(false)}
//                         style={{
//                             position: "absolute",
//                             backgroundColor: "#fff",
//                             border: "1px solid #ccc",
//                             boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
//                             borderRadius: "4px",
//                             padding: "5px",
//                             zIndex: 1000,
//                             minWidth: "160px",
//                         }}
//                     >
//                         {languages.map((lang) => (
//                             <div
//                                 translate="no"
//                                 key={lang.code}
//                                 onClick={() => handleLanguageSelect(lang.code)}
//                                 style={{
//                                     padding: "8px",
//                                     cursor: "pointer",
//                                     whiteSpace: "nowrap",
//                                     borderBottom: "1px solid #eee",
//                                 }}
//                             >
//                                 {lang.name}
//                             </div>
//                         ))}
//                     </div>
//                 </>
//                 )
//             }
//         </div >)
// };

// export default Translation;

import { useState, useEffect } from "react";
import { AiOutlineGlobal } from "react-icons/ai";

const Translation = () => {
    const [languageListVisible, setLanguageListVisible] = useState(false);
    const [currentLanguage, setCurrentLanguage] = useState(() => {
        return sessionStorage.getItem("currentLanguage") || "en";
});

    const googleTranslateElementInit = () => {
        new window.google.translate.TranslateElement(
            {
                pageLanguage: "en",
                includedLanguages: "en,zh-CN,ar,fr,de,it,es,pt,ja",
                autoDisplay: false,
            },
            "google_translate_element"
        );
    };

    useEffect(() => {
        const addScript = document.createElement("script");
        addScript.setAttribute(
            "src",
            "//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"
        );
        document.body.appendChild(addScript);
        window.googleTranslateElementInit = googleTranslateElementInit;
    }, []);

    useEffect(() => {
        deleteCookie('googtrans');
        return () => {
            sessionStorage.removeItem("currentLanguage");
        };
    },[])

    
    const handleLanguageSelect = (languageCode) => {
            sessionStorage.setItem("currentLanguage", languageCode);
            setCurrentLanguage(languageCode);
        
        const selectElement = document.querySelector(".goog-te-combo");
        if (selectElement) {

      if (currentLanguage === languageCode) {
        setLanguageListVisible(false);
        return;
      }
      
      setCurrentLanguage(languageCode);
            selectElement.value = languageCode;
            
            // Simulate a change event
            const event = new Event("change", { bubbles: true });
            selectElement.dispatchEvent(event);
    
            // Add a small delay to ensure the change is applied correctly
            setTimeout(() => {
                selectElement.dispatchEvent(event);
            }, 100); // Delay can be adjusted as needed
        }
        setLanguageListVisible(false);
    };
    

    const languages = [
        { code: "en", name: "English (English)" },
        { code: "zh-CN", name: "Chinese (中文)" },
        { code: "ar", name: "Arabic (العربية)" },
        { code: "fr", name: "French (Français)" },
        { code: "de", name: "German (Deutsch)" },
        { code: "it", name: "Italian (Italiano)" },
        { code: "es", name: "Spanish (Español)" },
        { code: "pt", name: "Portuguese (Português)" },
        { code: "ja", name: "Japanese (日本語)" },
    ];

    const deleteCookie = (name) => {
        document.cookie = `${name}=; Max-Age=0; path=/; domain=${window.location.hostname};`;
      };
    return (
        <div
            onMouseLeave={() => setLanguageListVisible(false)}
        >
            <AiOutlineGlobal
                onClick={() => setLanguageListVisible((prev) => !prev)}
                style={{ cursor: "pointer", fontSize: "24px" }}
                onMouseEnter={() => setLanguageListVisible(true)}
            />
            <div id="google_translate_element"></div>
            <style>{`
                                #google_translate_element {
                                    display:hidden;
                                }
                                    .goog-te-combo option[value="en"],
                                    .goog-te-combo option[value="zh-CN"],
                                    .goog-te-combo option[value="ar"],
                                    .goog-te-combo option[value="fr"],
                                    .goog-te-combo option[value="de"],
                                    .goog-te-combo option[value="it"],
                                    .goog-te-combo option[value="es"],
                                    .goog-te-combo option[value="pt"],
                                    .goog-te-combo option[value="ja"],
                                    .goog-te-combo option[value="reset"] {
                                    display: block;
                                    }
                                    .skiptranslate > iframe {
                                    height: 0 !important;
                                    border-style: none;
                                    box-shadow: none;
                                    overflow: hidden;
                                    display: none !important;
                                    }
                                    .VIpgJd-ZVi9od-ORHb-OEVmcd {
                                    height: 0px !important;
                                    }
                                    .skiptranslate iframe {
                                    display: none !important;
                                    }
                                    body {
                                    top: 0px !important;
                                    }
                                    .skiptranslate {
                                    color: transparent !important;
                                    }
                                    .goog-te-gadget {
                                        display:none;
                                    }
                                    .skiptranslate + .skiptranslate {
                                    display: none;
                                    }
                                    .skiptranslate select {
                                    outline: 0;
                                    border: 0;
                                    height: 48px;
                                    margin: 0 !important;
                                    position:relative;
                                    z-index: 999;
                                    color: white;
                                    background-color: #2B344C;
                                    border-left: 1px solid #414E91;
                                    border-right: 1px solid #414E91;
                                    }
                                    #google_translate_element a {
                                    display: none;
                                    }
                                    #goog-gt-tt {
                                    display: none !important;
                                    top: 0px !important;
                                    }
                                    .VIpgJd-ZVi9od-aZ2wEe-wOHMyf{
                                        display:none;
                                    }
                                    .goog-tooltip.skiptranslate {
                                    display: none !important;
                                    top: 0px !important;
                                    }
                                    .activity-root {
                                    display: none !important;
                                    }
                                    .status-message {
                                    display: none !important;
                                    }
                                    .started-activity-container {
                                    display: none !important;
                                    }
                                    .goog-te-gadget .goog-te-spinner {
                                    display: hidden !important;
                                    z-index: -1;
                                    }
                                `}</style>
            {languageListVisible && (
                <div
                    style={{
                        position: "absolute",
                        backgroundColor: "#fff",
                        border: "1px solid #ccc",
                        boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
                        borderRadius: "10px",
                        padding: "5px",
                        zIndex: 1000,
                        minWidth: "160px",
                        transform:"translateX(-40%)",
                    }}
                >
                    {languages.map((lang) => (
                        <div className="GlobalLanguage"
                            translate="no"
                            key={lang.code}
                            onClick={() => handleLanguageSelect(lang.code)}
                        >
                            {lang.name}
                        </div>
                    ))}
                </div>
            )}
        </div>
    );
};

export default Translation;
