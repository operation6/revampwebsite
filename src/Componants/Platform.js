import React from "react";
import platformImg1 from "./Assets/Platform-img/platform-img-1.png";
// import checkbox from "./Assets/Platform-img/checkbox.png";
// import businessProcessImg from "./Assets/Platform-img/stack-line 1.svg";
// import deliveryTimelyImg from "./Assets/Platform-img/timer-line 1.svg";
// import technologyImg from "./Assets/Platform-img/lightbulb-line 1.svg";
// import innovationImg from "./Assets/Platform-img/Vector.svg";
// import businessLogicImg from "./Assets/Platform-img/businessLogicImg.svg";
// import capabilitiesImg from "./Assets/Platform-img/capabilitiesImg.svg";
// import architectureImg from "./Assets/Platform-img/architectureImg.svg";
// import section5Img from "./Assets/Platform-img/section5Img.png";
import { ScrollAnimation } from "./motion/scrollanimation";
import { Link } from "react-router-dom";
import { motion } from "framer-motion";
import Partners from "../Landing_Page_Components/Partners";
import { Images } from "../assests";
// let checkbox = Images + 'Platform-img/checkbox.png'
// const businessProcessImg = Images + "Platform-img/stack-line 1.svg";
// const deliveryTimelyImg = Images + "Platform-img/timer-line 1.svg";
// const technologyImg = Images + "Platform-img/lightbulb-line 1.svg";
// const innovationImg = Images + "Platform-img/Vector.svg";
// const businessLogicImg = Images + "Platform-img/businessLogicImg.svg";
// const capabilitiesImg = Images + "Platform-img/capabilitiesImg.svg";
// const architectureImg = Images + "Platform-img/architectureImg.svg";
// const section5Img = Images + "Platform-img/section5Img.png";
let checkbox = Images + 'checkbox.png';
const businessProcessImg = Images + 'stack-line 1.svg';
const deliveryTimelyImg = Images + 'timer-line 1.svg';
const technologyImg = Images + 'lightbulb-line 1.svg';
const innovationImg = Images + 'Vector.svg';
const businessLogicImg = Images + 'businessLogicImg.svg';
const capabilitiesImg = Images + 'capabilitiesImg.svg';
const architectureImg = Images + 'architectureImg.svg';
const section5Img = Images + 'section5Img.png';

export default function Platform() {
  const benefits = [
    {
      img: businessProcessImg,
      heading: "Business Process",
      content:
        "Built by team of 50+ years of Experience in maritime chartering, commercial and trade area with proven record.",
    },

    {
      img: innovationImg,
      heading: "Innovation",
      content:
        "Innovation was the key of evolution for oceann solver. Team is continuously dedicated to provide the best solutions to the industry.",
    },

    {
      img: technologyImg,
      heading: "Technology",
      content:
        "With agile structure  and cloud based robust architect with highest security of Data with AWS cloud support.",
    },

    {
      img: deliveryTimelyImg,
      heading: "Deliver timely & reliably",
      content:
        "Cost for the integrated platform is lower than immediate offerings, ensuring efficient and reliable service for our clients.",
    },
  ];

  const unique = [
    {
      img: businessLogicImg,
      head: "Proven",
      heading: "Business Logic",
      content:
        "theoceann.ai business logic, refined over two decades by a leading maritime organization, has been thoroughly tested and validated.",
    },

    {
      img: capabilitiesImg,
      head: "Comprehensive",
      heading: "Capabilities",
      content:
        "With our agile and cloud-based architecture, enables remote access, advanced data sharing, and seamless integration, meeting the sophisticated data needs of modern maritime businesses",
    },

    {
      img: architectureImg,
      head: "Agile",
      heading: "Architecture",
      content:
        "theoceann.ai deliver the most complete suite of commercial capabilities available, and handles complex workflows like no other platform can.",
    },
  ];

  return (
    <>
      <div
        className="h-[85vh] flex items-center"
        style={{
          backgroundImage: `url(${platformImg1})`,
          backgroundSize: "cover",
        }}
      >
        <div className="flex flex-col justify-between max-w-2xl ml-12">
          <div className="first-child">
            <div
              style={{
                fontSize: "47px",
                fontStyle: "normal",
                fontWeight: 600,
                lineHeight: "normal",
                textTransform: "capitalize",
              }}
            >
              <h1>Setting Sail for Seamless</h1>
              <h1 style={{ color: "#F39C12" }}>Ship Trading</h1>
            </div>
          </div>
          <div className="second-child flex">
            <div
              style={{
                color: "#9F7272",
                fontFamily: "Inter",
                fontSize: "16px",
                fontStyle: "normal",
                lineHeight: "131.9%",
              }}
            >
              <div className="max-w-xl">
                Built by a team with 50+ years of experience in maritime
                chartering, commercial, and trade areas with a proven record.
              </div>
              <Link to="/demo">
                <motion.button
                  whileHover={{ scale: 1.04, backgroundColor: "#003e78" }}
                  className="bg-[#F39C12] text-white text-[1rem] max-sm:text-[0.6rem] py-3 px-8 max-sm:px-4 rounded-3xl shadow-md focus:outline-none my-[2rem]"
                >
                  Learn More
                </motion.button>
              </Link>
            </div>
          </div>
        </div>
      </div>
      {/* Partner section */}
      <ScrollAnimation>
        <div className="my-[1rem] mx-[1rem] max-sm:mx-[1rem]">
          <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-10 ">
            <span className="lg:text-2xl">Trusted collaborators</span>
          </h1>
          <Partners />
        </div>
      </ScrollAnimation>
      {/* Partner section end */}
      <div className="flex flex-col gap-[16px] md:gap-[32px] lg:gap-[48px]">
        <section>
          <ScrollAnimation>
            <div className="my-[3.5rem] lg:my-[4rem]">
              <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
                <span className="text-[24px] md:text-xl">
                  Why theoceann.ai?
                </span>
              </h1>
              <div className="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-2  xl:grid-cols-4 mx-[24px] my-[1rem] lg:my-[3rem] lg:mx-[6rem] xl:gap-6 ">
                {benefits.map((data) => (
                  <div
                    style={{
                      borderColor: "#e0e0e0",
                      borderWidth: "1px",
                    }}
                    className="border-[2px] relative flex flex-col justify-items-center justify-start py-10 px-6 rounded-md hover:shadow-xl overflow-hidden section-2-cart "
                  >
                    <div className=" p-[4px] ">
                      <img
                        src={data.img}
                        alt=""
                        className="h-[48px] bg-[rgb(255,248,234)] border-[1px] border-[#f39c12] rounded-xl p-[8px]"
                      />
                    </div>
                    <div className="flex flex-col mt-2">
                      <h1 className="text-md sm:text-[14px] md:text-[20px] font-semibold my-2">
                        {data.heading}
                      </h1>
                      <p className="text-[14px] sm:text-sm md:text-[15px] text-[#747070] ">
                        {data.content}
                      </p>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </ScrollAnimation>
        </section>
        <ScrollAnimation>
          <div>
            <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
              Our Core Values
            </h1>
            {/* <div className="flex flex-col w-[] text-md max-lg:text-sm max-md:text-xs justify-center items-center gap-4 mt-6">
              <div className="flex flex-col max-sm:items-center sm:flex-row lg:gap-[2rem] gap-4">
                <div className="flex flex-1 p-[1rem] max-sm:w-[100%] py-[1.75rem] items-center gap-2 rounded-lg border border-opacity-56 border-gray-300 bg-white shadow-md">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="61"
                    height="60"
                    viewBox="0 0 61 60"
                    fill="none"
                    className="w-[50px] h-[50px]"
                  >
                    <g clip-path="url(#clip0_732_418)">
                      <path
                        d="M31.5 55C17.6925 55 6.5 43.8075 6.5 30C6.5 16.1925 17.6925 5 31.5 5C45.3075 5 56.5 16.1925 56.5 30C56.5 43.8075 45.3075 55 31.5 55ZM29.0075 40L46.6825 22.3225L43.1475 18.7875L29.0075 32.93L21.935 25.8575L18.4 29.3925L29.0075 40Z"
                        fill="#003E78"
                      />
                    </g>
                    <defs>
                      <clipPath id="clip0_732_418">
                        <rect
                          width="60"
                          height="60"
                          fill="white"
                          transform="translate(0.5)"
                        />
                      </clipPath>
                    </defs>
                  </svg>
                  <p className="">
Innovation in Technology</p>
                </div>
                <div className="flex flex-1 p-[1rem]  max-sm:w-[100%] py-[1.75rem] items-center gap-2 rounded-lg border border-opacity-56 border-gray-300 bg-white shadow-md">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="61"
                    height="60"
                    viewBox="0 0 61 60"
                    fill="none"
                    className="w-[50px] h-[50px]"
                  >
                    <g clip-path="url(#clip0_732_418)">
                      <path
                        d="M31.5 55C17.6925 55 6.5 43.8075 6.5 30C6.5 16.1925 17.6925 5 31.5 5C45.3075 5 56.5 16.1925 56.5 30C56.5 43.8075 45.3075 55 31.5 55ZM29.0075 40L46.6825 22.3225L43.1475 18.7875L29.0075 32.93L21.935 25.8575L18.4 29.3925L29.0075 40Z"
                        fill="#003E78"
                      />
                    </g>
                    <defs>
                      <clipPath id="clip0_732_418">
                        <rect
                          width="60"
                          height="60"
                          fill="white"
                          transform="translate(0.5)"
                        />
                      </clipPath>
                    </defs>
                  </svg>
                  <p className="">
Integrity in Every Deal</p>
                </div>
              </div>
              <div className="flex flex-col max-sm:items-center sm:flex-row lg:gap-[2rem] gap-4">
                <div className="flex p-[1rem] max-sm:w-[100%] py-[1.75rem] items-center gap-2 rounded-lg border border-opacity-56 border-gray-300 bg-white shadow-md ">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="61"
                    height="60"
                    viewBox="0 0 61 60"
                    fill="none"
                    className="w-[50px] h-[50px]"
                  >
                    <g clip-path="url(#clip0_732_418)">
                      <path
                        d="M31.5 55C17.6925 55 6.5 43.8075 6.5 30C6.5 16.1925 17.6925 5 31.5 5C45.3075 5 56.5 16.1925 56.5 30C56.5 43.8075 45.3075 55 31.5 55ZM29.0075 40L46.6825 22.3225L43.1475 18.7875L29.0075 32.93L21.935 25.8575L18.4 29.3925L29.0075 40Z"
                        fill="#003E78"
                      />
                    </g>
                    <defs>
                      <clipPath id="clip0_732_418">
                        <rect
                          width="60"
                          height="60"
                          fill="white"
                          transform="translate(0.5)"
                        />
                      </clipPath>
                    </defs>
                  </svg>
                  <p className="">Consistent Reliability</p>
                </div>
                <div className="flex p-[1rem]  max-sm:w-[100%] py-[1.75rem] items-center gap-2 rounded-lg border border-opacity-56 border-gray-300 bg-white shadow-md">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="61"
                    height="60"
                    viewBox="0 0 61 60"
                    fill="none"
                    className="w-[50px] h-[50px]"
                  >
                    <g clip-path="url(#clip0_732_418)">
                      <path
                        d="M31.5 55C17.6925 55 6.5 43.8075 6.5 30C6.5 16.1925 17.6925 5 31.5 5C45.3075 5 56.5 16.1925 56.5 30C56.5 43.8075 45.3075 55 31.5 55ZM29.0075 40L46.6825 22.3225L43.1475 18.7875L29.0075 32.93L21.935 25.8575L18.4 29.3925L29.0075 40Z"
                        fill="#003E78"
                      />
                    </g>
                    <defs>
                      <clipPath id="clip0_732_418">
                        <rect
                          width="60"
                          height="60"
                          fill="white"
                          transform="translate(0.5)"
                        />
                      </clipPath>
                    </defs>
                  </svg>
                  <p className="">
Client-Centric Approach</p>
                </div>
              </div>
            </div> */}
          </div>
          <div className="flex flex-col justify-center items-center my-4 lg:my-[2rem] gap-4 xl:gap-[2rem]">
            <div className="flex flex-col md:flex-row gap-4 md:gap-[2rem] xl:gap-[2rem] w-[85%] md:w-[70%]">
              <div className="flex flex-1 items-center gap-4 py-[1.5rem] px-[16px] border-[2px] rounded-lg border-[#e0e0e0] hover:shadow-xl section-2-cart">
                <div className="flex flex-col ">
                  <div className="flex flex-row gap-4">
                    <img src={checkbox} alt="" className="h-[40px] " />

                    <div className="flex flex-col">
                      <p className="text-[20px] font-bold">
                        Innovation in Technology
                      </p>
                      <p className="text-[14px]">
                        Harnessing the latest tech trends to enhance and
                        simplify maritime commercial operations.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="flex flex-1 items-center gap-4 py-[1.5rem] px-[16px] border-[1px] rounded-lg border-[#e0e0e0] hover:shadow-xl section-2-cart">
                <div className="flex flex-col ">
                  <div className="flex flex-row gap-4">
                    <img src={checkbox} alt="" className="h-[40px]" />

                    <div className="flex flex-col">
                      <p className="text-[20px] font-bold">
                        Integrity in Every Deal
                      </p>
                      <p className="text-[14px]">
                        Ensuring every business interaction is governed by
                        truthfulness, openness, and fairness.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="flex flex-col md:flex-row gap-4 md:gap-[2rem] xl:gap-[2rem] w-[85%] md:w-[70%]">
              <div className="flex flex-1 items-center gap-4 py-[1.5rem] px-[16px] border-[1px] rounded-lg border-[#e0e0e0] hover:shadow-xl section-2-cart">
                <div className="flex flex-col ">
                  <div className="flex flex-row gap-4">
                    <img src={checkbox} alt="" className="h-[40px]" />

                    <div className="flex flex-col">
                      <p className="text-[20px] font-bold">
                        Consistent Reliability
                      </p>
                      <p className="text-[14px]">
                        Our unwavering commitment to consistently delivering
                        dependable services every time.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="flex flex-1 items-center gap-4 py-[1.5rem] px-[16px] border-[1px] rounded-lg border-[#e0e0e0] hover:shadow-xl section-2-cart">
                <div className="flex flex-col ">
                  <div className="flex flex-row gap-4">
                    <img src={checkbox} alt="" className="h-[40px]" />

                    <div className="flex flex-col">
                      <p className="text-[20px] font-bold">
                        Client-Centric Approach
                      </p>
                      <p className="text-[14px]">
                        Prioritizing our clients and tailoring solutions to
                        cater to their specific maritime needs.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </ScrollAnimation>
        <section>
          <ScrollAnimation>
            <div className="my-[3.5rem] lg:my-[4rem]">
              <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
                <span className="text-[24px] lg:text-xl">
                  What makes us Unique?
                </span>
              </h1>
              <div className="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 mx-[24px] my-[1rem] lg:my-[3rem] xl:mx-[8rem] xl:gap-8 ">
                {unique.map((data) => (
                  <div
                    style={{
                      borderColor: "#e0e0e0",
                      borderWidth: "1px",
                    }}
                    className="border-[2px] relative flex flex-col justify-items-center justify-center py-10 px-6 rounded-md hover:shadow-xl overflow-hidden section-2-cart "
                  >
                    <div className=" p-[4px] ">
                      <img src={data.img} alt="" className="rounded-xl" />
                    </div>
                    <div className="flex flex-col mt-2">
                      <p className="text-[14px] font-bold">{data.head}</p>
                      <h1 className="text-md sm:text-[14px] md:text-[24px] font-semibold my-2">
                        {data.heading}
                      </h1>
                      <p className="text-[14px] sm:text-sm md:text-[15px] text-[#747070] ">
                        {data.content}
                      </p>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </ScrollAnimation>
        </section>

        <ScrollAnimation>
          <section>
            <div className=" md:flex md:flex-row-reverse mx-auto p-5 items-center justify-start rounded-lg border-[1px] lg:mx-[8rem]">
              <div className="flex justify-center pt-3 pb-6 md:w-[40%]">
                <img
                  src={section5Img}
                  alt="section5Img"
                  className=" h-80 items-center"
                />
              </div>
              <div className="md:w-[50%] items-center mx-auto md:mx-0">
                <h1 className="text-md sm:text-lg md:text-[2rem] font-semibold ">
                  Our Services
                </h1>
                <p>
                  Navigate your way to great deals! Explore a vast ocean of
                  opportunities on our ship trading website - where every voyage
                  begins with the perfect vessel.
                </p>
                <Link to="/demo">
                  <motion.button
                    whileHover={{ scale: 1.04, backgroundColor: "#003e78" }}
                    className="bg-[#F39C12] text-white text-[1rem] max-sm:text-[0.6rem] py-2 px-8 max-sm:px-4 rounded-3xl shadow-md focus:outline-none my-[2rem]"
                  >
                    Let's Talk
                  </motion.button>
                </Link>
              </div>
            </div>
          </section>
        </ScrollAnimation>
      </div>
    </>
  );
}
