import { Button, message, Tooltip } from "antd";
import moment from "moment/moment";
import { useEffect, useState } from "react";
import { USD_CODE } from "../../constants";
import { DownloadOutlined } from "@ant-design/icons";
import { toast } from "react-toastify";

const PaymentDetails = ({ data, title }) => {
  const handleDownloadAllSlips = async (item) => {
    const token = localStorage.getItem("oceanAllToken");
    const url = `${process.env.REACT_APP_BASE_URL}accounts/payment-pdf/${item?.id}`;

    try {
      const response = await fetch(url, {
        method: "POST",
        headers: {
          Authorization: token,
          "Content-Type": "application/json", // Assuming the response is JSON
        },
      });

      

      const data = await response.json();
      const pdfUrl = data.pdf_url; // Extract the PDF URL from the response

      if (!response.ok ||pdfUrl == null) {
        toast.info("Please first generate payment invoice");
      }

      // Create a download link and click it to download the PDF
      if (response.ok && pdfUrl!==null) {
        const a = document.createElement("a");
        a.href = pdfUrl;
        a.target = "_blank";
        a.download = "payment_slip.pdf";
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
      }
    } catch (error) {
      console.error("Error downloading PDF:", error.message);
      // Handle error as needed
    }
  };

  return (
    <>
      <div className="paymentCard">
        <h2>{title || "Product"}</h2>
        <div className="paymentCard-details">
          {data?.map((item, index) => (
            <div
              key={index}
              className={"detail_list card_status" + item.payment_status}
            >
              <p>
                ID: <span>{item.id}</span>
              </p>
              <p>
                Payment Date:{" "}
                <span>
                  {moment(item.created_at).format("DD-MM-YYYY HH:mm")}
                </span>
              </p>
              <p>
                Total Amount:{" "}
                <span>
                  {item.total_amount
                    ? `${parseFloat(item.total_amount).toFixed(2)} ${
                        item.payment_description?.currency === USD_CODE
                          ? "USD"
                          : "INR"
                      }`
                    : ""}
                </span>
              </p>
              {item.payment_id && (
                <p>
                  Payment ID: <span>{item.payment_id}</span>
                </p>
              )}

              {item.payment_description && (
                <details>
                  <summary>Charges included*</summary>
                  {
                    <div className="extra_charge">
                      <p>
                        Annual Maintenance :{" "}
                        {item.payment_description.Annual_Maintaines || "0.00"}
                      </p>
                      <p>
                        Customization Charges :{" "}
                        {item.payment_description.Customization_charges ||
                          "0.00"}
                      </p>
                      <p>
                        Data Migration Charges :{" "}
                        {item.payment_description.Data_Migrations || "0.00"}
                      </p>
                      <p>
                        Discount : {item.payment_description.Discount || "0.00"}
                      </p>
                      <p>
                        Email Import :{" "}
                        {item.payment_description.Email_Import || "0.00"}
                      </p>
                      <p>GST : {item.payment_description.GST || "0.00"}</p>
                      <p>
                        Manual Services :{" "}
                        {item.payment_description.Manual_Service || "0.00"}
                      </p>
                      <p>
                        Training Charges :{" "}
                        {item.payment_description.TrainingCharges || "0.00"}
                      </p>
                      <p>
                        Number of users :{" "}
                        {item.payment_description.numberOfUsers || "0.00"}
                      </p>
                    </div>
                  }
                </details>
              )}

              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                {item.payment_status ? (
                  ""
                ) : (
                  <p>
                    <a className="payNow_link" href={item.payment_url}>
                      Pay now
                    </a>
                  </p>
                )}
                <div style={{ marginLeft: "auto" }}>
                  <Tooltip title="Download">
                    <Button
                      onClick={() => handleDownloadAllSlips(item)}
                      className="payment_info_download"
                      style={{ border: "1px solid" }}
                    >
                      <DownloadOutlined
                        style={{ fontSize: "20px" }}
                        color="navy"
                      />
                    </Button>
                  </Tooltip>
                </div>
              </div>
              <div className={"payment_status status_" + item.payment_status}>
                {item.payment_status ? "PAID" : "PENDING"}
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

const PaymentInfo = () => {
  const [paymentInfo, setPaymentInfo] = useState([]);
  const [loading, setLoading] = useState(true); // Add loading state
  const [error, setError] = useState(null); // Add error state

  const [mailQuery, setMailQuery] = useState([]); // Add mail query
  const [vmQuery, setVmQuery] = useState([]); // Add VM query

  const token = localStorage.getItem("oceanAllToken");
  const userData = JSON.parse(localStorage.getItem("oceanAllUserData"));

  const getQuerytInfo = async () => {
    try {
      const url = `${process.env.REACT_APP_BASE_URL}accounts/plan-status-list/${userData.email}`;
      const response = await fetch(url, {
        method: "POST",
        headers: {
          Authorization: `${token}`,
        },
      });

      const data = await response.json();

      if (response.ok) {
        setMailQuery(data.mail_data);
        setVmQuery(data.vm_data);
      } else {
        message.info(data.msg || "An error occurred.");
      }
    } catch (err) {
      console.error(err);
      setError("Failed to fetch payment information.");
    } finally {
      setLoading(false);
    }
  };

  const getPaymentInfo = async () => {
    try {
      const url = `${process.env.REACT_APP_BASE_URL}client/payment_history`;
      const response = await fetch(url, {
        method: "POST",
        headers: {
          Authorization: `${token}`,
        },
      });

      const data = await response.json();

      if (response.ok) {
        setPaymentInfo(data.data || []);
      } else {
        message.info(data.msg || "An error occurred.");
      }
    } catch (err) {
      console.error(err);
      setError("Failed to fetch payment information.");
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    if (token) {
      getPaymentInfo();
      getQuerytInfo();
    } else {
      setError("Authorization token not found.");
      setLoading(false);
    }
  }, [token]);

  const handleCheckProduct = (item, type) => {
    return item.product_name === type;
  };

  const forVM =
    paymentInfo.length > 0 &&
    paymentInfo.some((item) => handleCheckProduct(item, "oceanVm"));

  const forMail =
    paymentInfo.length > 0 &&
    paymentInfo.some((item) => handleCheckProduct(item, "oceannMail"));

  const MailObj =
    paymentInfo.length > 0 &&
    paymentInfo.filter((item) => item.product_name !== "oceanVm");

  const VMObj =
    paymentInfo.length > 0 &&
    paymentInfo.filter((item) => item.product_name === "oceanVm");

  if (loading) {
    return <p>Loading...</p>;
  }

  return (
    <>
      {/* Query status */}
      <div className="query_status">
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <h3 style={{ fontSize: "24px" }}>
            <u>QUERY STATUS</u>
          </h3>
        </div>
        {mailQuery.length > 0 &&
          mailQuery?.map((item) => (
            <div className="query_card" key={item.id}>
              <p>
                Contact Name : <span>{item.contact_name}</span>
              </p>
              <p>
                Contact No. : <span>{item.contact_number}</span>
              </p>
              <p>
                Date :{" "}
                <span>
                  {moment(item.created_at).format("DD-MM-YYYY HH:mm")}
                </span>
              </p>
              <p>
                Email : <span>{item.email}</span>
              </p>
              <p>
                Min. Users : <span>{item.min_users}</span>
              </p>
              <p>
                No. of Email: <span>{item.no_of_emails}</span>
              </p>
              <p>
                Plan Name : <span>{item.plan_name}</span>
              </p>
              <p>
                Plan Status : <span>{item.plan_status}</span>
              </p>
              <p>
                Required Users : <span>{item.required_no_users}</span>
              </p>
              <p>
                Query Message : <span>{item.message}</span>
              </p>
            </div>
          ))}

        {vmQuery.length > 0 &&
          mailQuery?.map((item) => (
            <div className="query_card" key={item.id}>
              <p>
                Contact Name : <span>{item.contact_name}</span>
              </p>
              <p>
                Contact No. : <span>{item.contact_number}</span>
              </p>
              <p>
                Date :{" "}
                <span>
                  {moment(item.created_at).format("DD-MM-YYYY HH:mm")}
                </span>
              </p>
              <p>
                Email : <span>{item.email}</span>
              </p>
              <p>
                Min. Users : <span>{item.min_users}</span>
              </p>
              <p>
                No. of Email: <span>{item.no_of_emails}</span>
              </p>
              <p>
                Plan Name : <span>{item.plan_name}</span>
              </p>
              <p>
                Plan Status : <span>{item.plan_status}</span>
              </p>
              <p>
                Required Users : <span>{item.required_no_users}</span>
              </p>
              <p>
                Query Message : <span>{item.message}</span>
              </p>
            </div>
          ))}
      </div>

      {/* Payment body */}
      <div className="payment_information_box">
        <h3 style={{ fontSize: "24px" }}>
          <u>PAYMENT INFORMATION</u>
        </h3>
        {paymentInfo.length > 0 ? (
          <div>
            {forVM && <PaymentDetails data={VMObj} title="Oceann VM" />}
            {forMail && <PaymentDetails data={MailObj} title="Oceann Mail" />}
          </div>
        ) : (
          <div
            style={{ padding: "20px", textAlign: "center" }}
            className="no_data_found"
          >
            No Data Found.
          </div>
        )}
      </div>
    </>
  );
};

export default PaymentInfo;
