import React, { useState } from "react";
import { Link } from "react-router-dom";
// import { BiSolidDownArrow } from "react-icons/bi";
import "../App.css";

export const ResourcesMenu = ({ submenuList, page }) => {
    const [isMenuVisible, setMenuVisible] = useState(false);
    const handleMouseEnter = () => {
        setMenuVisible(true);
    };

    const handleMouseLeave = () => {
        setMenuVisible(false);
    };

    const RolesAndIndustries = [
        {
            title: "Dry",
            link: "/dry",
        },
        {
            title: "Project",
            link: "/project",
        },
        {
            title: "Parceling",
            link: "/parceling",
        },
        {
            title: "Tanker",
            link: "/tanker",
        },
        {
            title: "Pooling",
            link: "/pooling",
        },
        // "Project",
        // "Parceling",
        // "Tankers",
        // "Pooling",
    ];
    // console.log(RolesAndIndustries, "999999");
    const Players = [
        "Shiping",
        "Charter-Operator",
        "Commodities Traders",
        "Ports Agents",
        "Shiping Broker",
    ];
    // console.log(submenuList);
    return (
        <div>
            <div
                onMouseEnter={handleMouseEnter}
                onMouseLeave={handleMouseLeave}
                className="flex gap-2 items-center"
            >
                <Link
                    to={`/knowledge-Hub/news`}
                    className="hover:text-blue-400 text-[1.1rem] custom-dropdown-icon cust-font-size"
                >
                    {/* {page === "#" ? "Knowledege-hub" : page} */}
                    Resources
                    <svg
                        stroke="currentColor"
                        fill="currentColor"
                        stroke-width="0"
                        viewBox="0 0 320 512"
                        class="rotate-0"
                        height="20"
                        width="20"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path d="M143 352.3L7 216.3c-9.4-9.4-9.4-24.6 0-33.9l22.6-22.6c9.4-9.4 24.6-9.4 33.9 0l96.4 96.4 96.4-96.4c9.4-9.4 24.6-9.4 33.9 0l22.6 22.6c9.4 9.4 9.4 24.6 0 33.9l-136 136c-9.2 9.4-24.4 9.4-33.8 0z"></path>
                    </svg>
                </Link>
                {/* <BiSolidDownArrow size={10} /> */}
                {isMenuVisible && (
                    <div className="top-[1.5rem] min-w-[800px] w-[675px] flex justify-center items-center absolute left-[1rem] menuOpen solutions">
                        {/* <div className="menuWrap gap-[2rem] mt-5 bg-white flex rounded-md px-6 py-5"> */}
                        <div className="menuWrap mt-5 w-[inherit] bg-white flex px-6 py-5">
                            <div className="justify-between w-[inherit] flex-wrap gap-[0rem] flex" >
                                {/* <h3 className="text-black font-bold text-[1rem] mt-3 ml-2">
                  Solutions
                </h3> */}
                                {submenuList.map(({ route, title, logo, list }) => {
                                    return (
                                        <div className="menuItem text-slate-700 w-48 m-1 mb-3 pb-1">
                                            <h3 className="text-md font-semibold font-[poppins] w-[80%] bg-white  pl-2 x-[.4rem] py-[0rem] text-black">
                                                <Link to={route}>{title}</Link>
                                            </h3>
                                            {/* <hr className="w-[40%] h-[3px] mb-2  rounded-lg hover:bg-gray-200" /> */}

                                            <ul className="font-[poppins] min-w-[200px] gap-2">
                                                {list.map((subList) => {
                                                    return (
                                                        <Link to={route}>
                                                            {/* <li className="text-[14px] pl-[0px] py-[3px]"> */}
                                                            <li className="text-[0.8375rem] pl-[6px] px-[4px] py-[6px] text-slate-700 hover:bg-[#0070c7] hover:text-[#ffffff]">
                                                                {subList}
                                                            </li>
                                                        </Link>
                                                    );
                                                })}
                                            </ul>
                                        </div>
                                    );
                                })}
                            </div>
                            <div className="flex justify-center">
                                <div>
                                    {/* <h3 className="text-black font-bold text-[1rem]">Roles</h3> */}
                                    {/* {RolesAndIndustries?.map((role, index) => {
                   
                    return (
                      <div className="menuItem text-black hover:border-[#003e78] border-b mb-3 pb-1">
                        <div key={index}>
                          <h3 className="text-[0.8375rem] font-[poppins] pl-[6px] px-[4px] py-[6px] hover:bg-[#39b1ff] hover:text-[#ffffff]">
                         
                            <Link to={role.link}>{role.title}</Link>
                          </h3>
                         
                        </div>
                       
                      </div>
                    );
                  })} */}
                                </div>
                                <div>
                                    {/* <h3 className="text-black font-bold text-[1rem]">
                    Industries
                  </h3> */}
                                    {/* {Players.map((item) => {
                    return (
                      <div className="menuItem text-black hover:border-[#003e78] border-b mb-3 pb-1">
                        <div>
                          
                          <h3 className="text-[0.8375rem] font-[poppins] pl-[6px] px-[4px] py-[6px] hover:bg-[#39b1ff] hover:text-[#ffffff]">
                            <Link to={"#"}>{item}</Link>
                          </h3>
                       
                        </div>
                     
                      </div>
                    );
                  })} */}
                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
};
