import React from "react";
import { AiOutlineUser } from "react-icons/ai";
import { Link } from "react-router-dom";
import { motion } from "framer-motion";

export const DestopNavbar = () => {
  return (
    <div
      className={`gap-3 flex justify-between relative justify-items-center place-items-center max-lg:text-[1rem] max-lx:text-[1.2rem] font-[poppins]  text-[1.3rem] mx-xl:mr-[3rem] mr-[4rem]`}
    >
      <Link to="/product" className="hover:text-blue-400">Product</Link>
      <Link to="/solution" className="hover:text-blue-400">Solution</Link>
      <Link to="/platform" className="hover:text-blue-400">Platform</Link>
      <Link to="#" className="hover:text-blue-400">Knowledge Hub</Link>
      <Link to="/about-us" className="hover:text-blue-400">About Us</Link>
      <Link to="/contact" className="hover:text-blue-400">Contact Us</Link>
      <Link to='/demo'>
        <motion.button whileHover={{ scale: 1.04, backgroundColor: '#ec9107' }} onClick={() => navigator("/demo")} className="max-sm:p-2 max-sm:mt-3 mt-4 px-6 py-2 rounded-[2rem] text-xs sm:text-sm md:text-md text-white bg-[#003e78]">
          BOOK DEMO
        </motion.button>
      </Link>

      <p className="flex justify-center">
        <AiOutlineUser className="text-[2rem] max-xl:text-[1rem]" />
      </p>
    </div>
  );
};
