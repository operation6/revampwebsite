import React from "react";
// import whatsApp from "./Assets/whatsapp_logo.svg";
import { Images } from "../assests";
let whatsApp = Images + 'whatsapp_logo.svg'

const WhatsappIcon = () => {
  return (
    <div
      style={{
        display: "flex",
        backgroundColor: "#28A219",
        height: "40px",
        width: "40px",
        justifyContent: "center",
        alignContent: "center",
        position: "fixed",
        bottom: "10px",
        right: "10px",
        borderRadius: "50%",
        border: "1px solid #abf1a1",
        zIndex:1900
      }}
    >
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <div style={{ width: "24px", height: "24px !important" }}>
          <a
            href="https://wa.me/message/O6TTMFKOOMNGJ1"
            target="blank"
            style={{ textDecoration: "none" }}
            className="whatsappAnchor"
          >
            <img src={whatsApp} width="24px" />
          </a>
        </div>
      </div>
    </div>
  );
};

export default WhatsappIcon;
