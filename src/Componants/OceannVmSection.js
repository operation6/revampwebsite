import React from "react";
import { FiArrowRight } from "react-icons/fi";
import { useNavigate } from "react-router-dom";
import { motion } from "framer-motion";

export const OceannVmSection = ({ route, title, heading, content, image }) => {
  const navigator = useNavigate();
  return (
    <div className="flex flex-wrap max-sm:flex-col justify-between max-sm:items-start xl:mx-[6rem] xl:gap-[4rem] w-full md:w-[80%] mx-auto">
      <div className="flex flex-1 object-cover">
        <img src={image} className="object-cover w-[100%] rounded-lg " alt="" />{" "}
      </div>
      <div className="flex flex-col flex-1 max-sm:w-full max-sm:ml-0 ml-4">
        <h3 className="text-md xl:text-[2.2rem] leading-9 max-sm:text-center max-sm:mt-2 text-left sm:text-lg md:text-lg font-semibold gap-4">
          {heading}
        </h3>
        <p className="mt-3 lg:text-[16px] max-sm:text-center text-black max-sm:text-sm">
          {content}
        </p>
        <div className="max-sm:flex max-sm:justify-center">
          <motion.button
            onClick={() => navigator(route)}
            whileHover={{
              scale: 1.04,
              backgroundColor: "#003e78",
              shadow: "#003e7823",
            }}
            className="max-sm:p-2 max-sm:mt-3 mt-4 px-6 py-2 rounded-[2rem] text-xs sm:text-sm md:text-md text-white bg-[#ec9107] "
          >
            <p className="flex flex-row justify-center items-center gap-[4px] max-sm:px-4">
              Learn More <FiArrowRight />
            </p>
          </motion.button>
          {/* <FiArrowRight />
          <p>Learn More</p> */}
        </div>
      </div>
    </div>
  );
};
