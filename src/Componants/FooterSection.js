import React from "react";
import { Link } from "react-router-dom";
import "../App.css";
import cookieIcon from "../Componants/Assets/footer-icon/cookie-icon.svg";
import img4 from "./Assets/image/Group1350.png";
import SocialMediaiconFooter from "./SocialMediaiconFooter";

const FooterSection = (props) => {
  const footerStyle = {
    backgroundImage: `url(${img4})`,
    backgroundSize: "cover",
    backgroundPosition: "center",
  };
  return (
    <>
      <footer
        className="bg-white dark:bg-gray-900 text-white mt-[4rem] max-sm:mt-[1rem]"
        style={footerStyle}
      >
        <div className="grid grid-cols-4 gap-8 max-md:gap-4  px-4 w-[90%] mx-auto pt-[5%] md-px-2 max-sm:grid-cols-2 max-md:grid-cols-3 max-lg:grid-cols-4">
          <div className="text-white">
            <h3 className="text-sm sm:text-md md:text-lg w-[90%] text-left  mb-2 ">
              Products
            </h3>
            <ul className="text-white font-light ">
              <li className=" max-sm:mb-0">
                <Link
                  to={"/solution/oceann-mail"}
                  className=" text-xs sm:text-sm list-none md:text-md"
                >
                  {/* AI Enabled Mail Intelligence */}
                  Oceann Mail
                </Link>
              </li>
              <li className=" max-sm:mb-0">
                <Link
                  to={"/product/pre-trade-intelligence"}
                  className=" text-[0.9rem] max-md:text-[0.6rem] list-none"
                >
                  {/* AI Enabled Trade Intelligence */}
                  Oceann AI
                </Link>
              </li>

              <li className=" max-sm:mb-0">
                <Link
                  to={"/solution/oceann-vm"}
                  className=" text-[0.9rem] max-md:text-[0.6rem] list-none"
                >
                  {/* Integrated Freight Management */}
                  Oceann Voyage Manager
                </Link>
              </li>
              {/* <li className=" max-sm:mb-0">
                <Link
                  to={"/solution/oceann-bi"}
                  className=" text-[0.9rem] max-md:text-[0.6rem] list-none"
                >
                  Report & Analytics
                </Link>
              </li> */}
              <li className=" max-sm:mb-0">
                <Link
                  to={"product/ocean-zero"}
                  className=" text-[0.9rem] max-md:text-[0.6rem] list-none"
                >
                  {/* Decarbonization */}
                  Oceann Zero
                </Link>
              </li>
              <li className=" max-sm:mb-0">
                <Link
                  to={"/product/oceann-x"}
                  className=" text-[0.9rem] max-md:text-[0.6rem] list-none"
                >
                  Oceann X
                </Link>
                {/* <div
                  className=" text-[0.9rem] max-md:text-[0.6rem] list-none"
                >
                  Oceann X
                </div> */}
              </li>
            </ul>
          </div>
          <div className="text-white">
            <h3 className="text-sm sm:text-md md:text-lg w-[90%] text-left  mb-2 ">
              {/* Roles & Industries */}
              Industries
            </h3>
            <ul className="text-white font-light">
              <li className=" max-sm:mb-0 ">
                <Link
                  to={"/product/oceann-finance"}
                  className=" text-[0.9rem] max-md:text-[0.6rem] list-none"
                >
                  Financial Markets & Trading
                </Link>
              </li>
              <li className=" max-sm:mb-0">
                <Link
                  to={"/solution/oceann-ai"}
                  className="
                 text-[0.9rem] max-md:text-[0.6rem] list-none"
                >
                  Maritime Market Intelligence
                </Link>
              </li>
              <li className=" max-sm:mb-0">
                <Link
                  to={"/product/chartering"}
                  className=" text-[0.9rem] max-md:text-[0.6rem] list-none"
                >
                  Chartering
                </Link>
              </li>

              {/* <li className=" max-sm:mb-0">
                <p className="hover:underline text-[0.9rem] max-md:text-[0.6rem]">
                  Ship Brokers
                </p>
              </li> */}
              <li className=" max-sm:mb-0">
                <Link
                  to={"/product/operations"}
                  className=" text-[0.9rem] max-md:text-[0.6rem] list-none"
                >
                  Operations
                </Link>
              </li>

              <li className=" max-sm:mb-0">
                <Link
                  to={"/product/oceann-finance"}
                  className=" text-[0.9rem] max-md:text-[0.6rem] list-none"
                >
                  Finance
                </Link>
              </li>
            </ul>
          </div>
          <div className="text-white">
            <h3 className="text-sm sm:text-md md:text-lg w-[90%] text-left mb-2  ">
              Explore
            </h3>
            <ul className="text-white font-light">
              <li className=" max-sm:mb-0">
                <Link
                  to="/plans"
                  className=" text-[0.9rem] max-md:text-[0.6rem] list`-none"
                >
                  Plans
                </Link>
              </li>
              <li className=" max-sm:mb-0 font-light ">
                <p className=" text-[0.9rem] max-md:text-[0.6rem] list-none">
                  Community
                </p>
              </li>
              <li className=" max-sm:mb-0">
                <Link
                  className=" text-[0.9rem] max-md:text-[0.6rem] list-none"
                  to={"/career"}
                >
                  Careers
                </Link>
              </li>
              <li className=" max-sm:mb-0">
                <Link
                  to="/contact"
                  className=" text-[0.9rem] max-md:text-[0.6rem] list-none"
                >
                  Contact us
                </Link>
              </li>
              <li className=" max-sm:mb-0">
                <Link
                  // to="/mentorship"
                  to="/mentors"
                  className=" text-[0.9rem] max-md:text-[0.6rem] list-none"
                >
                  Mentorship
                </Link>
              </li>
            </ul>
          </div>

          <div className="text-white">
            <h3 className="text-sm sm:text-md md:text-lg w-[90%] text-left mb-2">
              Security
            </h3>
            <ul className="text-white font-light">
              <li className=" max-sm:mb-0">
                <Link
                  to="/privacy"
                  className=" text-[0.9rem] max-md:text-[0.6rem] list-none"
                >
                  Privacy
                </Link>
              </li>

              <li className=" max-sm:mb-0">
                <Link
                  to="/legal-information"
                  className=" text-[0.9rem] max-md:text-[0.6rem] list-none"
                >
                  Legal Information
                </Link>
              </li>
              {/* <li className=" max-sm:mb-0">
                <Link to='/cookies'  className="hover:underline text-[0.9rem] max-md:text-[0.6rem]">
                  Coi
                </Link >
              </li> */}
            </ul>
          </div>
        </div>

        <SocialMediaiconFooter />
        {/* <CookieBanner /> */}
        {/* <NewsComponent /> */}
      </footer>

      <div
        onClick={()=>props?.setcardModal3(true)} 
        class="cookie-wrapper"
        data-tag="revisit-consent"
        data-tooltip="Cookie Settings"
      >
        <button class="cc_btn" aria-label="Cookie Settings">
          <img className="cc-img-foot" src={cookieIcon} alt="cookieIcon" />
        </button>
        <span class="text_cc"> Cookie Settings </span>
      </div>
    </>
  );
};

export default FooterSection;
