// import "../styles/navbar.css";
import styles from "../styles/navbar.module.css";
import React, { useEffect, useMemo, useState } from "react";
import { AiOutlineClose, AiOutlineGlobal, AiOutlineMenu, AiOutlineUser } from "react-icons/ai";
import { FaUserAlt } from "react-icons/fa";
import { RiLogoutBoxLine } from "react-icons/ri";
// import { spinnerStore } from "../store/globalStore";
// import { BiSolidDownArrow } from "react-icons/bi";
import { FaPen } from "react-icons/fa";
import { IoLockOpen } from "react-icons/io5";
import { FaTimes } from "react-icons/fa";
import { Link, useNavigate } from "react-router-dom";
import useIsMobile from "../customHooks/useWindowSize";
import { SubMenu } from "./SubMenu";
// import tradeicon from "./Assets/Submenu-icons/trade.png";
// import reporticon from "./Assets/Submenu-icons/report.png";
// import financeicon from "./Assets/Submenu-icons/finance.png";
// import chartering from "./Assets/Submenu-icons/chartering .png";
// import decarbonisation from "./Assets/Submenu-icons/decarbonisation.png";
import { MobileDropDown } from "./MobileDropDown";
import { motion } from "framer-motion";
// import OceannLogo from "./Assets/signup-img/company_logo.png";
import { SubMenuForSolutionKnowledgeHub } from "./subMenuForSolutionKnowledgeHub";
import { googleLogout } from "@react-oauth/google";
import Swal from "sweetalert2";
import { Images } from "../assests";
import "../App.css";
import { Drawer } from "antd";
import { ResourcesMenu } from "./ResourcesMenu";
import Translation from "./Translation";

let OceannLogo = Images + "company_logo.png";
let tradeicon = Images + "trade.png";
let reporticon = Images + "report.png";
let financeicon = Images + "finance.png";
let chartering = Images + "chartering.png";
let decarbonisation = Images + "decarbonisation.png";
const vmLogo = Images + "oceanvmLogo.png";
const mailLogo = Images + "oceanmailLogo.png";
const zeroLogo = Images + "oceannzeroLogo.png";
const OceannZero = Images + "oceamnZero.png";

const Navbar = () => {
  const navigate = useNavigate();
  const [navbar, setNavbar] = useState(false);
  const [touchStartX, setTouchStartX] = useState(0);
  const [touchEndX, setTouchEndX] = useState(0);
  useEffect(() => {
    window.addEventListener("scroll", changeBg);

    // Clean up the event listener on component unmount
    return () => {
      window.removeEventListener("scroll", changeBg);
    };
  }, []);
  const subMenuProductContent = [
    {
      route: "/solution/oceann-vm",
      title: "Oceann VM ",
      logo: vmLogo,
      list: [
        { 
          des: "Voyage Scheduling and Management", 
          link: '#' 
        },
        { 
          des: "Dynamic Voyage Estimate",
          link: '#' 
        },
        { 
          des: "Cargo & Vessel Scheduling",
          link: '#' 
        },
        { 
          des: "Freight Calculations Matrix",
          link: '#' 
        },
        { 
          des: "Laytime & Claim Management",
          link: '#' 
        },
        { 
          des: "Port Disburse Management",
          link: '#' 
        },
        { 
          des: "Off-Hire & Delay Monitoring",
          link: '#' 
        },
        { 
          des: "Freight Management",
          link: '#' 
        },
        
      ],
      description: 'Master your voyages with AI-powered control and seamless planning—every step streamlined.',
      id: 1
    },
    {
      route: "/solution/oceann-mail",
      title: "Oceann Mail",
      logo: mailLogo,
      list: [
        { 
          des: "AI-Powered Shipping Email",
          link: '#' 
        },
        { 
          des: "Trade Tonnage Tracker",
          link: '#' 
        },
        { 
          des: "Analytics for Market Dynamics",
          link: '#' 
        },
        { 
          des: "Predictive Modeling",
          link: '#' 
        },
        { 
          des: "Chatbot & Notifications",
          link: '#' 
        },
        { 
          des: "Live Tracking (relevant communication insights like bunker, port data)",
          link: '#' 
        },
        
      ],
      description: 'Transforming maritime email management into effortless efficiency and precision.',
      id: 2

    },
    {
      route: "/product/oceann-zero",
      title: "Oceann Zero ",
      logo: zeroLogo,
      list: [
        { 
          des: "Voyage Optimization",
          link: '#' 
        },
        { 
          des: "Route Optimization",
          link: '#' 
        },
        { 
          des: "Weather Optimization",
          link: '#' 
        },
        { 
          des: "CII (Carbon Intensity Indicator) Optimization", 
          link: '#' 
        },
        { 
          des: "Predictive CII, EU ETS",
          link: '#' 
        },
        { 
          des: "EU/UK MRV & Compliance",
          link: '#' 
        },
        { 
          des: "Carbon Emission Monitoring",
          link: '#' 
        },
        
      ],
      description: 'Zero compromise on performance—real-time insights for zero inefficiencies at sea.',
      id: 3
    },
    {
      route: "/product/oceann-x",
      title: "Oceann X",
      logo: decarbonisation,
      list: [
        { 
          des: 'MOBILE Application ( iOS )', 
          link: 'https://apps.apple.com/us/app/oceann-x/id6737811559' 
        },
        { 
          des: 'MOBILE Application ( ANDROID )', 
          link: 'https://play.google.com/store/apps/details?id=com.theoceann.app.oceannapp&hl=en' 
        },
        { 
          des: 'Windows Application', 
          link: 'https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/oceannX.exe' 
        }
      ],
      description: 'Oceann X: Your maritime AI companion for seamless management, anytime, anywhere—on mobile or desktop. Stay ahead with powerful features at sea or on shore.',
      id: 4
    },

  ];

  // const subMenuSolutionContent = [
  //   {
  //     route: "/solution/oceann-vm",
  //     title: "By Challenges",
  //     logo: "",
  //     // list: ["Integrated Freight Management Platform"],
  //     list: [
  //       "EU ETS",
  //       "Data and Analytics",
  //       "Operations and Finance",
  //       "Reports and Analytics",
  //       "Voyage Optimization",
  //       "Bunker Adjustment",
  //       "Automated Laytime Solution",
  //       "AI-Based Best Match for Cargo and Tonnage",
  //       "AI-Driven Voyage Management"
  //     ],

  //   },
  //   {
  //     route: "/solution/oceann-mail",
  //     title: "By Segment",
  //     logo: "",
  //     list: [
  //       "Charterers",
  //       "Commodity Traders",
  //       "Owner-Operators",
  //       "Tonnage Charterers",
  //       "Brokers"
  //     ],
  //   },
  //   {
  //     route: "/product/oceann-zero",
  //     title: "By Industry",
  //     logo: "",
  //     list: [
  //       "Crude Oil",
  //       "Dry Bulk",
  //       "Chemical"
  //     ],
  //   },
  //   // {
  //   //   route: "/solution/oceann-bi",
  //   //   title: "Oceann BI ",
  //   //   logo: "",
  //   //   list: ["Turn complex data into clear insights with powerful reports and analytics tools. "],
  //   // },

  //   // {
  //   //   route: "/solution/oceann-ai",
  //   //   title: "Oceann AI ",
  //   //   logo: "",
  //   //   list: ["Use AI insights to optimize routes, cut costs, and make better trade decisions."],
  //   // },
  //   // {
  //   //   route: "/solution/oceann-api",
  //   //   title: "Oceann API ",
  //   //   logo: "",
  //   //   list: ["Use AI insights to optimize routes, cut costs, and make better trade decisions."],
  //   // },
  // ];
  const SubMneuknowledgeHub = [
    {
      route: "/knowledge-Hub/news",
      title: "News",
      logo: "",
      list: [
        "Discover our latest news and innovations for easier global trade.",
      ],
    },
    {
      route: "/knowledge-Hub/events",
      title: "Events",
      logo: "",
      list: ["The Oceann engage in diverse events, join us to drive change."],
    },

  ];
  const subMenuSolutionContent = [
    {
      route: "",
      title: "By Challenges",
      logo: "",
      list: [
        { des: "EU ETS", link: "/product/oceann-zero" },
        { des: "Data and Analytics", link: "" },
        { des: "Operations and Finance", link: "/product/oceann-finance" },
        { des: "Reports and Analytics", link: "/solution/oceann-bi" },
        { des: "Voyage Optimization", link: "/product/operations" },
        { des: "Bunker Adjustment", link: "" },
        { des: "Automated Laytime Solution", link: "/product/oceann-analytics" },
        { des: "AI-Based Best Match for Cargo and Tonnage", link: "" },
        { des: "AI-Driven Voyage Management", link: "/product/pre-trade-intelligence" }
      ],
    },
    {
      route: "",
      title: "By Segment",
      logo: "",
      list: [
        { des: "Charterers", link: "" },
        { des: "Commodity Traders", link: "" },
        { des: "Owner-Operators", link: "" },
        { des: "Tonnage Charterers", link: "" },
        { des: "Brokers", link: "" }
      ],
    },
    {
      route: "/product/oceann-zero",
      title: "By Industry",
      logo: "",
      list: [
        { des: "Crude Oil", link: "" },
        { des: "Dry Bulk", link: "/dry" },
        { des: "Chemical", link: "" },
        { des: "Tanker", link: "/tanker" },

      ],
    }
  ];

  const SubMenuAbout = [
    {
      route: "/about-us",
      title: "About Us",
      logo: "",
      list: ["About Us Details for Maritime Info Lab Pvt Ltd"],
    },
    // {
    //   route: "/msg_ceo",
    //   title: "Message From the CEO",
    //   logo: "",
    //   list: ["Driving growth with stability in a dynamic industry"],
    // },
    {
      route: "/mentors",
      title: "Mentors & Advisors",
      logo: "",
      list: ["Specialized Expertise: Providing insights into maritime shipping,", "SaaS growth strategies, and technology adoption."],
    },
    {
      route: "/team",
      title: "Our Team",
      logo: "",
      list: ["A team is a group working together towards a common goal."],
    },
    {
      route: "/legal-information",
      title: "Legal Information",
      logo: "",
      list: ["Comprehensive Legal Details for Maritime Info Lab Pvt Ltd"],
    },

  ];


  const newContent = [
    {
      route: "/knowledge-Hub/news",
      title: "News",
      logo: "",
      // list: ["Integrated Freight Management Platform"],
      list: ['Read the latest news about our company, products, and innovations in the maritime industry']
    },
    {
      route: "/knowledge-Hub/events",
      title: "Events",
      logo: "",
      // list: ["Integrated Freight Management Platform"],
      list: ['Join exclusive webinars to learn about our innovative products and their impact on maritime operations']
    },
    {
      route: "/plans",
      title: "Plans",
      logo: "",
      // list: ["Integrated Freight Management Platform"],
      list: ['Explore new AI-powered features designed to optimize voyage planning and freight management']
    },
  ]


  const [isNavOpen, setNavOpen] = useState(false);
  const [isDropdown, setIsDropdown] = useState(false);
  const [dropdownMenu, setIsDropdownMenu] = useState([]);
  const { isMobile } = useIsMobile();
  const isMobileView = useMemo(() => isMobile, [isMobile]);

  // const { isLoading, setIsLoading } = spinnerStore();
  const [isMenuVisible, setMenuVisible] = useState(false);
  const handleMouseEnter = () => {
    setMenuVisible(true);
  };

  const handleMouseLeave = () => {
    setMenuVisible(false);
  };

  const [isMenuVisibleabout, setMenuVisibleabout] = useState(false);
  const handleMouseEnterabout = () => {
    setMenuVisibleabout(true);
  };

  const handleMouseLeaveabout = () => {
    setMenuVisibleabout(false);
  };

  const [isloggedIn, setIsloggedIn] = useState(false);
  const [refreshId, setRefreshId] = useState(0);

  const loginMenu = [
    {
      navigateUrl: "/signup",
      text: "Sign Up",
      icon: () => {
        return <FaPen />;
      },
    },
    {
      navigateUrl: "/login",
      text: "Login",
      icon: () => {
        return <IoLockOpen />;
      },
    },
  ];

  const signupMenu = [
    {
      navigateUrl: "/login",
      text: "Login",
      icon: () => {
        return <IoLockOpen />;
      },
    },
  ];

  const profileMenu = [
    {
      navigateUrl: "/profile",
      text: "Profile",
      icon: () => {
        return <FaUserAlt />;
      },
    },
    {
      navigateUrl: "/login",
      text: "Log Out",
      isLogout: true,
      icon: () => {
        return <RiLogoutBoxLine />;
      },
    },
  ];

  const handleBodyClick = (event) => {
    const clickedElement = event.target;
    if (
      !clickedElement.closest(".myDropdown") &&
      !clickedElement.closest("#excludedElement")
    ) {
      setIsDropdown(false);
    }
  };

  useEffect(() => {
    const token = localStorage.getItem("oceanAllToken");
    const signUp = JSON.parse(localStorage.getItem("oceanAllSignupData"));
    document.body.addEventListener("click", handleBodyClick);
    if (signUp) {
      setIsDropdownMenu(signupMenu);
    } else if (token) {
      setIsDropdownMenu(profileMenu);
      setIsloggedIn(true);
    } else {
      setIsDropdownMenu(loginMenu);
    }
  }, []);

  const dropdownHandler = () => {
    setIsDropdown(!isDropdown);
    // setIsLoading(!isLoading);
    const token = localStorage.getItem("oceanAllToken");
    const userData = localStorage.getItem("oceanAllSignupData");

    if (token) {
      setIsDropdownMenu(profileMenu);
    } else if (userData) {
      setIsDropdownMenu(signupMenu);
    } else {
      setIsDropdownMenu(loginMenu);
    }
  };

  const logoutFunction = () => {
    // console.log("logout function running");
    localStorage.removeItem("oceanAllToken");
    localStorage.removeItem("oceanAllUserData");
    localStorage.removeItem("oceanAllPaymentSession");
    localStorage.removeItem("isAdmin");
  };

  const changeBg = () => {
    if (window.scrollY >= 100) {
      setNavbar(true);
    } else {
      setNavbar(false);
    }
  };

  window.addEventListener("scroll", changeBg);

  const localCheck = localStorage.getItem("oceanAllToken");

  const handleLogout = () => {
    Swal.fire({
      title: "Are you sure?",
      text: "Did you really want to log out?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, logout!",
    }).then((result) => {
      if (result.isConfirmed) {
        if (localCheck !== null) {
          logoutFunction();
          googleLogout();
          navigate("/login");
        } else {
          window.location.reload();
        }
      }
    });
  };

  const minSwipeDistance = 50;

  const handleTouchStart = (e) => {
    setTouchStartX(e.touches[0].clientX);
  };

  const handleTouchMove = (e) => {
    setTouchEndX(e.touches[0].clientX);
  };

  const handleTouchEnd = () => {
    if (touchEndX - touchStartX > minSwipeDistance) {
      setNavOpen(false);
    }
  };

  useEffect(() => {
    const handleBackButton = (event) => {
      if (isNavOpen) {
        event.preventDefault();
        setNavOpen(false);
      }
    };
    window.addEventListener("popstate", handleBackButton);
    return () => {
      window.removeEventListener("popstate", handleBackButton);
    };
  }, [isNavOpen]);

  useEffect(() => {
    function handleClickOutside(event) {
      const navElement = document.querySelector(".isNavOpenDiv");
      if (navElement && !navElement.contains(event.target)) {
        setNavOpen(false);
      }
    }

    document.addEventListener("mousedown", handleClickOutside);

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [setNavOpen]);

  return (
    <div className="relative" style={{ zIndex: "201" }}>
      {/* <div
        style={{ paddingBlock: "16px" }}
        className={
          navbar
            ? "bg-[#003E78] z-100 fixed text-white flex justify-between top-[0rem] right-0 left-0 ml-0"
            : "bg-[#000000b4] bg:rgba(255,255,255,.6); z-100 fixed text-white flex justify-between top-[0rem] right-0 left-0 ml-0"
        }
      > */}
      <div
        style={{ paddingBlock: "16px" }}
        className={
          navbar
            ? "bg-[#fff] z-100 fixed text-black flex justify-between top-[0rem] right-0 left-0 ml-0"
            : "bg-[#fff] bg:rgba(255, 255, 255, 0.6); z-100 fixed text-black flex justify-between top-[0rem] right-0 left-0 ml-0"
        }
      >
        {/* navbar logo */}
        <div className="ml-[2.24rem] max-sm:ml-3 flex justify-center items-center ">
          <Link to="/">
            <img className="w-[10.4rem]" src={OceannLogo} alt="" />
          </Link>
        </div>
        {isMobileView ? (
          <div
            onTouchStart={handleTouchStart}
            onTouchMove={handleTouchMove}
            onTouchEnd={handleTouchEnd}
            className="isNavOpenDiv"
          >
            <div
              className={`w-[83%] overflow-y-auto sm:w-[40%]  flex z-50  flex-col bg-white transition-all duration-100
          ${!isNavOpen ? "right-[-200vw]" : "right-[0]"} fixed top-0 bottom-0`}
            >
              <div className="flex flex-row justify-between items-center pr-4">
                <img
                  className="h-[100px] static ml-4 mb-4 w-[150px] object-contain"
                  src={OceannLogo}
                  alt=""
                />
                <FaTimes size={30} className="text-black -mt-1" onClick={() => setNavOpen(false)} />
              </div>
              {/* mobile menu list */}
              <ul className="flex flex-col justify-center capitalize text-[1rem] text-black w-full text-uppercase  font-normal gap-[1rem]">
                <div className="flex gap-[2rem] px-4 items-center justify-between">

                  <Link to={"/login"} className="h-[40px] w-[40px] border rounded-full pt-2 pl-.5">
                    <AiOutlineUser className="text-[4rem] max-xl:text-[1rem] mx-auto" />
                  </Link>

                </div>
                <Link to={"/product"}>
                  <MobileDropDown
                    setNavOpen={setNavOpen}
                    page={"product"}
                    list={subMenuProductContent}
                  />
                </Link>
                <Link to={"/solution"}>
                  <MobileDropDown
                    setNavOpen={setNavOpen}
                    page={"solution"}
                    list={subMenuSolutionContent}
                  />
                </Link>

                <Link
                  onClick={() => setNavOpen(false)}
                  className="px-4 whitespace-nowrap font-semibold"
                  to="/platform"
                >
                  The Platform
                </Link>


                <MobileDropDown
                  setNavOpen={setNavOpen}
                  page={"resource"}
                  list={newContent}
                />


                <MobileDropDown
                  setNavOpen={setNavOpen}
                  page={"about-us"}
                  list={SubMenuAbout}
                />

              </ul>
            </div>
          </div>
        ) : (
          <div
            className={`flex gap-[1.2rem] justify-between relative xl:mx-[4rem] xl:text-[1.2rem] justify-items-center place-items-center max-lg:text-[0.5rem] max-lx:text-[1rem] font-[poppins] text-[0.8rem] topLeftLink font- font-normal`}
          >
            <div className="flex gap-[2rem] xl:gap-[3rem]">
              <div className="flex gap-5 wr">
                <div className="flex gap-[1.2rem] relative text-[1rem] ">
                  <SubMenu
                    page={"Products"}
                    subMenuProductContent={subMenuProductContent}
                    key={"product"}
                    className="text-[1rem]"
                  />
                  {/* no need to use the same component to show the menu in navar on hover */}

                  <SubMenuForSolutionKnowledgeHub
                    submenuList={subMenuSolutionContent}
                    page={"Solutions"}
                    className="text-[1rem]"
                  />
                </div>

                <ul className="flex items-center gap-[1rem] text-[1.1rem] ">
                  <Link
                    to="/platform"
                    className="hover:text-blue-400 cust-font-size text-nowrap"
                  >
                    The Platform
                  </Link>
                  <ResourcesMenu
                    submenuList={newContent}
                    page={"Solutions"}
                    className="text-[1rem]" />

                  <div className="flex gap-[1.2rem]  relative">
                    <div>
                      <div
                        onMouseEnter={handleMouseEnterabout}
                        onMouseLeave={handleMouseLeaveabout}
                        className="flex gap-2 items-center text-[1rem]"
                      >
                        <Link
                          to={`#`}
                          className="hover:text-blue-400 text-[1.1rem] custom-dropdown-icon cust-font-size"
                        >
                          {/* {page === "#" ? "Knowledege-hub" : page} */}
                          About Us
                          <svg
                            stroke="currentColor"
                            fill="currentColor"
                            stroke-width="0"
                            viewBox="0 0 320 512"
                            class="rotate-0"
                            height="20"
                            width="20"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path d="M143 352.3L7 216.3c-9.4-9.4-9.4-24.6 0-33.9l22.6-22.6c9.4-9.4 24.6-9.4 33.9 0l96.4 96.4 96.4-96.4c9.4-9.4 24.6-9.4 33.9 0l22.6 22.6c9.4 9.4 9.4 24.6 0 33.9l-136 136c-9.2 9.4-24.4 9.4-33.8 0z"></path>
                          </svg>
                        </Link>

                        {isMenuVisibleabout && (
                          <div className="menuOpen about top-[1.5rem] min-w-[800px] px-4 flex justify-center items-center absolute left-[-12rem]">
                            {/* <div className="menuWrap bg-white rounded-lg mt-[1.5rem] justify-center gap-[1rem] px-6 py-2"> */}
                            <div className="menuWrap bg-white mt-[1.5rem] justify-center gap-[1rem] px-6 py-2">
                              {SubMenuAbout.map(
                                ({ route, title, logo, list }) => {
                                  return (
                                    <div className="text-black menuItem  mb-2 pb-2 bg-white hover:border-[#003e78]">
                                      <Link
                                        to={route}
                                        style={{
                                          display: "flex",
                                          alignItems: "center",
                                          width: "100%",
                                        }}
                                      >
                                        <h3 className="text-md flex font-semibold font-[poppins] w-[100%]  x-[0rem] py-[.4rem] rounded-md">
                                          <span className="flex gap-0">
                                            <img
                                              src={logo}
                                              alt=""
                                            // className="mr-2"
                                            />
                                            {title}
                                          </span>
                                        </h3>
                                      </Link>

                                      {/* <hr className="w-[40%] h-[3px] mb-2  rounded-lg hover:bg-gray-200" /> */}

                                      <ul className="font-[poppins] min-w-[200px] gap-[4px] leading-tight">
                                        {list.map((subList) => {
                                          return (
                                            <Link to={route}>
                                              <li className="text-[0.8375rem] pl-[6px] px-[4px] py-[6px] hover:bg-[#0070c7] hover:text-[#ffffff] text-slate-700">
                                                {subList}
                                              </li>
                                            </Link>
                                          );
                                        })}
                                      </ul>
                                    </div>
                                  );
                                }
                              )}
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>{" "}

                </ul>
              </div>
            </div>
          </div>
        )}

        {/* Header right area  */}
        <div className="flex items-center mr-[1rem] max-md:ml-[0.6rem] rightTopLink rounded">
          <div className="rightTopWrap flex overflow-hidden rounded gap-[8px] justify-between">
            <div className="flex items-center cursor-pointer">
              <Translation />
            </div>
            {localCheck === null ? (
              <div className="header-login-signup bg-[#0070c7] text-[1rem] text-white max-sm:text-[0.6rem] py-2 px-6 max-sm:px-2 focus:outline-none custom-dropdown-icon rounded justify-end">
                <span
                  className="LoginButt cust-font-size"
                  onClick={() => {
                    navigate("/login");
                  }}
                >
                  Login
                </span>
                <span>|</span>
                <span
                  className="signupButt cust-font-size"
                  onClick={() => {
                    navigate("/signup");
                  }}
                >
                  Signup
                </span>
              </div>
            ) : (
              <div className="header-login-signup bg-[#0070c7] text-[1rem] text-white max-sm:text-[0.6rem] py-2 px-6 max-sm:px-2 shadow-md focus:outline-none custom-dropdown-icon">
                <span
                  className="LoginButt cust-font-size"
                  onClick={() => {
                    navigate("/profile");
                  }}
                >
                  Profile
                </span>

                <span style={{ filter: "drop-shadow(2px 4px 6px black)" }}>
                  |
                </span>

                <span
                  style={{ cursor: "pointer" }}
                  className="signupButt cust-font-size"
                  onClick={() => handleLogout()}
                >
                  Logout
                </span>
              </div>
            )}

            <Link
              to="/demo"
              className="rounded"
              style={{
                textWrap: "nowrap",
                display: "flex",
                alignItems: "center",
                // backgroundColor: "#FF8134",
              }}
            >
              <motion.button
                // whileHover={{ scale: 1.04, backgroundColor: "#39b1ff" }}
                whileHover={{
                  color: '#FF8134',
                  textDecoration: "underline",
                  backgroundColor: "#fff",
                  // border: '1px solid #FF8134'
                }}
                className="bg-[#FF8134] text-[1rem] text-white max-sm:text-[0.6rem] py-2 px-6 max-sm:px-2 focus:outline-none cust-font-size rounded "
                style={{
                  textWrap: "nowrap",
                  display: "flex",
                  alignItems: "center",
                  backgroundColor: "#FF8134",
                  transform: "none !important",
                }}
              >
                <div className="mr-1"> Book Demo</div>

                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                  <line x1="5" y1="12" x2="19" y2="12"></line>
                  <polyline points="12 5 19 12 12 19"></polyline>
                </svg>
              </motion.button>
            </Link>
            <div className="toggleBtn flex items-center ">
              {isMobile && (
                <button onClick={() => setNavOpen(!isNavOpen)} className="ml-[20px]">
                  {isNavOpen ? (
                    <FaTimes size={30} className="text-black -mt-1" />
                  ) : (
                    <AiOutlineMenu size={30} className="text-black text-2xl" />
                  )}
                </button>
              )}
            </div>
          </div>
        </div>
        {/* <div className="flex items-center mr-[3rem] max-md:ml-[0.6rem] rightTopLink"> */}

        {/* </div> */}
        {/* openMobileMenu Button */}


      </div>
    </div>
  );
};

export default Navbar;
