import React from "react";
import { Images } from "../../assests";
// import oceannZeroimg3 from "../Assets/OceannZero-img/oceanzero-img3.png";
// import oceanzeroicon1 from "../Assets/OceannZero-img/optimization.png";
// import oceanzeroicon2 from "../Assets/OceannZero-img/routeOptimization.png";
// import oceanzeroicon3 from "../Assets/OceannZero-img/weather.png";
// import oceanzeroicon4 from "../Assets/OceannZero-img/ciidashboard.png";
// import oceanzeroicon5 from "../Assets/OceannZero-img/predectivecii.png";
// import oceanzeroicon6 from "../Assets/OceannZero-img/compliance.png";

// import { BsSpeedometer2 } from "react-icons/bs";
// import { BsGlobe } from "react-icons/bs";
let oceanzeroicon1  = Images + 'routeOptimization.png'
let oceanzeroicon2 = Images + 'routeOptimization.png';
let oceanzeroicon3 = Images + 'weather.png';
let oceanzeroicon4 = Images + 'ciidashboard.png';
let oceanzeroicon5 = Images + 'predectivecii.png';
let oceanzeroicon6 = Images + 'compliance.png';


const zerocontent = [
  {
    img: oceanzeroicon1,
    heading: "Voyage Optimisation",
    content:
      "The oceann’s platform offers advanced route planning that is unrivaled in its capacity to empower operators in overseeing commercial voyages without compromising safety, profitability, or sustainability.",
  },

  {
    img: oceanzeroicon2,
    heading: "Noon Report Validation",
    content:
      "The approach for Data Collection, Verification, and Reporting with the implementation of regulations is now more critical than ever forship owners and charterers to identify effective and trustworthy techniques for gathering,validating, and reporting emissions data.",
  },

  {
    img: oceanzeroicon3,
    heading: "Weather Optimisation",
    content:
      "Seamless Real-Time Weather Route Optimization is our goal. Given the ever-changing nature ofweather, Theoceann’s weather optimization module, complete with integrated weather routing,delivers real-time data updates with a fresh forecast every six hours.",
  },

  {
    img: oceanzeroicon4,
    heading: "CII Dashboard",
    content:
      " Carbon Emissions OptimisationThe oceann’s Emissions Analytics delivers complete visibility into the performance of both your fleet and individual vessels in terms of Carbon Intensity Indicator (CII).",
  },

  {
    img: oceanzeroicon5,
    heading: "Predictive CII, EU ETS",
    content:
      "  Our team of hydrodynamics experts demonstrates excellence in monitoring vessel performanceand fuel consumption.",
  },

  {
    img: oceanzeroicon6,
    heading: "EU/UK MRV & Compliance",
    content:
      " A suite of services dedicated to evaluating Monitoring Plans (MP), validating Emission Reports (ER), and issuing the Document of Compliance (DOC) in alignment with the EU-MRV Regulation.We provide flawless service in report generation.",
  },
];
const Oceannzerocard = () => {
  return (
    <div
      className="grid grid-cols-1 gap-6 xl:gap-16 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3"
      style={{
        backgroundSize: "cover",
        backgroundPosition: "center",
      }}
    >
      {/* 1 card */}

      {zerocontent.map((data) => (
        <div
          style={{
            borderColor: "#e0e0e0",
            borderWidth: "1px",
          }}
          className="border-[2px] relative flex flex-col justify-items-center justify-center py-10 px-6 rounded-md hover:shadow-xl overflow-hidden section-2-cart "
        >
          <div className="flex">
            <img src={data.img} alt="" className="h-[2.75rem] md:h-[3.75rem]" />
          </div>
          <div className="flex flex-col mt-2">
            <h1 className="text-md sm:text-[14px] md:text-[20px] font-semibold my-2">
              {data.heading}
            </h1>
            <p className="text-xs sm:text-sm md:text-[15px] text-[#747070] ">
              {data.content}
            </p>
          </div>
        </div>
      ))}

      {/* <div className="relative flex flex-col justify-items-center justify-center bg-white py-6 px-6 rounded-3xl  my-4 hover:shadow-xl hover:scale-105 overflow-hidden">
          <div className="flex">
            <img src={oceanzeroicon2} alt="" />

          </div>
          <div className="flex flex-col mt-2">
            <h1 className="text-md sm:text-md md:text-lg font-semibold my-2">
              Noon Report Validation
            </h1>
            <p className="text-xs sm:text-sm md:text-md text-[#747070] ">
              The approach for Data Collection, Verification, and Reporting
              with the implementation of regulations is now more critical than
              ever forship owners and charterers to identify effective and
              trustworthy techniques for gathering,validating, and reporting
              emissions data.
            </p>
          </div>
        </div>
      
        <div className="relative flex flex-col justify-items-center justify-center bg-white py-6 px-6 rounded-3xl  my-4 hover:shadow-xl hover:scale-105 overflow-hidden">
          <div className="flex">
            <img
              src={oceanzeroicon3}
              alt="oceanzero-icon3"

            />
          </div>
          <div className="flex flex-col mt-2">
            <h1 className="text-md sm:text-md md:text-lg font-semibold my-2">
              Weather Optimisation
            </h1>
            <p className="text-xs sm:text-sm md:text-md text-[#747070] ">
              Seamless Real-Time Weather Route Optimization is our goal. Given
              the ever-changing nature ofweather, Theoceann’s weather
              optimization module, complete with integrated weather
              routing,delivers real-time data updates with a fresh forecast
              every six hours.
            </p>
          </div>
        </div>
      
        <div className="relative flex flex-col justify-items-center justify-center bg-white py-6 px-6 rounded-3xl  my-4 hover:shadow-xl hover:scale-105 overflow-hidden">
          <div className="flex">
            <img
              src={oceanzeroicon4}
              alt="oceanzero-icon4"

            />
          </div>
          <div className="flex flex-col mt-2">
            <h1 className="text-md sm:text-md md:text-lg font-semibold my-2">
              CII Dashboard
            </h1>
            <p className="text-xs sm:text-sm md:text-md text-[#747070] ">
              Carbon Emissions OptimisationThe oceann’s Emissions Analytics
              delivers complete visibility into the performance of both your
              fleet and individual vessels in terms of Carbon Intensity
              Indicator (CII).
            </p>
          </div>
        </div>
     
        <div className="relative flex flex-col justify-items-center justify-center bg-white py-6 px-6 rounded-3xl  my-4 hover:shadow-xl hover:scale-105 overflow-hidden">
          <div className="flex">
            <img
              src={oceanzeroicon5}
              alt="oceanzero-icon5"

            />
          </div>
          <div className="flex flex-col mt-2">
            <h1 className="text-md sm:text-md md:text-lg font-semibold my-2 ">
              Predictive CII, EU ETS
            </h1>
            <p className="text-xs sm:text-sm md:text-md text-[#747070] ">
              Our team of hydrodynamics experts demonstrates excellence in
              monitoring vessel performanceand fuel consumption.
            </p>
          </div>
        </div>
      
        <div className="relative flex flex-col justify-items-center justify-center bg-white py-6 px-6 rounded-3xl  my-4 hover:shadow-xl hover:scale-105 overflow-hidden">
          <div className="flex">
            <img
              src={oceanzeroicon6}
              alt="oceanzero-icon5"

            />
          </div>
          <div className="flex flex-col mt-2">
            <h1 className="text-md sm:text-md md:text-lg font-semibold my-2">
              EU/UK MRV & Compliance
            </h1>
            <p className="text-xs sm:text-sm md:text-md text-[#747070] ">
              A suite of services dedicated to evaluating Monitoring Plans
              (MP), validating Emission Reports (ER), and issuing the Document
              of Compliance (DOC) in alignment with the EU-MRV Regulation.We
              provide flawless service in report generation.
            </p>
          </div>
        </div> */}
    </div>
  );
};

export default Oceannzerocard;
