import React from "react";
import "./oceann-bi.css";
// import oceannBI_Img1 from "../Assets/oceann-bi-img/oceann-bi-img1.jpg";
import editImg from "../Assets/oceann-bi-img/Edit.svg";
import managementImg from "../Assets/oceann-bi-img/management-img.svg";
import financeImg from "../Assets/oceann-bi-img/finance-img.svg";
import performanceImg from "../Assets/oceann-bi-img/performance-img.png";
// import oceannBI_Img2 from "../Assets/oceann-bi-img/oceann-bm-img2.png";
// import oceannBI_Img3 from "../Assets/oceann-bi-img/oceann-bi-img3.png";
// import oceannBI_Img4 from "../Assets/oceann-bi-img/oceann-bi-img4.png";
// import oceannBI_Img5 from "../Assets/oceann-bi-img/oceann-bi-img5.png";
// import oceannBI_Img6 from "../Assets/oceann-bi-img/1st Image.png";
// import oceannBI_Img7 from "../Assets/oceann-bi-img/2nd Img.png";
// import oceannBI_Img8 from "../Assets/oceann-bi-img/3rd Image.png";
// import oceannBI_Img9 from "../Assets/oceann-bi-img/4th image.png";
// import people from "../Assets/Ocean-img/Oceanmail-features/people.png";
// import chart from "../Assets/Ocean-img/Oceanmail-features/chart.png";
// import star from "../Assets/Ocean-img/Oceanmail-features/star.png";
// import computerImg from "../Assets/ocean-vm-img/computer-img.svg";
// import trendImg from "../Assets/ocean-vm-img/trend-img.svg";
import system from "../Assets/Ocean-img/system.png";
// import dashboard from "../Assets/ocean-vm-img/analysis-img.svg";
// import forwardImg from "../Assets/ocean-vm-img/forward-img.svg";
// import globe from "../Assets/Ocean-img/Oceanmail-features/globe.png";
import Animatedwordchar from "../motion/animatewordchar";
import Animatedword from "../motion/animateword";

import { MdOutlineComputer } from 'react-icons/md';
import { BsGraphUp } from 'react-icons/bs';
import { AiOutlineTeam } from 'react-icons/ai';
import { BiHeart } from 'react-icons/bi';
import { Link } from "react-router-dom";
import { motion } from "framer-motion";
import { navigator, useNavigate } from "react-router-dom";
import {
  ScrollAnimation,
  animationVariants,
} from "../../Componants/motion/scrollanimation";
import Partners from "../../Landing_Page_Components/Partners";
import { Images } from "../../assests";
let globe = Images + 'globe.png';
let forwardImg = Images + 'forward-img.svg';
let dashboard = Images + 'analysis-img.svg';
let trendImg = Images + 'trend-img.svg';
let computerImg = Images + 'computer-img.svg';
let star = Images + 'star.png';
let chart = Images + 'chart.png';
let people = Images + 'people.png';
let oceannBI_Img9 = Images + '4th image.png';
let oceannBI_Img8 = Images + '3rd Image.png';
let oceannBI_Img7 = Images + '2nd Img.png';
let oceannBI_Img6 = Images + '1st Image.png';
let oceannBI_Img5 = Images + 'oceann-bi-img5.png';
let oceannBI_Img4 = Images + 'oceann-bi-img4.png';
let oceannBI_Img3 = Images + 'oceann-bi-img3.png';
let oceannBI_Img2 = Images + 'oceann-bm-img2.png';
let oceannBI_Img1 = Images + 'oceann-bi-img1.jpg';

export default function OceannFinance() {
  const navigator = useNavigate();
  const feature = [
    {
      img: people,
      heading: "Risk Mitigation"
    },
    {
      img: chart,
      heading: "Trend Recognition"
    },
    {
      img: star,
      heading: "Real-Time Monitoring"
    },
    {
      img: globe,
      heading: "Market Understanding"
    }
  ]

  const BI = [
    {
      img: trendImg,
      heading: "Intelligence Features",
      content: "It provides critical real-time data for optimizing vessel chartering and operations."
    },

    {
      img: dashboard,
      heading: "Chartering Dashboard",
      content: "It provides real-time data on fuel prices, consumption, and environmental regulations."
    },
    {
      img: computerImg,
      heading: "Finance Dashboard",
      content: "Accounting performance matrix offers real-time insights into financial performance, enabling better decision-making."
    },

    {
      img: forwardImg,
      heading: "Port Performance Dashboard",
      content: "Performance matrix in the marine industry provides a comprehensive evaluation of port operations."
    },
  ]
  return (

    <>

      <main>
        <div className="relative lg:h-[100vh] h-[50vh] pt-8 md:pt-0">
          <img

            src={oceannBI_Img1}
            alt="Description of the"
            className="w-full h-full object-cover object-center absolute top-0 left-0 z-0  "
          />
          <div className="relative z-10 flex flex-col items-center justify-center h-full text-center bg-[#000000a1] ">
            <h1 className="text-[20px] sm:text-md md:text-xl font-semibold text-white">

              <Animatedwordchar text="Your Navigational Hub for Maritime Excellence" />

            </h1>
            <p className="text-sm mt-3 sm:text-sm md:text-md text-white ">

              <Animatedword text="Captain's Compass: Set Sail for Success with Business Intelligence Tools." />


            </p>

            <Link to='/demo'>
              <div><motion.button style={{}} onMouseOver={(e) => (e.target.style.color = '#ffffff')} onClick={() => navigator("/demo")} whileHover={{ scale: 1.04, backgroundColor: '#003e78' }} className=" bg-[#f39c12] text-white border-[2px] px-[24px] mt-4 py-[8px] rounded-[42px] border-none">Learn More</motion.button></div>
            </Link>
          </div>
        </div>
      </main>

      {/* <section>
        <div className="container mx-auto p-4">
          <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 gap-4 section-2-cart">
            <div className="bg-white border rounded-lg hover:bg-[#23A6F0] hover:text-white p-4 shadow-md group">
              <AiOutlineTeam className="font-bold w-10 h-10 text-[#23A6F0] group-hover:text-white"/>
              <h2 class="text-lg font-semibold">Risk Mitigation</h2>
            </div>

            <div className="bg-white border rounded-lg hover:bg-[#23A6F0] hover:text-white p-4 shadow-md group">
            <BsGraphUp className="font-bold w-8 h-8 text-[#23A6F0] group-hover:text-white"/>
              <h2 class="text-lg font-semibold">Trend Recognition</h2>
            </div>

            <div className="bg-white border rounded-lg hover:bg-[#23A6F0] hover:text-white p-4 shadow-md group">
              <BiHeart className="w-10 h-10 text-[#23A6F0] group-hover:text-white"/>
              <h2 class="text-lg font-semibold  ">
                Real-Time Monitoring
              </h2>
            </div>

            <div className="bg-white border rounded-lg hover:bg-[#23A6F0] hover:text-white p-4 shadow-md group">
              
              <MdOutlineComputer className="w-10 h-10 text-[#23A6F0] group-hover:text-white"/>
              <h2 className="text-lg font-semibold">Market Understanding</h2>
            </div>
          </div>
        </div>
      </section> */}

      {/* Partner section */}
      <ScrollAnimation>
        <div className="my-[1rem] mx-[1rem] max-sm:mx-[1rem]">
          <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-10 ">
            <span className="lg:text-2xl">Trusted collaborators</span>
          </h1>
          <Partners />
        </div>
      </ScrollAnimation>
      {/* Partner section end */}

      <section>
        <ScrollAnimation>
          <div className="my-[3.5rem] lg:my-[1rem] py-[1rem]">

            <div className="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-4 xl:grid-cols-4 mx-[16px] my-[1rem] lg:my-[3rem] xl:mx-[6.4rem] xl:gap-8  ">
              {feature.map((data) => (
                <div
                  style={{
                    borderColor: "#e0e0e0",
                    borderWidth: "1px",

                  }}
                  className="border-[2px] relative flex flex-col justify-items-center justify-center py-5 px-6 rounded-sm hover:shadow-xl overflow-hidden section-2-cart "
                >
                  <div className="p-[4px]">
                    <img
                      src={data.img}
                      alt=""
                      className="h-[40px] bg-[#ffffea] border-[1px] border-[#0084DA] rounded-lg p-[8px]"
                    />
                  </div>
                  <div className="flex flex-col">
                    <h1 className="text-md sm:text-[14px] md:text-[20px] font-semibold my-2">
                      {data.heading}
                    </h1>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </ScrollAnimation>
      </section>

      {/* ------------- section 2 ------------ */}
      {/* 
      <section>
        <div className="flex flex-col items-center">
          
            <h1 className=" font-bold text-md md:text-lg lg:text-xl mt-10 mb-10"><span className="border-b-2 md:border-b-4 border-yellow-500">Our Business </span>Intelligence Features</h1>
          
          <div className="container mx-auto p-4">
            <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 gap-4 section-2-cart">
              <div className="bg-white border rounded-lg p-4 shadow-md hover:bg-[#0D1337] hover:text-white color-cart "
              
              >
                <div className="w-16 h-16 bg-[#fff] rounded-full flex items-center justify-center">
                  <img src={editImg} alt="editImg" className="bg-yellow-400 p-5 rounded-full"/>
                </div>

                <h2 className="text-lg font-semibold">Chartering dashboard</h2>
                <p>
                  It provides critical real-time data for optimizing vessel
                  chartering and operations.
                </p>
              </div>

              <div className="bg-white border rounded-lg p-4  shadow-md hover:bg-[#0D1337] hover:text-white">
                <div className="w-16 h-16 bg-[#00B4D8] rounded-full flex items-center justify-center">
                  <img src={managementImg} alt="managementImg" />
                </div>

                <h2 className="text-lg font-semibold">Bunker Performance Matrix</h2>
                <p>
                  It provides real-time data on fuel prices, consumption, and
                  environmental regulations, enabling shipowners and operators
                  to make informed decisions, minimize costs, and ensure
                  compliance with emissions standards.
                </p>
              </div>

              <div className="bg-white border rounded-lg p-4 shadow-md hover:bg-[#0D1337] hover:text-white">
                <div class="w-16 h-16 bg-[#7400B8] rounded-full flex items-center justify-center">
                  <img src={financeImg} alt="financeImg" />
                </div>

                <h2 class="text-lg font-semibold">Finance Dashboard</h2>
                <p>
                  Accounting Performance matrix offers real-time insights into
                  financial performance, enabling better decision-making.
                </p>
              </div>

              <div className="bg-white border rounded-lg p-4 shadow-md hover:bg-[#0D1337] hover:text-white">
                <div className="w-16 h-16 bg-[#FF4D6D] rounded-full flex items-center justify-center">
                  <img src={performanceImg} alt="performanceImg" />
                </div>

                <h2 className="text-lg font-semibold">
                  Port Performance Dashboard
                </h2>
                <p>
                  Performance Matrix in the marine industry provides a
                  comprehensive evaluation of port operations.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section> */}



      <section>
        <ScrollAnimation>
          <div className="my-[3.5rem] lg:my-[4rem]">
            <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
              <span className="text-[24px] lg:text-[32px]">Our Business Intelligence Features</span>
            </h1>
            <div className="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-2 mx-[24px] my-[1rem] lg:my-[3rem] xl:mx-[10rem] xl:gap-8 ">
              {BI.map((data) => (
                <div
                  style={{
                    borderColor: "#e0e0e0",
                    borderWidth: "1px",
                  }}
                  className="border-[2px] relative flex flex-col justify-items-center justify-center py-8 px-6 rounded-md hover:shadow-xl overflow-hidden section-2-cart "
                >
                  <div className=" p-[4px] ">
                    <img src={data.img} alt="" className="h-[48px] bg-[#ffffea] border-[1px] border-[#f39c12] rounded-xl p-[10px]" />
                  </div>
                  <div className="flex flex-col mt-2">
                    <h1 className="text-md sm:text-[14px] md:text-[20px] font-semibold my-2">
                      {data.heading}
                    </h1>
                    <p className="text-[14px] sm:text-sm md:text-[15px] text-[#747070] ">
                      {data.content}
                    </p>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </ScrollAnimation>
      </section>




      {/* --------------- section 3 ---------------- */}

      {/* <section>
        <div className="flex flex-col items-center section2">
          <div className="section2-heading pt-8 ml-7">
            Streamlined Insights for Strategic Navigation
          </div>

          <div className="flex">
            <div className=" mx-auto pt-6 pb-6 pl-6 ">
              <div className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2">
                <div className="flex2 pr-8">
                  <div className="heading">Chartering performance Matrix</div>
                  <div>
                    It provides critical real-time data for optimizing vessel
                    chartering and operations. It offers insights into market
                    rates, cargo opportunities, and vessel performance, enabling
                    informed decisions and enhancing operational efficiency.
                  </div>

                  <div>
                    <Link to="/solution/oceann-vm">
                    <button
                      style={{
                        backgroundColor: "#F39C12",
                        width: "184px",
                        height: "49px",
                        flexShrink: 0,
                        fontWeight: 600,
                        color: "white",
                        marginTop: "1.5rem",
                      }}
                      >
                      KNOW MORE
                    </button>
                      </Link>   
                  </div>
                </div>

                <div className="img-cls pt-3">
                  <img src={oceannBI_Img2} alt="oceannVM_Img2" />
                </div>
              </div>

              <div className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 mt-6">
                <div className="img-cls pt-3">
                  <img src={oceannBI_Img3} alt="oceannVM_Img3" />
                </div>

                <div className="flex2 md:pl-10">
                  <div className="heading">Bunker Performance Matrix</div>
                  <div>
                    Digital platform designed to monitor and manage marine fuel
                    operations. It provides real-time data on fuel prices,
                    consumption, and environmental regulations, enabling
                    shipowners and operators to make informed decisions,
                    minimize costs, and ensure compliance with emissions
                    standards.
                  </div>

                  <div>
                  <Link to="/solution/oceann-vm">
                    <button
                      style={{
                        backgroundColor: "#F39C12",
                        width: "184px",
                        height: "49px",
                        flexShrink: 0,
                        fontWeight: 600,
                        color: "white",
                        marginTop: "1.5rem",
                      }}
                    >
                      KNOW MORE
                    </button>
                    </Link>
                  </div>
                </div>
              </div>

              <div className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 mt-6">
                <div className="flex2 pr-8">
                  <div className="heading">Port Performance Matrix</div>
                  <div>
                    Port Performance Matrix in the marine industry provides a
                    comprehensive evaluation of port operations. It helps assess
                    efficiency, turnaround times, congestion, and overall
                    productivity, allowing port authorities and stakeholders to
                    make data-driven decisions for optimizing operations,
                    enhancing logistics, and improving the overall performance
                    of a port.
                  </div>

                  <div>
                  <Link to="/solution/oceann-vm">
                    <button
                      style={{
                        backgroundColor: "#F39C12",
                        width: "184px",
                        height: "49px",
                        flexShrink: 0,
                        fontWeight: 600,
                        color: "white",
                        marginTop: "1.5rem",
                      }}
                    >
                      KNOW MORE
                    </button>
                    </Link>
                  </div>
                </div>

                <div className="img-cls pt-3">
                  <img src={oceannBI_Img4} alt="oceannVM_Img4" />
                </div>
              </div>

              <div className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 mt-6">
                <div className="img-cls pt-3">
                  <img src={oceannBI_Img5} alt="oceannVM_Img5" />
                </div>

                <div className="flex2 pl-2">
                  <div className="heading">Accounting Performance Matrix</div>
                  <div>
                    Accounting Performnce matrix offers real-time insights into
                    financial performance, enabling better decision-making. It
                    helps with cost control, financial planning, and identifying
                    areas for operational optimization, ultimately contributing
                    to improved profitability and sustainability in this
                    competitive sector.
                  </div>

                  <div>
                  <Link to="/solution/oceann-vm">
                    <button
                      style={{
                        backgroundColor: "#F39C12",
                        width: "184px",
                        height: "49px",
                        flexShrink: 0,
                        fontWeight: 600,
                        color: "white",
                        marginTop: "1.5rem",
                      }}
                    >
                      KNOW MORE
                    </button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section> */}



      <ScrollAnimation>
        <section className="mx-[16px] bg-[#ecf0ff] py-[2rem] lg:my-[4rem] my-[2rem]">
          <h1 className="text-md lg:mb-[40px] text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 mb-4">
            <span className="text-[24px] lg:text-[32px] ">
              Streamlined Insights for Strategic Navigation
            </span>
          </h1>
          <div className="flex flex-col mx-[24px] lg:mx-[8rem] gap-[24px]">
            <div className="flex flex-col md:flex-row justify-between lg:mx-[6rem] lg:my-[16px] gap-[24px] md:gap-[3rem] ">
              <div
                style={{
                  border: "1px solid",
                  borderColor: "#e0e0e0",
                }}
                className="flex flex-1 flex-col rounded-xl md:px-[24px] md:py-[24px] px-[16px] py-[16px] section-3-cart"
              >
                <div className="my-[8px] rounded-xl border-[0.5px] border-[#e0e0e0]">
                  <img src={oceannBI_Img2} alt="" className="rounded-xl" />
                </div>
                <h1 className="text-[20px] font-bold mt-[16px]">
                  Chartering Performance Matrix
                </h1>
                <p className="text-[14px]">
                  It provides critical real-time data for optimizing vessel
                  chartering and operations. It offers insights into market
                  rates, cargo opportunities, and vessel performance, enabling
                  informed decisions and enhancing operational efficiency.
                </p>

                <div>
                  <motion.button
                    whileHover={{ scale: 1.04, backgroundColor: "#003e78" }} onClick={() => navigator("/solution/oceann-vm")}
                    className="bg-[#F39C12] text-white
                      max-sm:text-[12px] py-2 px-8 max-sm:px-4 rounded-3xl shadow-md focus:outline-none mt-[16px]"
                  >
                    Know More
                  </motion.button>
                </div>
              </div>
              <div
                style={{
                  border: "1px solid",
                  borderColor: "#e0e0e0",
                }}
                className="flex flex-1 flex-col rounded-xl md:px-[24px] md:py-[24px] px-[16px] py-[16px] section-3-cart"
              >
                <div className="my-[8px] rounded-xl border-[0.5px] border-[#e0e0e0]">
                  <img src={oceannBI_Img3} alt="" className="rounded-xl" />
                </div>
                <h1 className="text-[20px] font-bold mt-[16px]">
                  Bunker Performance Matrix
                </h1>
                <p className="text-[14px]">
                  Digital platform designed to monitor and manage marine fuel operations. It provides real-time data on fuel prices, consumption, and environmental regulations, enabling shipowners and operators to make informed decisions, minimize costs.
                </p>
                <div>
                  <motion.button
                    whileHover={{ scale: 1.04, backgroundColor: "#003e78" }} onClick={() => navigator("/solution/oceann-vm")}
                    className="bg-[#F39C12] text-white 
                      max-sm:text-[12px] py-2 px-8 max-sm:px-4 rounded-3xl shadow-md focus:outline-none mt-[16px]"
                  >
                    Know More
                  </motion.button>
                </div>
              </div>
            </div>
            <div className="flex flex-col md:flex-row justify-between lg:mx-[6rem] lg:my-[16px] gap-[24px] md:gap-[3rem] ">
              <div
                style={{
                  border: "1px solid",
                  borderColor: "#e0e0e0",
                }}
                className="flex flex-1 flex-col rounded-xl md:px-[24px] md:py-[24px] px-[16px] py-[16px] section-3-cart"
              >
                <div className="my-[8px] rounded-xl border-[0.5px] border-[#e0e0e0]">
                  <img src={oceannBI_Img4} alt="" className="rounded-xl" />
                </div>
                <h1 className="text-[20px] font-bold mt-[16px]">
                  Port Performance Matrix
                </h1>
                <p className="text-[14px]">
                  Port Performance Matrix in the marine industry provides a comprehensive evaluation of port operations. It helps assess efficiency, turnaround times, congestion, and overall productivity, allowing port authorities and stakeholders to make data-driven decisions.
                </p>
                <div>
                  <motion.button
                    whileHover={{ scale: 1.04, backgroundColor: "#003e78" }} onClick={() => navigator("/solution/oceann-vm")}
                    className="bg-[#F39C12] text-white 
                       max-sm:text-[12px] py-2 px-8 max-sm:px-4 rounded-3xl shadow-md focus:outline-none mt-[16px]"
                  >
                    Know More
                  </motion.button>
                </div>
              </div>
              <div
                style={{
                  border: "1px solid",
                  borderColor: "#e0e0e0",
                }}
                className="flex flex-1 flex-col rounded-xl md:px-[24px] md:py-[24px] px-[16px] py-[16px] section-3-cart"
              >
                <div className="my-[8px] rounded-xl border-[0.5px] border-[#e0e0e0]">
                  <img src={oceannBI_Img5} alt="" className="rounded-xl" />
                </div>
                <h1 className="text-[20px] font-bold mt-[16px]">
                  Accounting Performance Matrix
                </h1>
                <p className="text-[14px]">
                  Accounting Performnce matrix offers real-time insights into financial performance, enabling better decision-making. It helps with cost control, financial planning, and identifying areas for operational optimization, ultimately contributing to improved profitability and sustainability.
                </p>
                <div>
                  <motion.button
                    whileHover={{ scale: 1.04, backgroundColor: "#003e78" }} onClick={() => navigator("/solution/oceann-vm")}
                    className="bg-[#F39C12] text-white
                      max-sm:text-[12px] py-2 px-8 max-sm:px-4 rounded-3xl shadow-md focus:outline-none mt-[16px]"
                  >
                    Know More
                  </motion.button>
                </div>
              </div>
            </div>
          </div>
        </section>
      </ScrollAnimation>
      {/* --------------- section 4 ----------------  */}
      <ScrollAnimation>
        <section className="mx-[1rem] lg:mx-[6rem] xl:mx-[8rem]">
          <div className="flex flex-col items-center section2">
            <h1 className="text-md mb-4 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5">
              <span className="text-[24px] lg:text-[40px]">Services</span>
            </h1>

            <div className="flex section2-child2">
              <div className="mx-[24px] flex flex-col justify-center items-center gap-[2rem] lg:gap-[6rem] ">
                <div className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 gap-4 border-[1px] px-4 py-6 rounded-xl md:border-none">

                  <div className="flex2">
                    <div className="text-[20px] lg:text-[24px] font-bold">Fleet Management</div>
                    <div>
                      Marine companies often operate fleets of vessels. BI tools
                      can provide insights into the maintenance needs of vessels,
                      track vessel locations, and optimize the deployment of
                      assets for improved efficiency.
                    </div>
                  </div>

                  <div className="flex-2">
                    <img src={oceannBI_Img6} alt="oceannBI_Img6" className=" max-md:hidden object-cover" />
                  </div>
                </div>

                <div className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 gap-4 border-[1px] px-4 py-6 rounded-xl md:border-none">
                  <div className="img-cls pt-3 mr-2">
                    <img src={oceannBI_Img7} alt="oceannBI_Img7" className=" max-md:hidden object-cover" />
                  </div>

                  <div className="flex2 ">
                    <div className="text-[20px] lg:text-[24px] font-bold">Supply Chain Management</div>
                    <div>
                      BI tools can help monitor and manage the supply chain for
                      maritime companies. This includes tracking the movement of
                      goods, managing inventory, and ensuring timely delivery.
                    </div>
                  </div>
                </div>

                <div className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 gap-4 border-[1px] px-4 py-6 rounded-xl md:border-none">
                  <div className="flex2 ">
                    <div className="text-[20px] lg:text-[24px] font-bold">Market Analysis and Pricing</div>
                    <div>
                      Marine companies can use BI tools to analyze market trends, pricing strategies, and competitor performance to make informed decisions about routes, pricing, and market expansion.
                    </div>
                  </div>

                  <div className="img-cls pt-3">
                    <img src={oceannBI_Img8} alt="oceannBI_Img8" className=" max-md:hidden object-cover" />
                  </div>
                </div>

                <div className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 gap-4 border-[1px] px-4 py-6 rounded-xl md:border-none">
                  <div className="img-cls pt-3 mr-2">
                    <img src={oceannBI_Img9} alt="oceannBI_Img9" className="max-md:hidden object-cover" />
                  </div>

                  <div className="flex2 ">
                    <div className="text-[20px] lg:text-[24px] font-bold">Port and Terminal Operations</div>
                    <div>
                      BI tools can help optimize port and terminal operations by
                      tracking cargo movements, managing container handling, and
                      scheduling docking times for vessels.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </ScrollAnimation>
    </>
  );
}
