import React from "react";
// import oceannApi_img_1 from "../Assets/oceannApi_assests/oceannApi_img_1.png";
// import oceannApi_img_2 from "../Assets/oceannApi_assests/oceann_api_img_2.png";
// import oceannApi_img_3 from "../Assets/oceannApi_assests/oceannApi_img_3.png";
// import oceannApi_img_4 from "../Assets/oceannApi_assests/oceannApi_img_4.png";
// import oceannApi_img_5 from "../Assets/oceannApi_assests/oceannApi_img_5.png";
import { useInView } from "react-intersection-observer";
import Animatedwordchar from "../motion/animatewordchar";
import Animatedword from "../motion/animateword";
import {
  ScrollAnimation,
  animationVariants,
} from "../../Componants/motion/scrollanimation";
import Partners from "../../Landing_Page_Components/Partners";
import { Images } from "../../assests";
let oceannApi_img_1 = Images + 'oceannApi_img_1.png';
let oceannApi_img_2 = Images + 'oceann_api_img_2.png';
let oceannApi_img_3 = Images + 'oceannApi_img_3.png';
let oceannApi_img_4 = Images + 'oceannApi_img_4.png';
let oceannApi_img_5 = Images + 'oceannApi_img_5.png';

export const OceannApi = () => {
  const { ref, inView } = useInView({
    triggerOnce: true,
    rootMargin: "0px",
  });

  return (
    <>
      {/* Oceann API */}
      <div
        ref={ref}
        className="lg:h-[100vh] h-[50vh] md:h-[85vh] w-full flex justify-center items-center relativen bg-[#000000c5]"
      >
        {inView && (
          <img
            src={oceannApi_img_1}
            alt=""
            className="absolute w-full lg:h-[100vh] h-[50vh] left-0 top-0 z-0 "
          />
        )}

        <div className="text-[#FEFEFE] flex  flex-col justify-center items-center font-extrabold max-sm:text-[2rem] text-[3rem] z-10">

          <h1 className="text-center">

            <Animatedwordchar text="Oceann API" />
          </h1>
          <p className="text-center text-[1.25rem] max-sm:text-[0.8rem] font-light">

            <Animatedword text="Connecting Digital Voids With Effective Bridges" />
          </p>
        </div>
      </div>

      {/* Partner section */}
      <ScrollAnimation>
        <div className="my-[1rem] mx-[1rem] max-sm:mx-[1rem]">
          <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-20 ">
            <span className="lg:text-2xl">Trusted collaborators</span>
          </h1>
          <Partners />
        </div>
      </ScrollAnimation>
      {/* Partner section end */}

      <div className="w-[80%] mx-auto">
        {/* Establish your unique ties */}

        <ScrollAnimation>
          <div className="flex flex-col text-center max-md:text-center  mx-auto mt-[10%]">
            <h1 className="max-md:mb-[5%]  text-xl max-lg:text-lg max-md:text-md  font-extrabold  ">
              Establish Your Unique Ties
            </h1>
            <p className="text-md mt-4 max-lg:text-sm max-md:text-xs">
              Exchange information between TheOceann and external third-party
              solutions via two-way connections, offering endless integration
              opportunities and complete flexibility. This is a fundamental aspect
              of leveraging APIs for maritime management within TheOceann.
            </p>
          </div>
        </ScrollAnimation>


        {/* Retrieve and send data in real-time
         */}

        <ScrollAnimation>
          <div className="flex max-sm:text-center   max-md:flex-col-reverse mt-[10%] sm:justify-between gap-[5%]">
            <div className="w-[50%] max-md:w-[100%]">
              <h1 className="text-xl max-lg:text-lg max-md:text-md font-extrabold mb-[5%] text-black max-md:m-[10%] max-md:text-center">
                {" "}
                Retrieve & Send Real-Time
              </h1>
              <p className="text-md max-md:w-[100%] max-lg:text-sm max-md:text-xs">
                The maritime commerce API module at TheOceann equips IT
                professionals and maritime decision-makers with the ability to
                effortlessly exchange real-time data with a multitude of external
                solutions. This empowerment serves the interests of vital
                stakeholders, streamlines operational efficiency, and enables
                data-informed decision-making.
              </p>
            </div>
            <img
              src={oceannApi_img_2}
              alt="imagesAbout"
              className="rounded-2xl w-[40%] max-md:w-[100%]"
            ></img>
          </div>
        </ScrollAnimation>




        {/* We offer authentication for our APIs. */}
        <ScrollAnimation>
          <h1 className=" text-xl mt-[5%] text-center max-lg:text-lg max-md:text-md  font-extrabold  lg:mt-[8rem]">
            Attributes Of Oceann API’s
          </h1>
        </ScrollAnimation>
        {/* Two-Path Data Interaction*/}
        <ScrollAnimation>
          <div className="flex flex-col text-md  max-lg:text-sm max-md:text-xs  gap-4 mt-6">
            <div className="flex flex-col max-sm:items-center sm:flex-row justify-between gap-4">
              <div className="flex p-[1rem] max-sm:w-[100%]  w-[40%] h-[130px] items-center gap-2 rounded-lg border border-opacity-56 border-gray-300 bg-white shadow-md ">
                {/* <svg
                xmlns="http://www.w3.org/2000/svg"
                width="40"
                height="40"
                viewBox="0 0 61 60"
                fill="none"
              >
                <g clip-path="url(#clip0_732_418)">
                  <path
                    d="M31.5 55C17.6925 55 6.5 43.8075 6.5 30C6.5 16.1925 17.6925 5 31.5 5C45.3075 5 56.5 16.1925 56.5 30C56.5 43.8075 45.3075 55 31.5 55ZM29.0075 40L46.6825 22.3225L43.1475 18.7875L29.0075 32.93L21.935 25.8575L18.4 29.3925L29.0075 40Z"
                    fill="#003E78"
                  />
                </g>
                <defs>
                  <clipPath id="clip0_732_418">
                    <rect
                      width="60"
                      height="60"
                      fill="white"
                      transform="translate(0.5)"
                    />
                  </clipPath>
                </defs>
              </svg> */}
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="61"
                  height="60"
                  viewBox="0 0 61 60"
                  fill="none"
                  className="w-[50px] h-[50px]"
                >
                  <g clip-path="url(#clip0_732_418)">
                    <path
                      d="M31.5 55C17.6925 55 6.5 43.8075 6.5 30C6.5 16.1925 17.6925 5 31.5 5C45.3075 5 56.5 16.1925 56.5 30C56.5 43.8075 45.3075 55 31.5 55ZM29.0075 40L46.6825 22.3225L43.1475 18.7875L29.0075 32.93L21.935 25.8575L18.4 29.3925L29.0075 40Z"
                      fill="#003E78"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_732_418">
                      <rect
                        width="60"
                        height="60"
                        fill="white"
                        transform="translate(0.5)"
                      />
                    </clipPath>
                  </defs>
                </svg>
                <p className="w-[70%]">We offer authentication for our APIs.</p>
              </div>
              <div className="flex p-[1rem]  max-sm:w-[100%]  w-[40%] h-[130px] items-center gap-2 rounded-lg border border-opacity-56 border-gray-300 bg-white shadow-md">
                {/* <svg
                xmlns="http://www.w3.org/2000/svg"
                width="40"
                height="40"
                viewBox="0 0 61 60"
                fill="none"
              >
                <g clip-path="url(#clip0_732_418)">
                  <path
                    d="M31.5 55C17.6925 55 6.5 43.8075 6.5 30C6.5 16.1925 17.6925 5 31.5 5C45.3075 5 56.5 16.1925 56.5 30C56.5 43.8075 45.3075 55 31.5 55ZM29.0075 40L46.6825 22.3225L43.1475 18.7875L29.0075 32.93L21.935 25.8575L18.4 29.3925L29.0075 40Z"
                    fill="#003E78"
                  />
                </g>
                <defs>
                  <clipPath id="clip0_732_418">
                    <rect
                      width="60"
                      height="60"
                      fill="white"
                      transform="translate(0.5)"
                    />
                  </clipPath>
                </defs>
              </svg> */}
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="61"
                  height="60"
                  viewBox="0 0 61 60"
                  fill="none"
                  className="w-[50px] h-[50px]"
                >
                  <g clip-path="url(#clip0_732_418)">
                    <path
                      d="M31.5 55C17.6925 55 6.5 43.8075 6.5 30C6.5 16.1925 17.6925 5 31.5 5C45.3075 5 56.5 16.1925 56.5 30C56.5 43.8075 45.3075 55 31.5 55ZM29.0075 40L46.6825 22.3225L43.1475 18.7875L29.0075 32.93L21.935 25.8575L18.4 29.3925L29.0075 40Z"
                      fill="#003E78"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_732_418">
                      <rect
                        width="60"
                        height="60"
                        fill="white"
                        transform="translate(0.5)"
                      />
                    </clipPath>
                  </defs>
                </svg>
                <p className="w-[70%]">We furnish comprehensive API documentation.</p>
              </div>
            </div>

            <div className="flex justify-center">
              <div className="flex p-[1rem]  w-[40%] max-sm:w-[100%] h-[130px] items-center gap-2 rounded-lg border border-opacity-56 border-gray-300 bg-white shadow-md">
                {/* <svg
                xmlns="http://www.w3.org/2000/svg"
                width="40"
                height="40"
                viewBox="0 0 61 60"
                fill="none"
              >
                <g clip-path="url(#clip0_732_418)">
                  <path
                    d="M31.5 55C17.6925 55 6.5 43.8075 6.5 30C6.5 16.1925 17.6925 5 31.5 5C45.3075 5 56.5 16.1925 56.5 30C56.5 43.8075 45.3075 55 31.5 55ZM29.0075 40L46.6825 22.3225L43.1475 18.7875L29.0075 32.93L21.935 25.8575L18.4 29.3925L29.0075 40Z"
                    fill="#003E78"
                  />
                </g>
                <defs>
                  <clipPath id="clip0_732_418">
                    <rect
                      width="60"
                      height="60"
                      fill="white"
                      transform="translate(0.5)"
                    />
                  </clipPath>
                </defs>
              </svg> */}
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="61"
                  height="60"
                  viewBox="0 0 61 60"
                  fill="none"
                  className="w-[50px] h-[50px]"
                >
                  <g clip-path="url(#clip0_732_418)">
                    <path
                      d="M31.5 55C17.6925 55 6.5 43.8075 6.5 30C6.5 16.1925 17.6925 5 31.5 5C45.3075 5 56.5 16.1925 56.5 30C56.5 43.8075 45.3075 55 31.5 55ZM29.0075 40L46.6825 22.3225L43.1475 18.7875L29.0075 32.93L21.935 25.8575L18.4 29.3925L29.0075 40Z"
                      fill="#003E78"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_732_418">
                      <rect
                        width="60"
                        height="60"
                        fill="white"
                        transform="translate(0.5)"
                      />
                    </clipPath>
                  </defs>
                </svg>
                <p className="w-[70%]">
                  We deliver real-time capabilities to improve product request and
                  response, complete with a context menu.'
                </p>
              </div>
            </div>
          </div>
        </ScrollAnimation>
        {/* Enable a comprehensive array of integrations.*/}

        <ScrollAnimation>
          <div className="flex mt-[10%] mb-[10%] sm:justify-between gap-[5%] lg:mt-[8rem] max-md:flex-col">
            <img
              src={oceannApi_img_3}
              alt="imagesAbout"
              className="rounded-2xl w-[40%]  max-md:w-[100%]"
            ></img>

            <div className="w-[50%] max-md:w-[100%]">
              <h1 className="text-xl max-lg:text-lg max-md:text-md font-extrabold lg:mb-[2%] text-black max-md:m-[4%] max-md:text-center">
                {" "}
                Two-Path Data Interaction
              </h1>
              <p className="text-md max-md:w-[100%] max-lg:text-sm max-md:text-xs">
                TheOceann's APIs are equipped for two-way integration, enabling
                you to not only collect essential inputs from your third-party
                solutions but also deliver timely data outputs from TheOceann.
                This synchronization ensures that your solutions are empowered by
                the same timely and precise data, promoting well-informed
                decision-making.
              </p>
            </div>
          </div>
        </ScrollAnimation>

        <ScrollAnimation>
          <div className=" flex flex-col lg:flex-row mt-[10%] mb-[10%] sm:justify-between gap-[5%]  ">


            <div className=" flex flex-col w-[50%] max-md:w-[100%]">
              <h1 className="text-xl max-lg:text-lg max-md:text-md font-extrabold lg:mb-[2%] text-black max-md:m-[4%] max-md:text-center leading-tight">
                {" "}
                Enable a Comprehensive Array of Integrations.
              </h1>
              <p className="text-md max-md:w-[100%] max-lg:text-sm max-md:text-xs">
                The REST APIs provided by TheOceann offer versatility, accommodating both GET and POST requests. This empowers you with the freedom to forge your unique data connections with a diverse spectrum of external solutions.
              </p>
            </div>
            <img
              src={oceannApi_img_4}
              alt="imagesAbout"
              className="rounded-2xl w-[40%]  max-md:w-[100%] flex"
            ></img>
          </div>
        </ScrollAnimation>

        <ScrollAnimation>
          <div className="flex mt-[10%] mb-[10%] sm:justify-between gap-[5%] max-md:flex-col">

            <img
              src={oceannApi_img_5}
              alt="imagesAbout"
              className="rounded-2xl w-[40%]  max-md:w-[100%]"
            ></img>
            <div className="w-[50%] max-md:w-[100%]">

              <h1 className="text-xl max-lg:text-lg max-md:text-md font-extrabold text-black max-md:m-[4%] max-md:text-center leading-tight lg:mb-[2%] ">
                {" "}
                Grow Your Network as Technology Evolves
              </h1>
              <p className="text-md max-md:w-[100%] max-lg:text-sm max-md:text-xs">
                TheOceann APIs module empowers continuous connection growth,
                keeping you at the forefront of evolving technology and laying the
                foundation for a comprehensive, high-caliber solution centered
                around TheOceann.
              </p>
            </div>

          </div>
        </ScrollAnimation>

        {/* Grow Your Network as Technology Evolves */}

      </div>
    </>
  );
};
