// import React, { useState, useEffect } from "react";
// // import mailDashboard from "../Componants/Assets/mailSection_img/mail_dashboard.png"
// // import mailChat from "../Componants/Assets/mailSection_img/mail_chat.png"
// // import mailInbox from "../Componants/Assets/mailSection_img/mail_Inbox.png"
// import { Navigate, useNavigate } from "react-router-dom";
// import { ScrollAnimation, animationVariants } from "../motion/scrollanimation.js"
// import { motion } from "framer-motion";

// const OmPageSection = () => {
//   const Navigate = useNavigate()

//   const mainProduct = [

//     {
//       heading: "Oceann mail in operations",
//       // img: OceannVM,
//       content: "Serving as the vessel's vital hub, Operators act as the command center, ensuring seamless and cost-effective voyages. Oceann mail enhances operational efficiency by simplifying and organizing communication, allowing Operators to prioritize fleet management and optimization.Expedite voyage management: Place fleet management as a top priority and employ advanced AI to streamline your inbox. Focused voyage prioritization: Avoid overlooking critical emails; Oceann mail contextualizes messages with data, presenting important information first. Actionable visibility: Facilitate collaboration with other teams, obtaining valuable context and insights promptly when needed.",
//       // logo: vmLogo,
//       path: "/solution/oceann-vm",
//     },

//     {
//       heading: "Oceann mail in chartering:",
//       // img: OceannMail,
//       content: "Experience a revolution in maritime email solutions, prioritizing speed and efficiency. Our technology addresses existing platform inefficiencies to ensure your emails are not just read but understood. Benefits include 2x faster processing, augmented voyage efficiency, maximizing voyagers per user, automated email sorting, and innovative tonnage and cargo order automation.",
//       // logo: mailLogo,
//       path: "/solution/oceann-mail",
//     },

//     {
//       heading: "Oceann Zero",
//       // img: OceannZero,
//       content: "The oceann’s latest product Oceann zero delves into the formidable task of decarbonization in the maritime industry, and the strategies for the short, intermediate, and long haul that can guide us toward the correct course",
//       // logo: zeroLogo,
//       path: "/product/oceann-zero",

//     }
//   ]

//   return (

//     <>
//       <div className="flex flex-col lg:flex-row lg:my-28 lg:mx-24 mx-16 my-20">

//       <div className="flex flex-1 flex-col ml-16 mr-8 mt-4">
//       <h2 className="lg:text-[40px] font-bold text-[48px] leading-[40px] lg:leading-[80px]">
//              What Oceann Mail Offers?
//             </h2>
//           <div className=" flex flex-col gap-[72px]">
//             {mainProduct.map((data, index) => (
//               <div id="scrollSectionId" className="flex flex-col gap-[8px]">
//                 <h2 className="text-[28px] font-black">{data.heading}</h2>
//                 {/* <img src={data.img} alt="" /> */}
//                 <p className="font-100 mt-2">{data.content}</p>
//                 <div
//                   key={data.heading}
//                   id={data.heading}
//                   // as="div"
//                   className="flex flex-col gap-[8px]"
//                 // onChange={handleIntersectionChange}
//                 >
//                   <div> <motion.button style={{}} onMouseOver={(e) => (e.target.style.color = '#ffffff')} onMouseOut={(e) => (e.target.style.color = '#003E78')} onClick={() => Navigate(data.path)} whileHover={{ scale: 1.04, backgroundColor: '#003e78' }} className="text-[#003E78] bg-white border-[#003E78] border-[2px] px-[24px] mt-4 py-[8px] rounded-[42px]">Learn More</motion.button></div>
//                 </div>
//               </div>
//             ))}
//           </div>
//         </div>

//         <div
//           // as="div"
//           className="flex flex-1 lg:h-[40vh] gap-0 lg:ml-6 top-[18%] sticky xm-hidden"
//         // onChange={handleIntersectionChange}
//         >
//           <div className="flex flex-col mb-6">

//             {/*
//                     {visibleSection === "Oceann VM" && (
//                         <img style={{
//                             width: "140px",
//                             marginTop: "8px"
//                         }}
//                             className="vmlogo" src={vmLogo} alt="Oceann VM Logo" />
//                     )}
//                     {visibleSection === "Oceann Mail" && (
//                         <img style={{
//                             width: "140px",
//                             marginTop: "8px"
//                         }}
//                             className="maillogo" src={mailLogo} alt="Oceann Mail Logo" />
//                     )}
//                     {visibleSection === "Oceann Zero" && (
//                         <img style={{
//                             width: "140px",
//                             marginTop: "8px"
//                         }}
//                             className="zerologo" src={zeroLogo} alt="Oceann Zero Logo" />
//                     )} */}

//           </div>
//         </div>

//       </div>

//     </>
//   );

// };

// export default OmPageSection;

import React, { useState, useEffect } from "react";
// import mailDashboard from "../Componants/Assets/mailSection_img/mail_dashboard.png"
// import mailChat from "../Componants/Assets/mailSection_img/mail_chat.png"
// import mailInbox from "../Componants/Assets/mailSection_img/mail_Inbox.png"
import { Navigate, useNavigate } from "react-router-dom";
import {
  ScrollAnimation,
  animationVariants,
} from "../motion/scrollanimation.js";
// import star from "../Assets/Ocean-img/Oceanmail-features/star.png";
// import mailFilter from "../Assets/mailSection_img/mail_filter.png";
// import marketTrend from "../Assets/mailSection_img/market_trend.png";

// import mailChartering from "../Assets/mailSection_img/mail_chartering.png";
// import mapIntelligence from "../Assets/mailSection_img/map_intelligence.png";
// import calendar from "../Assets/Ocean-img/Oceanmail-features/calendar.png";
// import chat from "../Assets/Ocean-img/Oceanmail-features/chat.png";
// import chart from "../Assets/Ocean-img/Oceanmail-features/chart.png";
// import dashboard from "../Assets/Ocean-img/Oceanmail-features/dashboard.png";
// import hand from "../Assets/Ocean-img/Oceanmail-features/hand.png";
// import email from "../Assets/Ocean-img/Oceanmail-features/email.png";
// import fastEmail from "../Assets/Ocean-img/Oceanmail-features/fastEmail.png";
// import eye from "../Assets/Ocean-img/Oceanmail-features/eye.png";
// import globe from "../Assets/Ocean-img/Oceanmail-features/globe.png";
// import bulb from "../Assets/Ocean-img/Oceanmail-features/bulb.png";
// import people from "../Assets/Ocean-img/Oceanmail-features/people.png";
import { motion } from "framer-motion";
import { Images } from "../../assests.js";
const star = Images + "Ocean-img/Oceanmail-features/star.png";
const mailFilter = Images + "mail_filter.png";
const marketTrend = Images + "market_trend.png";

const mailChartering = Images + "mail_chartering.png";
const mapIntelligence = Images + "map_intelligence.png";
const calendar = Images + "calendar.png";
const chat = Images + "chat.png";
const chart = Images + "chart.png";
const dashboard = Images + "dashboard.png";
const hand = Images + "hand.png";
const email = Images + "email.png";
const fastEmail = Images + "fastEmail.png";
const eye = Images + "eye.png";
const globe = Images + "globe.png";
const bulb = Images + "bulb.png";
const people = Images + "people.png";

const OmPageSection = () => {
  const Navigate = useNavigate();

  const chartering = [
    {
      paraicon: 'https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/mail-filter.png',
      paraTitle: "Efficient Email Mastery",
      para: "Oceann Mail utilizes record systems for efficient email management, enabling chartering managers to effortlessly prioritize messages, analyze market data, and expedite decision-making.",
    },

    {
      paraicon: globe,
      paraTitle: "Market Insight on the Go",
      para: "Effortlessly assess the market: Swiftly monitor market conditions and access historical data from any location. Conduct route, vessel, and risk analysis with confidence when fixing ships.",
    },

    {
      paraicon: bulb,
      paraTitle: "Opportunity Spotlight",
      para: "Identify and seize opportunities in your inbox: Automatically prioritize messages, ensuring that you never overlook important communications from key contacts.",
    },

    {
      paraicon: people,
      paraTitle: "Collaborate with Ease",
      para: "Enhance collaboration and accelerate decision-making: Seamlessly transition between individual and shared spaces, collaborating with colleagues and teams across your organization. Access messages and relevant context easily to expedite the decision-making process.",
    },
  ];

  const Operation = [
    {
      paraicon: star,
      paraTitle: "Command Center Excellence",
      para: "Serving as the vessel's vital hub, operators act as the command center, ensuring seamless and cost-effective voyages.",
    },

    {
      paraicon: chat,
      paraTitle: "Effortless Communication",
      para: "Oceann Mail enhances operational efficiency by simplifying and organizing communication, allowing operators to prioritize fleet management and optimization.",
    },

    {
      paraicon: dashboard,
      paraTitle: "Swift Voyage Management",
      para: "Expedite voyage management: Place fleet management as a top priority and employ advanced AI to streamline your inbox.",
    },

    {
      paraicon: email,
      paraTitle: "Prioritized Inbox Precision",
      para: "Focused voyage prioritization: Avoid overlooking critical emails; Oceann Mail contextualizes messages with data, presenting important information first.",
    },
    {
      paraicon: eye,
      paraTitle: "Actionable Visibility",
      para: "Facilitate collaboration with other teams, obtaining valuable context and insights promptly when needed.",
    },
  ];

  const Finance = [
    {
      paraicon: globe,
      paraTitle: "Navigating Market Trends",
      para: "Leveraging Oceann Mail's forward-thinking cargo order data offers market participants a glimpse into upcoming commodity flows, enabling the anticipation of shifts in asset prices and serving as a predictive economic indicator.",
    },

    {
      paraicon: email,
      paraTitle: "Market Mastery with Oceann Mail",
      para: "Stay ahead in the market by utilizing Oceann Mail's foresight, foreseeing potential asset price fluctuations based on upcoming commodity movements. This invaluable data acts as a proactive tool, empowering decision-makers to make strategic moves in response to evolving economic trends.",
    },

    {
      paraicon: dashboard,
      paraTitle: "Predictive Edge",
      para: "Empower your business strategy with Oceann's predictive prowess, unlocking a competitive edge in understanding and adapting to changing market dynamics.",
    },

    {
      paraicon: chart,
      paraTitle: "Integrated Insights",
      para: "Seamlessly integrate these foresights into your decision-making process, ensuring you are well-positioned to capitalize on emerging opportunities and mitigate risks.",
    },
  ];

  const personal = [
    {
      paraicon: fastEmail,
      paraTitle: "Effortless Communication Mastery",
      para: "Our innovative personal mail management solution designed to streamline and organize your communication effortlessly. With intuitive features, this product ensures you never miss important messages, offering robust tools for categorization and prioritization.",
    },

    {
      paraicon: email,
      paraTitle: "Clarity in Your Inbox",
      para: "Enjoy a clutter-free inbox as the system intelligently filters out irrelevant emails, saving you valuable time.",
    },

    {
      paraicon: calendar,
      paraTitle: "Unified Organization",
      para: "Seamless integration with calendars and task lists keeps your personal and professional life organized in one place. Our product also provides advanced security features to safeguard your privacy, offering peace of mind in the digital realm.",
    },

    {
      paraicon: hand,
      paraTitle: "Tailored for You",
      para: "Experience a more efficient and enjoyable email experience with our personalized solution tailored to meet your individual communication needs.",
    },
  ];
  return (
    <>
      <div className="flex flex-col my-[2rem] lg:my-[2rem] mx-[10px]">
        <div className=" flex flex-col gap-[24px] lg:gap-16">
          <h2 className="text-center lg:text-[48px] font-bold text-[24px] mb-[16px] lg:mb-[2rem] leading-[40px] lg:leading-[80px]">
            What Oceann Mail Offers?
          </h2>

          <ScrollAnimation>
            <div className="flex flex-col-reverse my-[8px] gap-[18px] lg:my-[1rem] lg:flex-row mx-[2rem] lg:mx-[4rem] lg:gap-[3rem]">
              <div className="flex flex-col flex-1">
                <p className="tracking-[6px] lg:tracking-[12px] text-[14px]">
                  FEATURES
                </p>
                <h1 className="text-[20px] lg:text-[24px] font-bold mb-[8px] lg:mb-4 leading-[24px]">
                  Oceann Mail in Chartering
                </h1>
                <div className="flex flex-col gap-[1rem]">
                  {chartering.map((data) => (
                    <div className="flex flex-col ">
                      <div className="flex flex-row gap-2 items-center ">
                        <img src={data.paraicon} alt="" className="h-[20px]" />
                        <p className="text-black font-semibold">
                          {data.paraTitle}
                        </p>
                      </div>
                      <p className="md:pl-[1.8rem] font-light text-[#5a5a5a]">
                        {data.para}
                      </p>
                    </div>
                  ))}
                </div>
              </div>
              <div className="flex flex-1 border-[1px] rounded-lg">
                <img
                  src={'https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/mailbox.png'}
                  alt=""
                  className="object-contain rounded-lg w-[100%]"
                />
              </div>
            </div>
          </ScrollAnimation>

          <ScrollAnimation>
            <div className="flex flex-col my-[16px] gap-[18px] lg:my-[1rem] lg:flex-row mx-[2rem] lg:mx-[4rem] lg:gap-[3rem]">
              <div className="flex flex-1 border-[1px] rounded-lg ">
                <img

                  src={"https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/desktop-aaplication.png"}
                  alt=""
                  className="object-cover rounded-lg w-[100%]"
                />
              </div>
              <div className="flex flex-col flex-1">
                <p className="tracking-[6px] lg:tracking-[12px] text-[14px]">
                  FEATURES
                </p>
                <h1 className="text-[20px] lg:text-[24px] font-bold mb-[8px] lg:mb-4">
                  Oceann Mail in Operation
                </h1>
                <div className="flex flex-col gap-[1rem]">
                  {Operation.map((data) => (
                    <div className="flex flex-col ">
                      <div className="flex lex-row gap-2 items-center ">
                        <img src={data.paraicon} alt="" className="h-[20px]" />
                        <p className="text-black font-semibold">
                          {data.paraTitle}
                        </p>
                      </div>
                      <p className="md:pl-[1.8rem] font-light text-[#5a5a5a]">
                        {data.para}
                      </p>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </ScrollAnimation>

          <ScrollAnimation>
            <div className="flex flex-col-reverse my-[16px] gap-[18px] lg:my-[1rem] lg:flex-row mx-[2rem] lg:mx-[4rem] lg:gap-[3rem]">
              <div className="flex flex-col flex-1">
                <p className="tracking-[6px] lg:tracking-[12px] text-[14px]">
                  FEATURES
                </p>
                <h1 className="text-[20px] lg:text-[24px] font-bold mb-[8px] lg:mb-4">
                  Oceann Mail in Finance
                </h1>
                <div className="flex flex-col gap-[1rem]">
                  {Finance.map((data) => (
                    <div className="flex flex-col ">
                      <div className="flex flex-row gap-2 items-center ">
                        <img src={data.paraicon} alt="" className="h-[20px]" />
                        <p className="text-black font-semibold">
                          {data.paraTitle}
                        </p>
                      </div>
                      <p className="md:pl-[1.8rem] font-light text-[#5a5a5a]">
                        {data.para}
                      </p>
                    </div>
                  ))}
                </div>
              </div>
              <div className="flex flex-1 border-[1px] rounded-lg">
                <img
                  src={'https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/freight-index.png'}
                  alt=""
                  className="object-contain rounded-lg w-[100%]"
                />
              </div>
            </div>
          </ScrollAnimation>

          <ScrollAnimation>
            <div className="flex flex-col my-[16px] gap-[18px] lg:my-[1rem] lg:flex-row mx-[2rem] lg:mx-[4rem] lg:gap-[3rem]">
              <div className="flex flex-1 border-[1px] rounded-lg ">
                <img
                  src={'https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/mail-filter.png'}
                  alt=""
                  className="object-contain rounded-lg w-[100%] "
                />
              </div>
              <div className="flex flex-col flex-1">
                <p className="tracking-[6px] lg:tracking-[12px] text-[14px]">
                  FEATURES
                </p>
                <h1 className="text-[20px] lg:text-[24px] font-bold mb-[8px] lg:mb-4">
                  Oceann Mail for Personal
                </h1>
                <div className="flex flex-col gap-[1rem]">
                  {personal.map((data) => (
                    <div className="flex flex-col ">
                      <div className="flex lex-row gap-2 items-center ">
                        <img src={data.paraicon} alt="" className="h-[20px]" />
                        <p className="text-black font-semibold">
                          {data.paraTitle}
                        </p>
                      </div>
                      <p className="md:pl-[1.8rem] font-light text-[#5a5a5a]">
                        {data.para}
                      </p>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </ScrollAnimation>
        </div>
      </div>
    </>
  );
};

export default OmPageSection;
