import React from "react";
import "./oceann-vm.css";
// import oceannVM_Img1 from "../Assets/ocean-vm-img/oceannVM_img1.png";
// import trendImg from "../Assets/ocean-vm-img/trend-img.svg";
// import analysisImg from "../Assets/ocean-vm-img/analysis-img.svg";
// import contactManagerImg from "../Assets/ocean-vm-img/contact-manager-img.svg";
// import calculatorImg from "../Assets/ocean-vm-img/calculator-img.svg";
// import calenderImg from "../Assets/ocean-vm-img/calender-img.svg";

// import oceannVM_Img3 from "../Assets/ocean-vm-img/oceann-vm-img3.jpg";
// import oceannVM_Img4 from "../Assets/ocean-vm-img/oceann-vm-img4.jpg";
// import operationSection from "../Assets/image/operationSection_img.png";
// import operationlogo from "../Assets/oceann-operations-img/oceann-operations-img2.png";
// import financelogo from '../Assets/Fiance_logoes/finance-icon.png';
// import analyticslogo from "../Assets/oceann-analytics-img/oceann-analytics-img2.png";

// import accessImg from "../Assets/ocean-vm-img/access-img.svg";
// import compareImg from "../Assets/ocean-vm-img/compare-img.svg";
// import computerImg from "../Assets/ocean-vm-img/computer-img.svg";
// import forwardImg from "../Assets/ocean-vm-img/forward-img.svg";
// import oceannVM_Img8 from "../Assets/ocean-vm-img/oceann-vm-img8.png";
// import workflowImg from "../Assets/ocean-vm-img/workflow-img.svg";
// import saveImg from "../Assets/ocean-vm-img/save-img.svg";
// import alertImg from "../Assets/ocean-vm-img/alert-img.svg";
// import oceannVM_Img9 from "../Assets/ocean-vm-img/oceann-vm-img9.png";
// import oceannVM_Img11 from "../Assets/image/analyticSection_img.png";
// import captureImg from "../Assets/ocean-vm-img/capture-img.svg";
// import oceannVM_Img12 from "../Assets/ocean-vm-img/oceann-vm-img12.png";
// import oceannVM_Img13 from "../Assets/ocean-vm-img/oceann-vm-img13.png";
// import graphImg from "../Assets/ocean-vm-img/graph-img.svg";
// import controlImg from "../Assets/ocean-vm-img/control-img.svg";
// import automateImg from "../Assets/ocean-vm-img/automate-img.svg";
import Animatedword from "../../Componants/motion/animateword";

import Animatedwordchar from "../motion/animatewordchar";
import { motion } from "framer-motion";
import { InView } from "react-intersection-observer";
import { navigator, useNavigate } from "react-router-dom";
import charteringlogo from "../Assets/chartering/Chatring-logo.png";

import {
  ScrollAnimation,
  animationVariants,
} from "../../Componants/motion/scrollanimation";
// import charteringSection_img from "../../Componants/Assets/image/charteringSection_img.png";
import Partners from "../../Landing_Page_Components/Partners";
import { Images } from "../../assests";
let charteringSection_img = Images + "charteringSection_img_1.png";
let automateImg = Images + "automate-img.svg";
let controlImg = Images + "control-img.svg";
let graphImg = Images + "graph-img.svg";
let oceannVM_Img13 = Images + "oceann-vm-img13.png";
let oceannVM_Img12 = Images + "oceann-vm-img12.png";
let captureImg = Images + "capture-img.svg";
let oceannVM_Img11 = Images + "analyticSection_img.png";
let oceannVM_Img9 = Images + "oceann-vm-img9.png";
let alertImg = Images + "alert-img.svg";
let saveImg = Images + "save-img.svg";
let workflowImg = Images + "workflow-img.svg";
let oceannVM_Img8 = Images + "oceann-vm-img8.png";
const forwardImg = Images + "forward-img.svg";
const compareImg = Images + "compare-img.svg";
const computerImg = Images + "computer-img.svg";
const accessImg = Images + "access-img.svg";
const analyticslogo = Images + "oceann-analytics-img2.png";
const financelogo = Images + "finance-icon.png";
const operationlogo = Images + "oceann-operations-img2.png";
const operationSection = Images + "operationSection_img.png";
const oceannVM_Img4 = Images + "oceann-vm-img4.jpg";
const oceannVM_Img3 = Images + "oceann-vm-img3.jpg";
const calenderImg = Images + "calender-img.svg";
const calculatorImg = Images + "calculator-img.svg";
const contactManagerImg = Images + "contact-manager-img.svg";
const analysisImg = Images + "analysis-img.svg";
const trendImg = Images + "trend-img.svg";
const oceannVM_Img1 = Images + "oceannVM_img1.png";

export default function Oceann_VM() {
  const navigator = useNavigate();
  const benefit1 = [
    {
      img: trendImg,
      heading: "Faster Decision Making",
    },

    {
      img: analysisImg,
      heading: "Dynamic Analytical Dashboard",
    },

    {
      img: contactManagerImg,
      heading: "Contract Management",
    },

    {
      img: calculatorImg,
      heading: "Claim /Laytime Calculator",
    },

    {
      img: calenderImg,
      heading: "Bunker Planning",
    },
  ];

  const benefit2 = [
    {
      img: accessImg,
      heading: "Access live P & L report and variances.",
    },

    {
      img: compareImg,
      heading: "Compare live operational data with trader estimates.",
    },

    {
      img: analysisImg,
      heading: "Dynamic personalised dashboard",
    },

    {
      img: computerImg,
      heading: "Directly manage, monitor, and optimize voyage performance.",
    },

    {
      img: accessImg,
      heading: "Access & download various reports for voyage performance.",
    },
    {
      img: forwardImg,
      heading: " Vessel speed dynamic features automate Noon reporting.",
    },
  ];

  const benefit3 = [
    {
      img: workflowImg,
      heading: "Entire financial workflow captured ",
    },

    {
      img: saveImg,
      heading: "Save time, save money",
    },

    {
      img: accessImg,
      heading: "Dynamic P&L accounting view",
    },

    {
      img: analysisImg,
      heading: "Analytical accounting dashboard",
    },

    {
      img: accessImg,
      heading: "Payable/ receivable invoices scheduling",
    },
    {
      img: alertImg,
      heading: "Alert and task for accounting workflow.",
    },
  ];

  const benefit4 = [
    {
      img: captureImg,

      heading:
        "Capture the maximum value from all your data with our platform ",
    },

    {
      img: accessImg,
      alt: "accessImg",
      heading: "Dynamic P&L accounting view",
    },

    {
      img: saveImg,

      alt: "saveImg",
      heading: "Save time, which saves cost with better tools",
    },

    {
      img: compareImg,

      alt: "compareImg",
      heading: "Upload and process the raw data into intelligent input.",
    },

    {
      img: analysisImg,

      alt: "analysisImg",
      heading: "Dynamic dashboard",
    },
    {
      img: accessImg,

      alt: "accessImg",
      heading: "Pre-loaded report and customized report.",
    },
  ];

  const benefit5 = [
    {
      img: trendImg,

      heading: "Improve fleet performance",
    },

    {
      img: saveImg,
      alt: "accessImg",
      heading: "Optimize voyage planning",
    },

    {
      img: automateImg,

      alt: "saveImg",
      heading: "Automate Noon verification",
    },

    {
      img: accessImg,

      alt: "compareImg",
      heading: "Voyage performance report",
    },

    {
      img: graphImg,

      alt: "analysisImg",
      heading: "Compare various vessel reports and position",
    },
    {
      img: controlImg,

      alt: "accessImg",
      heading: "Control the CO2 emission",
    },
  ];

  const cardData = [
    {
      title: "Fleet on the Map",
      description:
        "It caters to all the live vessels on the map with intelligent data of the vessel, allowing users to have a quick look at all their vessels.",
      path: "/solution/oceann-ai",
    },
    {
      title: "Voyage History and Analytics",
      description:
        "A complete analytics dashboard for comparing all aspects of data, i.e., speed and consumption, ETA, pitch, RPM, weather data, and generator efficiency.",
      path: "/solution/oceann-vm",
    },
    {
      title: "Optimize Voyage Planning",
      description:
        "Our platform enables users to have recommended speed and consumption for particular fixtures and voyages, offering the best profit.",
      path: "/product/oceann-zero",
    },
  ];

  return (
    <>
      <main>
        <div
          className="h-[40vh] lg:h-[100vh] flex items-center bg-[#00000083] bg-blend-darken"
          style={{
            backgroundImage: `url(${oceannVM_Img1})`,
            backgroundSize: "cover",
          }}
        >
          <div className="oceann-vm-section1 flex flex-col gap-[8px]">
            <div className="text-center text-6xl">
              <Animatedwordchar text="Oceann Voyage Manager" />
            </div>
            <div className="font-medium ">
              <Animatedword text="Integrated Freight Management Platform" />
            </div>
          </div>
        </div>
      </main>

      <section className="flex flex-col gap-[1rem] lg:gap-[1rem]">
        {/* Partner section */}
        <ScrollAnimation>
          <div className="my-[1rem] mx-[1rem] max-sm:mx-[1rem]">
            <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-10 ">
              <span className="lg:text-2xl">Trusted collaborators</span>
            </h1>
            <Partners />
          </div>
        </ScrollAnimation>
        {/* Partner section end */}

        <section>
          <ScrollAnimation>
            <section className="flex flex-col my-[1rem] items-center mx-[2rem] lg:mx-[6rem] xl:mx-[12rem] xl:gap-[4rem]">
              <p className="mx-[2rem] lg:mx-[6rem] text-center text-[18px] my-[2rem]">
                In the dynamic realm of global logistics and shipping,
                harnessing efficiency and optimization is paramount. Enter
                OceannVM: the cutting-edge Integrated Freight Management
                Platform, meticulously crafted to seamlessly elevate and refine
                your freight operations, irrespective of your global sailing
                destination.
              </p>
              <div className="flex justify-start items-center gap-2 mt-[2rem]">
                <img src={charteringlogo} alt="" className="w-[3rem]" />
                <h3 className="text-[32px] xl:text-[3rem] leading-9 max-sm:text-center max-sm:mt-2 text-left sm:text-lg md:text-lg font-semibold gap-4 ">
                  Chartering
                </h3>
              </div>
              <div className="flex flex-wrap max-sm:flex-col justify-between max-sm:items-start max-md:mt-10 gap-[2rem] lg:gap-[3rem]">
                <div className="flex flex-1 object-cover">
                  <img
                    src={charteringSection_img}
                    className="object-cover w-[100%] rounded-lg "
                    alt=""
                  />
                </div>
                <div className="flex flex-col flex-1 max-sm:w-full max-sm:ml-0">
                  <h3 className="text-md xl:text-[2.2rem] leading-[2.75rem] max-sm:text-center max-sm:mt-2 text-left sm:text-lg md:text-lg font-semibold gap-4 text-[#003E78]">
                    Maritime Masters: Navigating Your Chartering Dreams
                  </h3>
                  <p className="mt-3 lg:text-[16px] max-sm:text-center text-black max-sm:text-sm">
                    Voyage Chartering Solutions provide a comprehensive
                    chartering workflow encompassing voyage estimation,
                    optimizing voyage planning, vessel scheduling, cargo
                    planning, and freight contract management processes.
                  </p>
                  <div>
                    <motion.button
                      style={{}}
                      onMouseOver={(e) => (e.target.style.color = "#ffffff")}
                      onClick={() => navigator("/product/chartering")}
                      whileHover={{ scale: 1.04, backgroundColor: "#003e78" }}
                      className=" bg-[#f39c12] text-white px-[24px] mt-4 py-[8px] rounded-[42px]"
                    >
                      Learn More
                    </motion.button>
                  </div>
                </div>
              </div>
            </section>
          </ScrollAnimation>
          {/* ------------------ section3 --------------- */}
          <section>
            <ScrollAnimation>
              <div className="my-[3.5rem] bg-[#ecf0ff]  py-[2rem]">
                <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
                  <span className="text-[24px] lg:text-[32px]">Benefits</span>
                </h1>
                <div className="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 mx-[24px] my-[1rem] xl:mx-[6.4rem] xl:gap-8  ">
                  {benefit1.map((data) => (
                    <div
                      style={{
                        borderColor: "#e0e0e0",
                        borderWidth: "1px",
                      }}
                      className="border-[2px] relative flex flex-col justify-items-center justify-center py-6 px-6 rounded-sm hover:shadow-xl overflow-hidden section-2-cart "
                    >
                      <div className="p-[4px]">
                        <img
                          src={data.img}
                          alt=""
                          className="h-[48px] bg-[#ffffea] border-[1px] border-[#F39C12] rounded-xl p-[8px]"
                        />
                      </div>
                      <div className="flex flex-col mt-2">
                        <h1 className="text-md sm:text-[14px] md:text-[20px] font-semibold my-2">
                          {data.heading}
                        </h1>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </ScrollAnimation>
          </section>
          <ScrollAnimation>
            <section className="mx-[16px] my-[2rem] lg:mx-[8rem]">
              <h1 className="text-md lg:mb-[40px] text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
                <span className="text-[24px] lg:text-[32px]">Features</span>
              </h1>
              <div className="flex flex-col md:flex-row justify-between lg:mx-[6rem] lg:my-[16px] gap-[24px] md:gap-[3rem] mx-[18px] ">
                <div
                  style={{
                    border: "1px solid",
                    borderColor: "#e0e0e0",
                  }}
                  className="flex flex-1 flex-col rounded-xl px-[24px] py-[24px] section-3-cart"
                >
                  <div className="my-[8px] rounded-xl border-[0.5px] border-[#e0e0e0]">
                    <img src={oceannVM_Img3} alt="" className="rounded-xl" />
                  </div>
                  <h1 className="text-[20px] font-bold mt-[16px]">
                    Analytical Trade Dashboard
                  </h1>
                  <p className="text-[14px]">
                    Gain a comprehensive outlook on market trade for each trader
                    through our Analytical Trade Dashboard. A quick view of
                    fixtures, profit and loss (P&L), planning, and reports will
                    efficiently save time for traders.
                  </p>
                </div>
                <div
                  style={{
                    border: "1px solid",
                    borderColor: "#e0e0e0",
                  }}
                  className="flex flex-1 flex-col rounded-xl px-[24px] py-[24px] section-3-cart"
                >
                  <div className="my-[8px] rounded-xl border-[0.5px] border-[#e0e0e0]">
                    <img src={oceannVM_Img4} alt="" className="rounded-xl" />
                  </div>
                  <h1 className="text-[20px] font-bold mt-[16px]">
                    Voyage Estimation and Optimisation
                  </h1>
                  <p className="text-[14px]">
                    Attain a complete overview of market trade for each trader
                    with our Voyage Estimation and Voyage Optimization features.
                    A swift glance at fixtures, P&L, planning, and reports
                    ensures significant time savings.
                  </p>
                </div>
              </div>
            </section>
          </ScrollAnimation>
        </section>

        <section>
          <ScrollAnimation>
            <section className="flex flex-col my-[3rem]  items-center mx-[2rem] lg:mx-[6rem] xl:mx-[12rem] xl:gap-[4rem]">
              <div className="flex justify-start items-center gap-2 mt-[2rem]">
                <img src={operationlogo} alt="" className="w-[3rem]" />
                <h3 className="text-[32px] xl:text-[3rem] leading-9 max-sm:text-center max-sm:mt-2 text-left sm:text-lg md:text-lg font-semibold gap-4 ">
                  Operation
                </h3>
              </div>
              <div className="flex flex-wrap max-sm:flex-col justify-between max-sm:items-start max-md:mt-10 gap-[2rem] lg:gap-[3rem]">
                <div className="flex flex-col flex-1 max-sm:w-full max-sm:ml-0">
                  <h3 className="text-md xl:text-[2.2rem] leading-[2.75rem] max-sm:text-center max-sm:mt-2 text-left sm:text-lg md:text-lg font-semibold gap-4 text-[#003E78]">
                    OceanNavigo: Seamlessly Managing Maritime Ventures
                  </h3>
                  <p className="mt-3 lg:text-[16px] max-sm:text-center text-black max-sm:text-sm">
                    Experience seamless management of maritime ventures with
                    OceanNavigo. Specifically designed to support all types of
                    voyage calculations associated with Voyage Estimation.
                  </p>
                  <div>
                    <motion.button
                      style={{}}
                      onMouseOver={(e) => (e.target.style.color = "#ffffff")}
                      onClick={() => navigator("/product/operations")}
                      whileHover={{ scale: 1.04, backgroundColor: "#003e78" }}
                      className=" bg-[#f39c12] text-white px-[24px] mt-4 py-[8px] rounded-[42px]"
                    >
                      Learn More
                    </motion.button>
                  </div>
                </div>

                <div className="flex flex-1 object-cover">
                  <img
                    src={operationSection}
                    className="object-cover rounded-lg "
                    alt=""
                  />
                </div>
              </div>
            </section>
          </ScrollAnimation>
          {/* ------------------ section3 --------------- */}
          <section>
            <ScrollAnimation>
              <div className="my-[3.5rem] lg:my-[2rem] bg-[#ecf0ff] py-[2rem]">
                <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
                  <span className="text-[24px] lg:text-[32px]">Benefits</span>
                </h1>
                <div className="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 mx-[24px] my-[1rem] lg:my-[2rem] xl:mx-[6.4rem] xl:gap-8 ">
                  {benefit2.map((data) => (
                    <div
                      style={{
                        borderColor: "#e0e0e0",
                        borderWidth: "1px",
                      }}
                      className="border-[2px] relative flex flex-col justify-items-center justify-center py-6 px-6 rounded-sm hover:shadow-xl overflow-hidden section-2-cart "
                    >
                      <div className="p-[4px]">
                        <img
                          src={data.img}
                          alt=""
                          className="h-[48px] bg-[#ffffea] border-[1px] border-[#F39C12] rounded-xl p-[8px]"
                        />
                      </div>
                      <div className="flex flex-col mt-2">
                        <h1 className="text-md sm:text-[14px] md:text-[20px] font-semibold my-2">
                          {data.heading}
                        </h1>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </ScrollAnimation>
          </section>
          <ScrollAnimation>
            <section className="mx-[16px] my-[2rem] lg:my-[1rem] lg:mx-[8rem]">
              <h1 className="text-md lg:mb-[40px] text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
                <span className="text-[24px] lg:text-[32px]">Features</span>
              </h1>
              <div className="flex flex-col md:flex-row justify-between lg:mx-[6rem] lg:my-[16px] gap-[24px] md:gap-[3rem] mx-[18px] ">
                <div
                  style={{
                    border: "1px solid",
                    borderColor: "#e0e0e0",
                  }}
                  className="flex flex-1 flex-col rounded-xl px-[24px] py-[24px] section-3-cart"
                >
                  <div className="my-[8px] rounded-xl border-[0.5px] border-[#e0e0e0]">
                    <img src={oceannVM_Img3} alt="" className="rounded-xl" />
                  </div>
                  <h1 className="text-[20px] font-bold mt-[16px]">
                    P&L variance and reporting
                  </h1>
                  <p className="text-[14px]">
                    theoceann.ai captures each element in the operation
                    workflow which enables users to compare P &L data with
                    estimates on a minute to minute basis.
                  </p>
                </div>
                <div
                  style={{
                    border: "1px solid",
                    borderColor: "#e0e0e0",
                  }}
                  className="flex flex-1 flex-col rounded-xl px-[24px] py-[24px] section-3-cart"
                >
                  <div className="my-[8px] rounded-xl border-[0.5px] border-[#e0e0e0]">
                    <img src={oceannVM_Img4} alt="" className="rounded-xl" />
                  </div>
                  <h1 className="text-[20px] font-bold mt-[16px]">
                    Voyage management
                  </h1>
                  <p className="text-[14px]">
                    {" "}
                    Introducing Oceann VM - We provide every feature a user
                    needs to optimize voyage management and operational
                    workflow.
                  </p>
                </div>
              </div>
            </section>
          </ScrollAnimation>
        </section>

        <section>
          <ScrollAnimation>
            <section className="flex flex-col my-[3rem] items-center mx-[2rem] lg:mx-[6rem] xl:mx-[12rem] xl:gap-[4rem]">
              <div className="flex justify-start items-center gap-2 mt-[2rem]">
                <img src={financelogo} alt="" className="w-[3rem]" />
                <h3 className="text-[32px] xl:text-[3rem] leading-9 max-sm:text-center max-sm:mt-2 text-left sm:text-lg md:text-lg font-semibold gap-4 ">
                  Finance
                </h3>
              </div>
              <div className="flex flex-wrap max-sm:flex-col justify-between max-sm:items-start max-md:mt-10 gap-[2rem] lg:gap-[3rem]">
                <div className="flex flex-1 object-cover">
                  <img
                    src={oceannVM_Img8}
                    className="object-cover rounded-lg "
                    alt=""
                  />
                </div>
                <div className="flex flex-col flex-1 max-sm:w-full max-sm:ml-0">
                  <h3 className="text-md xl:text-[2.2rem] leading-[2.75rem] max-sm:text-center max-sm:mt-2 text-left sm:text-lg md:text-lg font-semibold gap-4 text-[#003E78]">
                    Oceann Funds: Navigating Your Ship Financing Journey
                  </h3>
                  <p className="mt-3 lg:text-[16px] max-sm:text-center text-black max-sm:text-sm">
                    In this ever-evolving maritime landscape, securing
                    appropriate funding for your ship cargo ventures is
                    paramount. Let us guide you through the various aspects of
                    ship cargo financing and illuminate the path to financial
                    stability and operational excellence.
                  </p>
                  <div>
                    <motion.button
                      style={{}}
                      onMouseOver={(e) => (e.target.style.color = "#ffffff")}
                      onClick={() => navigator("/product/oceann-finance")}
                      whileHover={{ scale: 1.04, backgroundColor: "#003e78" }}
                      className=" bg-[#f39c12] text-white px-[24px] mt-4 py-[8px] rounded-[42px]"
                    >
                      Learn More
                    </motion.button>
                  </div>
                </div>
              </div>
            </section>
          </ScrollAnimation>
          {/* ------------------ section3 --------------- */}
          <section>
            <ScrollAnimation>
              <div className="my-[1rem] bg-[#ecf0ff] py-[2rem]">
                <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
                  <span className="text-[24px] lg:text-[32px]">Benefits</span>
                </h1>
                <div className="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 mx-[24px] my-[1rem] xl:mx-[6.4rem] xl:gap-8 ">
                  {benefit3.map((data) => (
                    <div
                      style={{
                        borderColor: "#e0e0e0",
                        borderWidth: "1px",
                      }}
                      className="border-[2px] relative flex flex-col justify-items-center justify-center py-6 px-6 rounded-sm hover:shadow-xl overflow-hidden section-2-cart "
                    >
                      <div className="p-[4px]">
                        <img
                          src={data.img}
                          alt=""
                          className="h-[48px] bg-[#ffffea] border-[1px] border-[#F39C12] rounded-xl p-[8px]"
                        />
                      </div>
                      <div className="flex flex-col mt-2">
                        <h1 className="text-md sm:text-[14px] md:text-[20px] font-semibold my-2">
                          {data.heading}
                        </h1>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </ScrollAnimation>
          </section>
          <ScrollAnimation>
            <section className="mx-[16px] my-[2rem]  lg:mx-[8rem]">
              <h1 className="text-md lg:mb-[40px] text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
                <span className="text-[24px] lg:text-[32px]">Features</span>
              </h1>
              <div className="flex flex-col md:flex-row justify-between lg:mx-[6rem] lg:my-[16px] gap-[24px] md:gap-[3rem] mx-[18px] ">
                <div
                  style={{
                    border: "1px solid",
                    borderColor: "#e0e0e0",
                  }}
                  className="flex flex-1 flex-col rounded-xl px-[24px] py-[24px] section-3-cart"
                >
                  <div className="my-[8px] rounded-xl border-[0.5px] border-[#e0e0e0]">
                    <img src={oceannVM_Img9} alt="" className="rounded-xl" />
                  </div>
                  <h1 className="text-[20px] font-bold mt-[16px]">
                    Dynamic P & L for each Voyage
                  </h1>
                  <p className="text-[14px]">
                    Voyage accounting has an important role to play in the
                    maritime accounting process. Each cost & revenue element is
                    captured under various contracts for proper entry into
                    voyage accounting for accurate P&L.
                  </p>
                </div>
                <div
                  style={{
                    border: "1px solid",
                    borderColor: "#e0e0e0",
                  }}
                  className="flex flex-1 flex-col rounded-xl px-[24px] py-[24px] section-3-cart"
                >
                  <div className="my-[8px] rounded-xl border-[0.5px] border-[#e0e0e0]">
                    <img src={oceannVM_Img4} alt="" className="rounded-xl" />
                  </div>
                  <h1 className="text-[20px] font-bold mt-[16px]">
                    Time charter payment
                  </h1>
                  <p className="text-[14px]">
                    {" "}
                    Our platform caters to the entire workflow for financial,
                    invoicing, payment, and record-keeping for various time
                    charter, i.e., TCI, TCO, time charter voyage out, owned
                    vessel, TC relet.
                  </p>
                </div>
              </div>
            </section>
          </ScrollAnimation>
        </section>

        <section>
          <ScrollAnimation>
            <section className="flex flex-col my-[3rem] items-center mx-[2rem] lg:mx-[6rem] xl:mx-[12rem] xl:gap-[4rem]">
              <div className="flex justify-start items-center gap-2 mt-[2rem]">
                <img src={analyticslogo} alt="" className="w-[3rem]" />
                <h3 className="text-[32px] xl:text-[3rem] leading-9 max-sm:text-center max-sm:mt-2 text-left sm:text-lg md:text-lg font-semibold gap-4 ">
                  Analytics
                </h3>
              </div>
              <div className="flex flex-wrap max-sm:flex-col justify-between max-sm:items-start max-md:mt-10 gap-[2rem] lg:gap-[3rem]">
                <div className="flex flex-col flex-1 max-sm:w-full max-sm:ml-0">
                  <h3 className="text-md xl:text-[2.2rem] leading-[2.75rem] max-sm:text-center max-sm:mt-2 text-left sm:text-lg md:text-lg font-semibold gap-4 text-[#003E78]">
                    Streamlined Insights for Strategic Navigation
                  </h3>
                  <p className="mt-3 lg:text-[16px] max-sm:text-center text-black max-sm:text-sm">
                    In the ever-evolving world of shipping and logistics,
                    informed decision-making is the wind in your sails. Welcome
                    to our Report & Analytics section, a dedicated space
                    designed to equip shipping companies with the insights and
                    tools they need to sail confidently through the complex
                    waters of the maritime industry.
                  </p>
                  <div>
                    <motion.button
                      style={{}}
                      onMouseOver={(e) => (e.target.style.color = "#ffffff")}
                      onClick={() => navigator("/product/oceann-analytics")}
                      whileHover={{ scale: 1.04, backgroundColor: "#003e78" }}
                      className=" bg-[#f39c12] text-white px-[24px] mt-4 py-[8px] rounded-[42px]"
                    >
                      Learn More
                    </motion.button>
                  </div>
                </div>

                <div className="flex flex-1 object-cover">
                  <img
                    src={oceannVM_Img11}
                    className="object-cover rounded-lg "
                    alt=""
                  />
                </div>
              </div>
            </section>
          </ScrollAnimation>
          {/* ------------------ section3 --------------- */}
          <section>
            <ScrollAnimation>
              <div className="my-[3.5rem] bg-[#ecf0ff] py-[2rem]">
                <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
                  <span className="text-[24px] lg:text-[32px]">Benefits</span>
                </h1>
                <div className="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 mx-[24px] my-[1rem] xl:mx-[6.4rem] xl:gap-8 ">
                  {benefit4.map((data) => (
                    <div
                      style={{
                        borderColor: "#e0e0e0",
                        borderWidth: "1px",
                      }}
                      className="border-[2px] relative flex flex-col justify-items-center justify-center py-6 px-6 rounded-sm hover:shadow-xl overflow-hidden section-2-cart "
                    >
                      <div className="p-[4px]">
                        <img
                          src={data.img}
                          alt=""
                          className="h-[48px] bg-[#ffffea] border-[1px] border-[#F39C12] rounded-xl p-[8px]"
                        />
                      </div>
                      <div className="flex flex-col mt-2">
                        <h1 className="text-md sm:text-[14px] md:text-[20px] font-semibold my-2">
                          {data.heading}
                        </h1>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </ScrollAnimation>
          </section>
          <ScrollAnimation>
            <section className="mx-[16px] my-[2rem] lg:mx-[8rem]">
              <h1 className="text-md lg:mb-[40px] text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
                <span className="text-[24px] lg:text-[32px]">Features</span>
              </h1>
              <div className="flex flex-col md:flex-row justify-between lg:mx-[6rem] lg:my-[16px] gap-[24px] md:gap-[3rem] mx-[18px] ">
                <div
                  style={{
                    border: "1px solid",
                    borderColor: "#e0e0e0",
                  }}
                  className="flex flex-1 flex-col rounded-xl px-[24px] py-[24px] section-3-cart"
                >
                  <div className="my-[8px] rounded-xl border-[0.5px] border-[#e0e0e0]">
                    <img src={oceannVM_Img12} alt="" className="rounded-xl" />
                  </div>
                  <h1 className="text-[20px] font-bold mt-[16px]">
                    Market Business Intelligence Tools
                  </h1>
                  <p className="text-[14px]">
                    Pre-loaded with platform data of voyage and vessel, it
                    provides complete visuals for chartering, operation, and
                    accounting users.
                  </p>
                </div>
                <div
                  style={{
                    border: "1px solid",
                    borderColor: "#e0e0e0",
                  }}
                  className="flex flex-1 flex-col rounded-xl px-[24px] py-[24px] section-3-cart"
                >
                  <div className="my-[8px] rounded-xl border-[0.5px] border-[#e0e0e0]">
                    <img src={oceannVM_Img13} alt="" className="rounded-xl" />
                  </div>
                  <h1 className="text-[20px] font-bold mt-[16px]">
                    Report Generation
                  </h1>
                  <p className="text-[14px]">
                    {" "}
                    theoceann.ai platform enables users to download
                    consolidated reports by selecting various parameters of
                    fixture, cargo, vessel, operation, and accounting.
                  </p>
                </div>
              </div>
            </section>
          </ScrollAnimation>
        </section>

        <section className="flex flex-col mt-[3rem] lg:mt-[6rem] items-center mx-[2rem] ">
          <p className="mx-[2rem] lg:mx-[6rem] text-center text-[20px] my-[16px] font-bold">
            Carbon Free Ocean
          </p>
          <h3 className="text-[32px] xl:text-[2rem] leading-9 max-sm:text-center text-left sm:text-lg md:text-lg font-semibold mb-4">
            VSPM- Vessel Speed Predictive matrix
          </h3>
          <p className="mx-[2rem] lg:mx-[8rem] text-center text-[18px] ">
            This platform helps our partner to visualise insight into their
            vessel economics by live tracking vessel, control of CO2 emission,
            standardise reporting for noon position and vessel performance
          </p>
        </section>

        <section>
          <ScrollAnimation>
            <div className="my-[3.5rem] bg-[#ecf0ff] py-[2rem]">
              <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
                <span className="text-[24px] lg:text-xl">Benefits</span>
              </h1>
              <div className="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 mx-[24px] my-[1rem] xl:mx-[6.4rem] xl:gap-8 ">
                {benefit5.map((data) => (
                  <div
                    style={{
                      borderColor: "#e0e0e0",
                      borderWidth: "1px",
                    }}
                    className="border-[2px] relative flex flex-col justify-items-center justify-center py-6 px-6 rounded-sm hover:shadow-xl overflow-hidden section-2-cart "
                  >
                    <div className="p-[4px]">
                      <img
                        src={data.img}
                        alt=""
                        className="h-[48px] bg-[#ffffea] border-[1px] border-[#F39C12] rounded-xl p-[8px]"
                      />
                    </div>
                    <div className="flex flex-col mt-2">
                      <h1 className="text-md sm:text-[14px] md:text-[20px] font-semibold my-2">
                        {data.heading}
                      </h1>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </ScrollAnimation>
        </section>

        <ScrollAnimation>
          <section>
            <h1 className=" text-center text-md lg:text-xl md:text-md  font-bold m-8">
              Features
            </h1>

            <div class="mx-[2rem] lg:mx-[6rem] grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3 ">
              {/* cards item 1 */}

              {cardData.map(({ title, description, path, index }) => (
                <div
                  class="card pl-[7%] pr-[7%] pt-[15%] pb-[15%] rounded-3xl relative overflow-hidden hover:scale-102 md:h-96 lg:h-80 group mx-[8%] border-[1px] solid "
                  key={index}
                >
                  <div className=" absolute w-24 h-24 bg-[#F39C12] rounded-full transition-transform duration-300 transform -top-10 -right-10 group-hover:scale-125 "></div>
                  <div className="flex flex-col">
                    <h1 class="text-md mt-[10%] lg:text-[32px] md:text-md xl:text-[24px] font-bold leading-tight mb-[8px]">
                      {title}
                    </h1>
                    <p class="text-md max-md:text-sm">{description}</p>
                  </div>
                </div>
              ))}
            </div>
          </section>
        </ScrollAnimation>

        <ScrollAnimation>
          <section className="mx-[16px] bg-[#ecf0ff] py-[2rem] my-[2rem]">
            <h1 className="text-md lg:mb-[40px] text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
              <span className="text-[24px] lg:text-[32px]">
                Special Features
              </span>
            </h1>
            <div className="flex flex-col mx-[2rem] lg:mx-[8rem]">
              <div className="flex flex-col md:flex-row justify-between lg:mx-[6rem] lg:my-[16px] gap-[24px] md:gap-[3rem] mx-[18px] ">
                <div
                  style={{
                    border: "1px solid",
                    borderColor: "#e0e0e0",
                  }}
                  className="flex flex-1 flex-col rounded-xl px-[24px] py-[24px] section-3-cart"
                >
                  <div className="my-[8px] rounded-xl border-[0.5px] border-[#e0e0e0]">
                    <img src={oceannVM_Img3} alt="" className="rounded-xl" />
                  </div>
                  <h1 className="text-[20px] font-bold mt-[16px]">
                    Analytical Trade Dashboard
                  </h1>
                  <p className="text-[14px]">
                    This will give an complete outlook of market trade  for each
                    trader. An quick view of their fixtures, P&L, Planning and
                    report will save plenty of time for traders
                  </p>
                </div>
                <div
                  style={{
                    border: "1px solid",
                    borderColor: "#e0e0e0",
                  }}
                  className="flex flex-1 flex-col rounded-xl px-[24px] py-[24px] section-3-cart"
                >
                  <div className="my-[8px] rounded-xl border-[0.5px] border-[#e0e0e0]">
                    <img src={oceannVM_Img4} alt="" className="rounded-xl" />
                  </div>
                  <h1 className="text-[20px] font-bold mt-[16px]">
                    Voyage Scheduling
                  </h1>
                  <p className="text-[14px]">
                    This combines all your vessel opening and closing position
                    so that user can plan their cargo trade, tonnage fixing to
                    maximize the profit.
                  </p>
                </div>
              </div>
              <div className="flex flex-col md:flex-row justify-between lg:mx-[6rem] lg:my-[16px] gap-[24px] md:gap-[3rem] mx-[18px] ">
                <div
                  style={{
                    border: "1px solid",
                    borderColor: "#e0e0e0",
                  }}
                  className="flex flex-1 flex-col rounded-xl px-[24px] py-[24px] section-3-cart"
                >
                  <div className="my-[8px] rounded-xl border-[0.5px] border-[#e0e0e0]">
                    <img src={oceannVM_Img3} alt="" className="rounded-xl" />
                  </div>
                  <h1 className="text-[20px] font-bold mt-[16px]">
                    Freight Contract Management
                  </h1>
                  <p className="text-[14px]">
                    This captures each data element, term, and condition of
                    various kinds of cargo and vessel fixtures, i.e., Time
                    Chartered In, Time Charter Out, Time Charter Voyage Out,
                    Cargo COA, Single Contract.
                  </p>
                </div>
                <div
                  style={{
                    border: "1px solid",
                    borderColor: "#e0e0e0",
                  }}
                  className="flex flex-1 flex-col rounded-xl px-[24px] py-[24px] section-3-cart"
                >
                  <div className="my-[8px] rounded-xl border-[0.5px] border-[#e0e0e0]">
                    <img src={oceannVM_Img4} alt="" className="rounded-xl" />
                  </div>
                  <h1 className="text-[20px] font-bold mt-[16px]">
                    P&L Finance and Reporting
                  </h1>
                  <p className="text-[14px]">
                    theoceann.ai captures each element in the operation
                    workflow, enabling users to generate invoices and compare P
                    & L data with estimates on a minute-to-minute basis.
                  </p>
                </div>
              </div>
            </div>
          </section>
        </ScrollAnimation>
      </section>
    </>
  );
}
