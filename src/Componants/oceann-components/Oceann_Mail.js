import React from "react";
import "./oceann-mail.css";
import {
  ScrollAnimation,
  animationVariants,
} from "../motion/scrollanimation.js";
import Animatedwordchar from "../motion/animatewordchar.js";
// import Animatedword from "../../Componants/motion/animateword";
import { motion } from "framer-motion";
// import video from "../Assets/Ocean-img/email-video.mp4";
import videoImg from "../Assets/Ocean-img/video_img.png";
// import mailDashboard from "../Assets/mailSection_img/mail_dashboard.png";
import { useNavigate } from "react-router-dom";
// import increase from "../Assets/Ocean-img/fasterProcess.png";
// import efficiency from "../Assets/Ocean-img/voyageEffeciency.png";
// import maximize from "../Assets/Ocean-img/maximizerVoyagers.png";
// import emailSorting from "../Assets/Ocean-img/emailSorting.png";
// import automation from "../Assets/Ocean-img/automation.png";
// import system from "../Assets/Ocean-img/system.png";
// import mailChat from "../Assets/mailSection_img/mail_chat.png";
// import mailInbox from "../Assets/mailSection_img/mail_Inbox.png";
// import tagline from "../Assets/Ocean-img/tagline-img.png"
// import mailBgimg from "../Assets/Ocean-img/oceann-mail-bgimg.png";
import OmPageSection from "./OmPageSection.js";
import "../../../src/index.css";
import Partners from "../../Landing_Page_Components/Partners.js";
import { Images } from "../../assests.js";
// import oceanMailImg1 from "../Assets/Ocean-img/oceanMailImg1.png";
// import oceanMailImg3 from "../Assets/Ocean-img/oceannMailImg3.png";
// import oceanMailImg4 from "../Assets/Ocean-img/oceannMailImg4.png";
// import trendImg from "../Assets/Ocean-img/trend-img.svg";
// import rightImg from "../Assets/Ocean-img/right-img.svg";
// import personImg from "../Assets/Ocean-img/person-img.svg";
// import messageImg from "../Assets/Ocean-img/message-img.svg";
import mobilecommand from "../Assets/Ocean-img/your-mobile-command.png";
// import rectangleImg1 from "../Assets/Ocean-img/RectangleImg1.png";
// import rectangleImg2 from "../Assets/Ocean-img/RectangleImg2.svg";
// import oceannMailAnimationVideo from '../Assets/Oceann_Mail_Assests/video.mp4'
const mailBgimg = Images + "Ocean-img/oceann-mail-bgimg.png";
const increase = Images + "Ocean-img/fasterProcess.png";
const efficiency = Images + "Ocean-img/voyageEffeciency.png";
const maximize = Images + "Ocean-img/maximizerVoyagers.png";
const emailSorting = Images + "Ocean-img/emailSorting.png";
const automation = Images + "Ocean-img/automation.png";
const system = Images + "Ocean-img/system.png";
const mailChat = Images + "mailSection_img/mail_chat.png";
const mailInbox = Images + "mailSection_img/mail_Inbox.png";
// const videoImg = Images + "Ocean-img/video_img.png";
const mailDashboard = Images + "mailSection_img/mail_dashboard.png";
function OceanMail() {
  const navigator = useNavigate();
  const cards = [
    {
      img: mailChat,
      title: "Intelligent ship filtering solutions",
      content:
        "Unlock the potential of AI in the maritime world with our cutting-edge Email Solution. Seamlessly streamline and enhance your communication processes for unprecedented efficiency. Harness the power of artificial intelligence to elevate your shipping operations to new horizons. Explore the future of maritime communication with us today.",
      path: "/solution/oceann-mail",
    },

    {
      img: mailInbox,
      title: "Seamless Communication, Smoother Voyages",
      content:
        "Our AI-powered email solution streamlines communication, ensuring that shipping operations run more smoothly and efficiently. Stay ahead in the ever-evolving maritime landscape with our innovative solution.",
      path: "/solution/oceann-mail",
    },
  ];

  const benefits = [
    {
      img: increase,
      heading: "2x Faster Processing",
      content:
        "Dive into a world where email management is twice as fast, guaranteeing swift decision-making for your voyages",
    },

    {
      img: efficiency,
      heading: "Augmented Voyage Efficiency",
      content:
        "Enhance your voyage plans and strategies with our tailored insights.",
    },

    {
      img: maximize,
      heading: "Maximize Voyagers per User",
      content:
        "Expand your horizons and capitalize on more voyagers without compromising user experience or safety.",
    },

    {
      img: emailSorting,
      heading: "Automated Email Sorting",
      content:
        "Say goodbye to the clutter. Our solution categorizes and prioritizes emails for you, ensuring you see what's important first.",
    },

    {
      img: automation,
      heading: "Innovative Tonnage and Cargo Order Automation",
      content:
        "Our system reads and understands your entire email database, turning thousands of emails into actionable insights on tonnage and cargo orders.",
    },

    {
      img: system,
      heading: "AI Powered Mail system",
      content:
        "Stay ahead of industry demands with our advanced AI system. Oceann mail automates email sorting and prioritization, minimizing manual tasks and accelerating response time.",
    },
  ];

  const card = [
    {
      img: mailDashboard,
      title: "Effortless Email Parsing",
      content:
        "Ocean Mail's email parsing feature streamlines communication by automatically extracting and organizing vital shipping information from emails.With advanced parsing algorithms, Ocean Mail efficiently captures details like shipment tracking numbers, delivery dates, and shipping addresses. ",
      path: "/solution/oceann-mail",
    },

    {
      img: mailChat,
      title: "Trade Patterns",
      content:
        "Oceann Mail through dynamic communication trends ensures optimal email management and efficient message delivery solutions tailored to evolving user needs. Oceann Mail's adaptive capabilities are finely tuned to changing communication patterns, providing a responsive email solution.",
      path: "/solution/oceann-mail",
    },

    {
      img: mailInbox,
      title: "Operational Intelligence",
      content:
        "Operational intelligence is the cornerstone of Oceann Mail, providing a comprehensive understanding of seamless communication for smooth operation. Oceann Mail's operational intelligence empowers users with insightful data, enabling them to optimize mail workflows and ensure seamless communication.",
      path: "/solution/oceann-mail",
    },
  ];

  return (
    <>
      <div className="flex flex-col gap-[24px]">
        <main>
          <ScrollAnimation>
            {/* <div className="md:h-[60vh] items-center justify-center oceann-mail-section1 my-[32px] md:my-[7rem]">
            </div> */}
            <div className="relative lg:h-[100vh] h-[50vh] ">
              <img
                src={mailBgimg}
                alt="Dry Bulk"
                className="w-full h-full object-cover object-center"
              />
              <div className="absolute z-1 top-[5%] left-0 right-0 flex justify-center items-center mx-6 my-2 md:mx-24">
                <div className=" flex flex-col justify-center items-center gap-[32px]">
                  <h1 className="text-[16px] lg:text-[32px] text-center font-regular text-[#e6e6e6] mt-[5rem] md:mt-[10rem]">
                    Oceann Mail
                  </h1>
                  {/* <div className="text-[3.8rem] font-bold text-[black] text-center leading-[72px]">
                <Animatedwordchar text=" Effortless Communications, tailored to elevate your Trading & Operational requirements" />
                 </div> */}
                  <div className="text-[18px] md:text-[40px] font-semibold leading-tight text-[#ffffff] text-center lg:text-[5.5rem] lg:leading-[90px]">
                    {/* Elevate your <b>trading and operational efficiency</b> with Oceann's tailored email solution—a cutting-edge data collaboration hub powered by <b> innovative AI tools </b> for the maritime and trade industries. Stay ahead in the game with Oceann. */}
                    {/* <img src={tagline} alt="" className="fade-image" /> */}
                    {/* Utilize the potential of email to enhance maritime profitability. */}
                    <Animatedwordchar text="Utilize the Potential of Oceann Mail to Enhance Maritime Profitability" />
                  </div>
                  <motion.button
                    whileHover={{ scale: 1.04, backgroundColor: "#003e78" }}
                    onClick={() => navigator("/demo")}
                    className="bg-[#F39C12] text-white text-[1rem] max-sm:text-[0.6rem] py-2 px-8 max-sm:px-4 rounded-3xl shadow-md focus:outline-none md:my-[2rem]"
                  >
                    Book Demo
                  </motion.button>
                </div>
              </div>
            </div>
          </ScrollAnimation>
        </main>

        {/* Partner section */}
        <ScrollAnimation>
          <div className="my-[1rem] mx-[1rem] max-sm:mx-[1rem]">
            <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-10 ">
              <span className="lg:text-2xl">Trusted collaborators</span>
            </h1>
            <Partners />
          </div>
        </ScrollAnimation>
        {/* Partner section end */}

        <section className="mx-[16px] my-[1rem] lg:my-[0rem] lg:mx-[2rem]">
          <h1 className="text-md lg:mb-[40px] text-center sm:text-lg md:text-xl font-semibold mt-1">
            <span className="text-[24px] lg:text-xl">
              What Oceann Mail Offers?
            </span>
          </h1>
          <ScrollAnimation>
            <div className="flex flex-col md:flex-row justify-between lg:mx-[40px] lg:my-[16px] gap-[30px] mx-[18px] ">
              {card.map((data) => (
                <div
                  style={{
                    border: "1px solid",
                    borderColor: "#e0e0e0",
                  }}
                  className="flex flex-1 flex-col rounded-xl px-[24px] py-[24px] section-3-cart"
                >
                  <div className="my-[8px] rounded-xl border-[0.5px] border-[#e0e0e0]">
                    <img src={data.img} alt="" className="rounded-xl" />
                  </div>
                  <h1 className="text-[20px] font-bold mt-[16px]">
                    {data.title}
                  </h1>
                  <p className="text-[14px]">{data.content}</p>
                </div>
              ))}
            </div>
          </ScrollAnimation>
        </section>

        {/* <section className="my-[2rem]">
          <div className="flex justify-center items-center mx-[2.75rem] rounded-xl">
            <video
                loop
                autoPlay
                muted
                src={video}
                className="rounded-lg lg:rounded-[1.3rem]"
                type="video/mp4"
              ></video>
            <img
              src={videoImg}
              alt=""
              className="object-cover rounded-lg w-[100%]"
            />
          </div>
        </section> */}

        <section className="flex flex-col md:gap-6 my-[3rem] lg:my-[2rem] items-center mx-[2rem] lg:mx-[5rem] xl:mx-[5rem] xl:gap-[4rem]">
          <ScrollAnimation>
            <div className="flex flex-wrap max-sm:flex-col justify-between max-sm:items-start max-md:mt-10 gap-[2rem] lg:gap-[3rem]">
              <div className="flex flex-1 object-cover">
                <img
                  src={videoImg}
                  className="object-cover rounded-md w-[100%]"
                  alt="AI-Driven Email Management for Maritime Excellence"
                />
              </div>
              <div className="flex flex-col items-center md:items-start flex-1 max-sm:w-full max-sm:ml-0">
                <h3 className="text-md xl:text-[2.2rem] leading:tight xl:leading-[2.75rem] max-sm:text-center max-sm:mt-2 text-left sm:text-lg md:text-lg font-semibold gap-4 text-[#003E78]">
                  AI-Driven Email Management for Maritime Excellence
                </h3>
                <p className="mt-3 lg:text-[16px] max-sm:text-center text-black max-sm:text-sm">
                  Oceann Mail uses AI to revolutionize maritime email workflows.
                  It automates tagging, extracts vessel and cargo details,
                  writes emails, and prioritizes communication, ensuring faster
                  and more accurate responses. By matching cargo with vessels
                  and enhancing search filters, it simplifies complex
                  operations. Integrated with Oceann AI, it provides predictive
                  insights to drive smarter decisions. Oceann Mail streamlines
                  operations, improves efficiency, and delivers a seamless email
                  management experience tailored for the maritime industry.
                </p>
              </div>
            </div>
          </ScrollAnimation>
        </section>

        {/* <section className=" bg-slate-400">
          <video
            loop
            autoPlay
            muted
            className="h-[85vh]  object-cover"
          >
        
        <section className=" my-[2rem] mx-[4rem]">
          <div className=" lg:mx-[12rem] max-sm:mx-[1rem]">
            <ScrollAnimation>
              <div className="flex flex-col lg:flex-row justify-between lg:mx-[40px] lg:my-[16px] gap-[30px] ">
                {cards.map((data) => (
                  <div
                    style={{
                      border: "1px solid",
                      borderColor: "#e0e0e0",
                    }}
                    className="flex flex-1 flex-col rounded-xl px-[24px] py-[24px] section-3-cart"
                  >
                    <div className="my-[8px] rounded-xl">
                      <img
                        src={data.img}
                        alt=""
                        className="rounded-xl w-[480px]"
                      />
                    </div>
                    <h1 className="text-[20px] font-bold mt-[16px]">
                      {data.title}
                    </h1>
                    <p className="text-[14px]">{data.content}</p>
                  </div>
                ))}
              </div>
            </ScrollAnimation>
          </div>
        </section>
            <source src={oceannMailAnimationVideo} type="video/mp4"></source>
          </video>
        </section> */}
        {/* <section>
          <ScrollAnimation>
            <div className="flex items-center justify-center oceann-mail-section1 mx-[3rem]  ">
              <div className=" flex flex-col justify-center items-center gap-10 mx-[2rem]">
                <h1 className="text-[20px] lg:text-[2rem] text-center text-[#141414] font-bold lg:mt-[10rem] lg:mx-[12rem]">
                  ”Navigating the Future of Communication: Our AI-Powered Email
                  Solution for Shipping”
                </h1>

                <img src={mailChat} alt="" className="w-[100%] rounded-xl" />

                <div className="text-[14px] mx-[4rem] xl:mx-[10rem] font-medium leading-tight text-[#4e4e4e] text-center md:text-[20px]">
                  Unlock the potential of AI in the maritime world with our
                  cutting-edge Email Solution. Seamlessly streamline and enhance
                  your communication processes for unprecedented efficiency.
                  Harness the power of artificial intelligence to elevate your
                  shipping operations to new horizons. Explore the future of
                  maritime communication with us today."
                </div>
              </div>
            </div>
          </ScrollAnimation>
        </section> */}

        <section className="flex flex-col md:gap-6 my-[3rem] lg:my-[2rem] items-center mx-[2rem] lg:mx-[5rem] xl:mx-[5rem] xl:gap-[4rem]">
          <ScrollAnimation>
            <div className="flex justify-start items-center gap-1 md:mt-[0.2rem]">
              <h1 class="text-[18px] lg:text-[2rem] text-center text-[#141414] font-bold lg:mt-[1rem] lg:mb-[2rem] lg:mx-[4rem]">
                ”Navigating the Future of Communication: Our AI-Powered Email
                Solution for Shipping”
              </h1>
            </div>
            <div className="flex flex-wrap max-sm:flex-col justify-between max-sm:items-start max-md:mt-10 gap-[2rem] lg:gap-[3rem]">
              <div className="flex flex-1 object-cover">
                <img
                  src={mobilecommand}
                  className="object-cover rounded-md w-[100%]"
                  alt="Oceann X: Your Mobile Command Center for Smarter Maritime
                  Operations"
                />
              </div>
              <div className="flex flex-col items-center md:items-start flex-1 max-sm:w-full max-sm:ml-0">
                <h3 className="text-md xl:text-[2.2rem] leading:tight xl:leading-[2.75rem] max-sm:text-center max-sm:mt-2 text-left sm:text-lg md:text-lg font-semibold gap-4 text-[#003E78]">
                  Oceann X: Your Mobile Command Center for Smarter Maritime
                  Operations
                </h3>
                <p className="mt-3 lg:text-[16px] max-sm:text-center text-black max-sm:text-sm">
                  Oceann X is the ultimate all-in-one mobile solution, powered
                  by AI, to revolutionize maritime management. With seamless
                  AI-driven email integration from Oceann Mail, it automates
                  tagging, extracts vessel and cargo information, writes emails,
                  auto-replies, and prioritizes communication.
                </p>
                <p className="mt-3 lg:text-[16px] max-sm:text-center text-black max-sm:text-sm">
                  Its advanced features match cargo with vessels, enhance search
                  filters, and integrate predictive insights from Oceann AI for
                  smarter decision-making. Delivering real-time maritime
                  insights, Oceann X streamlines operations, boosts efficiency,
                  and simplifies workflows—all accessible on iOS and Android for
                  on-the-go convenience.
                </p>
              </div>
            </div>
          </ScrollAnimation>
        </section>

        <section>
          <OmPageSection />
        </section>

        <section>
          <ScrollAnimation>
            <div className="my-[3.5rem] lg:my-[4rem]">
              <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
                <span className="text-[24px] lg:text-xl">Why Oceann Mail?</span>
              </h1>
              <div className="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 mx-[24px] my-[1rem] lg:my-[3rem] xl:mx-[10rem] xl:gap-16 ">
                {benefits.map((data) => (
                  <div
                    style={{
                      borderColor: "#e0e0e0",
                      borderWidth: "1px",
                    }}
                    className="border-[2px] relative flex flex-col justify-items-center justify-center py-10 px-6 rounded-md hover:shadow-xl overflow-hidden section-2-cart "
                  >
                    <div className=" p-[4px] ">
                      <img
                        src={data.img}
                        alt=""
                        className="h-[56px] bg-[#eaf0ff] border-[1px] border-[#003e78] rounded-xl p-[4px]"
                      />
                    </div>
                    <div className="flex flex-col mt-2">
                      <h1 className="text-md sm:text-[14px] md:text-[20px] font-semibold my-2">
                        {data.heading}
                      </h1>
                      <p className="text-[14px] sm:text-sm md:text-[15px] text-[#747070] ">
                        {data.content}
                      </p>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </ScrollAnimation>
        </section>
      </div>
    </>
  );
}

export default OceanMail;
