import React from "react";
import "./oceann-ai.css";
// import mail_frontpage from "../Assets/oceann-ai-img/mail_frontpage.png";
import mailDashboard from "../Assets/mailSection_img/mail_dashboard_new.png";
import mailChat from "../Assets/mailSection_img/mail_chat_new.png";
import mailInbox_cs from "../Assets/mailSection_img/mail_Inbox_new.png";
import mailInbox_ from "../Assets/mailSection_img/mailInbox_new.png";
import map_intelligence from "../Assets/mailSection_img/map_intelligence_new.png";
// import roketImg from "../Assets/oceann-ai-img/roket-img.svg";
// import bulbImg from "../Assets/oceann-ai-img/bulb-img.svg";
import oceannAI_Img4 from "../Assets/oceann-ai-img/oceann-img5.png";
import { ScrollAnimation } from "../motion/scrollanimation";
import { Link } from "react-router-dom";
// import checkbox from "../Assets/Platform-img/checkbox.png";
import Partners from "../../Landing_Page_Components/Partners";
import { Images } from "../../assests";
const mail_frontpage = Images + "mail_frontpage.png";
// const mailDashboard = Images + "mail_dashboard.png";
// const mailChat = Images + "mail_chat.png";
// const mailInbox = Images + "mail_Inbox.png";
const roketImg = Images + "roket-img.svg";
const bulbImg = Images + "bulb-img.svg";
// const oceannAI_Img4 = Images + "oceann-img4.png";
const checkbox = Images + "checkbox.png";
// const map_intelligence = Images + "map_intelligence_old.png";
let solnImg = Images + "solutionImg.png";

export default function Oceann_AI() {
  const card = [
    {
      img: mailDashboard,
      title: "Real-Time Monitoring",
      content:
        " Stay updated with real-time insights on market shifts and demand patterns, enabling quick responses to changing tr conditions.",
      // path: "/solution/oceann-mail"
    },

    {
      img: mailChat,
      title: "Global Market Advantage",
      content:
        "AI-Enabled Trade Intelligence provides a competitive edge in the global market. Make informed decisions that set you apart and drive business success.",
      // path: "/solution/oceann-mail"
    },

    {
      img: mailInbox_,
      // title: "Enhanced Supply Chain Efficiency",
      title: "Intelligent Answers for Maritime Minds – Powered by OceannAI",
      content:
        "Get real-time insights and support for every voyage – Oceann AI is here to assist you, anytime, Anywhere.",
      // path: "/solution/oceann-mail"
    },
    {
      img: map_intelligence,
      title: "Ask OceannBot – Where Maritime Expertise Meets AI",
      content:
        "Your virtual assistant for instant maritime insights – chat with Oceann AI anytime!",
      // path: "/solution/oceann-mail"
    },
    {
      img: mailChat,
      title: "The Future of Trade",
      content:
        "Embrace the future of trade intelligence with AI. Unlock unparalleled insights that enable you to thrive in a dynamic and evolving global trade landscape.",
      // path: "/solution/oceann-mail"
    },
    {
      img: mailInbox_cs,
      title: "Custom Solutions",
      content:
        "AI-Enabled Trade Intelligence can be customized to suit your specific industry, whether you're in shipping, logistics, or international trade.",
      // path: "/solution/oceann-mail"
    },
  ];

  return (
    <>
      <main>
        <div
          className="h-[70vh] flex items-center bg-[#00000083] bg-blend-darken px-3"
          style={{
            backgroundImage: `url(${solnImg})`,
            backgroundSize: "100% 100%",
            backgroundPosition: "",
            objectFit: "cover",
            display: "flex",
            justifyContent: "center",
          }}
        >
          <div className="flex flex-col justify-between max-w-2xl md:ml-12 font-extralight text-center">
            <div className="first-child" style={{ display: "flex" }}>
              <div
                style={{
                  display: "flex",
                  flex: "1",
                  fontSize: "30px",
                  fontStyle: "normal",
                  fontWeight: 600,
                  lineHeight: "normal",
                  textTransform: "capitalize",
                }}
              >
                <div style={{ color: "white" }}>
                  Sailing with AI powered ships Revolutionizing Maritime
                  Excellence
                </div>

                <div style={{ display: "flex", flex: "1" }}>
                  <img src={mail_frontpage} alt="" />
                </div>
              </div>
            </div>

            <div
              style={{
                color: "#9F7272",
                fontFamily: "Inter",
                fontSize: "12px",
                fontStyle: "normal",
                lineHeight: "100.9%",
              }}
            >
              <Link to="/demo">
                <button
                  style={{
                    backgroundColor: "#F39C12",
                    width: "125px",
                    height: "40px",
                    flexShrink: 0,
                    fontWeight: 600,
                    color: "white",
                    marginTop: "1.5rem",
                    borderRadius: "40px",
                  }}
                >
                  BOOK DEMO
                </button>
              </Link>
            </div>
          </div>
        </div>
      </main>

      <section>
        <div className="section2-container flex flex-col -mt-20 w-[95%] mx-auto py-[12px] md:py-[2rem]">
          <div className="section2-heading1 ">
            <h1>Our Intelligence Solution</h1>
          </div>

          <div className="p-8 md:w-[60%] text-center text-[14px] mx-auto font-extralight">
            AI-Enabled Trade Intelligence utilizes artificial intelligence to
            provide advanced data analysis, predictive insights, and tailored
            solutions for enhancing decision-making, optimizing supply chains,
            and gaining a competitive edge in the global market.
          </div>

          <div className="flex flex-col px-[12px] md:flex-row md:py-[1rem] py-[8px] gap-[24px]">
            {/* <img src={oceannAI_Img2} alt="oceannAI_Img2" /> */}
            <div className="flex flex-row justify-start items-start">
              <img
                src={roketImg}
                alt="roketImg"
                className="m-10 h-[2rem] md:h-[4rem]"
              />
              <div className="flex flex-col">
                <h1 className="text-[16px] lg:text-lg">
                  Advanced Data Analytics
                </h1>
                <p className="text-[12px] lg:text-[16px]">
                  Harness the power of artificial intelligence to analyze vast
                  datasets in real-time, enabling you to make data-driven
                  decisions that optimize your trade operations.
                </p>
              </div>
            </div>

            <div>
              <h1 className="text-[16px] lg:text-lg">Predictive Insights</h1>
              <div className="flex flex-row justify-start items-start">
                <p className="text-[12px] lg:text-[16px]">
                  AI-driven trade intelligence provides predictive analytics,
                  helping you anticipate market trends and changes, ensuring you
                  stay one step ahead of the competition.
                </p>
                <img src={bulbImg} alt="bulbImg" className="mx-10" />
              </div>
            </div>
            {/* <img
              src={oceannAI_Img3}
              alt="oceannAI_Img3"
              className=" md:mt-[100px] "
            /> */}
          </div>
        </div>
      </section>

      {/* Partner section */}
      <ScrollAnimation>
        <div className="my-[1rem] mx-[1rem] max-sm:mx-[1rem]">
          <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-20 ">
            <span className="lg:text-2xl">Trusted collaborators</span>
          </h1>
          <Partners />
        </div>
      </ScrollAnimation>
      {/* Partner section end */}

      <section>
        <div className="flex flex-col items-center section2 my-[2rem] md:my-[3rem]">
          <div className="flex section2-child2">
            <div className="container rounded-lg mx-auto pt-6 pb-6 pl-6 ">
              <div className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2">
                <div className="img-cls pt-3 mr-2">
                  <img
                    src={oceannAI_Img4}
                    className="object-cover rounded-md w-[80%]"
                    alt="Nautical Evolution Powered by AI: Setting Sail for Success"
                  />
                </div>

                <div className="flex2 my-auto">
                  <div className="heading">
                    <h3 className="text-[24px] lg:text-lg">
                      Nautical Evolution Powered by AI: Setting Sail for Success
                    </h3>
                  </div>

                  <div>
                    Oceann's AI-powered platform revolutionizes maritime
                    operations by integrating advanced technology into key
                    workflows. Oceann Voyage Manager optimizes vessel and cargo
                    matching using AI, reducing empty voyages and cutting costs.
                    Oceann Mail automates email management, prioritizes
                    communication, and enhances cargo-vessel matching. With
                    real-time data access on vessels, ports, and costs through
                    Oceann AI, and instant updates via WhatsApp integration, the
                    platform ensures smarter, more efficient maritime solutions
                    for seamless operations
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="mx-[16px] my-[2rem] lg:my-[4rem] lg:mt-[2rem] lg:mx-[4rem]">
        <h1 className="text-md lg:mb-[40px] text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
          <span className="text-[24px] lg:text-xl">
            Discover What Oceann AI Brings to the Table
          </span>
        </h1>
        <ScrollAnimation>
          <div className="grid md:grid-cols-2 lg:grid-cols-3 grid-cols-1 md:flex-row justify-between lg:mx-[40px] lg:my-[16px] gap-[30px] mx-[18px] ">
            {card.map((data) => (
              <div
                style={{
                  border: "1px solid",
                  borderColor: "#e0e0e0",
                }}
                className="flex flex-1 flex-col rounded-xl px-[24px] py-[24px] section-3-cart"
              >
                {/* <div className="my-[8px] rounded-2xl border-[0.5px] border-[#e0e0e0]"> */}
                <div className="my-[8px] rounded-2xl">
                  <img
                    src={data.img}
                    alt=""
                    className="rounded-2xl object-cover w-[100%]"
                    style={{ borderRadius: "0.7rem", filter: "blur(1.3px)" }}
                  />
                </div>
                <h1 className="text-[20px] font-bold mt-[16px]">
                  {data.title}
                </h1>
                <p className="text-[14px]">{data.content}</p>
              </div>
            ))}
          </div>
        </ScrollAnimation>
      </section>
      {/* ------------------ section2 -------------- */}

      {/* ------------------ section4 --------------- */}

      {/* <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 w-[90%] mx-auto">
        <div className="max-w-xs rounded overflow-hidden shadow-lg bg-blue-100 rounded-t-full ">
          <img
            className="w-full h-[60%]"
            src={oceannAI_Img5}
            alt="oceannAI_Img5"
          />
          <div
            className="px-6 py-4 rounded-b-lg text-whit
    h-[40%] overflow-auto  bg-white"
          >
            <div className="font-bold text-lg mb-2 leading-tight">
              Decision Support and Automation
            </div>
            <p className=" text-base">
              Integrates AIS, weather, port, trade, and compliance for precise
              AI analysis in maritime operations.
            </p>
          </div>
        </div>

        <div className="max-w-xs rounded overflow-hidden shadow-lg bg-blue-100 rounded-t-full">
          <img
            className="w-full h-[60%]"
            src={oceannAI_Img6}
            alt="oceannAI_Img6"
          />
          <div
            className="px-6 py-4 rounded-b-lg 
   h-[40%] overflow-auto  bg-white"
          >
            <div className="font-bold text-lg mb-2 leading-tight">
              Data Collection and Processing
            </div>
            <p className="text-base">
              AI guides routes, automation optimizes, boosts maritime
              competitiveness.
            </p>
          </div>
        </div>

        <div className="max-w-xs rounded overflow-hidden shadow-lg bg-blue-100 rounded-t-full">
          <img
            className="w-full h-[60%]"
            src={oceannAI_Img7}
            alt="oceannAI_Img7"
          />
          <div className="px-6 py-4  rounded-b-lg h-[40%] overflow-auto  bg-white">
            <div className="font-bold text-lg mb-2 leading-tight">
              Ongoing Enhancement
            </div>
            <p className=" text-base">
              AI learns from data, improves, and helps maritime adapt.
            </p>
          </div>
        </div>

        <div className="max-w-xs rounded overflow-hidden shadow-lg bg-blue-100 rounded-t-full">
          <img
            className="w-full h-[60%] "
            src={oceannAI_Img5}
            alt="oceannAI_Img5"
          />
          <div
            className="px-6 py-4 rounded-b-lg text-whit
    h-[40%] overflow-auto bg-white"
          >
            <div className="font-bold text-lg mb-2 leading-tight">
              Generation and Forecasting
            </div>
            <p className=" text-base">
              AI for data analysis, optimizing routes and predicting market
              trends, risks, and compliance
            </p>
          </div>
        </div>
      </div> */}
      <div class="text-center mt-[2rem]">
        <h2 className=" text-lg lg:text-xl m-10 font-bold">Our Key Features</h2>
      </div>
      <div className="flex flex-col justify-center items-center my-4 lg:my-[2rem] gap-4 xl:gap-[2rem]">
        <div className="flex flex-col md:flex-row gap-4 md:gap-[2rem] xl:gap-[2rem] w-[85%] md:w-[70%]">
          <div className="flex flex-1 items-center gap-4 py-[1.5rem] px-[16px] border-[2px] rounded-lg border-[#e0e0e0] hover:shadow-xl section-2-cart">
            <div className="flex flex-col ">
              <div className="flex flex-row gap-4">
                <img src={checkbox} alt="" className="h-[40px] " />

                <div className="flex flex-col">
                  <p className="text-[20px] font-bold">
                    Decision Support and Automation
                  </p>
                  <p className="text-[14px]">
                    Integrates AIS, weather, port, trade, and compliance for
                    precise AI analysis in maritime operations.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-1 items-center gap-4 py-[1.5rem] px-[16px] border-[1px] rounded-lg border-[#e0e0e0] hover:shadow-xl section-2-cart">
            <div className="flex flex-col ">
              <div className="flex flex-row gap-4">
                <img src={checkbox} alt="" className="h-[40px]" />

                <div className="flex flex-col">
                  <p className="text-[20px] font-bold">
                    Data Collection and Processing
                  </p>
                  <p className="text-[14px]">
                    AI guides routes, automation optimizes, boosts maritime
                    competitiveness.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="flex flex-col md:flex-row gap-4 md:gap-[2rem] xl:gap-[2rem] w-[85%] md:w-[70%]">
          <div className="flex flex-1 items-center gap-4 py-[1.5rem] px-[16px] border-[1px] rounded-lg border-[#e0e0e0] hover:shadow-xl section-2-cart">
            <div className="flex flex-col ">
              <div className="flex flex-row gap-4">
                <img src={checkbox} alt="" className="h-[40px]" />

                <div className="flex flex-col">
                  <p className="text-[20px] font-bold">Ongoing Enhancement</p>
                  <p className="text-[14px]">
                    AI learns from data, improves, and helps maritime adapt.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-1 items-center gap-4 py-[1.5rem] px-[16px] border-[1px] rounded-lg border-[#e0e0e0] hover:shadow-xl section-2-cart">
            <div className="flex flex-col ">
              <div className="flex flex-row gap-4">
                <img src={checkbox} alt="" className="h-[40px]" />

                <div className="flex flex-col">
                  <p className="text-[20px] font-bold">
                    Generation and Forecasting
                  </p>
                  <p className="text-[14px]">
                    AI for data analysis, optimizing routes and predicting
                    market trends, risks, and compliance
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="flex flex-col md:flex-row gap-4 md:gap-[2rem] xl:gap-[2rem] w-[85%] md:w-[70%]">
          <div className="flex flex-1 items-center gap-4 py-[1.5rem] px-[16px] border-[1px] rounded-lg border-[#e0e0e0] hover:shadow-xl section-2-cart">
            <div className="flex flex-col ">
              <div className="flex flex-row gap-4">
                <img src={checkbox} alt="" className="h-[40px]" />

                <div className="flex flex-col">
                  <p className="text-[20px] font-bold">
                    AI-Driven Optimization with Oceann Voyage Manager
                  </p>
                  <p className="text-[14px]">
                    Oceann Voyage Manager uses AI to predict vessel and cargo
                    matches by analyzing deadweight, cargo size, load ports, and
                    open area availability. The system ensures optimal
                    utilization, reduces empty voyages, and minimizes costs by
                    providing real-time, data-driven recommendations.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-1 items-center gap-4 py-[1.5rem] px-[16px] border-[1px] rounded-lg border-[#e0e0e0] hover:shadow-xl section-2-cart">
            <div className="flex flex-col ">
              <div className="flex flex-row gap-4">
                <img src={checkbox} alt="" className="h-[40px]" />

                <div className="flex flex-col">
                  <p className="text-[20px] font-bold">
                    AI-Powered Automation in Oceann Mail
                  </p>
                  <p className="text-[14px]">
                    Oceann Mail uses AI to automate tagging, extract vessel and
                    cargo info, write emails, auto-reply, and prioritize
                    communication. It matches cargo with vessels, enhances
                    search filters, and integrates with Oceann AI for predictive
                    insights. This streamlines operations, boosts efficiency,
                    and simplifies maritime email management.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* ------------------ section5 --------------- */}
    </>
  );
}
