import { Suspense, lazy, useEffect, useState } from "react";
import TagManager from "react-gtm-module";
import { Outlet, RouterProvider, createBrowserRouter } from "react-router-dom";
import "./App.css";
import googlePlay from "././Componants/Assets/footer-icon/google_play.png";
import appStore from "././Componants/Assets/footer-icon/app_store.png";
import windowApp from "././Componants/Assets/footer-icon/window_app.png";
import windappLogo from "././Componants/Assets/footer-icon/windapp-logo.png";
import FooterSection from "./Componants/FooterSection";
import Navbar from "./Componants/Navbar";
import WhatsappIcon from "./Componants/whatsappIcon.js";
import { Chartering } from "./Componants/oceann-components/Chartering";
import { Finance } from "./Componants/oceann-components/Finance";
import OceannAI from "./Componants/oceann-components/Oceann_AI";
import OceannAnalytics from "./Componants/oceann-components/Oceann_Analytics";
import OceannBI from "./Componants/oceann-components/Oceann_BI";
import OceannOperations from "./Componants/oceann-components/Oceann_Operations";
import Home from "./Landing_Page_Components/Home";
import Home2 from "./Landing_Page_Components/Home2";
import { BookDemo } from "./Pages/BookDemo";
import { ContactUs } from "./Pages/ContactUs";
import { Cookies } from "./Pages/Cookies";
import { Product } from "./Pages/Product";
import Career from "./Pages/Career";
import Paymentsuccess from "./Pages/payment/paymentSuccess";
import { OceannApi } from "./Componants/oceann-components/OceannApi";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ScrollToTop from "./Componants/ScrollToTop/ScrollToTop";
import Signup from "./Pages/signup/signup";
import Spinner from "./Componants/spinner/spinner";
import { globalStore } from "./store/globalStore";
import Protected from "./Componants/protectedRoutes/protected";
import LoginMiddleware from "./Componants/protectedRoutes/loginMiddleware";
import Plans from "./Pages/Plans";
import Profile from "./Pages/myProfile/profile";
import Login from "./Pages/login/login";
import Checkout from "./Pages/checkout/checkout";
import Mentorship from "./Pages/mentorship.js";
import LegalInformation from "./Pages/LegalInformation.js";
import { Modal, Button } from "antd";
import RippleComponent from "./Pages/ErrorPage.js";
import Chatbot from "./Componants/chatbot/chatboticon.js";
// import { CursorTwo } from "cursor-style";
import video from "./Componants/Assets/comming-soon/comming-soon.mp4";
import ChatBotInitiater from "./Componants/chatbot/ChatBotInitiater.js";
import Mentors from "./Pages/Mentors.js";
import CEO from "./Pages/MsgFromCEO.js";
import { Box, IconButton } from "@mui/material";
import { FaTimes } from 'react-icons/fa';
import OceannX from "./Pages/OceannX.js";

// import commingSoon from "../Componants/Assets/comming-soon/comming-soon.mp4";

// import Team from "./Pages/Team.js";
// const Plans = lazy(() => import("./Pages/Plans"));

const tagManagerArgs = {
  gtmId: "G-BZPXVPYQVL",
};
TagManager.initialize(tagManagerArgs);

const PreTradeIntelligence = lazy(() => import("./Pages/Pretradeintelligence"));

const AboutUs = lazy(() => import("./Pages/AboutUs"));
const Team = lazy(() => import("./Pages/Team"));
// const Signup = lazy(() => import("./Pages/signup/signup"));
// const Signup = lazy(() => import("./Pages/signup/signup"));
// import Solution from "./Pages/Solution.js"
const Solution = lazy(() => import("./Pages/Solution"));
const OceannZero = lazy(() => import("./Pages/OceannZero"));
const Platform = lazy(() => import("./Componants/Platform"));
const OceannMail = lazy(() =>
  import("./Componants/oceann-components/Oceann_Mail")
);
const OceannVM = lazy(() => import("./Componants/oceann-components/Oceann_VM"));
const Privacy = lazy(() => import("./Pages/Privacy"));
const Events = lazy(() => import("./Pages/Event"));
const News = lazy(() => import("./Pages/News"));
const NewsDetails = lazy(() => import("./Pages/NewsDetails"));
const Tanker = lazy(() => import("./Pages/tanker.js"));
const Dry = lazy(() => import("./Pages/dry"));
const Project = lazy(() => import("./Pages/project.js"));
const Pooling = lazy(() => import("./Pages/pooling"));
const Card = lazy(() => import("./Pages/card"));
// const Login = lazy(() => import("./Pages/login/login"));
// const Checkout = lazy(() => import("./Pages/checkout/checkout"));
// const Profile = lazy(() => import("./Pages/myProfile/profile"));
const Parceling = lazy(() => import("./Pages/parceling"));

const MainPage = () => {
  const { setIsLoading, setButtonDisabled } = globalStore();
  window.spinnerStoreFuntion = () => {
    return { setIsLoading, setButtonDisabled };
  };
  const [loading, setLoading] = useState(false);
  const [isopen, setIsOpen] = useState(false);
  const [cardModal, setcardModal] = useState(false);
  const [cardModal2, setcardModal2] = useState(true);
  const [cardModal3, setcardModal3] = useState(false);
  const [isChatbotOpen, setChatbotOpen] = useState(null);
  const appStore = 'https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/Apple+Store.png'
  const googlePlay = 'https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/google-play1.png';
  const video = 'https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/Blue+Modern+Investment+Mobile+App+Promotion+Facebook+Ad+(2).mp4'


  const toggleChatbot = (status) => setChatbotOpen(status);
  const [showChatBotInitiater, setShowChatBotInitiater] = useState(false);

  const handleClosecardModal = () => {
    setcardModal(false);

    setTimeout(() => {
      setShowChatBotInitiater(true);
    }, 3000);

    setTimeout(() => {
      openCookieModal();
    }, 10000);
  };

  const openCookieModal = () => {
    const hasShownModal = localStorage.getItem("hasShownCardModal3");

    if (!hasShownModal) {
      setcardModal3(true);
      localStorage.setItem("hasShownCardModal3", "true");
    }
  };

  const handleClosecommingSoonModal = () => {
    setcardModal2(false);
    setcardModal(true);
  };

  const handleClosecardModal_ = () => {
    setcardModal3(false);
  };

  const img = `${process.env.REACT_APP_IMAGE_PATH}/microsoft-website.jpeg`;

  return (
    <>
      {/* <CursorTwo
        size={40}
        delay={5}
        sizeDot={10}
        sizeOutline={35}
        bgColorDot="black"
        bgColorOutline="black"
      /> */}
      <Navbar />
      <WhatsappIcon />
      <div className="chatbot-container">
        {showChatBotInitiater && (
          <ChatBotInitiater
            toggleChatbot={toggleChatbot}
            setShowChatBotInitiater={setShowChatBotInitiater}
          />
        )}
        <Chatbot isOpen={isChatbotOpen} toggleChatbot={toggleChatbot} />
      </div>
      <ScrollToTop>
        {/* <Spinner /> */}
        <Outlet />
      </ScrollToTop>
      <FooterSection setcardModal3={setcardModal3} />
      {/* <Modal
        width={720}
        open={isopen}
        className="attention-modal"
        onCancel={() => setIsOpen(false)}
        title="Attention Cargo Owners"
        footer={false}
        centered
        destroyOnClose
      >
        <div className="enquiryModalBody" style={{ paddingBlockEnd: "20px" }}>
          <b
            style={{
              display: "block",
              marginBottom: "10px",
              textDecoration: "underline",
            }}
          >
            ATTENTION !!
          </b>{" "}
          Cargo Owners, Ship Charterers and Brokers Worldwide: Submit your
          Tonnage and Cargo inquiries to the{" "}
          <u>below email for Global Access.</u>{" "}
          <div>
            <span>Mail us At : </span>
            <a
              style={{
                textDecoration: "underline",
                marginTop: "10px",
                display: "inline-block",
                fontSize: "20px",
              }}
              href="mailto:Fix@theoceann.ai"
            >
              FIX@THEOCEANN.AI
            </a>
          </div>
        </div>
      </Modal> */}
      {/*  */}
      <Modal
        width={800}
        open={cardModal}
        // className="attention-modal"
        onCancel={() => handleClosecardModal()}
        title="TheOceann Secures Microsoft Founders Hub Level 4"
        footer={false}
        centered
        destroyOnClose
      >
        <div>
          {/* url of cdn image */}
          <img
            alt="coming-soon-baltic"
            src="https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/microsoft-website.jpeg"
            loading="lazy"
          />
        </div>
      </Modal>

      <Modal
        open={cardModal2}
        className="video-layout-position"
        onCancel={() => handleClosecommingSoonModal()}
        // title="We will be launching our mobile app soon"
        footer={false}
        centered={false}
        destroyOnClose
      >

                    <video
                        loop
                        muted
                        autoPlay
                        className="video-frame h-auto"
                        style={{ borderRadius: '8px', marginTop:'4%', height:'auto' }}
                    >
                        <source src={video} type="video/mp4" />
                        <source src={video} type="video/ogg" />
                        Your browser does not support the video tag.
                    </video>
                    <div className=" w-[100%]" style={{ display: 'flex', flexDirection: 'row', position:'absolute', width:'100%', marginBottom:'6%',marginLeft:'2%', bottom:0 }}>
                    <div
                      className="app-link flex flex-col justify-end mb-4 xxxxxs:w-[52px] xxxxxs:mb-[7%] xxxxs:w-[72px] xxxxs:mb-[5%] xxxs:w-[72px] xxs:w-[100%] md:mb-[4%] lg:mb-[3%] xl:mb-[4%]"
                    >
                      {/* Google Play Store Button */}
                      <a
                        href="https://play.google.com/store/apps/details?id=com.theoceann.app.oceannapp&hl=en"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <img
                          className="download-img-foot img-hov w-[20%] xs:w-full xxxxxs:w-[72px] sm:w-[82px] md:w-[20%] lg:w-[20%] rounded-lg shadow-md border border-white/70"
                          src={googlePlay}
                          alt="googlePlay"
                        />
                      </a>

                      {/* Apple App Store Button */}
                      <a
                        href="https://apps.apple.com/us/app/oceann-x/id6737811559"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <img
                          className="download-img-foot img-hov w-full md:w-[20%] xxxxxs:w-[72px] rounded-lg shadow-md"
                          src={appStore}
                          alt="appStore"
                        />
                      </a>
                    </div>

                           
                        </div>
        
        {/* <div class="row mobile-modal-row">
          <div class="first-col">
            <video
              loop="true"
              muted
              autoplay="autoplay"
              controls="controls"
              className="video-frame"
            >
              <source src={video} type="video/mp4" />
              <source src={video} type="video/ogg" />
              Your browser does not support the video tag.
            </video>
          </div>
          <div class="second-col">
            <div className="modal-head-cust">
              Seamless Maritime Solutions at Your Fingertips - Download the App
              Now on IOS, Andriod and Windows!
            </div>
            <div className="mobile-app-images">
              <div className="app-link">
                <a href="https://play.google.com/store/apps/details?id=com.theoceann.app.oceannapp&hl=en" target="_blank" rel="noopener noreferrer">
                  <img
                    className="download-img-foot"
                    src={googlePlay}
                    alt="googlePlay"
                    style={{ width: "100%", height: "auto" }}
                  />
                </a>
                <a href="https://apps.apple.com/us/app/oceann-x/id6737811559" target="_blank" rel="noopener noreferrer">
                  <img
                    className="download-img-foot"
                    src={appStore}
                    alt="appStore"
                    style={{ width: "100%", height: "auto" }}
                  />
                </a>
                <a href="#" target="_blank" rel="noopener noreferrer">
                  <img
                    className="download-img-foot"
                    src={windowApp}
                    alt="windowApp"
                    style={{ width: "100%", height: "auto" }}
                  />
                </a>
              </div>
              <div className="windapp-logo">
                <img
                  className="download-img-foot"
                  src={windappLogo}
                  alt="windappLogo"
                  style={{ width: "auto", height: "auto" }}
                />
              </div>
            </div>
          </div>
        </div> */}
      </Modal>

      <Modal
        width={800}
        open={cardModal3}
        className="cookies-modal"
        onCancel={() => handleClosecardModal_()}
        title="Cookie Preferences"
        // footer={true}
        centered
        destroyOnClose
        footer={[
          <Button
            style={{ background: "blue" }}
            type="primary"
            onClick={() => {
              toast("Cookies accepted");
              setcardModal3(false);
            }}
          >
            Accept All
          </Button>,
          <Button onClick={() => setcardModal3(false)} key="back">
            Reject All
          </Button>,
        ]}
      >
        <div className="cc_body">
          <p>
            We use cookies to improve your experience, support essential website
            functions, and analyze performance. By clicking "Accept All," you
            agree to our use of all cookies. You can adjust your preferences or
            learn more by reviewing our Privacy Policy.
          </p>
          <ul className="cc_wrapper">
            <li className="cc_list">
              <div className="heading">
                <div className="title">Necessary Cookies</div>
                <div className="status">Always Active</div>
              </div>
              <div className="des">
                <p>
                  These cookies are essential for the basic operation of the
                  website, such as enabling secure logins or saving your
                  preferences. They do not store any personal information and
                  cannot be disabled.
                </p>
              </div>
            </li>

            <li className="cc_list">
              <div className="heading">
                <div className="title">Functional Cookies</div>
              </div>
              <div className="des">
                <p>
                  These cookies enhance your experience by enabling additional
                  features, such as personalized settings, social sharing, and
                  feedback tools.
                </p>
              </div>
            </li>

            <li className="cc_list">
              <div className="heading">
                <div className="title">Analytics Cookies</div>
              </div>
              <div className="des">
                <p>
                  We use these cookies to collect data about how visitors
                  interact with our site. This includes tracking page visits,
                  bounce rates, and traffic sources to help us improve our
                  content and functionality.
                </p>
              </div>
            </li>

            <li className="cc_list">
              <div className="heading">
                <div className="title">Performance Cookies</div>
              </div>
              <div className="des">
                <p>
                  Performance cookies allow us to monitor and improve the speed
                  and usability of our website by analyzing technical data and
                  visitor behavior.
                </p>
              </div>
            </li>

            <li className="cc_list">
              <div className="heading">
                <div className="title">Advertising Cookies</div>
              </div>
              <div className="des">
                <p>
                  Advertising cookies are used to deliver personalized ads and
                  measure their effectiveness. They help ensure you see content
                  relevant to your interests.
                </p>
              </div>
            </li>

            <li className="cc_list">
              <div className="heading">
                <div className="title">Other Cookies</div>
              </div>
              <div className="des">
                <p>
                  These include cookies that have not yet been categorized. They
                  are under review and will be classified soon.
                </p>
              </div>
            </li>
          </ul>
        </div>
      </Modal>
    </>
  );
};

function App() {
  // const [authenticated, setIsAuthenticated] = useState(false);
  // console.log("in app");
  // const token = localStorage.getItem("oceanAllToken");
  // if (token) {
  //   setIsAuthenticated(true);
  // }

  const router = createBrowserRouter([
    {
      path: "/",
      element: <MainPage />,
      errorElement: <RippleComponent />,
      children: [
        {
          path: "/",
          element: (
            // <Suspense fallback={<div>Loading....</div>}>
            <Home />
            // </Suspense>
          ),
        },
        {
          path: "/home2",
          element: (
            // <Suspense fallback={<div>Loading....</div>}>
            <Home2 />
            // </Suspense>
          ),
        },
        {
          path: "/profile",
          element: (
            // <Suspense fallback={<div>Loading....</div>}>
            <Protected element={Profile} />
          ),
        },
        {
          path: "/about-us",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <AboutUs />
            </Suspense>
          ),
        },
        {
          path: "/login",
          element: (
            // <Suspense fallback={<div>Loading....</div>}>
            <LoginMiddleware element={Login} />

            // </Suspense>
          ),
        },
        {
          path: "/signup",
          element: (
            // <Suspense fallback={<div>Loading....</div>}>
            <LoginMiddleware element={Signup} />
            // </Suspense>
          ),
        },
        {
          path: "/solution",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <Solution />{" "}
            </Suspense>
          ),
        },
        {
          path: "/platform",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <Platform />
            </Suspense>
          ),
        },
        {
          path: "/payment/success",
          element: (
            // <Suspense fallback={<div>Loading....</div>}>
            <Protected element={Paymentsuccess} />
            // </Suspense>
          ),
        },

        {
          path: "/solution/oceann-mail",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <OceannMail />
            </Suspense>
          ),
        },
        {
          path: "/solution/oceann-api",
          element: (
            // <Suspense fallback={<div>Loading....</div>}>
            <OceannApi />
            // </Suspense>
          ),
        },
        {
          path: "/solution/oceann-vm",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <OceannVM />
            </Suspense>
          ),
        },
        {
          path: "/solution/oceann-bi",
          element: (
            // <Suspense fallback={<div>Loading....</div>}>
            <OceannBI />
            // </Suspense>
          ),
        },
        {
          path: "/checkout",
          element: (
            // <Suspense fallback={<div>Loading....</div>}>
            <Protected element={Checkout} />
            // </Suspense>
          ),
        },
        {
          path: "/product/oceann-analytics",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <OceannAnalytics />
            </Suspense>
          ),
        },
        {
          path: "/product/operations",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <OceannOperations />
            </Suspense>
          ),
        },
        {
          path: "/solution/oceann-ai",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <OceannAI />
            </Suspense>
          ),
        },
        {
          path: "product/ocean-zero",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <OceannZero />
            </Suspense>
          ),
        },
        {
          path: "product/oceann-x",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <OceannX />
            </Suspense>
          ),
        },
        {
          path: "/mentors",
          element: (
            // <Suspense fallback={<div>Loading....</div>}>
            <Mentors />
            // </Suspense>
          ),
        },
        {
          path: "/plans",
          element: (
            // <Suspense fallback={<div>Loading....</div>}>
            <Plans />
            // </Suspense>
          ),
        },
        {
          path: "/product/pre-trade-intelligence",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <PreTradeIntelligence />
            </Suspense>
          ),
        },
        {
          path: "/product",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <Product />
            </Suspense>
          ),
        },
        {
          path: "/contact",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <ContactUs />
            </Suspense>
          ),
        },
        {
          path: "product/Pre-Trade-Intelligence",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <PreTradeIntelligence />
            </Suspense>
          ),
        },
        {
          path: "/product/oceann-mail",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <ContactUs />
            </Suspense>
          ),
        },
        {
          path: "/product/oceann-zero",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <OceannZero />
            </Suspense>
          ),
        },
        {
          path: "/demo",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <BookDemo />
            </Suspense>
          ),
        },
        {
          path: "/product/oceann-finance",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <Finance />
            </Suspense>
          ),
        },
        {
          path: "/product/chartering",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <Chartering />
            </Suspense>
          ),
        },
        {
          path: "/privacy",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <Privacy />
            </Suspense>
          ),
        },
        {
          path: "/knowledge-Hub/events",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <Events />
            </Suspense>
          ),
        },
        {
          path: "/knowledge-Hub/news",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <News />
            </Suspense>
          ),
        },
        {
          path: "/knowledge-Hub/news/details/:id",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <NewsDetails />
            </Suspense>
          ),
        },
        {
          path: "/cookies",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <Cookies />
            </Suspense>
          ),
        },
        {
          path: "/career",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <Career />
            </Suspense>
          ),
        },
        {
          path: "/tanker",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <Tanker />
            </Suspense>
          ),
        },

        {
          path: "/dry",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <Dry />
            </Suspense>
          ),
        },

        {
          path: "/project",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <Project />
            </Suspense>
          ),
        },
        {
          path: "/pooling",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <Pooling />
            </Suspense>
          ),
        },
        {
          path: "/parceling",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <Parceling />
            </Suspense>
          ),
        },
        {
          path: "/mentorship",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <Mentorship />
            </Suspense>
          ),
        },
        {
          path: "/legal-information",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <LegalInformation />
            </Suspense>
          ),
        },
        {
          path: "/msg_ceo",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <CEO />
            </Suspense>
          ),
        },
        {
          path: "/team",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <Team />
            </Suspense>
          ),
        },
        {
          path: "/card",
          element: (
            <Suspense fallback={<div>Loading....</div>}>
              <Card />
            </Suspense>
          ),
        },
      ],
    },
  ]);

  return (
    <>
      <RouterProvider router={router} />
      <ToastContainer />
    </>
  );
}

export default App;
