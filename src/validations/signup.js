const signUpValidationFunction = (userInput) => {
  const errors = {};

  const email_pattern =
    /^(?=.{1,35}$)[a-zA-Z0-9.,_@-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/i;

  // const passwordRegex =
  //   /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$%^&+=!]).{8,}$/;

   // Individual password patterns
   const hasLowercase = /(?=.*[a-z])/; // At least one lowercase letter
   const hasUppercase = /(?=.*[A-Z])/; // At least one uppercase letter
   const hasDigit = /(?=.*\d)/; // At least one digit
   const hasSpecialChar = /(?=.*[@#$%^&+=!])/; // At least one special character
   const hasMinLength = /.{8,}/; // Minimum 8 characters
 

  if (!userInput.first_name) {
    errors.first_name = "First Name is required";
  }
  if (!userInput.last_name) {
    errors.last_name = "Last Name is required";
  }
  if (!userInput.email) {
    errors.email = "Email is required";
  }
  if (!email_pattern.test(userInput?.email)) {
    errors.email = "Please enter a valid Email";
  }
  if (!userInput.company_name) {
    errors.company_name = "Organization name is required";
  }
  if (!userInput.phone_number) {
    errors.phone_number = "Contact is required";
  }

  if (!userInput.country) {
    errors.country = "Please select country";
  }
  if (!userInput.country_code) {
    errors.country_code = "Please select country code";
  }

  if (!userInput.password) {
    errors.password = "Password is required";
  } else {
    // Check individual password conditions
    if (!hasLowercase.test(userInput.password)) {
      errors.password =
        "Password must contain at least one lowercase letter (a-z)";
    } else if (!hasUppercase.test(userInput.password)) {
      errors.password =
        "Password must contain at least one uppercase letter (A-Z)";
    } else if (!hasDigit.test(userInput.password)) {
      errors.password = "Password must contain at least one numeric character (0-9)";
    } else if (!hasSpecialChar.test(userInput.password)) {
      errors.password =
        "Password must contain at least one special character (@#$%^&+=!)";
    } else if (!hasMinLength.test(userInput.password)) {
      errors.password =
        "Password must be at least 8 characters long";
    }
  }
  if (!userInput.confirmPassword) {
    errors.confirmPassword = "Confirm Password is required";
  }
  if (userInput.confirmPassword !== userInput.password) {
    errors.confirmPassword = "Password didn`t match";
  }

  return errors;
};

export default signUpValidationFunction;
