import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import { GoogleLogin, GoogleOAuthProvider } from "@react-oauth/google";
import { Button, Col, Flex, Form, Input, Modal, Row, Spin, Typography } from 'antd';
import React, { useEffect, useState } from "react";
import { FaRegEye } from "react-icons/fa";
import MicrosoftLogin from "react-microsoft-login";
import { useNavigate } from "react-router";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import { Images } from "../../assests";
import companyLogo from "../../Componants/Assets/signup-img/company_logo.webp";
import { loginService } from "../../services/all";
import loginValidations from "../../validations/login";
import styles from "./login.module.css";

const Login = () => {
  const [loginForm, setLoginForm] = useState({
    email: "",
    password: "",
    rememberMe: false,
  });
  const [errors, setErrors] = useState({});
  const [showPassword, setShowPassword] = useState(false);
  const navigate = useNavigate();
  const [sendOtpModal, setSendOtpModal] = useState(false);
  const [otpModal, setOtpModal] = useState(false)
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState("");
  const [otp, setOtp] = useState("");
  const [resetModal, setResetModal] = useState(false);
  const [timer, setTimer] = React.useState(0)

  const { Title } = Typography

  const changeHandler = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    setLoginForm({ ...loginForm, [name]: value });
  };

  // useEffect(() => {
  //   let interval;
  //   if (timer > 0) {
  //     interval = setInterval(() => {
  //       setTimer((prev) => prev - 1);
  //     }, 1000);
  //   }
  //   return () => clearInterval(interval);
  // }, [timer]);
  useEffect(() => {
    // Decrement timer every second
    const timer = setInterval(() => {
      setTimer((prevTime) => {
        if (prevTime <= 0) {
          clearInterval(timer); // Stop timer when it reaches 0
          return 0;
        }
        return prevTime - 1;
      });
    }, 1000);

    return () => clearInterval(timer); // Cleanup on component unmount
  }, [timer]);

  // Format time as MM:SS
  const formatTime = (time) => {
    const minutes = Math.floor(time / 60)
      .toString()
      .padStart(2, "0");
    const seconds = (time % 60).toString().padStart(2, "0");
    return `${minutes}:${seconds}`;
  };

  const blurHandler = (event) => {
    event.preventDefault();
    const { name } = event.target;
    const validationErrors = loginValidations({
      ...loginForm,
      [name]: loginForm[name],
    });
    setErrors({ ...errors, [name]: validationErrors[name] });
  };

  const isProduction = () => {

    const isProd = `${process.env.REACT_APP_IS_PROD}`;
    return isProd


  };


  const submitHandler = async (event) => {
    event.preventDefault();
    const validationErrors = loginValidations(loginForm);
    setErrors(validationErrors);

    if (Object.keys(validationErrors).length < 1) {
      try {
        const data = await loginService(loginForm);
        // console.log("Dataa --", data);
        if (data.status === 200) {
          // Successful login
          toast.success("Login Success");

          localStorage.removeItem("oceanAllSignupData");
          localStorage.setItem("oceanAllToken", data.token);
          const userData = atob(data.token.split(".")[1]);
          localStorage.setItem("oceanAllUserData", userData);
          const parsedUserInfo = JSON.parse(userData);

          localStorage.setItem("isAdmin", data.is_admin);
          if (parsedUserInfo.is_subscribed === true && parsedUserInfo.MAIL_ACCESS) {
            const MailURL = `https://mail-${parsedUserInfo.company_name}.theoceann.${isProduction() ? 'ai' : 'com'}/login?token=${localStorage.getItem(
              "oceanAllToken"
            )}`;
            navigate('/profile')
            window.open(MailURL, "_blank");
            // window.location.href = `http://192.168.18.37:3000/login?token=${data.token}`;
            // window.location.href = `${process.env.REACT_APP_REDIRECT_URL}?token=${data.token}`;
          } else if (
            !Object.keys(parsedUserInfo).includes("is_subscribed") &&
            !Object.keys(parsedUserInfo).includes("user_domain")
          ) {
            localStorage.setItem("isAdmin", false);
            // setIsAdmin(false);

            navigate("/profile");
          } else {
            navigate("/profile");
          }
        } else {
          // Handle other status codes (e.g., display an error message)
          toast.error(data.msg);
          console.error(`Login failed with status code: ${data.status}`);
        }
      } catch (error) {
        //
        console.error("An error occurred during login:", error);
      }
    }
  };

  const loginFunc = async (e) => {
    let obj = e;
    let cred = e.credential;
    let data = cred.split(".");
    let userData = data[1];

    let decode_cred = atob(userData);
    decode_cred = JSON.parse(decode_cred);


    obj.email = decode_cred.email;
    obj.metaData = decode_cred;

    Social_login(obj);
    // loginService(e)
  };

  const Social_login = async (obj) => {
    const resp = await fetch(
      `${process.env.REACT_APP_BASE_URL}client/googleLogin`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(obj),
      }
    );
    let res = await resp.json();

    if ("token" in res) {
      res = res.token;
    } else {
      toast.error(res.error);
      return;
    }

    if (res.token) {
      localStorage.setItem("oceanAllToken", res.token);
      let userData = JSON.parse(atob(res.token.split(".")[1]));
      localStorage.setItem("oceanAllUserData", JSON.stringify(userData));
      if (userData.role !== "admin") {
        fetchPermissionList();
      } else {
        toast("Successfull Login")
        navigate("/plans", { replace: true });
      }
    }
  };
  const authHandler = (err, data) => {
    let obj = {};
    obj.loginType = "Microsoft";
    obj.metaData = data;
    obj.email = data?.account?.username;
    Social_login(obj);
  };

  const fetchPermissionList = async () => {
    const url = `${process.env.REACT_APP_BASE_URL}client/access-controll`;
    const response = await fetch(url, {
      method: 'post',
      payload: {}
    })
    localStorage.setItem(
      "permissionList",
      JSON.stringify(response.accessControl)
    );
    toast.success(response.msg);
    navigate("/chartering-dashboard", { replace: true });
  };

  const sendOtpFunc = async (values) => {
    try {
      setEmail(values)
      const url = `${process.env.REACT_APP_BASE_URL}accounts/send-otp`
      setLoading(true)
      const response = await fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(values),
      });

      const data = await response?.json();


      if (!response.ok || data.error) {
        toast.error(data.error)
        setLoading(false)
        return
      }

      toast.success("OTP sent Successfully!")
      setSendOtpModal(false)
      setTimer(300);
      setOtpModal(true)


      // You can return any data that might be useful
      setLoading(false)
    } catch (error) {
      console.error('Error sending OTP:', error.message);
      setLoading(false)
    }
  };

  const onChange = (text) => {
    handleOtpSend(text)
    setOtp(text)
  };

  const sharedProps = {
    onChange,
  };

  const handleOtpSend = async (text) => {
    try {
      const url = `${process.env.REACT_APP_BASE_URL}accounts/confirm-otp`;

      const payload = {
        email: email.email,
        otp: text || otp,
      }

      setLoading(true)

      const response = await fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload),
      });

      const data = await response?.json();


      if (!response.ok || data.error) {
        toast.error(data.error)
        setLoading(false)
        return
      }

      toast(data.message)

      setLoading(false)
      setOtpModal(false)
      setResetModal(true)

    } catch (error) {
      console.error('Error verifying OTP:', error.message);
      setLoading(false)
    }
  }

  const handleResetForm = async (values) => {
    try {
      const url = `${process.env.REACT_APP_BASE_URL}accounts/forgot-password`
      setLoading(true)
      const payload = {
        email: email.email,
        otp: otp,
        ...values
      }
      const response = await fetch(url, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload),
      });

      const data = await response?.json();


      if (!response.ok || data.error) {
        toast.error(data.error)
        setLoading(false)
        return
      }

      toast.success("Password succesfully Reset. Login again!!")
      setLoading(false)
      setResetModal(false)

      // You can return any data that might be useful
    } catch (error) {
      console.error('Error sending OTP:', error.message);
      setLoading(false)
    }
  }

  return (
    <>
      <body className={styles.loginBody}>
        <div className={styles.background}>
          <div className={styles.maincontent}>
            <div className={styles.formDiv}>
              <div className={styles.formContainer}>
                <div className={styles.formwidthbox}>
                  <div className={styles.companylogodiv}>
                    <img
                      src={companyLogo}
                      alt="Company Logo"
                      className={styles.companylogo}
                    />
                  </div>
                  <div className={styles.topheadingSignup}>
                    <h3 className={styles.signUp}>Login</h3>
                    <p className={styles.subheading}>
                      Login with your E-mail & Password
                    </p>
                  </div>
                  <form onSubmit={submitHandler}>
                    <div className={styles.firstRow}>
                      <div className={styles.inputbox}>
                        <span className={styles.span}>Email</span>
                        <input
                          type="text"
                          name="email"
                          id=""
                          onChange={changeHandler}
                          onBlur={blurHandler}
                          placeholder="Enter your email"
                          className={styles.inputdata}
                        />
                        <div className={styles.errorFirstname}>
                          {errors.email && (
                            <p className={styles.validationErrortext}>
                              {errors.email}
                            </p>
                          )}
                        </div>
                      </div>
                    </div>

                    <div className={styles.firstRow}>
                      <div className={styles.inputbox}>
                        <span>Password</span>
                        <div className={styles.passwordContainerLogin}>
                          <div id="passwordField" className={styles.eyeclass}>
                            <input
                              type={showPassword ? "text" : "password"}
                              name="password"
                              onChange={changeHandler}
                              onBlur={blurHandler}
                              id=""
                              placeholder="Enter password"
                              className={styles.inputdata}
                            />
                            <div
                              onClick={() => {
                                setShowPassword(!showPassword);
                              }}
                              className={styles.eyeIconLogin}
                            >
                              <FaRegEye />
                            </div>
                          </div>
                        </div>

                        <div className={styles.errorPassword}>
                          {errors.password && (
                            <p className={styles.validationErrortext}>
                              {errors.password}
                            </p>
                          )}
                        </div>
                      </div>
                    </div>

                    <div className={styles.remember}>
                      <label className={styles.remembermediv}>
                        <input
                          type="checkbox"
                          name=""
                          id=""
                          className={styles.rememberme}
                        />
                        Remember me
                      </label>

                      <span
                        className={styles.loginLink}
                        style={{ marginLeft: "141px", cursor: 'pointer' }}
                        onClick={() => setSendOtpModal(!sendOtpModal)}
                      >
                        Forgot Password?
                      </span>
                    </div>

                    <div className={styles.signinbox}>
                      <button className={styles.signIn_button}>Login</button>
                    </div>
                    <p className={styles["login-link"]} style={{ marginBottom: '10px' }}>
                      Don't have an account?{"  "}
                      <Link
                        className={styles.loginLink}
                        onClick={() => {
                          navigate("/signup");
                        }}
                      >
                        SignUp
                      </Link>
                    </p>

                    <div className={styles.orbox}>
                      <hr className={styles.orhr} />
                      OR
                      <hr className={styles.orhr} />
                    </div>
                    {/* <div className={styles.options}>
                      <div className={styles["icon_div"]}>
                        <img src={google} alt="" />
                      </div>
                      <div className={styles["icon_div"]}>
                        <img src={outlook} alt="" />
                      </div>
                      <div className={styles["icon_div"]}>
                        <img src={facebook} alt="" />
                      </div>
                    </div> */}
                    <div className="social-login">
                      <div className="googleLogin-Box">
                        <GoogleOAuthProvider clientId="380315452391-tbvplprrntar187htkon5i03kl21i77j.apps.googleusercontent.com">
                          <GoogleLogin
                            theme="filled_blue"
                            size="medium"
                            shape="circle"
                            width="215px"
                            height="41px"
                            useOneTap
                            auto_select
                            onSuccess={(e) => loginFunc(e)}
                            onFailure={(e) => console.log("Failed to log in", e)}
                          />
                        </GoogleOAuthProvider>
                      </div>
                      <div className="ms-login-box">
                        <MicrosoftLogin
                          buttonTheme="light"
                          className="MS-login"
                          prompt="select_account"
                          clientId={"5c49e47c-a2fd-486a-8875-fad8418a6da5"}
                          authCallback={authHandler}
                        />
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>

            <div className={styles.gifDiv}>
              <img
                src={Images + "signin_Img.png"}
                alt=""
                className={styles.gif}
              />
            </div>
          </div>
        </div>
      </body>
      <Modal title="Send OTP on your Mail" open={sendOtpModal} onCancel={() => setSendOtpModal(false)} footer={false}>
        <Form
          onFinish={sendOtpFunc}
          layout="vertical"
        >
          <div style={{ paddingBlock: '10px' }}>
            <Row gutter={10} align="bottom">
              <Col sm={24} md={18} lg={18}>
                <Form.Item
                  label="Email"
                  name="email"
                  style={{ marginBottom: '0px' }}
                  rules={[
                    {
                      required: true,
                      message: 'Please input your email!',
                    },
                  ]}
                >
                  <Input type="email" />
                </Form.Item>

              </Col>
              <Col sm={24} md={6} lg={6}>
                <Button className="login_signIn_button__zFcg7" style={{ width: '100%', margin: '0' }} htmlType="submit">Send OTP <Spin size="small" spinning={loading} /></Button>
              </Col>
            </Row>
          </div>
        </Form>
      </Modal>

      {/* OTP Modal */}
      <Modal open={otpModal} onCancel={() => setOtpModal(false)} footer={false}>
        <Title level={5}>Enter your OTP</Title>
        <p style={{ textAlign: 'center', marginBlock: '20px' }}>Enter the OTP to validate your Email.</p>
        <Flex gap="middle" align="flex-start" justify="center" vertical className="send_otp_input">
          <Input.OTP disabled={timer === 0 ? true : false} formatter={(str) => str.toUpperCase()} {...sharedProps} />
          <Button
            disabled={timer === 0 ? true : false}
            className="login_signIn_button__zFcg7" style={{ width: '100px', margin: '0 auto' }}
            onClick={() => handleOtpSend(otp)}>Submit <Spin size="18" spinning={loading} /></Button>
        </Flex>
        <p style={{ textAlign: 'center', marginBlock: '10px' }}>{timer !== 0 ? <span>
          OTP is valid for <strong>{formatTime(timer)}</strong>
        </span> : <span style={{ color: 'red' }}>{"OTP Expired, Please Resend!!"}</span>}</p>
      </Modal>

      {/* Reset Password Modal */}
      <Modal title="Reset your Password" footer={false} open={resetModal} onCancel={() => setResetModal(false)}>
        <Form
          layout="vertical"
          onFinish={handleResetForm}
        >
          <Form.Item

            label="New Password"
            name="password"
            style={{ marginBottom: '8px' }}
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
          >
            <Input.Password type="password" iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)} />
          </Form.Item>

          <Form.Item
            label="Confirm Password"
            name="re_password"
            // style={{ marginBottom: '10px' }}
            rules={[
              {
                required: true,
                message: 'Please input your confirm password!',
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Button className="login_signIn_button__zFcg7" style={{ width: '100px', margin: '0 auto' }} htmlType="submit">Submit <Spin spinning={loading} /></Button>
        </Form>
      </Modal>
    </>
  );
};

export default Login;
