import React, { useEffect, useRef, useState } from "react";
import { AiFillCheckCircle } from "react-icons/ai";
import { useNavigate } from "react-router";
import Swal from "sweetalert2";
import "../styles/paymentSegment.css";
import Partners from "../Landing_Page_Components/Partners";
import { ScrollAnimation } from "../Componants/motion/scrollanimation";
import PlansAccordian from "../Componants/PlanAccordian.js";
import {
  Flex,
  Modal,
  Select,
  Form,
  Input,
  InputNumber,
  Button,
  Spin,
} from "antd";
import TextArea from "antd/es/input/TextArea";
import { Images } from "../assests";
import { Box } from "@mui/material";

const Plans = () => {
  const Navigate = useNavigate();
  const [planForm, setPlanForm] = useState({
    durationType: "monthly",
    duration: 1,
    numberOfUsers: 1,
    months: 12,
  });

  const [products, setProducts] = useState([]);
  const [OMPlans, setOMPlans] = useState([])
  const [OMPlanDesc,setOMPlanDesc] = useState([])
  const [isChoosePlan, setIsChoosePlan] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [form] = Form.useForm();
  const [vmButtonState, setVmButtonState] = useState("buyNow");
  const [omButtonState, setOmButtonState] = useState("buyNow");
  const prevVmButtonState = useRef(vmButtonState);
  const prevOmButtonState = useRef(omButtonState);

  useEffect(() => {
    const newForm = { ...planForm };
    if (newForm.durationType === "yearly") {
      newForm.months = 12 * newForm.duration;
    } else {
      newForm.months = newForm.duration;
    }
    setPlanForm(newForm);
  }, [planForm.duration]);

  useEffect(() => {
    handleOMPlans();
    setProducts([
      {
        id: 8,
        created_at: "2024-01-16T16:11:12+05:30",
        product_name: "Oceann VM",
        price: 200,
        product_description:
          "Transform marine operations with our cost-efficient, data-driven voyage management solution",
        status: 1,
        benefits: [
          "All the offerings delivered by Oceann Mail",
          "All the offerings delivered by Oceann Zero",
          "Chartering: Comprehensive management of TC, VC, COA, cargo trade, and assets",
          "Operation: Streamlined vessel, voyage, routing, optimization, and reporting.",
          "Finance: Automated invoice generation and tracking.",
          "Data & Integration",
          "Bunker Management planForm",
          "Port DA Management",
          "Claim and Laytime Management",
          "Tanker Pooling",
          "STS (Ship-to-Ship transfer)",
          "Market Trade",
          "Third-Party API Service",
        ],
        modified_at: null,
        discount: "10.00",
        training_charges: "20.00",
        ATC_adjustment: "0.00",
        minimum_users: 5,
        color: "#0647CD",
      },
      {
        id: 9,
        created_at: "2024-01-16T16:11:12+05:30",
        product_name: "Oceann Mail",
        product_description:
          // "Leverage the power of communication with Market trend and best Voyage estimate",
          "Transforming maritime email management into effortless efficiency and precision.",
        status: 1,
        benefits: [
          "Oceann Mail Communication Suite",
          "Smart Email Processing with AI",
          "AI based Email auto sorting",
          "Market Order and Index",
          "Enhanced Key and tag Mechanism",
          "Data & Integration",
          "Team Skype & whatsapp chat Features",
          "Comprehensive Search Functionality",
          "Cargo-Tonnage Position Dashboard",
          "CharterParty Drafting, storing and e-sign Handling",
          "Comprehensive Port & Ship Directory",
          "Real-time Ship Tracking via AIS",
          "AI based order and Tonnage Screen",
          "Live Bunker past and forward price",
        ],
        modified_at: null,
        discount: "10.00",
        training_charges: "20.00",
        ATC_adjustment: "0.00",
        minimum_users: 5,
        // color: "#06cd10",
        color: "#003e78",
      },
    ]);

  }, []);

  useEffect(() => {
    handleBuyNowButton();
  }, [products])



  useEffect(() => {
    if (vmButtonState !== prevVmButtonState.current || omButtonState !== prevOmButtonState.current) {
      handleBuyNowButton();
      prevVmButtonState.current = vmButtonState;
      prevOmButtonState.current = omButtonState;
    }
  }, [vmButtonState, omButtonState]);

  const handleOMPlans = async() => {
    try {
      const token = localStorage.getItem("oceanMailToken");
      const url = `${process.env.REACT_APP_BASE_URL}accounts/modules-details`;
      const options = {
        method: "GET",
        headers: {},
      };
      const response = await fetch(url, options);
      let res = await response.json();
      if(res) {
        setOMPlans(res)
        setOMPlanDesc(res[0].product_description.split(","))
      }
    } catch (error) {
      console.log(error);
    }
  }
  const handleBuyNowButton = async () => {
    try {
      const token = localStorage.getItem("oceanAllToken");
      // console.log("token", JSON.parse(atob(token.split(".")[1])));
      const email = JSON.parse(atob(token.split(".")[1])).email;
      // console.log("email", email);

      const url = `${process.env.REACT_APP_BASE_URL}accounts/plan-status/${email}`;
      const options = {
        method: "GET",
        headers: {},
        // body: {},
      };
      const response = await fetch(url, options);
      let res = await response.json();
      const responseData = res.data;
      // console.log("responsedata", responseData);


      products.forEach((product) => {
        // console.log("prodcts.id", product.id);
        if (product.id === 8) {
          if (!responseData?.VM_ACCESS) {
            if (responseData?.VM_STATUS === "Pending" || responseData?.VM_STATUS === "Processing") {
              setVmButtonState("myProfile");
            } else if (responseData?.VM_STATUS === "Approved") {
              setVmButtonState("subscribe");
            } else if (responseData?.VM_STATUS === "Rejected") {
              setVmButtonState("buyNow");
            }
          } else if (responseData?.VM_ACCESS) {
            if (responseData?.VM_STATUS === "Approved") {
              setVmButtonState("subscribe");
            }
          }
        }

        if (product.id === 9) {
          if (!responseData?.MAIL_ACCESS) {
            if (responseData?.MAIL_STATUS === "Pending" || responseData?.MAIL_STATUS === "Processing") {
              setOmButtonState("myProfile");
            } else if (responseData?.MAIL_STATUS === "Approved") {
              setOmButtonState("subscribe");
            } else if (responseData?.MAIL_STATUS === "Rejected") {
              setOmButtonState("buyNow");
            }
          } else if (responseData?.MAIL_ACCESS) {
            if (responseData?.MAIL_STATUS === "Approved") {
              setOmButtonState("subscribe");
            }
          }
        }
      });
    } catch (error) {
      console.log(error);
    }
  };

  const token = localStorage.getItem("oceanAllToken");

  if (token) {
    try {
      var userData = JSON.parse(atob(token.split(".")[1]));
    } catch (error) {
      console.error("Error parsing token:", error);
    }
  } else {
    console.log("Token does not exist.");
  }

  const onFinish = async (values) => {
    console.log("Received values:", values);
    const url = `${process.env.REACT_APP_BASE_URL}accounts/client-request`;
    try {
      setIsLoading(true);
      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(values),
      });

      const data = await response.json();

      if (response.ok) {
        Swal.fire({
          title: "Success!",
          text: data.msg,
          icon: "success",
          confirmButtonText: "Close",
        });
        form.resetFields();
        setIsChoosePlan(false);
        setIsLoading(false);
        handleBuyNowButton()
      }
    } catch (err) {
      setIsLoading(false);
      console.error(err);
    }
  };

  const handleChoosePlan = (planName, productId) => {
    setSelectedProduct(planName);
    setIsChoosePlan(true);
    form.setFieldsValue({ plan_name: planName });
  };

  const redirectToProfile = () => {
    Navigate("/profile")
  }

  return (
    <>
      <div className="my-[8rem]">
        <div className="m-5 md:m-10 mx-[6rem]">
          <h1 className="text-md text-center sm:text-md md:text-xl font-bold ">
            Explore Our Plans
          </h1>
          <p className="text-xs text-center sm:text-sm md:text-md font-light">
            Custom Crafting Solutions to Meet Your Distinctive Needs
          </p>
        </div>
        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-2 gap-4 w-[90%] mx-auto h-auto">
          {products.map((product, index) => (
            <div
              key={product.id}
              className="shadow-2xl border rounded-xl h-fit"
            >
              <h1
                style={{ backgroundColor: product.color }}
                className={
                  index % 2 === 0
                    ? "text-sm text-center text-white sm:text-md md:text-lg font-semibold p-4 rounded-t-xl"
                    : "text-sm text-center text-white sm:text-md md:text-lg font-semibold p-4 rounded-t-xl"
                }
              >
                {product.product_name}
              </h1>
              <div className="p-4 gap-4">
                <p className="text-xs text-left mx-auto sm:text-sm md:text-md font-light w-[90%]">
                  <b>{product.product_description}</b>
                </p>
                {product.product_name === 'Oceann Mail' ? (<div className="p-8 flex flex-col gap-2">
                  {OMPlanDesc.map(part => <div className="flex flex-row gap-2"><div className="flex items-center"><AiFillCheckCircle className="text-[#183165]" /></div><div className="flex items-center">{part?.split(':')[0]?.trim()}</div></div>)}
                </div>):(null)}
                <div className="flex flex-col gap-2 md:gap-4 mt-2 md:mt-4 text-xs sm:text-sm lg:text-md xl:text-md justify-center w-[90%] mx-auto">
                  
                {
                  product.product_name === 'Oceann VM' ? (
                    <>
                      <hr className="m-2 md:m-3" />
                      {product.benefits && product.benefits.map((item) => (
                        <div className="flex gap-2" key={item}>
                          <p style={{ width: "30px" }}>
                            <AiFillCheckCircle className="text-[#183165]" />
                          </p>
                          <p style={{ width: "85%" }}>{item}</p>
                        </div>
                      ))}
                    </>
                  ) : product.product_name === 'Oceann Mail' ? (
                    <Box > 
                      {OMPlans.map((plan) => (
                        <div key={plan.id} className="gap-2 flex flex-col">
                          <PlansAccordian name={plan.product_name} description={OMPlanDesc} minUsers={plan.minimum_users} price={plan.product_price_per_user} id={plan.id} /> {/*Test */}
                        </div>
                      ))}
                    </Box>
                  ) : null // Optional else case when neither condition is met
                }
                  {product.id === 8 ? (
                    vmButtonState === "buyNow" ? (
                      <button
                        className="bg-[#F39C12] ml-2 p-2 text-white w-[100%] rounded-sm my-[1.5rem]"
                        onClick={() =>
                          handleChoosePlan(product.product_name, product.id)
                        }
                      >
                        Buy Now
                      </button>
                    ) : vmButtonState === "myProfile" ? (
                      <button onClick={redirectToProfile} className="bg-[yellow] ml-2 p-2 text-black w-[100%] rounded-sm my-[1.5rem]">
                        My Profile
                      </button>
                    ) : (
                      <button
                        className="bg-[green] ml-2 p-2 text-white w-[100%] rounded-sm my-[1.5rem]"

                      >
                        Subscribed
                      </button>
                    )
                  ) : product.id === 9 ? (
                    omButtonState === "buyNow" ? (
                      <button
                        className="bg-[#FF8134] ml-2 p-2 text-white w-[100%] rounded-sm my-[1.5rem]"
                        onClick={() =>
                          handleChoosePlan(product.product_name, product.id)
                        }
                      >
                        Buy Now
                      </button>
                    ) : omButtonState === "myProfile" ? (
                      <button onClick={redirectToProfile} className="bg-[yellow] ml-2 p-2 text-black w-[100%] rounded-sm my-[1.5rem]">
                        My Profile
                      </button>
                    ) : (
                      <button
                        className="bg-[green] ml-2 p-2 text-white w-[100%] rounded-sm my-[1.5rem]"

                      >
                        Subscribed
                      </button>
                    )
                  ) : null}
                </div>
              </div>
            </div>
          ))}
        </div>

        <ScrollAnimation>
          <div className="my-[1rem] mx-[1rem] max-sm:mx-[1rem]">
            <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-5 md:mt-40 ">
              <span className="lg:text-2xl">Trusted collaborators</span>
            </h1>
            <Partners />
          </div>
        </ScrollAnimation>
      </div>
      <Modal
        centered
        className="get_quote_form_box"
        width="80%"
        open={isChoosePlan}
        onCancel={() => setIsChoosePlan(false)}
        footer={false}
      >
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <Form
            style={{
              background: "#FFF",
            }}
            form={form}
            name="basic"
            layout="vertical"
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
            initialValues={{
              plan_name: selectedProduct,
              min_users: 5,
              required_no_users: 10,
              contact_name: userData?.first_name ? userData?.first_name : "",
              contact_number: userData?.phone_number
                ? userData?.phone_number
                : "",
              email: userData?.email ? userData?.email : "",
              company_name: userData?.company_name ? userData?.company_name : "",
            }}
            onFinish={onFinish}
            autoComplete="off"
          >
            <h1
              style={{ fontSize: "20px", marginBottom: "16px" }}
              className="text-md text-center mb-0 font-semibold"
            >
              <u>Get a Quote</u>
            </h1>
            <Form.Item
              label="Product Name"
              name="plan_name"
              rules={[
                { required: true, message: "Please select the plan name!" },
              ]}
            >
              <Select
                placeholder="Choose Product"
                onChange={(value) => setSelectedProduct(value)}
              >
                {products.map((product) => (
                  <Select.Option value={product.product_name} key={product.id}>
                    {product.product_name}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>

            {selectedProduct === "Oceann Mail" && (
              <Form.Item
                label="No. of employees."
                name="no_of_emails"
              >
                <InputNumber
                  accept="number"
                  placeholder="0"
                  min={1}
                  style={{ width: "100%", color: "black" }}
                />
              </Form.Item>
            )}

            <Form.Item
              label="Company Name"
              name="company_name"
              rules={[
                { required: true, message: "Please input the company name!" },
              ]}
            >
              <Input placeholder="Enter company name" />
            </Form.Item>

            <Form.Item
              label="Contact Name"
              name="contact_name"
              rules={[
                { required: true, message: "Please input the contact name!" },
              ]}
            >
              <Input placeholder="Enter contact name" />
            </Form.Item>

            <Form.Item
              label="Contact Number"
              name="contact_number"
              rules={[
                { required: true, message: "Please input the contact number!" },
              ]}
            >
              <Input placeholder="Enter contact number" />
            </Form.Item>

            <Form.Item
              label="Email"
              name="email"
              rules={[
                {
                  required: true,
                  message: "Please input the email address!",
                  type: "email",
                },
              ]}
            >
              <Input placeholder="Enter contact email" />
            </Form.Item>

            <Flex style={{ width: "100%" }}>
              <Form.Item
                label="Required No. of Users"
                name="required_no_users"
                style={{ width: "50%" }}
                rules={[
                  {
                    required: true,
                    message: "Please input the required number of users!",
                  },
                ]}
              >
                <InputNumber
                  placeholder="0"
                  style={{ width: "100%" }}
                  min={1}
                />
              </Form.Item>

              <Form.Item
                label="Minimum Users"
                name="min_users"
                style={{ marginLeft: "10px", width: "50%" }}
                rules={[
                  {
                    required: true,
                    message: "Please input the minimum number of users!",
                  },
                ]}
              >
                <InputNumber
                  disabled
                  placeholder="0"
                  min={1}
                  style={{ width: "100%", color: "black" }}
                />
              </Form.Item>
            </Flex>

            <Form.Item label="Message" name="message">
              <TextArea placeholder="Write any message for us." />
            </Form.Item>

            <Form.Item wrapperCol={{ span: 24 }}>
              <Button
                style={{ width: "100%", minHeight: "45px", marginTop: "10px" }}
                className="bg-[#F39C12] text-[1rem] text-white  max-sm:text-[0.6rem] py-2 px-6 max-sm:px-2 shadow-md focus:outline-none"
                type="primary"
                htmlType="submit"
              >
                {!isLoading ? "Submit" : <Spin />}
              </Button>
            </Form.Item>
          </Form>

          <div className="qute_tagline">
            <div
              style={{ width: "350px", margin: "0 auto", marginTop: "60px" }}
            >
              <img src={Images + "Oceann_Logo_transp.png"} alt="" />
            </div>
            <p style={{ marginTop: "18px" }}>
              <b>
                Get Your Custom Quote for Cutting-Edge Maritime Solutions -
                Tailored Voyage Management and Advanced Mailing Services for the
                Maritime Industry.
              </b>
            </p>
          </div>
        </div>
      </Modal>
    </>
  );
};

export default Plans;
