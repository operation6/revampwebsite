import React from "react";
// import oceannZeroimg1 from "../Componants/Assets/OceannZero-img/oceanzero-img1.png";
// import oceannZeroimg2 from "../Componants/Assets/OceannZero-img/oceanzero-img2.png";
// import oceannZeroimg3 from "../Componants/Assets/OceannZero-img/oceanzero-img3.png";
// import oceannZeroimg4 from "../Componants/Assets/OceannZero-img/oceanzero-img4.png";
// import OurServicesicon1 from "../Componants/Assets/OceannZero-img/OurServices-icon1.png";
// import OurServicesicon2 from "../Componants/Assets/OceannZero-img/OurServices-icon2.png";
// import OurServicesicon3 from "../Componants/Assets/OceannZero-img/OurServices-icon3.png";
// import OurServicesicon4 from "../Componants/Assets/OceannZero-img/OurServices-icon4.png";
// import OurServicesicon5 from "../Componants/Assets/OceannZero-img/OurServices-icon5.png";
// import featureicon1 from "../Componants/Assets/OceannZero-img/feature-icon1.png";
// import featureicon2 from "../Componants/Assets/OceannZero-img/feature-icon2.png";
// import featureicon3 from "../Componants/Assets/OceannZero-img/feature-icon3.png";
// import featureicon4 from "../Componants/Assets/OceannZero-img/feature-icon4.png";
// import featureicon5 from "../Componants/Assets/OceannZero-img/feature-icon5.png";
// import featureicon6 from "../Componants/Assets/OceannZero-img/feature-icon6.png";
// import co2img from "../Componants/Assets/OceannZero-img/co2img.png";
import nature from "../Componants/Assets/OceannZero-img/nature.png";
import Oceannzerocard from "../Componants/OceannZero-Components/Oceannzerocard";
import { Link } from "react-router-dom";
import { navigator, useNavigate } from "react-router-dom";
import { motion } from "framer-motion";
import Animatedwordchar from "../Componants/motion/animatewordchar";
import {
  ScrollAnimation,
  animationVariants,
} from "../Componants/motion/scrollanimation";
import Partners from "../Landing_Page_Components/Partners";
import { Images } from "../assests";
import { BsAndroid2 } from "react-icons/bs";
import { FaApple, FaWindows } from "react-icons/fa";
import { GrAppleAppStore } from "react-icons/gr";
import { IoLogoGooglePlaystore } from "react-icons/io5";
import { IoLogoAndroid } from "react-icons/io";
import { MdOutlineLaptopWindows } from "react-icons/md";
let co2img = Images + 'co2img.png';
let featureicon6 = Images + 'feature-icon6.png';
let featureicon5 = Images + 'feature-icon5.png';
let featureicon4 = Images + 'feature-icon4.png';
let featureicon3 = Images + 'feature-icon3.png';
let featureicon2 = Images + 'feature-icon2.png';
let featureicon1 = Images + 'feature-icon1.png';
let OurServicesicon5 = Images + 'OurServices-icon5.png';
let OurServicesicon4 = Images + 'OurServices-icon4.png';
let OurServicesicon3 = Images + 'OurServices-icon3.png';
let OurServicesicon2 = Images + 'OurServices-icon2.png';
let OurServicesicon1 = Images + 'OurServices-icon1.png';
let oceannZeroimg4 = Images + 'oceanzero-img4.png';
let oceannXimg2 = Images + 'oceann_x+_website_image.png';
let oceannXimg1 = Images + 'oceannx-background.png';
let oceannXLogo = Images + 'oceannX-design.png';

const randomColors = [
  "#F8F9FA", 
  "#E3F2FD",
  "#E8F5E9", 
  
  "#F5F5F5",
  "#ECEFF1", 
  "#FAFAFA", 
  "#D7CCC8", 
  "#CFD8DC",
  "#F1F8E9",
  "#E1F5FE"
];


const getRandomColor = () => {
  return randomColors[Math.floor(Math.random() * randomColors.length)];
};
const lightenColor = (hex, percent) => {
  let num = parseInt(hex.replace("#", ""), 16);
  let amt = Math.round(2.55 * percent);
  let r = (num >> 16) + amt;
  let g = ((num >> 8) & 0x00ff) + amt;
  let b = (num & 0x0000ff) + amt;

  return `rgb(${Math.min(r, 255)}, ${Math.min(g, 255)}, ${Math.min(b, 255)})`;
};

const goToProductsPart = (className) => {
    const element = document.querySelector(`.${className}`);
    if (element) {
        element.scrollIntoView({ behavior: "smooth", block: "start" });
    }
}

const OceannX = () => {
  const navigator = useNavigate();
  const card = [
    {
      paraicon: OurServicesicon1,
      paraTitle: "Regulatory & Environmental Compliance",
      para: "EU ETS, FuelEU Regulatory Reporting IMO DCS, EU & UK MRV",
    },

    {
      paraicon: OurServicesicon2,
      paraTitle: "Fleet Performance Analytics",
      para: "Fleet overview with performance indicators fuel model visualisation performance optimization",
    },

    {
      paraicon: OurServicesicon3,
      paraTitle: "Post Voyage Analysis",
      para: "Vessel performance monitoring and its management, decarbonisation solutions,analyses speed & consumption .",
    },

    {
      paraicon: OurServicesicon4,
      paraTitle: "Hull Performance Monitoring",
      para: "Hull degradation monitoring real time hull fouling risk alerts.",
    },
    {
      paraicon: OurServicesicon5,
      paraTitle: "Emission Optimization",
      para: "Alternative fuels, technologies and maintenance and engine tuning",
    },
  ];
 const products = [
    {
        icon:<FaApple fontSize={24} />,
        TopIcon: 'https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/apple+store+icon.png',
        color:'#0FBAFC',
        link:'https://apps.apple.com/us/app/oceann-x/id6737811559',
        title: 'Apple',
        headline:'Oceann X IOS Mailing System – Reliable, Secure, and Seamless Maritime Communication Anywhere at Sea!'
    },
    {
        icon:<IoLogoAndroid fontSize={24} />,
        TopIcon: 'https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/google-play-store-logo.png',
        color:'#a4c639',
        link:'https://play.google.com/store/apps/details?id=com.theoceann.app.oceannapp&hl=en',
        title: 'Android',
        headline:'Oceann X Android Application – Navigate Smarter, Communicate Seamlessly!'
    },
    {
        icon: <FaWindows fontSize={24} />,
        color:'#357EC7',
        link:'https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/oceannX.exe',
        title: 'Windows',
        headline:'Oceann X Windows Application – Smart Maritime Communication, Open in Windows, Easy and Simple UI, Anytime, Anywhere!'
    }
 ]
  const benefit = [
    {
      img: featureicon1,
      heading: "Real-time vessel tracking 📍",
    },

    {
      img: featureicon2,
      heading: "AI-based insights for maritime operations ⚡",
    },

    {
      img: featureicon3,
      heading: "Secure messaging and communication 💬",
    },
    {
      img: featureicon4,
      heading: "Industry news & updates 🌍",
    },
    {
      img: featureicon5,
      heading: " Easy integration with Oceann AI services 🔗",
    },
    // {
    //   img: featureicon6,
    //   heading: "Machine Learning For Anomaly Detection",
    // },
  ];
  const unique = [
    {
      img: '',
      head: "Maritime-Optimized Email",
      heading: "Maritime-Optimized Email System",
      content: "Effortlessly handle high-volume email transactions with smart tagging, automated sorting, and AI-driven best matches for efficient workflow management."
    },
    
    {
      img: '',
      head: "AI-Powered Assistance",
      heading: "AI Chatbot for Instant Assistance",
      content: "Leverage an AI-powered assistant to quickly access voyage details, vessel positions, cargo tracking, and port insights—all within your communication hub."
    },
    
    {
      img: '',
      head: "Team Collaboration",
      heading: "Internal Chat & Groups",
      content: "Enhance team collaboration with real-time internal chat and group discussions, ensuring faster decision-making without switching platforms."
    },
    
    {
      img: '',
      head: "Smart Email Management",
      heading: "Smart Tags for Email Categorization",
      content: "Organize emails effectively with intelligent tagging, allowing you to filter and prioritize key messages with ease."
    },
    
    {
      img: '',
      head: "Live Vessel Tracking",
      heading: "Live Vessel Search & Tracking",
      content: "Access real-time vessel positions and details to monitor fleet movements and enhance operational efficiency."
    },
    
    {
      img: '',
      head: "Route Optimization",
      heading: "Port-to-Port Distance Calculator",
      content: "Quickly calculate the most efficient shipping routes with accurate port-to-port distance mapping and estimated voyage duration."
    },
    
    {
      img: '',
      head: "Contact Management",
      heading: "Comprehensive Contact Management",
      content: "Maintain an organized contact database for brokers, charterers, shipowners, and operators, enabling seamless communication and networking."
    },
    
    {
      img: '',
      head: "Advanced Analytics & Reporting",
      heading: "Powerful analytics to track performance,",
      content: "Gain valuable insights with detailed analytics and reporting tools, helping you track performance metrics, optimize operations, and make data-driven decisions."
    },
    
    {
      img: '',
      head: "AI-Driven Insights",
      heading: "Best Matches for Smarter Decision-Making",
      content: "Oceann X's AI-driven Best Match feature ensures that you instantly find the most relevant chartering opportunities, vessels, and cargo matches based on historical data and market insights."
    }
    
  ];

  return (
    <>
      <div>
        <div className="relative lg:h-[100vh] h-[65vh]">
          <img
            src={oceannXimg1}
            alt="Description of the"
            className="w-full h-full object-cover object-center absolute top-0 left-0 z-0 max-md:object-left max-lg:object-right max-xl:object-left"
          />
          <img
            src={oceannXLogo}
            alt="Oceann X Image"
            className="h-56 w-56 object-contain absolute top-24 left-16 max-xxxs:top-20 max-xxs:top-28 max-md:bottom-[0%] max-md:left-[6%]  max-lg:top-[62%] max-lg:left-[68%] max-xl:top-[4%] max-xl:left-20 max-2xl:left-16 max-2xl:top-16 z-50 "
          />
          <div className="relative z-10 flex flex-col items-start justify-center h-full text-center bg-[#00000083] p-16 max-md:p-8">
            <div className="leading-tight text-[24px] md:text-[40px] lg:text-xl text-white font-bold text-left max-xxxs:mt-56 max-xxs:mt-56">
              <Animatedwordchar text="Revolutionizing Maritime Communication" />
            </div>
            <p className="text-sm mt-3 sm:text-sm md:text-md w-[95%] md:w-[60%] text-white text-left">
              Seamless, Smart, and Secure Email Solutions.
            </p>
            <div>
              <motion.button
                onMouseOver={(e) => (e.target.style.color = "#ffffff")}
                onClick={() => goToProductsPart('products')}
                whileHover={{ scale: 1.04, backgroundColor: "#003e78" }}
                className="bg-[#f39c12] flex flex-row items-center gap-1 text-white border-[2px] px-[24px] py-[8px] mt-4 rounded-[42px] border-none"
              >
                Get Started
              </motion.button>
            </div>
          </div>
        </div>


        {/* Partner section */}
        <ScrollAnimation>
                  <section className="flex flex-col md:gap-6 my-[3rem] lg:my-[6rem] items-center mx-[2rem] lg:mx-[6rem] xl:mx-[12rem] xl:gap-[4rem]">
                    <div className="flex justify-start items-center gap-1 md:mt-[0.2rem]">
                      <h3 className="text-[24px] xl:text-[3rem] leading-9 max-sm:text-center max-sm:mt-0 text-left  font-semibold ">
                        Oceann X
                      </h3>
                    </div>
                    <div className="flex flex-wrap max-sm:flex-col justify-between max-sm:items-start max-md:mt-10 gap-[2rem] lg:gap-[3rem]">
                      <div className="flex flex-1 object-cover">
                        <img
                          src={oceannXimg2}
                          className="object-cover rounded-md "
                          alt=""
                        />
                      </div>
                      <div className="flex flex-col items-center md:items-start flex-1 max-sm:w-full max-sm:ml-0">
                        <h3 className="text-md xl:text-[2.2rem] leading:tight xl:leading-[2.75rem] max-sm:text-center max-sm:mt-2 text-left sm:text-lg md:text-lg font-semibold gap-4 text-[#003E78]">
                        Oceann X – Experience Maritime Communication Like Never Before
                        </h3>
                        <p className="mt-3 lg:text-[16px] max-sm:text-center text-black max-sm:text-sm">
                        Oceann X is an advanced maritime communication and management platform designed to streamline operations, enhance collaboration, and deliver real-time insights for shipping professionals. Built with powerful automation, AI-driven tools, and seamless integrations, Oceann X ensures that every message, vessel, and business decision is connected efficiently. Designed for scalability, Oceann X adapts to evolving maritime needs, fostering efficiency and sustainability across global shipping networks.
                        </p>
                        
                        <div>
                          <motion.button
                            style={{}}
                            onMouseOver={(e) => (e.target.style.color = "#ffffff")}
                            onClick={() => goToProductsPart('products')}
                            whileHover={{ scale: 1.04, backgroundColor: "#003e78" }}
                            className=" bg-[#f39c12] text-white px-[24px] mt-4 py-[8px] rounded-[42px] "
                          >
                            Learn More
                          </motion.button>
                        </div>
                      </div>
                    </div>
                  </section>
                </ScrollAnimation>
        {/* Partner section end */}

        <div>

          <section>
            <ScrollAnimation>
              <div className="my-[3.5rem] lg:my-[4rem] bg-[#ecf0ff]  py-[2rem]">
                <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
                  <span className="text-[24px] lg:text-[32px]">Features</span>
                </h1>
                <div className="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 mx-[24px] my-[1rem] lg:my-[3rem] xl:mx-[6.4rem] xl:gap-8  ">
                  {benefit.map((data) => (
                    <div
                      style={{
                        borderColor: "#e0e0e0",
                        borderWidth: "1px",
                      }}
                      className="border-[2px] relative flex flex-col justify-items-center justify-center py-6 px-6 rounded-sm hover:shadow-xl overflow-hidden section-2-cart "
                    >
                      <div className="p-[4px]">
                        <img
                          src={data.img}
                          alt=""
                          className="h-[32px] md:h-[48px] bg-[#ffffea] border-[1px] border-[#F39C12] lg:rounded-xl rounded-md p-[8px]"
                        />
                      </div>
                      <div className="flex flex-col mt-2">
                        <h1 className="text-md sm:text-[14px] md:text-[20px] font-semibold my-2">
                          {data.heading}
                        </h1>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </ScrollAnimation>
          </section>
        </div>
        <div>
        <section>
                  <ScrollAnimation>
                    <div className="my-[3.5rem] lg:my-[4rem]">
                      <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
                        <span className="text-[24px] lg:text-xl">
                          What makes Oceann X Unique?
                        </span>
                      </h1>
                      <div className="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 mx-[24px] my-[1rem] lg:my-[3rem] xl:mx-[8rem] xl:gap-8 ">
                        {unique.map((data) => (
                          <div
                            style={{
                              borderColor: "#e0e0e0",
                              borderWidth: "1px",
                              backgroundColor:getRandomColor()
                            }}
                            onMouseEnter={(e) => (e.currentTarget.style.backgroundColor = lightenColor(getRandomColor(), 20))}
                            onMouseLeave={(e) => (e.currentTarget.style.backgroundColor = getRandomColor())}
                            className="border-[2px] relative flex flex-col justify-items-center justify-center py-10 px-6 rounded-md hover:shadow-xl overflow-hidden section-2-cart "
                          >
                            {/* <div className=" p-[4px] ">
                              <img src={data.img} alt="" className="rounded-xl" />
                            </div> */}
                            <div className="flex flex-col mt-2">
                              <p className="text-[14px] font-bold underline">{data.head}</p>
                              <h1 className="text-md sm:text-[14px] md:text-[24px] text-[#0e0d0d] font-semibold my-2">
                                {data.heading}
                              </h1>
                              <p className="text-[14px] sm:text-sm md:text-[15px] text-[#747070] ">
                                {data.content}
                              </p>
                            </div>
                          </div>
                        ))}
                      </div>
                    </div>
                  </ScrollAnimation>
                </section>
          <section>
              <div className=" products my-[3.5rem] lg:my-[4rem] bg-[#ecf0ff]  py-[2rem]">
                <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
                  <span className="text-[24px] lg:text-[32px]">Products</span>
                </h1>
                <h1 className="text-md mb-0 text-center sm:text-md md:text-lg font-light md:mt-2 bg-[#161616] text-white ">
                Stay Connected – Anytime, Anywhere
                </h1>
                
                <div className="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 mx-[24px] my-[1rem] lg:my-[3rem] xl:mx-[6.4rem] xl:gap-8  ">
                  {products?.map((data) => (
                    <div
                      style={{
                        borderColor: "#e0e0e0",
                        borderWidth: "1px",
                      }}
                      className="border-[2px] relative flex flex-col items-center justify-center py-6 px-6 rounded-sm hover:shadow-xl overflow-hidden section-2-cart "
                    >
                      <Link to={data?.link}>
                        <div className="relative flex flex-col items-center justify-center rounded-sm hover:shadow-xl overflow-hidden  " 
                        // style={{
                        //   backgroundColor:data?.color,
                        // }}
                      >
                        {data?.title === 'Windows' ? (
                          <FaWindows fontSize={48} color="#357EC7" />
                        ) : (
                          <img src={data?.TopIcon} alt={data?.title} className="h-14 w-14 mix-blend-multiply" />
                        )}


                           
                        </div>
                      </Link>
                      <div className="w-full p-4 text-center text-md font-medium">{data.headline}</div>

                      <a href={data?.link} target="_blank" rel="noopener noreferrer">
                        <div>
                          <motion.button
                            style={{}}
                            onMouseOver={(e) => (e.target.style.color = "#ffffff")}
                            onClick={() => goToProductsPart('products')}
                            whileHover={{ scale: 1.04, backgroundColor: "#003e78" }}
                            className=" bg-[#050505] flex flex-row items-center gap-1 text-white border-[2px] px-[24px] mt-4 py-[8px] rounded-[42px] border-none"
                          >
                            {data?.icon} {data?.title}
                          </motion.button>
                        </div>
                      </a>
                    </div>
                  ))}
                </div>
              </div>
          </section>
        </div>
      </div>
    </>
  );
};

export default OceannX;
