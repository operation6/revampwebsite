import React from "react";
import Animatedwordchar from "../Componants/motion/animatewordchar";
import { ScrollAnimation } from "../Componants/motion/scrollanimation";
import { useKeenSlider } from "keen-slider/react";
import "keen-slider/keen-slider.min.css";

const Mentors = () => {
  const ImgURL = `https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/guidence-image.jpeg`;
  const MentorsImg = [
    "https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/White+and+Green+Simple++Professional+Business+Project+Presentation.png",
    "https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/_Mentor+Peter+Andersen+.png",
    "https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/Mentor+Mr.+Anurag+Singhal.png",
    "https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/Mentor+Mr.+Dhee.png",
    "https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/ajay-reshamwala.jpeg",
  ];

  const [sliderRef] = useKeenSlider(
    {
      loop: true,
    },
    [
      (slider) => {
        let timeout;
        let mouseOver = false;

        function clearNextTimeout() {
          clearTimeout(timeout);
        }

        function nextTimeout() {
          clearTimeout(timeout);
          if (mouseOver) return;
          timeout = setTimeout(() => {
            slider.next();
          }, 2000);
        }

        slider.on("created", () => {
          slider.container.addEventListener("mouseover", () => {
            mouseOver = true;
            clearNextTimeout();
          });
          slider.container.addEventListener("mouseout", () => {
            mouseOver = false;
            nextTimeout();
          });
          nextTimeout();
        });
        slider.on("dragStarted", clearNextTimeout);
        slider.on("animationEnded", nextTimeout);
        slider.on("updated", nextTimeout);
      },
    ]
  );

  return (
    <>
      <main>
        <div
          className="h-[50vh] lg:h-[100vh] flex items-center bg-[#00000083] bg-blend-darken mt-[66px]"
          style={{
            backgroundImage: `url(${ImgURL})`,
            backgroundSize: "cover",
            // backgroundPosition: "center",
          }}
        >
          <div className="oceann-vm-section1 flex flex-col gap-[8px]">
            <h1 className="text-center text-[20px] leading-tight text-white  md:text-[3.75rem]">
              <Animatedwordchar text="Our Mentors & Advisors" />
            </h1>
          </div>
        </div>
      </main>


      {/* Mentor Section */}
      <ScrollAnimation>
        <div className="meetTeams">
          <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-10">
            <span className="lg:text-2xl">Our Mentors & Advisors</span>
          </h1>
          <section className="mt-[4rem] p-8" style={{ backgroundColor: "#efefef" }}>
            {/* <div ref={sliderRef} className="keen-slider">
              {MentorsImg.map((img, index) => (
                <div key={index} className="keen-slider__slide flex justify-center items-center">
                  <img
                    src={img}
                    alt={`Mentor ${index + 1}`}
                    style={{
                      width: "80%", // Adjust as needed
                      height: "auto",
                      borderRadius: "20px", // Rounded corners for images
                      boxShadow:'2px 2px 10px black'
                    }}
                  />
                </div>
              ))}
            </div> */}
            <h2
              style={{ lineHeight: "8rem" }}
              className="lg:font-extrabold sm:text-2xl lg:text-6xl xl:text-[6rem] xl:leading-[4.5rem] mb-4 break-words bg-gradient-to-r from-[#28435c] via-[#2076c7] to-[#003e78] bg-clip-text text-transparent text-4xl items-center justify-center flex "
            >
              <Animatedwordchar text="Coming Soon..." />
            </h2>
          </section>
        </div>
      </ScrollAnimation>
    </>
  );
};

export default Mentors;
