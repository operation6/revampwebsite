import React from "react";
import product_img_1 from "../Componants/Assets/product_page_images/productImg.png";
// import checkbox from "../Componants/Assets/product_page_images/checkbox.png";
import { useInView } from "react-intersection-observer";
import { Card } from "../Componants/productComponents/Card";
import { Macbook } from "../Componants/MacBook";
import { useNavigate } from "react-router-dom";
import Animatedwordchar from "../Componants/motion/animatewordchar";
import "../index";
import { motion } from "framer-motion";
import { FiArrowRight } from "react-icons/fi";

import { CoreSolution } from "../Componants/CoreSolution/CoreSolution";
import { ScrollAnimation } from "../Componants/motion/scrollanimation";
import Partners from "../Landing_Page_Components/Partners";
import { Images } from "../assests";
let checkbox = Images + 'checkbox.png'
export const Product = () => {
  const { ref, inView } = useInView({
    triggerOnce: true,
    rootMargin: "0px",
  });
  const Navigate = useNavigate();


  const card = [
    {
      // img: oceannAI_Img8,
      color: "#FF8A00",
      title: "Chartering",
      content: "Discover our all-in-one maritime solutions for success through innovation and data excellence.",
      path: "/product/chartering"

    },

    {
      // img: oceannAI_Img9,
      color: "#3200F9",
      title: "Operation",
      content: "Enhance maritime operations, optimize routes & ensure successful voyages with our comprehensive solutions.",
      path: "/product/operations"


    },

    {
      // img: oceannAI_Img10,
      color: "#8F00FF",
      title: "Finance",
      content: "Enhance voyage fiscal processes and ensure the protection of financial success.",
      path: "/product/oceann-finance"


    },
    {
      // img: oceannAI_Img11,
      color: "#00B728",
      title: "Analysis",
      content: "Gain deep insights into your maritime financial performance with our Voyage Financial Report.",
      path: "/product/oceann-analytics"


    },
    {
      // img: oceannAI_Img12,
      color: "#D10000",
      title: "Report",
      content: "Welcome to our Report & Analytics section, a dedicated space designed to equip shipping companies.",
      path: "/product/oceann-analytics"



    },
    {
      // img: oceannAI_Img13,
      color: "#0088C3",
      title: "Mail",
      content: "AI-powered email solution streamlines communication, ensuring that shipping operations run more smoothly and efficiently.",
      path: "/solution/oceann-mail"


    }
  ]

  const cardContent = [
    {
      color: "#1B6879",
      bgColor: "#40DDFF87",
      title: "Chartering",
      detail:
        "Discover our all-in-one maritime solutions for success through innovation and data excellence.",
      page: "chartering",
      path: "/product/chartering"
    },
    {
      color: "#108731",
      bgColor: "#40FF75B5",
      title: "Operations",
      detail:
        "Enhance maritime operations, optimize routes & ensure successful voyages with our comprehensive solutions.",
      page: "operations",
      path: "/product/operations"
    },
    {
      color: "#932481",
      bgColor: "#FF40E070",
      title: "Finance",
      detail:
        "Enhance voyage fiscal processes and ensure the protection of financial success.",
      page: "finance",
      path: "/product/oceann-finance"
    },
    {
      color: "#1D7958",
      bgColor: "#40FFBAE8",
      title: "Analysis",
      detail:
        "Gain deep insights into your maritime financial performance with our Voyage Financial Report.",
      page: "analysis",
      path: "/product/oceann-analytics"
    },
    {
      color: "#8EA025",
      bgColor: "#E4FF407A",
      title: "Report",
      detail:
        "Welcome to our Report & Analytics section, a dedicated space designed to equip shipping companies.",
      page: "report",
      path: "/product/oceann-analytics"
    },
    {
      color: "#1B6879E3",
      bgColor: "#40DDFF87",
      title: "Mail",
      detail:
        "AI-powered email solution streamlines communication, ensuring that shipping operations run more smoothly and efficiently.",
      page: "Mail",
      path: "/solution/oceann-mail"
    },
  ];

  const core = [
    { heading: "Pre Trade Intelligence" },
    { heading: "Decarbonization" },
    { heading: "Chartering" },
    { heading: "Operation" },
    { heading: "Finance" },
    { heading: "Analytics" },
  ]


  return (
    <>
      {/* <div
        ref={ref}
        className="h-[85vh] md:h-[100vh] w-full flex justify-center items-center relative"
      >
        {inView && (
          <img
            src={product_img_1}
            alt=""
            className="absolute w-full h-[85vh] md:h-[100vh] left-0 top-0 z-0"
          />
        )}

        <div className="oceann-vm-section1 flex flex-col gap-[8px]">
          <div className="text-center text-6xl">
            <Animatedwordchar text="Advanced Solutions for Maritime Excellence" />
          </div>
          <div className="font-medium ">
           <p>  Accelerate your digital journey and navigate the realities of your
            business with a market-leading commercial platform and high-integrity
            insights</p>
          </div>
        </div>


      
      </div> */}

      <main>
        <div
          className="h-[50vh] lg:h-[100vh] flex items-center bg-[#00000071] bg-blend-darken"
          style={{
            backgroundImage: `url(${product_img_1})`,
            backgroundSize: "cover",
          }}
        >
          <div className="oceann-vm-section1 flex flex-col gap-[8px]">
            <div className="text-center text:20px md:text-[3.75rem] leading-tight">
              <Animatedwordchar text="Advanced Solutions for Maritime Excellence" />
            </div>
            <div className="text-sm md:font-medium text-center">
              <p>Accelerate your digital journey and navigate the realities of your
                business with a market-leading commercial platform and high-integrity
                insights</p>
            </div>
          </div>
        </div>
      </main>

      {/* Partner section */}
      <ScrollAnimation>
        <div className="my-[1rem] mx-[1rem] max-sm:mx-[1rem]">
          <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-10 ">
            <span className="lg:text-2xl">Trusted collaborators</span>
          </h1>
          <Partners />
        </div>
      </ScrollAnimation>
      {/* Partner section end */}


      <div className="flex flex-col gap-[1.5rem]">
        <section>
          <ScrollAnimation>
            <div className="mt-[1rem] md:mt-[2.75rem]">
              <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1  ">
                Our Core Values
              </h1>

              <div className="flex flex-col justify-center items-center my-4 lg:my-[2rem] gap-4 xl:gap-[2rem]">

                <div className="grid grid-cols-1 md:grid-cols-3 justify-between lg:mx-[40px] lg:my-[16px] gap-[30px] mx-[18px] md:gap-[3rem] xl:gap-[2rem] w-[80%] ">
                  {core.map((data) => (

                    <div className="flex flex-1 items-center gap-4 py-[1.75rem] pl-[16px] border-[1px] rounded-lg border-[#e0e0e0] hover:shadow-xl section-2-cart">
                      <div className="flex flex-col ">
                        <div className="flex flex-row justify-center items-center gap-4">
                          <img src={checkbox} alt="" className="h-[24px]" />
                          <p className="text-[20px] font-bold">
                            {data.heading}
                          </p>
                        </div>
                      </div>
                    </div>


                  ))}
                </div>
              </div>

            </div>
          </ScrollAnimation>
        </section>




        <section className="mx-[16px] my-[2rem] lg:my-[4rem] lg:mx-[4rem]">
          <h1 className="text-md lg:mb-[40px] text-center sm:text-lg md:text-xl font-semibold mt-1">
            <span className="text-[24px] lg:text-xl">What Oceann Mail Offers?</span>
          </h1>
          <ScrollAnimation>
            <div className="grid grid-cols-1 md:grid-cols-3 justify-between lg:mx-[40px] lg:my-[16px] gap-[30px] mx-[18px] ">
              {card.map((data) => (
                <div style={{
                  border: "1px solid",
                  borderColor: "#e0e0e0"
                }} onClick={() => Navigate(data.path)} className="flex flex-1 flex-col rounded-xl px-[24px] py-[24px] section-3-cart">
                  <div className="py-2 my-[1rem]" >
                    <h1 className="inline text-[24px] font-bold py-[4px] mt-[8px] mb-[16px] rounded-md"
                    //  style={{backgroundColor:data.color}}
                    >{data.title}</h1>
                  </div>
                  <p className="text-[14px]">{data.content}</p>
                  <div className="flex flex-row"> <motion.button whileHover={{ scale: 1.04 }} onClick={() => Navigate(data.path)} className="max-sm:p-2 flex flex-row justify-center items-center gap-4 max-sm:mt-3 mt-4 text-[#003e78] py-2 rounded-[2rem] text-xs sm:text-sm md:text-md font-bold">
                    Know more  <FiArrowRight />
                  </motion.button>
                  </div>
                </div>
              ))}
            </div>
          </ScrollAnimation>
        </section>





        <ScrollAnimation>
          <div className="flex justify-center items-center flex-col gap-4">
            <h6 className="text-md sm:text-lg font-bold text-center">Get in Touch</h6>
            <p className="text-xs sm:text-sm text-center mx-4 ">Experience hassle-free dry bulk shipping solutions with Theoceann. <br />Contact us today to discuss your shipping needs and let us tailor a <br />solution that aligns with your requirements.</p>
            <a href="https://theoceann.ai/contact">
              <motion.button style={{}} onMouseOver={(e) => (e.target.style.color = '#ffffff')} whileHover={{ scale: 1.04, backgroundColor: '#003e78' }} className=" bg-[#f39c12] text-white px-[24px] mt-4 py-[8px] rounded-[42px]">Learn More</motion.button>
            </a>
          </div>
        </ScrollAnimation>


      </div>















      {/* <main>
        <Macbook />
        <CoreSolution />
        <div className=" mt-[1.5rem] flex justify-center">
          <div className="w-[100%]">
            <div className="flex flex-wrap gap-[1rem] w-[100%] items-center justify-center max-sm:flex-col">
              {cardContent.map(({ color, bgColor, title, detail, page, path }) => {
                return (
                  <Card
                    color={color}
                    bgColor={bgColor}
                    title={title}
                    detail={detail}
                    page={page}
                    path={path}
                  />
                );
              })}
            </div>
          </div>
        </div>
      </main> */}
    </>
  );
};
