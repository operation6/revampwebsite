import React from "react";
import {
  ScrollAnimation,
  animationVariants,
} from "../Componants/motion/scrollanimation";
import { Row, Col, Card, Avatar } from 'antd';
import Animatedwordchar from "../Componants/motion/animatewordchar";
import team from "../Componants/Assets/Product-img/our-team-bg.jpg";
import Partners from "../Landing_Page_Components/Partners";
// import Mentorship from "./mentorship";

const { Meta } = Card;
const Team = () => {
  // Example team members data
  const teamMembers = [
    { name: 'John Doe', role: 'CEO', avatar: 'https://aliciabots.com/images/sanjay.jpg' },
    { name: 'Jane Smith', role: 'CTO', avatar: 'https://aliciabots.com/images/sanjay.jpg' },
    { name: 'Michael Brown', role: 'Developer', avatar: 'https://aliciabots.com/images/sanjay.jpg' },
    // Add more team members as needed
  ];

  return (
    <>
      <main>
        <div
          className="h-[50vh] lg:h-[100vh] flex items-center bg-[#00000083] bg-blend-darken"
          style={{
            backgroundImage: `url(${team})`,
            backgroundSize: "cover",
          }}
        >
          <div className="oceann-vm-section1 flex flex-col gap-[8px]">
            <h1 className="text-center text-[3.75rem] leading-tight">
              <Animatedwordchar text="Meet The Team" />
            </h1>
          </div>
        </div>
      </main>

      {/* Partner section */}
      {/* <ScrollAnimation>
        <div className="meetTeams">
          <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-10">
            <span className="lg:text-2xl">Meet Our Team</span>
          </h1>
          <section className="flex justify-center items-center mt-[4rem] p-8 max-sm:p-4">
            <div className="flex flex-col w-[80vw] max-sm:w-full" style={{ alignItems: 'center' }}>
              <div className="flex w-full my-4 mx-4 h-[60vh] max-sm:h-auto gap-[20px] justify-between max-sm:flex-col">
              

                <div
                  className="w-[40%] max-sm:w-full rounded-2xl flex h-full max-sm:h-[40vh] max-sm:w-[fit] p-[5%] rounded-2xl flex h-full max-sm:h-[40vh]  bg-no-repeat bg-contain sm:bg-cover max-sm:bg-cover"
                  style={{
                    backgroundImage: `url('https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/CTO-THEOCEANN.png')`,

                  }}
                ></div>

             
                <div className="w-[50%] max-sm:w-full flex pl-4 max-sm:pl-2 justify-start items-center">
                  <div>
                    <h1 className="text-xl max-lg:text-lg max-md:text-md font-semibold mb-5 text-[#0070C7]">
                      Amar Dixit (CTO)
                    </h1>
                    <p className="text-md max-lg:text-sm max-md:text-xs font-[400]">
                      At TheOceann.ai, our mission is to revolutionize the way businesses operate by providing innovative and intuitive solutions tailored to meet your unique needs. We are committed to simplifying complex processes, enabling you to focus on what truly matters: achieving your goals and driving growth.
                      By combining cutting-edge technology with a deep understanding of industry challenges, we design tools that not only address today’s operational hurdles but also empower you to embrace the opportunities of tomorrow. Our solutions are crafted with precision, keeping efficiency, reliability, and your success at the forefront.
                      We are honored to be your trusted partner in this journey toward excellence. Thank you for choosing us to help you thrive and create a brighter, more productive future. Together, we are building a smarter, more efficient tomorrow.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </ScrollAnimation> */}
      <ScrollAnimation>
        <div className="meetTeams">

          <section className="flex justify-center items-center mt-[4rem] p-8 max-sm:p-4">
            <div className="flex flex-col w-[80vw] max-sm:w-full" style={{ alignItems: 'center' }}>
              <div className="flex w-full my-4 mx-4 h-[60vh] max-sm:h-auto gap-[20px] justify-between max-sm:flex-col">
                {/* Background image */}
                <div
                  className="w-[50%] max-sm:w-full p-[5%] rounded-2xl flex h-full max-sm:h-[40vh]"
                  style={{
                    backgroundImage: `url('https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/employee-image.jpeg')`,
                    backgroundSize: "cover",
                    backgroundPosition: "center",
                    backgroundRepeat: "no-repeat",
                  }}
                ></div>

                {/* Content */}
                <div className="w-[50%] max-sm:w-full flex pl-4 max-sm:pl-2 justify-start items-center">
                  <div>
                    <h1 className="text-xl max-lg:text-lg max-md:text-md font-semibold mb-5 text-[#0070C7]">
                      Our Team
                    </h1>
                    <p className="text-md max-lg:text-sm max-md:text-xs font-[400]">
                      At TheOceann.ai, we are dedicated to empowering maritime businesses with innovative solutions that streamline operations, enhance efficiency, and drive sustainable growth. We understand the unique challenges you face and are here to provide the tools and expertise needed to help you succeed.
                      Your success is at the core of everything we do. By combining advanced technologies with a partnership driven approach, we aim to deliver impactful results that align with your goals.
                      Thank you for trusting TheOceann.ai. Together, let’s navigate the future of maritime operations with confidence.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </ScrollAnimation>




      {/* Partner section end */}
      {/* Partner section */}
      <ScrollAnimation>
        <div className="my-[1rem] mx-[1rem] max-sm:mx-[1rem]">
          <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-10 ">
            <span className="lg:text-2xl">Trusted collaborators</span>
          </h1>
          <Partners />
        </div>
      </ScrollAnimation>
      {/* Partner section end */}

      {/* <section className=" flex justify-center align-middle mt-[4rem] ">
        <div className="flex flex-col w-[56vw] border-[1px] p-8 rounded-md gap-[0.2rem]">
          <h2 className="text-xl font-bold py-4">Legal Information</h2>
          <div className="grid grid-cols-2">
            <p className="font-semibold">Company Name</p>
            <p className="font-bold">Maritime Info Lab Pvt LTd.</p>
          </div>
          <div className="grid grid-cols-2">
            <p className="font-semibold">Address</p>
            <p>
              B-431/432, Alphathum By Bhutani, Sector 90, Noida, Uttar Pradesh,
              India-201305
            </p>
          </div>

          <div className="grid grid-cols-2">
            <p className="font-semibold">Contact</p>
            <p className="font-bold">+919871626990</p>
          </div>

          <p className="text-[20px] mt-10 mb-2 font-bold">
            Domain Information for theoceann.ai
          </p>
          <div className="grid grid-cols-2">
            <p className="font-semibold">Registrar</p>
            <p>Google Domains</p>
          </div>

          <div className="grid grid-cols-2">
            <p className="font-semibold">Registration Date</p>
            <p>February 15, 2022</p>
          </div>

          <div className="grid grid-cols-2">
            <p className="font-semibold">Registrant</p>
            <p>
              Maritime Info Lab Pvt LTd. <br /> B-431/432, ALphathum by Bhutani,
              Sector 90, Noida, Uttar Pradesh, India-201305
            </p>
          </div>
          <div className="grid grid-cols-2">
            <p className="font-semibold">Cloud Hoisting</p>
            <p>
              <b>Amazon Web Services,</b> Inc. 410 Terry Avenue North Seattle,
              WA 98109-5210, United States
            </p>
          </div>
        </div>
      </section> */}
    </>
  );
};

export default Team;
