import React from "react";
import Newsimg1 from "../Componants/Assets/News-img/News-img1.png";
import Newsimg2 from "../Componants/Assets/News-img/News-img2.png";
import Newsimg3 from "../Componants/Assets/News-img/News-img3.png";
import Newsimg4 from "../Componants/Assets/News-img/News-img4.png";
import Animatedword from "../Componants/motion/animateword";
import Newsimg5 from "../Componants/Assets/News-img/News-img5.png";
import Animatedwordchar from "../Componants/motion/animatewordchar";
import { ScrollAnimation } from "../Componants/motion/scrollanimation";
import Partners from "../Landing_Page_Components/Partners";
import { Link as RouterLink } from "react-router-dom";

import {
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  Avatar,
  ListItemButton,
  Divider,
  Typography,
  Button,
  Link,
  Grid,
  Box,
} from "@mui/material";
import { IoArrowForwardCircleOutline } from "react-icons/io5";
import { useParams } from "react-router-dom";
import { Stack } from "rsuite";

const NewsDetails = () => {
  let { id } = useParams();

  const newsArticles = [
    {
      id: "1",
      title:
        "Tanker Collides With Passenger Boat In Port Aransas, Leaving 1 Dead & 1 Missing",
      description:
        "Endeavour, a Bermuda-registered charity that builds self-confidence and life skills for Bermuda’s youth through experiential education, partners with the Bermuda College Athora Division.Endeavour, a Bermuda-registered charity that builds self-confidence and life skills for Bermuda’s youth through experiential education, partners with the Bermuda College Athora Division",
      date: "2024-07-16", // Example date
      imageUrl: "https://via.placeholder.com/150",
      info: "Bermuda-registered charity that builds self-confidence and life skills for Bermuda’s youth through experiential education, partners with the Bermuda College Athora Division",
      subInfo:
        "A Bermuda-registered charity that builds self-confidence and life skills for Bermuda’s youth through experiential education, partners with the Bermuda College Athora Division",
      link: "#",
    },
    {
      id: "2",
      title: "Leaving 1 Dead & 1 Missing",
      description:
        "Endeavour, a Bermuda-registered charity that builds self-confidence and life skills for Bermuda’s youth through experiential education, partners with the Bermuda College Athora Division.Endeavour, a Bermuda-registered charity that builds self-confidence and life skills for Bermuda’s youth through experiential education, partners with the Bermuda College Athora Division",
      date: "2024-07-15", // Example date
      imageUrl: "https://via.placeholder.com/150",
      info: "Bermuda-registered charity that builds self-confidence and life skills for Bermuda’s youth through experiential education, partners with the Bermuda College Athora Division",
      subInfo:
        "A Bermuda-registered charity that builds self-confidence and life skills for Bermuda’s youth through experiential education, partners with the Bermuda College Athora Division",
      link: "#",
    },
    {
      id: "3",
      title: "Tanker Collides With Passenger Boat In Port Aransas",
      description:
        "Endeavour, a Bermuda-registered charity that builds self-confidence and life skills for Bermuda’s youth through experiential education, partners with the Bermuda College Athora Division.Endeavour, a Bermuda-registered charity that builds self-confidence and life skills for Bermuda’s youth through experiential education, partners with the Bermuda College Athora Division",
      date: "2024-07-14", // Example date
      imageUrl: "https://via.placeholder.com/150",
      info: "Bermuda-registered charity that builds self-confidence and life skills for Bermuda’s youth through experiential education, partners with the Bermuda College Athora Division",
      subInfo:
        "A Bermuda-registered charity that builds self-confidence and life skills for Bermuda’s youth through experiential education, partners with the Bermuda College Athora Division",
      link: "#",
    },
  ];

  const newsLatest = [
    {
      id: "1",
      title: "Understanding Ship Salvage Operations",
      description:
        "Salvage is recovering a ship or cargo after an accident or disaster. Whether it’s a...",
      date: "2024-07-16", // Example date
      imageUrl:
        "https://www.marineinsight.com/wp-content/uploads/2024/07/10-3.jpg.webp",
      link: "#",
    },
    {
      id: "2",
      title: "News 2",
      description:
        "Salvage is recovering a ship or cargo after an accident or disaster. Whether it’s a...",
      date: "2024-07-16", // Example date
      imageUrl:
        "https://www.marineinsight.com/wp-content/uploads/2024/07/Untitled-design-22-1024x536.jpg.webp",
      link: "#",
    },
    {
      id: "3",
      title: "News 3",
      description:
        "Salvage is recovering a ship or cargo after an accident or disaster. Whether it’s a...",
      date: "2024-07-16", // Example date
      imageUrl:
        "https://www.marineinsight.com/wp-content/uploads/2024/07/Untitled-design-22-1024x536.jpg.webp",
      link: "#",
    },
    {
      id: "4",
      title: "News 4",
      description:
        "Salvage is recovering a ship or cargo after an accident or disaster. Whether it’s a...",
      date: "2024-07-16", // Example date
      imageUrl:
        "https://www.marineinsight.com/wp-content/uploads/2024/07/Untitled-design-22-1024x536.jpg.webp",
      link: "#",
    },
    {
      id: "5",
      title: "News 5",
      description:
        "Salvage is recovering a ship or cargo after an accident or disaster. Whether it’s a...",
      date: "2024-07-16", // Example date
      imageUrl:
        "https://www.marineinsight.com/wp-content/uploads/2024/07/Untitled-design-22-1024x536.jpg.webp",
      link: "#",
    },
  ];

  // Assuming you have some way to retrieve the article details by ID
  const article = newsArticles.find((article) => article.id === id);
  // console.log("articleww", article, useParams(), id, newsArticles);

  return (
    <>
      <main>
        <div
          className="h-[15vh] lg:h-[50vh] flex items-center bg-[#00000083] bg-blend-darken"
          style={{
            backgroundImage: `url(${Newsimg1})`,
            backgroundSize: "cover",
          }}
        >
          <div className="oceann-vm-section1 flex flex-col gap-[8px]">
            <div className="text-center text-6xl">
              <Animatedwordchar text="NEWS DETAILS" />
            </div>
            <div className="font-medium ">
              <Animatedword text="Maritime Matters Unfold: Your Ocean of News" />
            </div>
          </div>
        </div>
      </main>

      {/* Partner section */}
      {/* <ScrollAnimation>
        <div className="my-[1rem] mx-[1rem] max-sm:mx-[1rem]">
          <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-20 ">
            <span className="lg:text-2xl">Our Partners</span>
          </h1>
          <Partners />
        </div>
      </ScrollAnimation> */}
      {/* Partner section end */}
      <ScrollAnimation>
        <div className="flex lg:gap-[2rem] xl:my-[1rem] newsDetailsMain">
          <div className="w-[90%] mx-auto">
            {article ? (
              <div>
                <Typography variant="h5" fontWeight="bold" mb={1}>
                  {article.title}
                </Typography>
                <Typography
                  variant="body2"
                  color="textSecondary"
                  mb={2}
                  class="entry-meta entry-meta-divider-vline"
                >
                  <Typography class="posted-by">
                    <Typography
                      class="meta-label"
                      style={{ marginRight: "4px" }}
                    >
                      By{" "}
                    </Typography>
                    <Typography class="author vcard">
                      <a href="https://www.marineinsight.com/author/marine-insight-news-network/">
                        MI News Network
                      </a>
                    </Typography>
                  </Typography>{" "}
                  <Typography class="posted-on">{article.date}</Typography>
                  <Typography class="category-links">
                    <a
                      href="https://www.marineinsight.com/category/shipping-news/"
                      rel="category tag"
                    >
                      Shipping News
                    </a>
                  </Typography>
                </Typography>

                <Grid container alignItems="self-start" spacing={4}>
                  <Grid item xs={12} sm={7} md={8}>
                    <Typography variant="body2" color="textSecondary" mb={2}>
                      <img
                        src="https://www.marineinsight.com/wp-content/uploads/2024/07/8-8.jpg"
                        alt="News details image"
                      // className="max-width-[100%]"
                      />
                    </Typography>
                    <Typography variant="body1">
                      {article.description}
                    </Typography>
                    {article?.info ? (
                      <Typography
                        style={{
                          backgroundColor: "#37affb",
                          marginTop: "1.5rem",
                          fontWeight: "500",
                          color: "#fff",
                          padding: "2rem",
                        }}
                        variant="body1"
                      >
                        {article?.info}
                      </Typography>
                    ) : (
                      ""
                    )}
                    {article?.subInfo ? (
                      <Typography
                        style={{
                          marginTop: "1.5rem",
                        }}
                        variant="body1"
                      >
                        {article?.subInfo}
                      </Typography>
                    ) : (
                      ""
                    )}
                  </Grid>
                  <Grid item xs={12} sm={5} md={4}>
                    <p className="pl-2 pr-2 text-xs sm:text-sm md:text-[20px] pb-[0.2rem] lg:pb-[0.2rem]">
                      RECENT NEWS
                      <Divider style={{ paddingBottom: "0.5rem" }} />
                    </p>

                    <List
                      className="latestNewsItem"
                    // style={{ height: "370px", overflowY: "auto" }}
                    >
                      {newsLatest.map((news, newsIndex) => (
                        <div key={newsIndex}>
                          <Link
                            component={RouterLink}
                            to={`/knowledge-Hub/news/details/${news.id}`}
                            underline="none"
                            color="inherit"
                            style={{ textDecoration: "none", display: "block" }}
                          >
                            <ListItem
                              alignItems="flex-start"
                              component="div"
                              style={{ cursor: "pointer" }}
                              className="listItem"
                            >
                              <Grid container spacing={0}>
                                <Grid item xs={12} sm={5} md={4}>
                                  <Avatar
                                    alt={news.title}
                                    src={news.imageUrl}
                                    variant="square"
                                    className="newsAvatar"
                                    style={{
                                      width: "110px",
                                      height: "60px",
                                      // marginRight: "15px",
                                    }}
                                  />
                                </Grid>
                                <Grid item xs={12} sm={7} md={8}>
                                  <ListItemText
                                    style={{ marginTop: "0px" }}
                                    primary={
                                      <Typography
                                        variant="h6"
                                        component="div"
                                        style={{
                                          lineHeight: "1.2",
                                          fontSize: "1rem",
                                        }}
                                      >
                                        {news.title}
                                      </Typography>
                                    }
                                    secondary={
                                      <>
                                        <Typography
                                          variant="body2"
                                          color="#979797"
                                          component="div"
                                        >
                                          {news.date}
                                        </Typography>
                                      </>
                                    }
                                  />
                                </Grid>
                              </Grid>
                            </ListItem>
                          </Link>
                          {newsIndex < newsLatest.length - 1 && (
                            <Divider
                              style={{
                                marginLeft: "5px",
                                border: "none",
                                borderBottom: "1px dashed #dfdddd",
                              }}
                            /> // Adjust margin-left to match avatar size
                          )}
                        </div>
                      ))}
                    </List>
                    <Typography textAlign="right" fontWeight="bold">
                      <Link href="/knowledge-Hub/news" underline="always">
                        Read All Latest News
                      </Link>
                    </Typography>
                  </Grid>
                </Grid>
              </div>
            ) : (
              <Typography variant="body1">Article not found.</Typography>
            )}
          </div>
        </div>
      </ScrollAnimation>
    </>
  );
};

export default NewsDetails;
