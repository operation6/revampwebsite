// Profile.js
import styles from "./profile.module.css";
import Swal from "sweetalert2";
import React, { useEffect, useState } from "react";
import PersonalDetails from "../../Componants/personalDetails/personalDetails";
import Subscriptions from "../../Componants/subscriptions/subscriptions";
import { Modal, message } from "antd";
import PaymentInfo from "../../Componants/paymentInfo";
import { Images } from "../../assests";

const Profile = () => {
  const [showTabs, setShowTabs] = useState([]);
  const [component, setComponent] = useState();
  const [companyName, setCompanyName] = useState("");
  const [portalModal, setPortalModal] = useState(false);
  const [tokenData, setTokenData] = useState({});

  const myData = {
    personalDetails: <PersonalDetails />,
    subscriptions: <Subscriptions />,
    payementInfo: <PaymentInfo />,
  };

  function Copy(url) {
    const el = document.createElement("textarea");
    el.value = url;
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);

    message.success("URL copied to clipboard");
  }
  

  useEffect(() => {
    // debugger
    setComponent(myData.personalDetails);
    const isAdmin = JSON.parse(localStorage.getItem("isAdmin"));
    const token = localStorage.getItem("oceanAllToken");
    const userData = JSON.parse(atob(token.split(".")[1]));
    setTokenData(userData)
    setCompanyName(userData.company_name);
    // const url = window.location.href
    // console.log(url, '--hh');
    isAdmin ? setShowTabs(adminTabs) : setShowTabs(userTabs);
  }, []);

  const adminTabs = [
    {
      text: "Personal Details",
      componentName: "personalDetails",
      isAdmin: true,
      function: (userInput) => {
        setComponent(myData[userInput]);
      },
    },
    {
      text: "Payment Information",
      componentName: "payementInfo",
      isAdmin: true,
      function: (userInput) => {
        setComponent(myData[userInput]);
      },
    },
    {
      text: "Subscriptions",
      componentName: "subscriptions",
      isAdmin: true,
      function: (userInput) => {
        const userData = JSON.parse(localStorage.getItem("oceanAllUserData"));
        if (userData.is_subscribed) {
          setComponent(myData[userInput]);
        } else {
          Swal.fire({
            title: "Not subscribed",
            text: "Please buy now to proceed ",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Buy Now",
          }).then((result) => {
            if (result.isConfirmed) {
              window.location.href = "/plans";
            }
          });
        }
      },
    },
    {
      text: "My Portal",
      componentName: "myportal",
      isAdmin: true,
      function: (userInput) => {
        const userData = JSON.parse(localStorage.getItem("oceanAllUserData"));
        if (userData.is_subscribed) {
          setPortalModal(true);
          // const token = localStorage.getItem('oceanAllToken');
          // const userData = JSON.parse(atob(token.split(".")[1]));
          // const companyName = userData.company_name;
          // const url = `http://vm-${companyName}.theoceann.ai/#/user/login?token=${token}`;
          // window.open(url, '_blank');
        } else {
          Swal.fire({
            title: "Not subscribed",
            text: "Please buy now to proceed ",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Buy Now",
          }).then((result) => {
            if (result.isConfirmed) {
              window.location.href = "/plans";
            }
          });
        }

        // setComponent(myData[userInput]);
      },
    },
    {
      text: "Admin Dashboard",
      componentName: "admindashboard",
      isAdmin: true,
      function: (userInput) => {
        const userData = JSON.parse(localStorage.getItem("oceanAllUserData"));
        const token = localStorage.getItem("oceanAllToken");
        if (userData.is_subscribed) {
          const token = localStorage.getItem("oceanAllToken");
          const userData = JSON.parse(atob(token.split(".")[1]));
          const companyName = userData.company_name;
          window.location.href = `https://admin-${companyName}.theoceann.${isProduction() ? 'ai' : 'com'}/login?token=${token}`;
        } else {
          Swal.fire({
            title: "Not subscribed",
            text: "Please buy now to proceed ",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Buy Now",
          }).then((result) => {
            if (result.isConfirmed) {
              window.location.href = "/plans";
            }
          });
        }

        // setComponent(myData[userInput]);
      },
    },
  ];

  const userTabs = [
    {
      text: "Personal Details",
      componentName: "personalDetails",
      isAdmin: true,
      function: (userInput) => {
        setComponent(myData[userInput]);
      },
    },
    {
      text: "My Portal",
      componentName: "myportal",
      isAdmin: true,
      function: (userInput) => {
        // const token = localStorage.getItem("oceanAllToken");
        // const userData = JSON.parse(atob(token.split(".")[1]));
        // const companyName = userData.company_name;
        // window.location.href = `https://vm-${companyName}.theoceann.ai/login?token=${token}`;
        const userData = JSON.parse(localStorage.getItem("oceanAllUserData"));
        if (userData.is_subscribed) {
          setPortalModal(true);
        } else {
          Swal.fire({
            title: "Not subscribed",
            text: "Please buy now to proceed ",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Buy Now",
          }).then((result) => {
            if (result.isConfirmed) {
              window.location.href = "/plans";
            }
          });
        }
      },
    },
  ];

  const isProduction = () => {
    const prodDomain = "https://theoceann.ai";
    const currentHostname = window.location.origin;

    const isProd = currentHostname === prodDomain;
  
    return isProd
  };
  
  const VMURL = `https://vm-${companyName}.theoceann.${isProduction() ? 'ai' : 'com'}/#/user/login?token=${localStorage.getItem(
    "oceanAllToken"
  )}`;
  const MailURL = `https://mail-${companyName}.theoceann.${isProduction() ? 'ai' : 'com'}/login?token=${localStorage.getItem(
    "oceanAllToken"
  )}`;

  return (
    <>
      <div className={styles.tableBox}>
        <div className={styles.leftPart}>
          <div className={styles.imgBox}>
            <img
              src={Images + "user.jpg"}
              alt=""
              className={styles.profilePhoto}
            />
          </div>

          <div className="linkBox">
            {showTabs.map((tab) => (
              <div
                className={styles.linkItems}
                onClick={() => {
                  tab.function(tab.componentName);
                }}
              >
                {tab.text}
              </div>
            ))}
          </div>
        </div>

        <div className={styles.rightPart}>{component}</div>
        {portalModal && (
          <Modal
            open={portalModal}
            onCancel={() => setPortalModal(false)}
            footer={false}
            width={700}
            title="Subscribed Product Links"
          >
            <div>
              <div style={{ paddingBlock: "14px" }}>
                {tokenData?.VM_ACCESS && (
                  <div style={{ display: "flex" }}>
                    <div
                      className="col-md-6"
                      style={{
                        whiteSpace: "nowrap",
                        marginRight: "15px",
                        marginBottom: "10px",
                      }}
                    >
                      Your OceannVM Link :-{" "}
                    </div>
                    <a
                      href={VMURL}
                      target="_blank"
                      className="text-ellipsis productLink"
                    >
                      {VMURL}
                    </a>
                  </div>
                )}
                {tokenData?.MAIL_ACCESS && (
                  <div style={{ display: "flex" }}>
                    <div style={{ whiteSpace: "nowrap", marginRight: "15px" }}>
                      Your OceannMail Link :-{" "}
                    </div>
                    <a
                      href={MailURL}
                      target="_blank"
                      className="text-ellipsis productLink"
                    >
                      {MailURL}
                    </a>
                  </div>
                )}
                {tokenData?.MAIL_ACCESS || tokenData.VM_ACCESS ? "" : "You have not subscribed any of our Product yet!" }
              </div>
            </div>
          </Modal>
        )}
      </div>
    </>
  );
};

export default Profile;
