import React from "react";
import { Row, Col, Typography } from 'antd';
import { GiArtificialIntelligence, GiCargoShip, GiPositionMarker, GiPriceTag, GiStockpiles, GiTrade, GiWeightScale } from 'react-icons/gi';
import { SiAddthis, SiChatbot, SiLivejournal } from 'react-icons/si';
import { MdEmail, MdLiveHelp, MdMarkEmailRead } from 'react-icons/md';
import { BiChat, BiMenuAltLeft, BiSearch } from 'react-icons/bi';
import { RiExchangeLine, RiLiveLine, RiMapPinTimeLine, RiOilLine, RiPinDistanceLine, RiStockLine } from 'react-icons/ri';
import "../styles/card.css";

const { Title, Text } = Typography;

const Card = () => {
    const openExternalPage = (link) => {
        if (link) {
            window.open(link, '_blank');
        }
    };

    return (
        <>
            <div className="flex flex-col ">
                <div className="relative" style={{ marginTop: '100px' }}>

                </div>

                <div style={{ width: 1000, minHeight: 829, marginLeft: 'auto', marginRight: 'auto' }}>
                    <div className='text-sm text-center sm:text-center md:text-[25px]'>Welcome to the new era of maritime OceanAI.</div>
                    <div className='text-sm text-center sm:text-center md:text-[16px]'>Ease your tasks with OceanAI and save 4.5% of your time daily.</div>

                    <div className="masonry-grid" style={{marginTop: '30px'}}>
                        {itemData.map((item, index) => (
                            <div key={index} className='masonry-item masonaryWrap' onClick={() => openExternalPage(item?.href)}>
                                <div className="image-container">
                                    <img
                                        srcSet={`${item?.img}?w=162&auto=format&dpr=2 2x`}
                                        src={`${item?.img}?w=162&auto=format`}
                                        alt={item?.title}
                                        loading="lazy"
                                        style={{
                                            display: 'block',
                                            width: '100%',
                                        }}
                                    />
                                    <Title level={4} className="title">
                                        {item?.title}
                                        {item?.subtitle ? (
                                            <>
                                                <br /> {item?.subtitle}
                                            </>
                                        ) : null}
                                        <div style={{ fontSize: '4rem' }}>
                                            {item?.icon && <item.icon />}
                                        </div>
                                    </Title>
                                    <Text className="description">
                                        {item?.description}
                                    </Text>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </>
    );
};

export default Card;

const itemData = [
    {
        img: './card-images/vessel-tracking.jpg',
        title: 'Vessel Position',
        description: 'Create your own Vessel Position',
        icon: GiPositionMarker,
        href: 'https://google.co.in/'
    },
    {
        img: './card-images/search-filter.jpg',
        title: 'Search Filters.',
        description: 'Check our Tag, Search Filters.',
        icon: BiSearch,
        href: 'https://theoceann.ai/'
    },
    {
        img: './card-images/email-database.png',
        title: 'OceanMail database',
        description: 'Store millions of emails in the OceanMail database.',
        icon: MdMarkEmailRead,
        href: 'https://www.google.com/intl/en/gmail/about/'
    },
    {
        img: './card-images/market-insights.png',
        title: 'Market Insight',
        description: 'Global Tonnage and Market Position.',
        icon: RiStockLine,
        href: 'https://www.google.com/intl/en/gmail/about/'
    },
    {
        img: './card-images/search-filter.jpeg',
        title: 'Chatbot AI',
        description: 'Search your mailbox faster than ever with DWT range, vessel name, and tonnage enquiry.',
        icon: SiChatbot,
        href: 'https://chatbotapp.ai/'
    },
    {
        img: 'https://png.pngtree.com/png-clipart/20230326/original/pngtree-chip-ai-human-intelligence-technology-chip-high-tech-circuit-board-ai-png-image_9004996.png',
        title: 'OceanAI',
        description: 'OceanAI provides port call information, live bunker prices for any port, port congestion, draft restrictions, and more.',
        icon: GiArtificialIntelligence,
        href: 'https://theoceann.ai/'
    },
    {
        img: 'https://701digital.com/wp-content/uploads/2020/03/Email-Database-Marketing-750x750.png',
        title: 'Import Email',
        description: 'Import millions of emails, categorize and tag all emails for better visibility via AI/ML processing.',
        icon: MdEmail,
        href: 'https://www.google.com/intl/en/gmail/about/'
    },
    
    {
        img: './card-images/cargo-pic.png',
        title: 'My Order',
        subtitle: 'Cargo and Tonnage',
        description: 'For Cargo and Tonnage Position from Market.',
        icon: GiCargoShip,
        href: 'https://theoceann.ai/'
    },
    {
        img: 'https://png.pngtree.com/png-clipart/20230326/original/pngtree-chip-ai-human-intelligence-technology-chip-high-tech-circuit-board-ai-png-image_9004996.png',
        title: 'Intelligence tool',
        description: 'Map intelligence, Voyage Estimate',
        icon: GiArtificialIntelligence,
        href: 'https://theoceann.ai/'
    },
    {
        img: './card-images/cargo-vertical.png',
        title: 'Voyage Estimate',
        description: '',
        icon: GiWeightScale,
        href: 'https://theoceann.ai/'
    },
    {
        img: 'https://img.indiafilings.com/learn/wp-content/uploads/2017/12/12010226/Tonnage-Tax-Scheme.jpg',
        title: 'Chat',
        description: '',
        icon: BiChat,
        href: 'https://theoceann.ai/'
    },
    {
        img: './card-images/cargo-vertical.png',
        title: 'Port Congestion',
        description: '',
        icon: GiWeightScale,
        href: 'https://theoceann.ai/'
    },    
    {
        img: 'https://img.indiafilings.com/learn/wp-content/uploads/2017/12/12010226/Tonnage-Tax-Scheme.jpg',
        title: 'Global Trade Flow',
        description: '',
        icon: GiTrade,
        href: 'https://theoceann.ai/'
    },
    {
        img: './card-images/cargo-vertical.png',
        title: 'Baltic Exchange',
        description: '',
        icon: RiExchangeLine,
        href: 'https://theoceann.ai/'
    },
    {
        img: 'https://img.indiafilings.com/learn/wp-content/uploads/2017/12/12010226/Tonnage-Tax-Scheme.jpg',
        title: 'Add Fleet',
        description: '',
        icon: SiAddthis,
        href: 'https://theoceann.ai/'
    },
    {
        img: 'https://img.indiafilings.com/learn/wp-content/uploads/2017/12/12010226/Tonnage-Tax-Scheme.jpg',
        title: 'Bunker Live Price for many ports',
        description: '',
        icon: GiPriceTag,
        href: 'https://theoceann.ai/'
    },
    {
        img: './card-images/cargo-vertical.png',
        title: 'Netpas',
        subtitle: 'Port to Port distance',
        description: '',
        icon: RiPinDistanceLine,
        href: 'https://theoceann.ai/'
    },
    {
        img: 'https://img.indiafilings.com/learn/wp-content/uploads/2017/12/12010226/Tonnage-Tax-Scheme.jpg',
        title: 'Map Intelligence',
        description: '',
        icon: RiMapPinTimeLine,
        href: 'https://theoceann.ai/'
    }
    
];
