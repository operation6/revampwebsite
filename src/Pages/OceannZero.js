import React from "react";
// import oceannZeroimg1 from "../Componants/Assets/OceannZero-img/oceanzero-img1.png";
// import oceannZeroimg2 from "../Componants/Assets/OceannZero-img/oceanzero-img2.png";
// import oceannZeroimg3 from "../Componants/Assets/OceannZero-img/oceanzero-img3.png";
// import oceannZeroimg4 from "../Componants/Assets/OceannZero-img/oceanzero-img4.png";
// import OurServicesicon1 from "../Componants/Assets/OceannZero-img/OurServices-icon1.png";
// import OurServicesicon2 from "../Componants/Assets/OceannZero-img/OurServices-icon2.png";
// import OurServicesicon3 from "../Componants/Assets/OceannZero-img/OurServices-icon3.png";
// import OurServicesicon4 from "../Componants/Assets/OceannZero-img/OurServices-icon4.png";
// import OurServicesicon5 from "../Componants/Assets/OceannZero-img/OurServices-icon5.png";
// import featureicon1 from "../Componants/Assets/OceannZero-img/feature-icon1.png";
// import featureicon2 from "../Componants/Assets/OceannZero-img/feature-icon2.png";
// import featureicon3 from "../Componants/Assets/OceannZero-img/feature-icon3.png";
// import featureicon4 from "../Componants/Assets/OceannZero-img/feature-icon4.png";
// import featureicon5 from "../Componants/Assets/OceannZero-img/feature-icon5.png";
// import featureicon6 from "../Componants/Assets/OceannZero-img/feature-icon6.png";
// import co2img from "../Componants/Assets/OceannZero-img/co2img.png";
import nature from "../Componants/Assets/OceannZero-img/nature.png";
import Oceannzerocard from "../Componants/OceannZero-Components/Oceannzerocard";
import { Link } from "react-router-dom";
import { navigator, useNavigate } from "react-router-dom";
import { motion } from "framer-motion";
import Animatedwordchar from "../Componants/motion/animatewordchar";
import {
  ScrollAnimation,
  animationVariants,
} from "../Componants/motion/scrollanimation";
import Partners from "../Landing_Page_Components/Partners";
import { Images } from "../assests";
let co2img = Images + 'co2img.png';
let featureicon6 = Images + 'feature-icon6.png';
let featureicon5 = Images + 'feature-icon5.png';
let featureicon4 = Images + 'feature-icon4.png';
let featureicon3 = Images + 'feature-icon3.png';
let featureicon2 = Images + 'feature-icon2.png';
let featureicon1 = Images + 'feature-icon1.png';
let OurServicesicon5 = Images + 'OurServices-icon5.png';
let OurServicesicon4 = Images + 'OurServices-icon4.png';
let OurServicesicon3 = Images + 'OurServices-icon3.png';
let OurServicesicon2 = Images + 'OurServices-icon2.png';
let OurServicesicon1 = Images + 'OurServices-icon1.png';
let oceannZeroimg4 = Images + 'oceanzero-img4.png';
let oceannZeroimg2 = Images + 'oceanzero-img2.png';
let oceannZeroimg1 = Images + 'oceanzero-img1.png';

const OceannZero = () => {
  const navigator = useNavigate();
  const card = [
    {
      paraicon: OurServicesicon1,
      paraTitle: "Regulatory & Environmental Compliance",
      para: "EU ETS, FuelEU Regulatory Reporting IMO DCS, EU & UK MRV",
    },

    {
      paraicon: OurServicesicon2,
      paraTitle: "Fleet Performance Analytics",
      para: "Fleet overview with performance indicators fuel model visualisation performance optimization",
    },

    {
      paraicon: OurServicesicon3,
      paraTitle: "Post Voyage Analysis",
      para: "Vessel performance monitoring and its management, decarbonisation solutions,analyses speed & consumption .",
    },

    {
      paraicon: OurServicesicon4,
      paraTitle: "Hull Performance Monitoring",
      para: "Hull degradation monitoring real time hull fouling risk alerts.",
    },
    {
      paraicon: OurServicesicon5,
      paraTitle: "Emission Optimization",
      para: "Alternative fuels, technologies and maintenance and engine tuning",
    },
  ];

  const benefit = [
    {
      img: featureicon1,
      heading: "Emission Tracking",
    },

    {
      img: featureicon2,
      heading: "Renewable Energy Integration",
    },

    {
      img: featureicon3,
      heading: "Dashboard Visualization",
    },
    {
      img: featureicon4,
      heading: "Predictive Analysis",
    },
    {
      img: featureicon5,
      heading: "Geo Spatial Analysis",
    },
    {
      img: featureicon6,
      heading: "Machine Learning For Anomaly Detection",
    },
  ];

  return (
    <>
      <div>
        <div className="relative lg:h-[100vh] h-[65vh]">
          <img
            src={oceannZeroimg1}
            alt="Description of the"
            className="w-full h-full object-cover object-center absolute top-0 left-0 z-0  "
          />
          <div className="relative z-10 flex flex-col items-center justify-center h-full text-center bg-[#00000083]">
            <div className="text-center leading-tight text-[24px] md:text-[40px] lg:text-xl text-white bold">
              <Animatedwordchar text="Chartering a Greener Path for Worldwide Trade" />
            </div>
            <p className="text-sm mt-3 sm:text-sm md:text-md w-[95%] md:w-[60%] text-white">
              The oceann’s latest product, Oceannzero, delves into the
              formidable task of decarbonization in the maritime industry and
              outlines strategies for the short, intermediate, and long haul
              that can guide us toward the correct course.
            </p>

            <Link to="/demo">
              <div>
                <motion.button
                  style={{}}
                  onMouseOver={(e) => (e.target.style.color = "#ffffff")}
                  onClick={() => navigator("/product/chartering")}
                  whileHover={{ scale: 1.04, backgroundColor: "#003e78" }}
                  className=" bg-[#f39c12] text-white border-[2px] px-[24px] mt-4 py-[8px] rounded-[42px] border-none"
                >
                  Learn More
                </motion.button>
              </div>
            </Link>
          </div>
        </div>

        {/* Partner section */}
        <ScrollAnimation>
          <div className="my-[1rem] mx-[1rem] max-sm:mx-[1rem]">
            <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-10 ">
              <span className="lg:text-2xl">Trusted collaborators</span>
            </h1>
            <Partners />
          </div>
        </ScrollAnimation>
        {/* Partner section end */}

        <ScrollAnimation>
          <section className="flex flex-col md:gap-6 my-[3rem] lg:my-[6rem] items-center mx-[2rem] lg:mx-[6rem] xl:mx-[12rem] xl:gap-[4rem]">
            <div className="flex justify-start items-center gap-1 md:mt-[0.2rem]">
              <h3 className="text-[24px] xl:text-[3rem] leading-9 max-sm:text-center max-sm:mt-0 text-left  font-semibold ">
                Our Origin 2
              </h3>
            </div>
            <div className="flex flex-wrap max-sm:flex-col justify-between max-sm:items-start max-md:mt-10 gap-[2rem] lg:gap-[3rem]">
              <div className="flex flex-1 object-cover">
                <img
                  src={oceannZeroimg2}
                  className="object-cover rounded-md "
                  alt=""
                />
              </div>
              <div className="flex flex-col items-center md:items-start flex-1 max-sm:w-full max-sm:ml-0">
                <h3 className="text-md xl:text-[2.2rem] leading:tight xl:leading-[2.75rem] max-sm:text-center max-sm:mt-2 text-left sm:text-lg md:text-lg font-semibold gap-4 text-[#003E78]">
                  Oceann Funds: Navigating Your Ship Financing Journey
                </h3>
                <p className="mt-3 lg:text-[16px] max-sm:text-center text-black max-sm:text-sm">
                  In this ever-evolving maritime landscape, securing appropriate
                  funding for your ship cargo ventures is paramount. Let us
                  guide you through the various aspects of ship cargo financing
                  and illuminate the path to financial stability and operational
                  excellence.
                </p>
                <div>
                  <motion.button
                    style={{}}
                    onMouseOver={(e) => (e.target.style.color = "#ffffff")}
                    onClick={() => navigator("/product/oceann-finance")}
                    whileHover={{ scale: 1.04, backgroundColor: "#003e78" }}
                    className=" bg-[#f39c12] text-white px-[24px] mt-4 py-[8px] rounded-[42px] "
                  >
                    Learn More
                  </motion.button>
                </div>
              </div>
            </div>
          </section>
        </ScrollAnimation>

        <div className=" mx-auto">
          <div className="my-[5rem] max-sm:mx-[1rem] lg:my-[10rem]">
            <ScrollAnimation>
              <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
                <span className="lg:text-2xl">Why Oceann Zero?</span>
              </h1>
              {/* oceann Zero card component */}
              <div className="p-2">
                <Oceannzerocard />
              </div>
            </ScrollAnimation>

            <section className="flex flex-col my-[3rem] lg:my-[6rem] items-center mx-[1rem] xl:mx-[6rem] ">
              <div className="flex justify-start items-center gap-2 mt-[2rem]">
                <h3 className="text-[24px] xl:text-[3rem] leading-9 max-sm:text-center text-left sm:text-lg md:text-lg font-semibold mb-[1rem]">
                  AI Driven Performance Optimization
                </h3>
              </div>
              <div className="flex flex-col justify-center items-center lg:mx-[10rem] max-sm:flex-col max-sm:items-start ">
                <img
                  src={oceannZeroimg4}
                  className="text-center rounded-md w-[50rem]"
                  alt=""
                />
                <p className="lg:text-[16px] max-sm:text-center text-black max-sm:text-sm text-center">
                  Our machine learning & algorithm-powered route planning
                  solution instantly generates the shortest and most
                  cost-efficient routes based on 100s of business, service,
                  order, and vehicle constraints to achieve optimal operational
                  efficiency. We can also provide guidance on minimizing fuel
                  usage to lower emissions and achieve an elevated CII rating.
                </p>
              </div>
            </section>

            <ScrollAnimation>
              <div className="flex justify-center items-center gap-2 my-[2rem] xl:my-[8rem]">
                <h3 className="text-[24px] xl:text-[3rem] leading-9 max-sm:text-center max-sm:mt-2 text-center sm:text-lg md:text-lg font-semibold gap-4">
                  What we offer?
                </h3>
              </div>
              <div className="flex flex-col-reverse my-[8px] gap-[18px] lg:my-[1rem] md:flex-row mx-[1.25rem] lg:mx-[6rem] lg:gap-[4rem]">
                <div className="flex flex-col flex-1 lg:ml-[2rem]">
                  <h1 className="text-[20px] lg:text-[28px] font-bold mb-[8px] lg:mb-4 leading-[24px]">
                    Our Services
                  </h1>
                  <div className="flex flex-col gap-[1rem] ">
                    {card.map((data) => (
                      <div className="flex flex-col ">
                        <div className="flex flex-row gap-2 items-center ">
                          <img
                            src={data.paraicon}
                            alt=""
                            className="h-[20px] bg-[#003e78] rounded-[16px] p-[3px]"
                          />
                          <p className="text-black font-semibold text-[16px]">
                            {data.paraTitle}
                          </p>
                        </div>
                        <p className="md:pl-[1.8rem] font-light text-[#5a5a5a]">
                          {data.para}
                        </p>
                      </div>
                    ))}
                  </div>
                </div>
                <div className="flex flex-1 justify-center items-center rounded-lg">
                  <img
                    src={co2img}
                    alt=""
                    className="object-cover rounded-lg"
                  />
                </div>
              </div>
            </ScrollAnimation>

            {/* <div>
              <h1 className="text-md text-center sm:text-md md:text-xl font-semibold text-[#003E78] mt-5 mb-10">
                Our Se
                <span className="border-b-4 border-[#F39C12]">rvices</span>
              </h1>
            </div> */}
          </div>
          {/* OurServiceCard */}

          {/* <div className="grid grid-cols-1 gap-10 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5">
          
            <div className="shadow-xl rounded-2xl  bg-blue-600 flex flex-col justify-center items-center mt-10 relative">
              <div className="bg-blue-600 rounded-2xl h-1/2 flex flex-col items-center justify-center relative">
                <div className="rounded-full bg-blue-600 w-32 h-32 border-2 flex flex-col items-center justify-center border-white absolute  -mt-20 md:-mt-32 mx-auto mb-4">
                  <img
                    src={OurServicesicon1}
                    alt="OurServicesicon1"
                    className="w-1/2"
                  />
                </div>
                <div className=" justify-center justify-items-center relative">
                  <p className="text-white text-xs sm:text-sm md:text-md text-center bottom-2 p-3 mb-2 mt-20 ">
                    Regulatory & Environmental Compliance
                  </p>
                </div>
              </div>
              <div className="bg-white h-1/2 w-full  rounded-b-2xl flex flex-col pt-1 ">
                <ul className="list-disc pl-6 pr-6 text-xs sm:text-sm md:text-md top-0">
                  <li>EU ETS, FuelEU</li>
                  <li>Regulatory</li>
                  <li>Reporting IMO DCS, EU & UK MRV</li>
                </ul>
              </div>
            </div>

          
            <div className="shadow-xl rounded-2xl  bg-[#119BBf] flex flex-col justify-center items-center mt-10 relative">
              <div className="bg-[#119BBf] rounded-2xl h-1/2 flex flex-col items-center justify-center relative">
                <div className="rounded-full bg-[#119BBf] w-32 h-32 border-2 flex flex-col items-center justify-center border-white absolute  -mt-20 md:-mt-32 mx-auto mb-4">
                  <img
                    src={OurServicesicon2}
                    alt="OurServicesicon1"
                    className="w-1/2"
                  />
                </div>
                <div className=" justify-center justify-items-center relative">
                  <p className="text-white text-xs sm:text-sm md:text-md text-center bottom-2 p-3 mb-2 mt-20 ">
                    Fleet Performance Analytics
                  </p>
                </div>
              </div>
              <div className="bg-white h-1/2 w-full  rounded-b-2xl flex flex-col  pt-1">
                <ul className="list-disc pr-6 pl-6 text-xs sm:text-sm md:text-md">
                  <li>Fleet Overview with Performance Indicators</li>
                  <li>Fuel Model Visualisation</li>
                  <li>Performance Optimization</li>
                </ul>
              </div>
            </div>
            
            <div className="shadow-xl rounded-2xl  bg-[#22AD86] flex flex-col justify-center items-center mt-10 relative">
              <div className="bg-[#22AD86] rounded-2xl h-1/2 flex flex-col items-center justify-center relative">
                <div className="rounded-full bg-[#22AD86] w-32 h-32 border-2 flex flex-col items-center justify-center border-white absolute  -mt-20 md:-mt-32 mx-auto mb-4">
                  <img
                    src={OurServicesicon3}
                    alt="OurServicesicon3"
                    className="w-1/2"
                  />
                </div>
                <div className=" justify-center justify-items-center relative">
                  <p className="text-white text-xs sm:text-sm md:text-md text-center bottom-2 p-3 mt-20 ">
                    Post Voyage Analysis
                  </p>
                </div>
              </div>
              <div className="bg-white h-1/2 w-full  rounded-b-2xl flex flex-col justify-items-center justify-center">
                <ul className="list-disc p-6 text-xs sm:text-sm md:text-md">
                  <li>Vessel performance monitoring and its management.</li>
                  <li> Decarbonisation solutions.</li>
                  <li>Analyses speed & consumption .</li>
                </ul>
              </div>
            </div>

       
            <div className="shadow-xl rounded-2xl  bg-[#6AB737] flex flex-col justify-center items-center mt-10 relative">
              <div className="bg-[#6AB737] rounded-2xl h-1/2 flex flex-col items-center justify-center relative">
                <div className="rounded-full bg-[#6AB737] w-32 h-32 border-2 flex flex-col items-center justify-center border-white absolute  -mt-20 md:-mt-32 mx-auto mb-4">
                  <img
                    src={OurServicesicon4}
                    alt="OurServicesicon4"
                    className="w-1/2"
                  />
                </div>
                <div className=" justify-center justify-items-center relative">
                  <p className="text-white text-xs sm:text-sm md:text-md text-center bottom-2 p-3 mb-2 mt-20 ">
                    Hull Performance Monitoring
                  </p>
                </div>
              </div>
              <div className="bg-white h-1/2 w-full  rounded-b-2xl flex flex-col  ">
                <ul className="list-disc pr-6 pl-6 pt-2 text-xs sm:text-sm md:text-md">
                  <li>Hull degradation monitoring</li>
                  <li>Real time hull fouling risk alerts.</li>
                </ul>
              </div>
            </div>

            <div className="shadow-xl rounded-2xl  bg-[#003E78] flex flex-col justify-center items-center mt-10 relative">
              <div className="bg-[#003E78] rounded-2xl h-1/2 flex flex-col items-center justify-center relative">
                <div className="rounded-full bg-[#003E78] w-32 h-32 border-2 flex flex-col items-center justify-center border-white absolute  -mt-20 md:-mt-32 mx-auto mb-4">
                  <img
                    src={OurServicesicon5}
                    alt="OurServicesicon5"
                    className="w-1/2"
                  />
                </div>
                <div className=" justify-center justify-items-center relative">
                  <p className="text-white text-xs sm:text-sm md:text-md text-center bottom-2 p-3 mb-2  mt-20 ">
                    Emission Optimization
                  </p>
                </div>
              </div>
              <div className="bg-white h-1/2 w-full  rounded-b-2xl flex flex-col pt-2">
                <ul className="list-disc pr-6 pl-6 text-xs sm:text-sm md:text-md">
                  <li>Alternative Fuels and Technologies</li>
                  <li>Maintenance and Engine Tuning</li>
                </ul>
              </div>
            </div>
          </div> */}

          <ScrollAnimation>
            <section className="flex flex-col my-[3rem] lg:my-[6rem] items-center mx-[2rem] lg:mx-[6rem] xl:mx-[12rem] xl:gap-[4rem]">
              <div className="flex flex-wrap max-sm:flex-col justify-between max-sm:items-start max-md:mt-10 gap-[2rem] lg:gap-[3rem]">
                <div className="flex flex-1 object-cover">
                  <img
                    src={oceannZeroimg1}
                    className="object-cover rounded-md "
                    alt=""
                  />
                </div>
                <div className="flex flex-col flex-1 max-sm:w-full max-sm:ml-0">
                  <h3 className="text-md xl:text-[2.2rem] leading-[2.75rem] max-sm:text-center max-sm:mt-2 text-left sm:text-lg md:text-lg font-semibold gap-4 text-[#003E78]">
                    Why Choose Us?
                  </h3>
                  <p className="mt-3 lg:text-[16px] max-sm:text-center text-black max-sm:text-sm">
                    TheOceann is committed to providing concrete, measurable
                    advantages through result-driven procedures, utilizing our
                    worldwide connections and economies of scale. TheOceann
                    functions as an extension of your in-house team, dedicated
                    to assisting you in data interpretation and the maintenance
                    of control and efficiency. Our focus is on delivering
                    tailored reports to empower you in adapting to and
                    successfully addressing the ever-evolving market intricacies
                    and business requirements.
                  </p>
                </div>
              </div>
            </section>
          </ScrollAnimation>

          <section>
            <ScrollAnimation>
              <div className="my-[3.5rem] lg:my-[4rem] bg-[#ecf0ff]  py-[2rem]">
                <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-5 ">
                  <span className="text-[24px] lg:text-[32px]">Features</span>
                </h1>
                <div className="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 mx-[24px] my-[1rem] lg:my-[3rem] xl:mx-[6.4rem] xl:gap-8  ">
                  {benefit.map((data) => (
                    <div
                      style={{
                        borderColor: "#e0e0e0",
                        borderWidth: "1px",
                      }}
                      className="border-[2px] relative flex flex-col justify-items-center justify-center py-6 px-6 rounded-sm hover:shadow-xl overflow-hidden section-2-cart "
                    >
                      <div className="p-[4px]">
                        <img
                          src={data.img}
                          alt=""
                          className="h-[32px] md:h-[48px] bg-[#ffffea] border-[1px] border-[#F39C12] lg:rounded-xl rounded-md p-[8px]"
                        />
                      </div>
                      <div className="flex flex-col mt-2">
                        <h1 className="text-md sm:text-[14px] md:text-[20px] font-semibold my-2">
                          {data.heading}
                        </h1>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </ScrollAnimation>
          </section>
        </div>
      </div>
    </>
  );
};

export default OceannZero;
