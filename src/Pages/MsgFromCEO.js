import React from "react";
import {
  ScrollAnimation,
  animationVariants,
} from "../Componants/motion/scrollanimation";
import { Row, Col, Card, Avatar } from 'antd';
import Animatedwordchar from "../Componants/motion/animatewordchar";
import team from "../Componants/Assets/Product-img/our-team-bg.jpg";
import Partners from "../Landing_Page_Components/Partners";
import Mentorship from "./mentorship";

const { Meta } = Card;

const CEO = () => {
  // Example team members data
  const teamMembers = [
    { name: 'John Doe', role: 'CEO', avatar: 'https://aliciabots.com/images/sanjay.jpg' },
    { name: 'Jane Smith', role: 'CTO', avatar: 'https://aliciabots.com/images/sanjay.jpg' },
    { name: 'Michael Brown', role: 'Developer', avatar: 'https://aliciabots.com/images/sanjay.jpg' },
    // Add more team members as needed
  ];

  return (
    <>
      <main>
        <div
          className=" w-full h-[50vh] lg:h-[100vh] flex items-center w-[100%] bg-cover max-sm:bg-contain  bg-no-repeat max-md:bg-cover mt-[66px]"
          style={{
            backgroundImage: `url(${'https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/Message+from+the+CEO.png'})`,

          }}
        >

        </div>
      </main>

      {/* Partner section */}
      <ScrollAnimation>
        {/* 
        <div className="flex w-[100%] my-[1rem] mx-[1rem] max-sm:mx-[1rem]">
       
          <div className="flex items-center w-[50%]  "
            style={{
              backgroundImage: `url(${'https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/Message+from+the+CEO.png'})`,

            }}></div>
        
        </div> */}
      </ScrollAnimation>
      {/* Partner section end */}
      {/* Partner section */}
      <ScrollAnimation>
        <div className="my-[1rem] mx-[1rem] max-sm:mx-[1rem]">
          <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-10 ">
            <span className="lg:text-2xl">Trusted collaborators</span>
          </h1>
          <Partners />
        </div>
      </ScrollAnimation>
      {/* Partner section end */}

      {/* <section className=" flex justify-center align-middle mt-[4rem] ">
        <div className="flex flex-col w-[56vw] border-[1px] p-8 rounded-md gap-[0.2rem]">
          <h2 className="text-xl font-bold py-4">Legal Information</h2>
          <div className="grid grid-cols-2">
            <p className="font-semibold">Company Name</p>
            <p className="font-bold">Maritime Info Lab Pvt LTd.</p>
          </div>
          <div className="grid grid-cols-2">
            <p className="font-semibold">Address</p>
            <p>
              B-431/432, Alphathum By Bhutani, Sector 90, Noida, Uttar Pradesh,
              India-201305
            </p>
          </div>

          <div className="grid grid-cols-2">
            <p className="font-semibold">Contact</p>
            <p className="font-bold">+919871626990</p>
          </div>

          <p className="text-[20px] mt-10 mb-2 font-bold">
            Domain Information for theoceann.ai
          </p>
          <div className="grid grid-cols-2">
            <p className="font-semibold">Registrar</p>
            <p>Google Domains</p>
          </div>

          <div className="grid grid-cols-2">
            <p className="font-semibold">Registration Date</p>
            <p>February 15, 2022</p>
          </div>

          <div className="grid grid-cols-2">
            <p className="font-semibold">Registrant</p>
            <p>
              Maritime Info Lab Pvt LTd. <br /> B-431/432, ALphathum by Bhutani,
              Sector 90, Noida, Uttar Pradesh, India-201305
            </p>
          </div>
          <div className="grid grid-cols-2">
            <p className="font-semibold">Cloud Hoisting</p>
            <p>
              <b>Amazon Web Services,</b> Inc. 410 Terry Avenue North Seattle,
              WA 98109-5210, United States
            </p>
          </div>
        </div>
      </section> */}
    </>
  );
};

export default CEO;
