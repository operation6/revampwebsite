import React from "react";
// import pretradeintelligenceimg1 from "../Componants/Assets/Pre-trade-intelligence-img/pre-trade-intelligence-img1.png";
import mail_chat_new from "../Componants/Assets/trade-intelligence/mail_chat_new.png";
// import pretradeintelligenceimg3 from "../Componants/Assets/Pre-trade-intelligence-img/pre-trade-intelligence-img3.png";
// import pretradeintelligenceimg4 from "../Componants/Assets/Pre-trade-intelligence-img/pre-trade-intelligence-img4.png";
// import pretradeintelligenceimg5 from "../Componants/Assets/Pre-trade-intelligence-img/pre-trade-intelligence-img5.png";
// import pretradeintelligenceimg6 from "../Componants/Assets/Pre-trade-intelligence-img/pre-trade-intelligence-img6.png";
import pretradeintelligenceimg7 from "../Componants/Assets/trade-intelligence/pretradeintelligenceimg7.png";
import Partners from "../Landing_Page_Components/Partners";
import { ScrollAnimation } from "../Componants/motion/scrollanimation";
import { Images } from "../assests";
// let pretradeintelligenceimg7 = Images + "pre-trade-intelligence-img7.png";
let pretradeintelligenceimg6 = Images + "pre-trade-intelligence-img6.png";
let pretradeintelligenceimg5 = Images + "pre-trade-intelligence-img5.png";
let pretradeintelligenceimg4 = Images + "pre-trade-intelligence-img4.png";
let pretradeintelligenceimg3 = Images + "pre-trade-intelligence-img3.png";
// let mail_chat_old = Images + 'mail_chat_old.png';
let pretradeintelligenceimg1 = Images + "pre-trade-intelligence-img1.png";

const PreTradeIntelligence = () => {
  return (
    <>
      <div>
        <div className="relative h-[50vh] md:h-[100vh]">
          <img
            src={pretradeintelligenceimg1}
            alt="Description of the"
            className="w-full h-full object-center absolute top-0 left-0 z-0"
          />
          <div className="relative z-10 flex flex-col items-center justify-center w-full md:w-[60vw] h-full text-center">
            <h1 className="text-lg sm:text-xl md:text-2xl font-semibold text-white">
              Pre-Trade Intelligence and Analytics
            </h1>
            <p className="text-sm sm:text-sm md:text-md w-full md:w-[50vw] text-white">
              Strengthen your market analysis by leveraging accurate,
              up-to-the-minute data and advanced market analytics
            </p>
          </div>
        </div>

        {/* Partner section */}
        <ScrollAnimation>
          <div className="my-[1rem] mx-[1rem] max-sm:mx-[1rem]">
            <h1 className="text-md mb-0 text-center sm:text-lg md:text-xl font-semibold mt-1 md:mt-10 ">
              <span className="lg:text-2xl">Trusted collaborators</span>
            </h1>
            <Partners />
          </div>
        </ScrollAnimation>
        {/* Partner section end */}

        <div className="w-[90vw]   mx-auto">
          <h1 className="text-md text-center sm:text-lg md:text-xl font-semibold text-[#003E78] mt-[2rem] ">
            <span className="border-b-4 border-[#F39C12]">Our Pr</span>
            e-Trade Intelligence Features
          </h1>

          <div className="md:flex flex-row-reverse space-between items-center gap-10 mt-[2rem] md:mt-[2rem] mx-auto">
            <img
              src={mail_chat_new}
              alt="oceannZeroimg4"
              className="md:w-[40%] h-80 rounded-lg mx-auto"
              style={{ filter: "blur(1.3px)" }}
            />
            <div className="text-sm sm:text-md md:text-md lg:text-lg text-[#747070]">
              <h1 className="text-md text-center md:text-left sm:text-lg md:text-lg font-semibold text-[#003E78] mt-1 mb-1 p-2 md:p-5 pb-0 mx-auto">
                AI based Email Solution for Shipping
              </h1>
              <p className="p-5 text-sm sm:text-md md:text-md text-[#747070]">
                Our AI-driven shipping email solution transforms logistics
                communication with automation, real-time tracking, and
                predictive analytics, ensuring a seamless and customer-centric
                experience, all while boosting efficiency. Say farewell to
                manual emails and embrace our tailored, smart, and responsive
                system.
              </p>
            </div>
          </div>

          <div className=" md:flex gap-10 mt-[2rem] md:mt-[3rem] mx-auto">
            <img
              src={pretradeintelligenceimg3}
              alt="oceanuimg2"
              className="  md:w-[40%] h-80 rounded-[10%] mx-auto "
            />
            <div>
              <h1 className="text-md text-center md:text-left sm:text-lg md:text-lg font-semibold text-[#003E78] mt-1 mb-1   p-2 md:p-5">
                Market Screens- <br />
                <span className="border-b-4 border-[#F39C12]  ">Tonna</span>
                ge, Cargo trade
              </h1>
              <p className="p-5 text-sm sm:text-md md:text-md text-[#747070]">
                Market screens for tonnage and cargo trade provide valuable
                insights into the shipping industry, helping businesses make
                informed decisions. These screens offer real-time data and
                analytics on vessel tonnage availability and cargo trade trends,
                empowering stakeholders with the information they need to
                optimize their operations and stay ahead in this dynamic sector.
              </p>
            </div>
          </div>
          <div className="md:flex flex-row-reverse gap-10 mt-[2rem] md:mt-[3rem] mx-auto">
            <img
              src={pretradeintelligenceimg4}
              alt="oceannZeroimg4"
              className="md:w-[40%] h-80 rounded-[10%] mx-auto"
            />
            <div className="text-sm sm:text-md md:text-md lg:text-lg text-[#747070]">
              <h1 className="text-md text-center md:text-left  sm:text-lg md:text-lg font-semibold text-[#003E78] mt-1 mb-1   p-2 md:p-5">
                Market Demand and  <br />
                <span className="border-b-4 border-[#F39C12]">Suppl</span>y
                Analytics
              </h1>
              <p className="p-5 text-sm sm:text-md md:text-md text-[#747070]">
                Analytics for market demand and supply are crucial for
                businesses seeking a profound understanding of their industry's
                dynamics. These insights offer valuable information about the
                balance between customer demand and the availability of products
                or services. Through data analysis of trends and patterns,
                companies can make informed decisions, enhance their operations,
                and remain competitive in an ever-changing market environment.
              </p>
            </div>
          </div>
          <div className=" md:flex gap-10 mt-[2rem] md:mt-[3rem] mx-auto">
            <img
              src={pretradeintelligenceimg5}
              alt="oceanuimg2"
              className="  md:w-[40%] h-80 rounded-[10%] mx-auto "
            />
            <div>
              <h1 className="text-md text-center md:text-left sm:text-lg md:text-lg font-semibold text-[#003E78] mt-1 mb-1   p-2 md:p-5">
                Map Overlay, AI <br />
                <span className="border-b-4 border-[#F39C12]  ">enabl</span>e
                Estimate
              </h1>
              <p className="p-5 text-sm sm:text-md md:text-md text-[#747070]">
                Map overlay and AI-powered estimation enable precise and
                data-driven decision-making. These technologies allow businesses
                to superimpose critical information on maps and utilize AI
                algorithms to generate accurate estimates, enhancing efficiency
                and strategic planning across various industries.
              </p>
            </div>
          </div>
          <div className="md:flex flex-row-reverse gap-10 mt-[2rem] md:mt-[3rem] mx-auto">
            <img
              src={pretradeintelligenceimg6}
              alt="oceannZeroimg4"
              className="md:w-[40%] h-80 rounded-[10%] mx-auto"
            />
            <div className="text-sm sm:text-md md:text-md lg:text-lg text-[#747070]">
              <h1 className="text-md text-center md:text-left sm:text-lg md:text-lg font-semibold text-[#003E78] mt-1 mb-1   p-2 md:p-5">
                Live Tracking, Bunker <br />
                <span className="border-b-4 border-[#F39C12]">Live, P</span>
                ort Data
              </h1>
              <p className="p-5 text-sm sm:text-md md:text-md text-[#747070]">
                Live tracking, bunker data, and real-time port information
                provide an integrated solution for the shipping industry. With
                live tracking, companies can monitor vessels, while bunker data
                and port updates offer valuable insights for efficient voyage
                planning, fuel management, and port operations. This integrated
                approach ensures transparency, cost savings, and operational
                excellence in maritime logistics
              </p>
            </div>
          </div>
          <div className=" md:flex gap-10 mt-[2rem] md:mt-[3rem] mx-auto">
            <img
              src={pretradeintelligenceimg7}
              alt="oceanuimg2"
              className="md:w-[30%] h-80 rounded-[10%] mx-auto "
            />
            <div>
              <h1 className="text-md text-center md:text-left sm:text-lg md:text-lg font-semibold text-[#003E78] mt-1 mb-1 p-2 md:p-5">
                The Oceann AI <br />
                <span className="border-b-4 border-[#F39C12]">
                  WhatsApp channel
                </span>
                &nbsp;provides real-time maritime insights, <br />
                from vessel schedules to port costs, streamlining operations
                with easy, instant access.
              </h1>
              <p className="p-5 text-sm sm:text-md md:text-md text-[#747070]">
                <ul style={{ listStyle: "inside" }}>
                  <li>Check scheduled vessels for specific ports</li>
                  <li>Access live bunker prices by location</li>
                  <li>Track current vessel positions</li>
                  <li>Retrieve detailed vessel information</li>
                  <li>Calculate distances between ports</li>
                  <li>Identify a vessel's next port of call</li>
                </ul>
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default PreTradeIntelligence;
