import { Card, Col, Image, Layout, List, Row, Spin, Typography } from "antd";
import moment from "moment/moment";
import React, { useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import { Images } from "../assests";
import interceptorFunction from "../services/interceptot";

const { Content, Sider } = Layout;
const { Title, Text } = Typography;

const News = () => {
  const [newsLatest, setNewsLatest] = useState([]);
  const [currentNews, setCurrentNews] = useState();
  const [newsId, setNewsId] = useState();
  const [loading, setLoading] = useState(false);
  const autoScrollRef = useRef(null);
  const inactivityTimeoutRef = useRef(null);
  const autoScrollIntervalRef = useRef(null);

  useEffect(() => {
    fetchNewsLatest();
    resetInactivityTimer();
    document.addEventListener("mousemove", resetInactivityTimer);
    document.addEventListener("click", resetInactivityTimer);
    return () => {
      document.removeEventListener("mousemove", resetInactivityTimer);
      document.removeEventListener("click", resetInactivityTimer);
      clearInterval(autoScrollIntervalRef.current);
      clearTimeout(inactivityTimeoutRef.current);
    };
  }, []);

  useEffect(() => {
    setCurrentNews(newsLatest[0]);
  }, [newsLatest]);

  const fetchNewsLatest = async () => {
    try {
      setLoading(true);
      const url = `${process.env.REACT_APP_BASE_URL}accounts/getting-news-letter-data`;
      const options = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      };

      const response = await interceptorFunction(url, options);
      const data = await response.json();

      if (data.length > 0) {
        setLoading(false);
        const filteredCurrentNews = data?.filter(news => news.status !== 0);
        setNewsLatest(filteredCurrentNews);
        setCurrentNews(filteredCurrentNews[0]);
        setNewsId(filteredCurrentNews[0].id);
      } else {
        setLoading(false);
        toast.info("No news available for now.");
      }
    } catch (error) {
      console.error(error);
      setLoading(false);
    }
  };

  const resetInactivityTimer = () => {
    clearTimeout(inactivityTimeoutRef.current);
    clearInterval(autoScrollIntervalRef.current);
    inactivityTimeoutRef.current = setTimeout(() => {
      startAutoScroll();
    }, 5000); // 5 seconds of inactivity
  };

  const startAutoScroll = () => {
    autoScrollIntervalRef.current = setInterval(() => {
      // autoScroll();
    }, 10); // Scroll every 100ms for smooth scrolling
  };

  const autoScroll = () => {
    if (autoScrollRef.current) {
      const { scrollTop, clientHeight, scrollHeight } = autoScrollRef.current;

      // Check if the scroll has reached the bottom
      if (scrollTop + clientHeight >= scrollHeight) {
        // Reset scroll position to the top
        autoScrollRef.current.scrollTo({ top: 0, behavior: "smooth" });

        // Stop auto-scrolling temporarily
        clearInterval(autoScrollIntervalRef.current);

        // Optionally restart scrolling after some delay if desired
        inactivityTimeoutRef.current = setTimeout(() => {
          startAutoScroll();
        }, 2000); // Restart after 2 seconds
      } else {
        // Continue scrolling
        autoScrollRef.current.scrollBy({
          top: 1, // Scroll by 1 pixel each time
          behavior: "smooth", // Smooth scrolling behavior
        });
      }
    }
  };

  // const autoScroll = () => {
  //   if (autoScrollRef.current) {
  //     autoScrollRef.current.scrollBy({
  //       top: 1, // Scroll by 1 pixel each time
  //       behavior: "smooth", // Smooth scrolling behavior
  //     });

  //     // If the scroll reaches the bottom, scroll back to the top
  //     if (
  //       autoScrollRef.current.scrollTop + autoScrollRef.current.clientHeight >=
  //       autoScrollRef.current.scrollHeight
  //     ) {
  //       autoScrollRef.current.scrollTo({ top: 0, behavior: "smooth" });
  //     }
  //   }
  // };

  const DetailNews = ({ news }) => {
    return (
      <div className="detail_newss_card">
        <Card bordered={false}>
          <div className="news_card_content">
            <Title level={4}>{news?.title}</Title>
            <Text type="secondary">
              {moment(news?.created_at).format("MMMM DD, YYYY")}
            </Text>
            <Image src={news?.image_urls[1].url} alt="News" />
            <div className="news_card_meta">
              <Text>
                Published By - <b>{news?.created_by}</b>
              </Text>
            </div>
            <p
              className="news_card_description"
              dangerouslySetInnerHTML={{ __html: news?.details }}
            ></p>
          </div>
        </Card>
      </div>
    );
  };

  const handleCurrent = (news) => {
    setCurrentNews(news);
    setNewsId(news.id);
    resetInactivityTimer(); // Reset inactivity timer on click
  };

  let solnImg = Images + "solutionImg.png";

  function removeHTMLTags(html) {
    var tempDiv = document.createElement("div");
    tempDiv.innerHTML = html;
    return tempDiv.textContent || tempDiv.innerText || "";
  }

  return (
    <>
      <main>
        <div
          className="h-[60vh] md:h-[85vh] flex justify-center items-center bg-[#00000083] bg-blend-darken"
          style={{
            backgroundImage: `url(${solnImg})`,
            backgroundSize: "100% 100%",
            backgroundPosition: "",
            objectFit: "cover",
          }}
        >
          <div className="flex flex-col justify-between text-center">
            <div className="first-child" style={{ display: "flex" }}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  flex: "1",
                  fontSize: "30px",
                  fontStyle: "normal",
                  fontWeight: 600,
                  lineHeight: "normal",
                  textTransform: "capitalize",
                }}
              >
                <h1 style={{ color: "white", fontSize: "40px" }}>
                  <u>NEWS</u>
                </h1>
                <div style={{ color: "white", fontSize: "20px" }}>
                  Maritime Matters Unfold: Your Ocean of News
                </div>
              </div>
            </div>

            <div
              style={{
                color: "#9F7272",
                fontFamily: "Inter",
                fontSize: "12px",
                fontStyle: "normal",
                lineHeight: "100.9%",
                marginTop: "20px",
              }}
            >
              <Link to="/demo">
                <button
                  style={{
                    backgroundColor: "#F39C12",
                    width: "125px",
                    height: "40px",
                    flexShrink: 0,
                    fontWeight: 600,
                    color: "white",
                    marginTop: "1.5rem",
                    borderRadius: "40px",
                  }}
                >
                  BOOK DEMO
                </button>
              </Link>
            </div>
          </div>
        </div>
      </main>

      <Layout>
        <Content style={{ padding: "0 20px" }}>
          <div
            className="news_container"
            style={{
              background: "#fff",
              padding: 24,
              minHeight: 280,
              fontFamily: "Poppins",
            }}
          >
            {newsLatest.length > 0 ? (
              <Row gutter={30}>
                <Col span={16}>
                  <DetailNews news={currentNews} />
                </Col>

                <Col span={8} className="news_list_sider">
                  <p style={{ paddingBlock: "5px" }}>
                    <b>News List</b>
                  </p>
                  <Sider
                    title="News List"
                    width={"100%"}
                    style={{
                      background: "#fff",
                      maxHeight: "1200px",
                      overflowY: "auto",
                    }}
                  // ref={autoScrollRef}
                  >
                    {newsLatest.length > 0 && (
                      <List
                        itemLayout="horizontal"
                        dataSource={newsLatest.filter(
                          (news) => news.status === 1
                        )}
                        renderItem={(news) => (
                          <List.Item
                            key={news.id}
                            onClick={() => handleCurrent(news)}
                            className={newsId === news.id ? "active" : ""}
                            style={{ cursor: "pointer", marginBottom: 16 }}
                          >
                            <List.Item.Meta
                              avatar={
                                <Image
                                  width={130}
                                  src={news.image_urls[0].url}
                                  alt="Thumbnail"
                                />
                              }
                              title={
                                <span
                                  className="ellipsis2"
                                  style={{ fontWeight: "bold" }}
                                >
                                  {news.title}
                                </span>
                              }
                              description={
                                <p className="news_card_description ellipsis">
                                  {removeHTMLTags(news.details)}
                                </p>
                              }
                            />
                          </List.Item>
                        )}
                      />
                    )}
                  </Sider>
                </Col>
              </Row>
            ) : (
              <>
                <h2 style={{ textAlign: "center" }}>
                  No News Available for now.
                </h2>
                <Spin fullscreen spinning={loading} />
              </>
            )}
          </div>
        </Content>
      </Layout>


      {/* Partner section */}
      {/* <ScrollAnimation>
        <div className="my-[1rem] mx-[1rem] max-sm:mx-[1rem]">
          <Title level={2} className="text-center">
            Our Partners
          </Title>
          <Partners />
        </div>
      </ScrollAnimation> */}
      {/* Partner section end */}
    </>
  );
};

export default News;
