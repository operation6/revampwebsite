import React from "react";
import {
  ScrollAnimation,
  animationVariants,
} from "../Componants/motion/scrollanimation";
import Animatedwordchar from "../Componants/motion/animatewordchar";
import legalInformation from "../Componants/Assets/Product-img/legal-information.png";
import Partners from "../Landing_Page_Components/Partners";

const LegalInformation = () => {
  return (
    <>
      <main>
        <div
          className="h-[40vh] lg:h-[100vh] flex items-center justify-center bg-[#00000083] bg-blend-darken"
          style={{
            backgroundImage: `url(${legalInformation})`,
            backgroundSize: "cover",
          }}
        >
          <div className="text-center text-white">
            <h1 className="text-[30px] md:text-[3.75rem] leading-tight">
              <Animatedwordchar text="Legal Information" />
            </h1>
          </div>
        </div>
      </main>

      {/* Partner section */}
      <ScrollAnimation>
        <div className="my-[1rem] mx-[1rem] max-sm:mx-[1rem] text-center">
          <h1 className="text-lg sm:text-xl md:text-2xl font-semibold my-4">
            Trusted collaborators
          </h1>
          {/* <Partners /> */}
        </div>
      </ScrollAnimation>
      {/* Partner section end */}

      <section className="flex justify-center items-center mt-[4rem] px-4">
        <div className="flex flex-col w-full md:w-3/4 lg:w-1/1 border p-8 rounded-md gap-4 bg-white shadow-lg">
          <h2
            style={{ fontSize: "24px", paddingTop: "0px" }}
            className="font-bold py-4 border-b"
          >
            Legal Information
          </h2>

          <div className="grid grid-col-1 md:grid-cols-2 gap-2 py-2">
            <p className="font-semibold">Company Name</p>
            <p className="font-bold">Maritime Info Lab Pvt Ltd.</p>
          </div>
          <div className="grid grid-col-1 md:grid-cols-2 gap-2 py-2">
            <p className="font-semibold">Address</p>
            <p>
              <a
                target="_blank"
                style={{ textDecoration: 'underline', color: 'navy' }}
                href="https://maps.app.goo.gl/xaY8TZzW4F5gkzXQ9"
              >
                B-431/432, Alphathum By Bhutani, Sector 90, Noida, Uttar
                Pradesh, India-201305
              </a>
            </p>
          </div>
          <div className="grid grid-col-1 md:grid-cols-2 gap-2 py-2">
            <p className="font-semibold">Contact</p>
            <p className="font-bold">
              <a href="tel:+919871626990"><u>+91 9871626990</u></a>
            </p>
          </div>

          <h2 style={{ fontSize: "24px" }} className="font-bold py-4 border-b">
            Domain Information - theoceann.ai
          </h2>
          <div className="grid grid-col-1 md:grid-cols-2 gap-2 py-2">
            <p className="font-semibold">Registrar</p>
            <p>
              <a style={{ textDecoration: 'underline', color: 'navy' }} href="http://GoDaddy.com">GoDaddy</a>
            </p>
          </div>
          <div className="grid grid-col-1 md:grid-cols-2 gap-2 py-2">
            <p className="font-semibold">Registration Date</p>
            <p>December 07, 2023</p>
          </div>
          <div className="grid grid-col-1 md:grid-cols-2 gap-2 py-2">
            <p className="font-semibold">Registrant</p>
            <p>
              <a
                target="_blank"
                href="https://maps.app.goo.gl/xaY8TZzW4F5gkzXQ9"
              >
                Maritime Info Lab Pvt Ltd. <br />
                <a
                  target="_blank"
                  style={{ textDecoration: 'underline', color: 'navy' }}
                  href="https://maps.app.goo.gl/xaY8TZzW4F5gkzXQ9"
                >
                  B-431/432, Alphathum By Bhutani, Sector 90, Noida, Uttar
                  Pradesh, India-201305
                </a>
              </a>
            </p>
          </div>
          <div className="grid grid-col-1 md:grid-cols-2 gap-2 py-2">
            <p className="font-semibold">Cloud Hosting</p>
            <p>
              <b>GoDaddy Inc.</b> 2155 E. GoDaddy Way Tempe, AZ 85284 United
              States
            </p>
          </div>

          <h2 style={{ fontSize: "24px" }} className="font-bold py-4 border-b">
            Domain Information - <span style={{}}>theoceann.ai</span>
          </h2>
          <div className="grid grid-col-1 md:grid-cols-2 gap-2 py-2">
            <p className="font-semibold">Registrar</p>
            <p>
              <a style={{ textDecoration: 'underline', color: 'navy' }} href="https://domains.google/">Google Domains</a>
            </p>
          </div>
          <div className="grid grid-col-1 md:grid-cols-2 gap-2 py-2">
            <p className="font-semibold">Registration Date</p>
            <p>February 15, 2022</p>
          </div>
          <div className="grid grid-col-1 md:grid-cols-2 gap-2 py-2">
            <p className="font-semibold">Registrant</p>
            <p>
              Maritime Info Lab Pvt Ltd. <br />
              <a
                target="_blank"
                style={{ textDecoration: 'underline', color: 'navy' }}
                href="https://maps.app.goo.gl/xaY8TZzW4F5gkzXQ9"
              >
                B-431/432, Alphathum By Bhutani, Sector 90, Noida, Uttar
                Pradesh, India-201305
              </a>
            </p>
          </div>
          <div className="grid grid-col-1 md:grid-cols-2 gap-2 py-2">
            <p className="font-semibold">Cloud Hosting</p>
            <p>
              <b>Amazon Web Services Inc.</b> 410 Terry Avenue North Seattle,
              WA 98109-5210, United States
            </p>
          </div>
        </div>
      </section>
    </>
  );
};

export default LegalInformation;
