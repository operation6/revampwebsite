import React from "react";
import Animatedwordchar from "../Componants/motion/animatewordchar";
import SocialMediaiconFooter from "../Componants/SocialMediaiconFooter";
const Mentorship = () => {
  return (
    <>
      <div
        style={{
          height: "90vh",
          display: "flex",
          paddingTop: "10rem",
          paddingLeft: "6rem",
          //   alignItems: "center",
          flexDirection: "column",
          //   justifyContent: "center",
        }}
      >
        <p className="lg:text-lg"> We are launching soon</p>
        <h2
          style={{ lineHeight: "8rem" }}
          className="lg:font-extrabold sm:text-2xl lg:text-6xl xl:text-[6rem] xl:leading-[4.5rem] mb-4 break-words bg-gradient-to-r from-[#28435c] via-[#2076c7] to-[#003e78] bg-clip-text text-transparent text-4xl "
        >
          <Animatedwordchar text="Coming Soon..." />
        </h2>
      </div>
      {/* <SocialMediaiconFooter /> */}
    </>
  );
};

export default Mentorship;
