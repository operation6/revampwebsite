import { Button, Flex, Input, Modal, Select, Spin, Tooltip } from "antd";
import companyLogo from "../../Componants/Assets/signup-img/company_logo.webp";
import Title from "antd/es/skeleton/Title";
import CountryData from "country-data";
import { useEffect, useState } from "react";
import { FaInfoCircle, FaRegEye, FaRegEyeSlash } from "react-icons/fa";
import { useNavigate } from "react-router";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import { signupService } from "../../services/all";
import { globalStore } from "../../store/globalStore";
import signUpValidationFunction from "../../validations/signup";
import styles from "./signup.module.css";
import { Images } from "../../assests";
const Signup = () => {
  const [signUpForm, setsignUpForm] = useState({
    first_name: "",
    last_name: "",
    company_name: "",
    email: "",
    phone_number: "",
    country_code: "+91",
    reg_no: '',
    country: "",
    dob: "",
    password: "",
    confirmPassword: "",
    rememberMe: false,
    tnc: false,
  });
  const [errors, setErrors] = useState({});
  const [countries, setCountries] = useState([]);
  const [selectedCountry, setSelectedCountry] = useState("");
  const { buttonDisabled } = globalStore();
  const [showPassword, setShowPassword] = useState(false);
  const [confirmPassword, setConfirmPassword] = useState(false);
  const [signupData, setSignupData] = useState({});
  const [loading, setLoading] = useState(false);
  const [isVerify, setIsVerify] = useState(false);
  const [isOtpSent, setIsOtpSent] = useState(false);
  const [timer, setTimer] = useState(0);
  const [otp, setOtp] = useState('');
  const [otpVerified, setOtpVerified] = useState(false);

  const { Option } = Select

  useEffect(() => {
    setCountries(CountryData.countries.all);
  }, []);
  const navigate = useNavigate();

  const handleChange = (field, value) => {
    const newForm = { ...signUpForm };
    newForm[field] = value;
    // console.log(newForm, "55");
    setsignUpForm(newForm);
  };

  const countryHandler = (userInput) => {
    setSelectedCountry(userInput.countryCallingCodes[0]);
    const newForm = { ...signUpForm };
    newForm.country_code = userInput.countryCallingCodes[0];
    newForm.country = userInput.name;
    setsignUpForm(newForm);
  };

  const keyHandler = (userInput) => {
    const { event, name } = userInput;
    // if (name === "company_name" && !/^[A-Za-z \b]+$/.test(event.key)) {
    //   event.preventDefault(); // Prevent entering numbers
    // }
    if (name === "first_name" && !/^[A-Za-z \b]+$/.test(event.key)) {
      event.preventDefault(); // Prevent entering numbers
    }
    if (name === "last_name" && !/^[A-Za-z \b]+$/.test(event.key)) {
      event.preventDefault(); // Prevent entering numbers
    }
    if (name === "email" && !/^[a-zA-Z0-9\-_.@]|Backspace$/.test(event.key)) {
      event.preventDefault();
    }
    if (
      name === "phone_number" &&
      !/^[0-9]|Backspace$|^ArrowLeft$|^ArrowRight$|^Control$|^a$|^c$|^v$/.test(
        event.key
      )
    ) {
      event.preventDefault();
    }
  };

  const handleSubmit = async (e) => {
    try {
      e.preventDefault();

      if (!otpVerified) {
        toast.dismiss()
        toast.error("Please verify your email first.");
        return;
      }
      const validationErrors = signUpValidationFunction(signUpForm);
      setErrors(validationErrors);
      // console.log(validationErrors);
      if (Object.keys(validationErrors).length < 1) {
        const newForm = { ...signUpForm };
        newForm.company_name = newForm.company_name
          .toLowerCase()
          .replace(/\s/g, "");
        const response = await signupService(newForm);
        console.log("response", response)
        if (response.status === 200) {
          toast.dismiss()
          toast.success("Signup success, please continue");

          localStorage.setItem(
            "oceanAllSignupData",
            JSON.stringify(signUpForm)
          );
          localStorage.setItem("oceanAllToken", response.token.token);
          const userData = atob(response?.token?.token?.split(".")[1]);
          localStorage.setItem("oceanAllUserData", userData);
          const parsedUserInfo = JSON.parse(userData);
          localStorage.setItem("isAdmin", response.token.is_admin);
          console.log("parsedinfo", parsedUserInfo)

          navigate("/profile");

        }
        else if (response?.status === 400 && response?.msg) {
          toast.error(response?.msg)
          setOtpVerified(false)
        }
        else {
          toast.dismiss()
          toast.error(
            `Account already exists with given ${Object.keys(response.error)[0]
            }`
          );
        }
      }
    } catch (error) {
      alert(error);
    }
  };

  function blurhandler(event) {
    event.preventDefault();
    const { name } = event.target;
    const validationErrors = signUpValidationFunction({
      ...signUpForm,
      [name]: signUpForm[name],
    });
    setErrors({ ...errors, [name]: validationErrors[name] });
  }

  // useEffect(() => {
  //   let interval;
  //   if (timer > 0) {
  //     interval = setInterval(() => {
  //       setTimer((prev) => prev - 1);
  //     }, 1000);
  //   }
  //   return () => clearInterval(interval);
  // }, [timer]);

  useEffect(() => {
    // Decrement timer every second
    const timer = setInterval(() => {
      setTimer((prevTime) => {
        if (prevTime <= 0) {
          clearInterval(timer); // Stop timer when it reaches 0
          return 0;
        }
        return prevTime - 1;
      });
    }, 1000);

    return () => clearInterval(timer); // Cleanup on component unmount
  }, [timer]);

  // Format time as MM:SS
  const formatTime = (time) => {
    const minutes = Math.floor(time / 60)
      .toString()
      .padStart(2, "0");
    const seconds = (time % 60).toString().padStart(2, "0");
    return `${minutes}:${seconds}`;
  };

  const isValidEmail = (email) => {
    const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    return emailRegex.test(email);
  };

  const handleVerification = async () => {

    if (signUpForm.email === "") {
      toast.dismiss()
      toast.error("Please enter email")
      return;
    }

    if (!isValidEmail(signUpForm.email)) {
      toast.dismiss()
      toast.error("Please enter valid email")
      return;
    }

    try {
      setLoading(true)
      const url = `${process.env.REACT_APP_BASE_URL}accounts/send-otp-directly`
      const resp = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: signUpForm.email,
        }),
      })

      if (resp.ok) {
        toast.success("OTP send successfully.")
        setIsOtpSent(true)
        setTimer(300);
        setIsVerify(true);
      } else {
        const data = await resp.json();
        toast.dismiss()
        toast.info(data.error)
        setIsVerify(false);
        setLoading(false)
      }

    } catch (err) {
      console.log(err)
      setIsVerify(false);
    }

    setLoading(false)
  }

  // confirm-otp

  const handleCloseModal = () => {
    console.log("Close Modal Functionality")
    setIsVerify(false)
  }

  const onChange = (text) => {
    console.log('onChange:', text);
    setOtp(text);
  };
  const sharedProps = {
    onChange,
  };

  const handleOtpVerification = async () => {

    try {
      setLoading(true)
      const url = `${process.env.REACT_APP_BASE_URL}accounts/confirm-otp`
      const resp = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: signUpForm.email,
          otp: otp,
        }),
      })

      const data = await resp.json()

      if (resp.ok) {
        toast.success("OTP verified successfully.")
        setOtpVerified(true)
        setIsVerify(false)
        setIsOtpSent(false)
        setLoading(false)
        setOtp("")
      } else {
        setOtp("")
        toast.dismiss()
        toast.error(data.error)
        setLoading(false)
      }

    } catch (err) {
      console.log(err)
    }
  }

  return (
    <>
      <div className={styles.signupBody}>
        <div className={styles.background}>
          <div className={styles.maincontent}>
            <div className={styles.formDiv}>
              <div className={styles.formContainer}>
                <div className={styles.formwidthbox}>
                  <div className={styles.companylogodiv}>
                    <img
                      src={companyLogo}
                      alt="Company Logo"
                      className={styles.companylogo}
                    />
                  </div>
                  <div className={styles.topheadingSignup}>
                    <h2 className={styles.signUp}>Sign Up</h2>
                    <p className={styles.subheading}>
                      Create your account by filling the information
                    </p>
                  </div>

                  <div className={styles.firstRow}>
                    <div className={styles.inputbox}>
                      <span>First Name</span>
                      <input
                        type="text"
                        value={signUpForm.first_name}
                        name="first_name"
                        id=""
                        onBlur={blurhandler}
                        onKeyDown={(event) => {
                          keyHandler({ event, name: "first_name" });
                        }}
                        onChange={(e) =>
                          handleChange("first_name", e.target.value)
                        }
                        placeholder="Enter First Name"
                        className={styles.inputdata}
                      />
                      <div className={styles.errorFirstname}>
                        {errors.first_name && (
                          <p className={styles.validationErrortext}>
                            {errors.first_name}
                          </p>
                        )}
                      </div>
                    </div>

                    <div className={styles.inputbox}>
                      <span>Last Name</span>
                      <input
                        type="text"
                        value={signUpForm.last_name}
                        onBlur={blurhandler}
                        name="last_name"
                        onKeyDown={(event) => {
                          keyHandler({ event, name: "last_name" });
                        }}
                        onChange={(e) =>
                          handleChange("last_name", e.target.value)
                        }
                        placeholder="Enter Last Name"
                        className={styles.inputdata}
                      />
                      <div className={styles.errorLastname}>
                        {errors.last_name && (
                          <p className={styles.validationErrortext}>
                            {errors.last_name}
                          </p>
                        )}
                      </div>
                    </div>
                  </div>

                  <div className={styles.firstRow}>
                    <div className={styles.inputbox}>
                      <span>Company Name</span>
                      <input
                        type="text"
                        value={signUpForm.company_name}
                        name="company_name"
                        onBlur={blurhandler}
                        // onKeyDown={(event) => {
                        //   keyHandler({ event, name: "company_name" });
                        // }}
                        onChange={(e) =>
                          handleChange("company_name", e.target.value)
                        }
                        placeholder="Enter Company Name "
                        className={styles.inputdata}
                      />
                      <div className={styles.errorCompanyname}>
                        {errors.company_name && (
                          <p className={styles.validationErrortext}>
                            {errors.company_name}
                          </p>
                        )}
                      </div>
                    </div>

                    <div className={styles.inputbox}>
                      <span>Company Registration No (Optional)</span>
                      <input
                        type="text"
                        value={signUpForm.reg_no}
                        name="reg_no"
                        onBlur={blurhandler}
                        // onKeyDown={(event) => {
                        //   keyHandler({ event, name: "reg_no" });
                        // }}
                        onChange={(e) =>
                          handleChange("reg_no", e.target.value)
                        }
                        placeholder="Enter Company No."
                        className={styles.inputdata}
                      />
                    </div>

                    <div className={styles.inputbox}>
                      <span>Country</span>
                      <select
                        id=""
                        className={styles.selectCountry}
                        defaultValue={selectedCountry}
                        onBlur={blurhandler}
                        name="country"
                        onChange={(e) => {
                          handleChange("country", e.target.value);
                          const selectedCountry = countries.find(
                            (country) => country.name === e.target.value
                          );
                          if (selectedCountry) {
                            countryHandler(selectedCountry);
                          }
                        }}
                      >
                        <option value="">Select Country</option>
                        {countries.map((country, index) => (
                          <option key={index} value={country.name}>
                            {country.name}
                          </option>
                        ))}
                      </select>

                      <div className={styles.errorCountry}>
                        {errors.country && (
                          <p className={styles.validationErrortext}>
                            {errors.country}
                          </p>
                        )}
                      </div>
                    </div>
                  </div>

                  <div className={styles.firstRow}>
                    <div className={styles.inputbox}>
                      <span>Email</span>
                      <div style={{ display: 'flex' }}>
                        <input
                          type="text"
                          value={signUpForm.email}
                          onBlur={blurhandler}
                          disabled={otpVerified}
                          name="email"
                          onKeyDown={(event) => {
                            keyHandler({ event, name: "email" });
                          }}
                          onChange={(e) => handleChange("email", e.target.value)}
                          placeholder="Enter Email Id"
                          className={styles.inputdata}
                        />
                        <button disabled={otpVerified} style={{ background: otpVerified ? 'green' : '' }} className={styles.verifyMail} onClick={() => handleVerification()}>{loading && !otpVerified ? <Spin /> : `${otpVerified ? 'Verified' : 'Verify Email'}`}</button>
                      </div>
                      <div className={styles.errorEmail}>
                        {errors.email && (
                          <p className={styles.validationErrortext}>{errors.email}</p>
                        )}
                      </div>
                    </div>

                    <div className={styles.inputbox}>
                      <span className={styles.span}>Mobile Number</span>
                      <div className={styles.mbdiv}>
                        <div className={styles.cselectdiv} style={{ maxWidth: "80px" }}>
                          {/* <div className={styles.codeSelector}> */}
                          <Select
                            value={selectedCountry}
                            onSelect={(value) => setSelectedCountry(value)}
                            placeholder="Select a country code"
                          >
                            {countries.map((country, index) =>
                              country.countryCallingCodes[0] ? (
                                <Option key={index} value={country.countryCallingCodes[0]}>
                                  {country.countryCallingCodes[0]}
                                </Option>
                              ) : null
                            )}
                          </Select>
                          {/* <p>{selectedCountry}</p> */}
                          {/* </div> */}
                          <div className={styles.errorCountrycode}></div>
                        </div>
                        <div className={styles.mobileDiv}>
                          <input
                            type="number"
                            value={signUpForm.phone_number}
                            onBlur={blurhandler}
                            name="phone_number"
                            onKeyDown={(event) => {
                              keyHandler({ event, name: "phone_number" });
                            }}
                            onChange={(e) =>
                              handleChange("phone_number", e.target.value)
                            }
                            placeholder="Enter Mobile Number"
                            className={styles.inputdataMob}
                          />
                          <div className={styles.errorPhonenumber}>
                            {errors.phone_number && (
                              <p className={styles.validationErrortext}>
                                {errors.phone_number}
                              </p>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className={styles.firstRow}>
                    <div className={styles.inputbox}>
                    <div style={{ display: 'flex', alignItems: 'center', gap: '2px', height: '100%' }}>
                      <span>Password</span>
                        <Tooltip title={"A valid password contains at least 1 lowercase letter, 1 uppercase letter, 1 number, and 1 special character, with a minimum length of 8 characters."} placement="top">
                          <FaInfoCircle fontSize={9} style={{ display: 'flex', alignItems: 'center' }} />
                        </Tooltip>
                    </div>
                      <div className={styles.passwordInputDiv}>
                        <div id="passwordInput" className={styles.eyeclass}>
                          <input
                            type={showPassword ? "text" : "password"}
                            value={signUpForm.password}
                            onBlur={blurhandler}
                            name="password"
                            onChange={(e) =>
                              handleChange("password", e.target.value)
                            }
                            placeholder="Enter password"
                            className={styles.inputdata}
                          />

                          <div
                            onClick={() => {
                              setShowPassword(prev => !prev);
                            }}
                            className={styles.eyeIcon}
                          >
                            {(!showPassword) ? <FaRegEye /> : <FaRegEyeSlash />}
                          </div>
                        </div>

                      </div>
                      <div className={styles.errorPassword}>
                        {errors.password && (
                          <p className={styles.passworderror}>{errors.password}</p>
                        )}
                      </div>
                    </div>
                    <div className={styles.inputbox}>
                      <span>Confirm Password</span>
                      <div className={styles.passwordInputDiv}>
                        <div id="passwordInput" className={styles.eyeclass}>
                          <input
                            type={confirmPassword ? "text" : "password"}
                            value={signUpForm.confirmPassword}
                            name="confirmPassword"
                            onBlur={blurhandler}
                            onChange={(e) =>
                              handleChange("confirmPassword", e.target.value)
                            }
                            placeholder="Confirm Password"
                            className={styles.inputdata}
                          />
                          <div
                            onClick={() => {
                              setConfirmPassword(prev => !prev);
                            }}
                            className={styles.eyeIcon}
                          >
                            {(!confirmPassword) ? <FaRegEye /> : <FaRegEyeSlash />}
                          </div>
                        </div>

                      </div>
                      <div className={styles.errorConfirmpassword}>
                        {errors.confirmPassword && (
                          <p className={styles.passworderror}>
                            {errors.confirmPassword}
                          </p>
                        )}
                      </div>
                    </div>
                  </div>

                  <div className={styles.remember}>
                    <label className={styles.remembermediv}>
                      <input
                        type="checkbox"
                        id="rememberMe"
                        checked={signUpForm.rememberMe}
                        onBlur={blurhandler}
                        onChange={(e) =>
                          handleChange("rememberMe", e.target.checked)
                        }
                        className={styles.rememberme}
                      />
                      Remember me
                    </label>
                  </div>
                  <div className={styles.remember}>
                    <label className={styles.remembermediv}>
                      <input
                        type="checkbox"
                        id="agreeToTerms"
                        checked={signUpForm.agreeToTerms}
                        onBlur={blurhandler}
                        name="tnc"
                        onChange={(e) => handleChange("tnc", e.target.checked)}
                        className={styles.rememberme}
                      />
                      <div>
                        {" "}
                        I accept the
                        <a style={{ marginLeft: "2px" }}>
                          Terms and conditions
                        </a>
                      </div>
                    </label>
                  </div>

                  <div className={styles.signinbox}>
                    <button
                      className={styles["signup_button"]}
                      disabled={buttonDisabled}
                      onClick={handleSubmit}
                    >
                      Sign Up
                    </button>
                  </div>
                  <p className={styles["login-link"]}>
                    Already have an account?{" "}
                    <Link
                      className={styles.loginLink}
                      onClick={() => {
                        navigate("/login");
                      }}
                    >
                      Login Here
                    </Link>
                  </p>
                </div>
              </div>
            </div>

            <div className={styles.gifDiv}>
              <img
                src={Images + "signup_img.png"}
                alt=""
                className={styles.gif}
              />
            </div>
          </div>
        </div>
      </div>

      <Modal open={isVerify} onCancel={handleCloseModal} footer={false} title="Verify OTP">
        <Flex gap="middle" align="flex-start" vertical className="website-otp-verification">
          <Title level={5}>With formatter (Upcase)</Title>
          <Input.OTP value={otp} formatter={(str) => str.toUpperCase()} {...sharedProps} />
        </Flex>
        <span style={{ color: timer === 0 ? 'red' : 'black', textAlign: 'center', display: 'block', marginTop: '10px' }}>
          {isOtpSent && timer !== 0 ? (
            <>OTP valid till: <strong>{formatTime(timer)}</strong></>
          ) : (
            'OTP expired.'
          )}
          {isOtpSent && timer !== 0 ? "" : <span onClick={() => handleVerification()}>{"   "}<u style={{ cursor: 'pointer', color: 'navy' }}>Please click to Resend.</u>{loading && <Spin style={{ fontSize: '12px' }} />}</span>}
        </span>
        <Button onClick={() => handleOtpVerification()} disabled={timer === 0 ? true : false} className={styles.verifyMail} style={{ width: '100px', margin: '0 auto', marginTop: '10px', display: 'block' }} >Submit</Button>
      </Modal>
    </>
  );
};

export default Signup;
