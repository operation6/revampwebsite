import React from "react";
import { Link } from "react-router-dom";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import {
  ScrollAnimation,
  animationVariants,
} from "../Componants/motion/scrollanimation";
import { Images } from "./../assests";

const Partners = () => {
  const cards = [
    Images + "bimco.png",
    Images + "Sertica.jpg",
    Images + "mongoDB.png",
    Images + "BunkerEx-logo-1.png",
    Images + "AWS.png",
    Images + "kpler.png",
    Images + "microsoft-logo.svg",
  ];

  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 5,
      slidesToSlide: 3,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
      slidesToSlide: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 100 },
      items: 1,
      slidesToSlide: 1,
    },
  };

  return (
    <ScrollAnimation>
      <div
        style={{ position: "unset !important" }}
        className="lg:mt-[1%] xl:mt-[1%] 2xl:mt-[2%] mb-12 "
      >
        <Carousel
          style={{ position: "unset !important" }}
          responsive={responsive}
          // containerClass="carousel-container"
          // removeArrowOnDeviceType={["tablet", "mobile"]}
          infinite
          autoPlay={true}
          autoPlaySpeed={3000}
          // transitionDuration={3000}
          pauseOnHover={true}
          // showDots={true}
          arrows={false}
          className="w-[85%] mx-auto z-0 sliderPartner"
        >
          {cards.map((src, index) => (
            // <Link key={index} to={cardLinks[index]}>
            <div className="sm:m-1 rounded-lg mx-auto w-full sm:w-[100%] flex flex-col justify-center">
              <div className="mx-[8px] my-[10px] rounded-lg cursor-default">
                <div
                  className="bg-white rounded-md flex justify-center items-center"
                  style={{ borderRadius: "20px" }}
                >
                  {" "}
                  {/* Add these classes */}
                  <img
                    src={src}
                    alt={`Card ${index + 1}`}
                    className="border rounded-md p-2 h-[150px] md:h-[100px] object-contain shadow w-full"
                  />
                </div>
              </div>
            </div>
            // </Link>
          ))}
        </Carousel>
      </div>
    </ScrollAnimation>
  );
};

export default Partners;
