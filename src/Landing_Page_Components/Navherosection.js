import React, { useEffect, useState } from "react";
// import home_image from "../Componants/Assets/navherosection-img/HeroImgHQ.webp";
// import home_image from "../Componants/Assets/navherosection-img/slider8.png";
import { motion } from "framer-motion";
import Animatedwordchar from "../Componants/motion/animatewordchar";
import Animatedword from "../Componants/motion/animateword";
// import SheenButton from "./motion/buttonsheenanimation";
import { useNavigate } from "react-router-dom";
import { Button, Modal } from "antd";
import { CloseOutlined } from "@ant-design/icons";
import { Images } from "../assests";
import SliderCard from "./SliderCard";
let home_image = Images + 'navherosection-img/HeroImgHQ.webp'
// let home_image = Images + 'navherosection-img/slider3.jpeg';
const Navherosection = () => {
  const navigator = useNavigate();
  const [selectedVideo, setSelectedVideo] = useState(null);
  const [videoModal, setVideoModal] = useState(false);

  useEffect(() => {
    const token = localStorage.getItem("oceanAllToken");
    console.log("token", token);
    if (token) {
      const userData = atob(token?.split(".")[1]);
      const parsedUserInfo = JSON.parse(userData);

      if (!parsedUserInfo?.is_subscribed) {
        setSelectedVideo(
          "https://www.youtube.com/embed/xDVJbYwROI0?si=0rrN0cU-_MMFx8nZ&autoplay=1&mute=1"
        );
        setVideoModal(true);
      }
    }
  }, []);

  return (
    <>
      <div className="flex justify-start relative min-h-[100vh] ">
        <div
          className="absolute w-full h-full object-cover"
          style={{
            minHeight: "100vh",
            zIndex: "-1",
            opacity: "1"
          }}
        >
          {/* Display image on small screens */}
          <img
            src="https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/mobile-homepage.jpg"
            alt="Background"
            className="object-cover w-[100%] h-[100vh] md:hidden"
          />

          {/* Display video on medium and larger screens */}
          <video
            src="https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/Blue+And+White+Digital+Background+Gradient+Jamboard+Background+1.mp4"
            autoPlay
            loop
            muted
            className="object-cover w-[100%] h-[100vh] hidden md:block"
          />
        </div>
        <motion.div className=" text-[#FFF] text-2xl z-10 mt-[3.8rem] w-[98vw] flex flex-col justify-end gap-[15%]">

          <div className="mt-[2.5rem] md:mt-[13rem] z-50  pl-[5%] sm:pl-[8%] ">
            <p className="font-regular  text-[14px] lg:text-[1.2rem] mb-6 drop-shadow-[0_3px_3px_rgba(0,0,0,0.9)]">
              <Animatedword text="Your all-in-one, AI-powered shipping solution" />
            </p>

            <h2 className="text-md sm:text-lg lg:text-2xl xl:text-[28px] lg:font-extrabold font-bold  sm:leading-tight xl:leading-[4.5rem] text-white drop-shadow-[0_3px_3px_rgba(0,0,0,0.9)] mb-1 sm:mb-2 break-words w-full">
              <Animatedwordchar text="AI That Does It All: From Sorting Emails to Planning Voyages and Managing Accounts, Effortless Integration at Every Step" />
            </h2>

            {/* <motion.button
              onClick={() => navigator("/demo")}
              whileHover={{ scale: 1.04, backgroundColor: "#003e78" }}
              className="max-sm:p-2 max-sm:px-5 px-6 py-2 rounded-[2rem] text-xs sm:text-sm md:text-md text-white bg-[#ec9107]"
            >
              Get started
            </motion.button> */}
          </div>
          <div className="mx-auto w-full z-50 ">
            <SliderCard />
          </div>
        </motion.div>

        {videoModal ? (
          <Modal
            visible={videoModal}
            footer={null}
            onCancel={() => setVideoModal(false)}
            centered
            width="60vw"
            style={{ top: 20 }}
            bodyStyle={{ padding: 0, position: "relative" }}
            closable={false}
          >
            <Button
              style={{
                color: "red",
                position: "absolute",
                top: 10,
                right: 10,
                zIndex: 10001,
              }}
              icon={<CloseOutlined />}
              onClick={() => {
                setSelectedVideo(null);
                setVideoModal(false);
              }}
            />

            <div
              style={{
                position: "relative",
                width: "100%",
                height: "60vh",
              }}
            >
              <iframe
                src={selectedVideo}
                frameBorder="0"
                allow="accelerometer; autoplay; muted; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
                title="YouTube video"
                style={{
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  width: "100%",
                  height: "100%",
                  transform: "translate(-50%, -50%)",
                  zIndex: 10000,
                }}
              ></iframe>
            </div>
          </Modal>
        ) : null}
      </div>

    </>
  );
};

export default Navherosection;
