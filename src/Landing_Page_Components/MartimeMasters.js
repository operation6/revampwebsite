import React, { useState } from "react";
// import analytics_img from "../Componants/Assets/image/analyticSection_img.png";
// import charteringSection_img from "../Componants/Assets/image/charteringSection_img.png";
// import financeSection_img from "../Componants/Assets/image/financeSection_img.png";
// import opertionSection_img from "../Componants/Assets/image/operationSection_img.png";
import { OceannVmSection } from "../Componants/OceannVmSection";
import { Images } from "../assests";
let analytics_img = Images + 'analyticSection_img.png'
let charteringSection_img = Images + 'charteringSection_img.png'
let financeSection_img = Images + 'financeSection_img.png'
let opertionSection_img = Images + 'operationSection_img.png'
export const MartimeMasters = () => {
  const [currentSection, setcurrentSection] = useState("Chartering");
  const oceannVmSections = [
    {
      route: "/product/chartering",
      title: "Chartering",
      heading: "Maritime Masters: Elevating Your Chartering Experience.",
      content:
        'Our Voyage Chartering Solutions simplify the entire chartering journey. We specialize in voyage estimation, route optimization, vessel scheduling, cargo planning, and freight contract management, delivering an integrated approach for maximum efficiency and success.',
      image: 'https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/Leonardo_Phoenix_10_Depict_a_futuristic_hightech_container_shi_3+1.jpg',
    },
    {
      route: "/product/operations",
      title: "Operations",
      heading: "OceanNavigo: Seamlessly Managing Maritime Ventures",
      content:
        "theoceann.ai capture the complete workflow of Port fixture operation for various kind of time charter and voyage charter contract which increase the productivity of operator by more than 20%.",
      image: opertionSection_img,
    },
    {
      route: "/product/oceann-finance",
      title: "Finance",
      heading: "Oceann Funds: Navigating Your Ship Financing Journey",
      content:
        "In this ever-evolving maritime landscape, securing appropriate funding for your ship cargo ventures is paramount. Let us guide you through the various aspects of ship cargo financing and illuminate the path to financial stability and operational excellence.",
      image: financeSection_img,
    },
    {
      route: "/product/oceann-analytics",
      title: "Analytics",
      heading: "Streamlined Insights for Strategic Navigation",
      content:
        "In the ever-evolving world of shipping and logistics, informed decision-making is the wind in your sails. Welcome to our Report & Analytics section, a dedicated space designed to equip shipping companies with the insights and tools they need to sail confidently through the complex waters of the maritime industry.",
      image: analytics_img,
    },
  ];

  return (
    <section className="oceannVmWrap">
      <div className="flex justify-center">
        <div className="text-center flex flex-col justify-center items-center">
          <p className="lg:w-[70%] flex justify-center items-center">
            OceannVM, the Integrated Freight Management Platform designed to
            streamline and enhance your freight operations, no matter where in
            the world you're sailing.
          </p>
        </div>
      </div>

      <main className="flex flex-col items-center justify-center">
        <div className="text-[#7C8598] max-md:flex-col flex items-center max-sm:justify-start max-md:bg-white bg-[#EFF2F8] rounded-[29px] my-[2rem] sm:my-[3rem]">
          {oceannVmSections.map(({ title, heading, content, image }) => {
            return (
              <button
                style={{
                  backgroundColor: currentSection === title && "#F39C12",
                  color: currentSection === title ? "#FFFFFF" : "#7C8598",
                }}
                onClick={() => setcurrentSection(title)}
                key={title}
                className="px-14 py-2 max-sm:px-4 max-sm:py-1 text-[white] rounded-3xl"
              >
                {title}
              </button>
            );
          })}
        </div>
        {oceannVmSections
          .filter(({ route, title, heading, content, image }) => {
            return title === currentSection;
          })
          .map(({ route, title, heading, content, image }) => {
            return (
              <OceannVmSection
                route={route}
                title={title}
                heading={heading}
                content={content}
                image={image}
                key={title}
              />
            );
          })}
      </main>
    </section>
  );
};
