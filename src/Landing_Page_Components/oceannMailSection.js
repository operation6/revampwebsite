import React, { useState, useEffect } from "react";
import mailDashboard from "../Componants/Assets/oceann-mail-offers/mail_dashboard_new.png";
import mailChat from "../Componants/Assets/oceann-mail-offers/mail_chat_new.png";
import mailInbox from "../Componants/Assets/oceann-mail-offers/mail_Inbox_new.png";
import { Navigate, useNavigate } from "react-router-dom";
import {
  ScrollAnimation,
  animationVariants,
} from "../Componants/motion/scrollanimation";
import { motion } from "framer-motion";
import { Images } from "../assests";

// let mailDashboard = Images +'mail_dashboard.png'
// let mailChat = Images + 'mail_chat.png'
// let mailInbox = Images + 'mail_Inbox.png'
const MailSection = () => {
  const Navigate = useNavigate();

  const cards = [
    {
      img: mailDashboard,
      title: "Effortless Email Parsing",
      content:
        "Oceann Mail's email parsing feature streamlines communication by automatically extracting and organizing vital shipping information from emails.With advanced parsing algorithms, Oceann Mail efficiently captures details like shipment tracking numbers, delivery dates, and shipping addresses. ",
      path: "/solution/oceann-mail",
    },

    {
      img: mailChat,
      title: "Unlock Trade Insights.",
      content:
        "Oceann Mail through dynamic communication trends ensures optimal email management and efficient message delivery solutions tailored to evolving user needs. Oceann Mail's adaptive capabilities are finely tuned to changing communication patterns, providing a responsive email solution.",
      path: "/solution/oceann-mail",
    },

    {
      img: mailInbox,
      title: 'AI-Driven Operational Intelligence.',
      content:
        "Operational intelligence is the cornerstone of Oceann Mail, providing a comprehensive understanding of seamless communication for smooth operation. Oceann Mail's operational intelligence empowers users with insightful data, enabling them to optimize mail workflows and ensure seamless communication.",
      path: "/solution/oceann-mail",
    },
  ];

  return (
    <>
      <div className="oceannMailOffers">
        <ScrollAnimation>
          <div className="flex flex-col lg:flex-row justify-between lg:my-[16px] gap-[30px] ">
            {cards.map((data) => (
              <div
                style={{
                  border: "1px solid",
                  borderColor: "#e0e0e0",
                }}
                className="flex flex-1 flex-col rounded-xl px-[24px] py-[24px] section-3-cart"
              >
                {/* <div className="my-[8px] rounded-xl border-[0.5px] border-[#e0e0e0]"> */}
                <div className="my-[8px] rounded-xl">
                  <img
                    src={data.img}
                    alt=""
                    className="rounded-xl"
                    style={{ borderRadius: "0.7rem" }}
                  />
                </div>
                <h1 className="text-[20px] font-bold mt-[16px]">
                  {data.title}
                </h1>
                <p className="text-[14px]">{data.content}</p>
                <div>
                  {" "}
                  <motion.button
                    style={{}}
                    onMouseOver={(e) => (e.target.style.color = "#ffffff")}
                    onMouseOut={(e) => (e.target.style.color = "#003E78")}
                    onClick={() => Navigate(data.path)}
                    whileHover={{ scale: 1.04, backgroundColor: "#003e78" }}
                    className="text-[#003E78] bg-white border-[#003E78] border-[2px] px-[24px] mt-4 py-[8px] rounded-[42px]"
                  >
                    Learn More
                  </motion.button>
                </div>
              </div>
            ))}
          </div>
        </ScrollAnimation>
      </div>
    </>
  );
};

export default MailSection;
