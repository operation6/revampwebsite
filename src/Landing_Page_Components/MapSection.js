import React, { useState, useEffect } from "react";
import styles from "../styles/map.module.css";
import {
  ScrollAnimation,
  animationVariants,
} from "../Componants/motion/scrollanimation";
// import decarbonization from "../Componants/Assets/map-section-img/decarbonizationImg.png";
// import aienableTrade from "../Componants/Assets/map-section-img/aiTradeImg.png";
// import aiEnableComm from "../Componants/Assets/map-section-img/aiEnableComm.png";
// import reportAnalysis from "../Componants/Assets/map-section-img/report&analysisImg.png";
// import vm from "../Componants/Assets/map-section-img/voyageManagement.png";

import { useNavigate } from "react-router-dom";
import { Images } from "../assests";
let decarbonization = Images + 'decarbonizationImg.png'
let aienableTrade = Images + 'aiTradeImg.png'
let aiEnableComm = Images + 'aiEnableComm.png'
let reportAnalysis = Images + 'report&analysisImg.png'
let vm = Images + 'voyageManagement.png'
const MapSection = () => {
  const [itemActive, setItemActive] = useState(0);
  const navigator = useNavigate();

  const bgContent = [
    {
      title: "OUR SOLUTIONS",
      heading: "Decarbonization",
      contentText:
        "Embrace the future of sustainable maritime solutions with our cutting-edge technologies for decarbonisation in shipping. Our innovative product Oceannzero strive to reduce carbon emissions, ensuring a cleaner and greener future for the maritime industry. ",
      image: decarbonization,
      path: "/product/oceann-zero",
    },
    {
      title: "OUR SOLUTIONS",
      heading: "AI Enabled Trade",
      contentText:
        "Revolutionize global trade with our AI-enabled solutions for the shipping industry. Seamlessly integrate advanced artificial intelligence into your logistics operations, optimizing routes, predicting demand, and enhancing overall efficiency.",
      image: aienableTrade,
      path: "/solution/oceann-ai",
    },

    {
      title: "OUR SOLUTIONS",
      heading: "Report & Analysis",
      contentText:
        "Elevate your shipping operations with our comprehensive reporting and analysis tools. Gain real-time insights into vessel performance, fuel consumption, and operational efficiency. ",
      image: reportAnalysis,
      path: "/solution/oceann-bi",
    },

    {
      title: "OUR SOLUTIONS",
      heading: "AI Enabled Communication",
      contentText:
        "Experience the future of maritime communication with our AI-enabled solutions. Elevate collaboration and efficiency through intelligent communication tools tailored for the shipping industry.",
      image: aiEnableComm,
      path: "/solution/oceann-mail",
    },
    {
      title: "OUR SOLUTIONS",
      heading: "Voyage Management",
      contentText:
        "Navigate the seas with confidence using our advanced voyage management solutions. Utilize predictive analytics to enhance decision-making, optimize fuel consumption, and minimize operational costs. ",
      image: vm,
      path: "/solution/oceann-vm",
    },
    // ... (other entries)
  ];

  const handleNext = () => {
    setItemActive((prevItemActive) =>
      prevItemActive === bgContent.length - 1 ? 0 : prevItemActive + 1
    );
  };

  const handlePrev = () => {
    setItemActive((prevItemActive) =>
      prevItemActive === 0 ? bgContent.length - 1 : prevItemActive - 1
    );
  };

  useEffect(() => {
    const intervalId = setInterval(() => {
      handleNext(); // Automatically move to the next thumbnail
    }, 6000); // Change the interval (in milliseconds) as needed

    return () => {
      clearInterval(intervalId); // Cleanup the interval on component unmount
    };
  }, [itemActive]);

  return (
    <>
      <ScrollAnimation>
        <div className={`tabSlider ${styles.slider}`}>
          <div className={styles.list}>
            {bgContent.map((data, index) => (
              <div
                key={index}
                className={`${styles.item} ${
                  index === itemActive ? styles.active : ""
                }`}
              >
                <img className={styles.sliderImg} src={data.image} alt="sliderImg" />
                <div className={styles.content}>
                  <p className={styles.title}>{data.title}</p>
                  <h2 className={styles.contentHeading}>{data.heading}</h2>
                  <p className={styles.discription}>{data.contentText}</p>
                </div>
              </div>
            ))}
          </div>

          <div className={styles.arrows}>
            <button id="previous" onClick={handlePrev}>
              {"<"}
            </button>
            <button id="next" onClick={handleNext}>
              {">"}
            </button>
          </div>

          <div className={`${styles.thumbnail}`}>
            {bgContent.map((data, index) => (
              <div
                key={index}
                className={`${styles.item} ${
                  index === itemActive ? styles.active : ""
                }`}
              >
                <a onClick={() => navigator(data.path)} className="pointer">
                  {" "}
                  <img
                    className={styles.thumbnailImg}
                    src={data.image}
                    alt="thumbnailImg"
                  />
                </a>

                <div className={styles.content}></div>
              </div>
            ))}
          </div>
        </div>
      </ScrollAnimation>
    </>
  );
};

export default MapSection;
