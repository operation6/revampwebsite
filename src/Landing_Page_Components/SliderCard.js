import React from "react";
import { Link } from "react-router-dom";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
// import oceanAiimg from "../Componants/Assets/slider_img/Oceann-Ai-img.png";
// import oceanmailimg from "../Componants/Assets/slider_img/Oceann-Mail-img.png";
// import oceannZeroimg from "../Componants/Assets/slider_img/Oceann-Zero-img.png";
// import oceannapiimg from "../Componants/Assets/slider_img/Oceann-Api-img.png";
// import oceannvmimg from "../Componants/Assets/slider_img/Oceann-vm-img.png";


import {
  ScrollAnimation,
  animationVariants,
} from "../Componants/motion/scrollanimation";
import { Images } from "../assests";
let oceanmailimg = Images + 'Oceann-Mail-img.png'
let oceanAiimg = Images + 'Oceann-Ai-img.png'
let oceannZeroimg = Images + 'Oceann-Zero-img.png'
let oceannapiimg = Images + 'Oceann-Api-img.png'
let oceannvmimg = Images + 'Oceann-vm-img.png'
const SliderCard = () => {
  const cards = [
    oceannvmimg,
    oceanmailimg,
    oceannZeroimg,
    oceanAiimg,
    oceannapiimg,
  ];
  const imageUrls = [
    "https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/JAHAZ.png",
    "https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/gujarathi+empire.png",
    "https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/Avneer-shipping.png",
    "https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/aryacorp.png",
    "https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/Ocean-Fortune-Marine__1_-removebg-preview.png",
    "https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/Multimax-Shipping-DMCC-logo__1.png",
    "https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/loadline-shipping.png",
    "https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/midas+global.png",
    "https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/Navya+shipping.png",
    "https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/reshamwala+shipbroker.png",
    "https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/trinity.png",
    "https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/WhatsApp_Image_2024-12-31_at_11.44.33_AM-removebg-preview.png"
  ];


  const text = [
    "Integrated Freight Management Platform",
    "AI Enable Communication Intelligence",
    "Decarbonisation",
    "AI Enable Trade Intelligence",
    "Connecting digital voids with effective bridges",
  ];
  // const cardLinks = [
  //   Images + "oceann-vm",
  //   Images + "oceann-mail",
  //   Images + "product/ocean-zero",
  //   Images + "oceann-ai",
  //   Images + "oceann-api",
  // ];

  const cardLinks = [
    "/solution/oceann-vm",
    "/solution/oceann-mail",
    "product/ocean-zero",
    "product/oceann-x",
    "/solution/oceann-ai",
    "/solution/oceann-api",
  ];


  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 5,
      slidesToSlide: 1,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
      slidesToSlide: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 100 },
      items: 1,
      slidesToSlide: 1,
    },
  };

  return (
    <ScrollAnimation>
      <div
        // style={{ position: "unset !important" }}
        className="mt-2 lg:mt-[1%] xl:mt-[2%] 2xl:mt-[8%] md:mb-8"
      >
        <Carousel
          style={{ position: "unset !important" }}
          responsive={responsive}
          // containerClass="carousel-container"
          removeArrowOnDeviceType={["tablet", "mobile"]}
          infinite
          autoPlay
          // autoPlaySpeed={3000}
          showDots={false}
          arrows={false}
          className="w-[85%] mx-auto z-0 sliderPartner"
        >
          {/* 
           <div className="sm:m-1 rounded-lg mx-auto w-full sm:w-[100%] flex flex-col justify-center">
              <div className="mx-[8px] my-[10px] rounded-lg cursor-default">
                <div
                  className="bg-white rounded-md flex justify-center items-center"
           */}
          {imageUrls.map((src, index) => (
            <Link key={index} to={cardLinks[index]}>
              <div className="sm:m-1 rounded-lg mx-auto w-full sm:w-[100%] flex flex-col justify-center">
                <div className="mx-[8px] my-[10px] rounded-lg cursor-default">
                  <div
                    className="bg-white rounded-md flex justify-center items-center"
                    style={{ borderRadius: "20px" }}
                  >
                    {" "}
                    {/* Add these classes */}
                    <img
                      src={src}
                      alt={`Card ${index + 1}`}
                      className="border rounded-md p-2 h-[65px] md:h-[120px] lg:h-[120px]  xl:h-[120px] object-contain shadow w-full"
                    />
                  </div>
                  {/* <div className="border-2 md:border-4 mt-2 border-yellow-500  "></div> */}
                  {/* <div className="text-center mx-2 text-[10px]">{text[index]}</div> */}
                </div>
              </div >
            </Link >
          ))}
        </Carousel >
      </div >
    </ScrollAnimation >
  );
};

export default SliderCard;
