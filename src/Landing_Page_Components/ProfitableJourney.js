import React from "react";
// import shipImg from "../Componants/Assets/image/home_last_img.png";
import { useNavigate } from "react-router-dom";
import { motion } from "framer-motion";
import { Images } from "../assests";
let shipImg = Images + 'home_last_img.png';
const ProfitableJourney = () => {
  const navigate = useNavigate();
  return (
    <>
      <div className="flex lg:gap-[2rem]">
        <div className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 xl:gap-16 max-sm:mt-6">
          <div className="img-cls pt-3 mr-2 max-sm:mb-6">
            <img src={shipImg} alt="oceannVM_Img2" />
          </div>

          <div className="flex2 pr-8">
            <div className="heading ">
              <h1 className="text-[2rem] max-sm:text-[1.2rem] xl:text-[2rem] text-black font-bold mb-3 xl:mt-4 leading-snug sm:leading-normal">
                Profitable Journey, Sustainable Future
              </h1>
            </div>

            <div className="xl:w-[92%]">
              Leverage cutting-edge technology to transform your maritime operations. Discover data-driven insights that optimize voyages, enhance profitability, and align your operations with sustainable practices. Experience the future of maritime efficiency today.
            </div>
            <motion.button
              onClick={() => navigate("/demo")}
              whileHover={{ scale: 1.04, backgroundColor: "#003e78" }}
              className="max-sm:p-2 max-sm:px-5 max-sm:mt-3 mt-4 px-6 py-2 rounded-[2rem] text-xs sm:text-sm md:text-md text-white bg-[#ec9107]"
            >
              Book Demo
            </motion.button>
          </div>
        </div>
      </div>
    </>
  );
};

export default ProfitableJourney;
