import { React, useState, useRef, useEffect } from "react";
import { useNavigate } from "react-router-dom";
// import mapImg from "./Assets/image/Rectangle4066.png";
import NumberAnimation from "../Componants/motion/numberAnimation";
import { motion } from "framer-motion";

export const Community = () => {
  const navigator = useNavigate();

  const [animateNumbers, setAnimateNumbers] = useState(false);
  const numberAnimationRef = useRef();

  const handleScroll = () => {
    const element = numberAnimationRef.current;
    if (!element) return;

    const elementTop = element.getBoundingClientRect().top;
    const windowHeight = window.innerHeight;

    if (elementTop < windowHeight) {
      setAnimateNumbers(true);
      window.removeEventListener("scroll", handleScroll);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const numbers = [
    {
      number: (
        <NumberAnimation
          value={100}
          duration={800}
          className="font-black"
          fontSize="text-[4rem]"
        />
      ),
      text: "+",
      line: "Vendor",
    },

    {
      number: (
        <NumberAnimation
          className="flex justify-center items-center text-[4rem] font-black"
          value={312}
          duration={800}
          fontSize="text-[4rem]"
        />
      ),
      text: "M$+",
      line: "Freight managed",
    },

    {
      number: (
        <NumberAnimation
          className="flex justify-center items-center text-[4.2rem] font-black"
          value={2}
          duration={400}
          fontSize="text-[4rem]"
        />
      ),
      text: "M+",
      line: "Email Volume",
    },

    {
      number: (
        <NumberAnimation
          className="flex justify-center items-center text-[4.2rem] font-black"
          value={157}
          duration={400}
          fontSize="text-[4rem]"
        />
      ),
      text: "+",
      line: "Tonnage managed",
    },
  ];
  return (
    <section className="milliionViews">
      <div className="flex flex-col">
        <div className="">
          {/* <div className="text-xl mt-2 ml-4 mb-2 max-sm:mx-0">
            <h3 className="text-center xl:text-[3rem] max-sm:mx-[1rem] max-sm:text-[1.8rem] font-bold">
              Join the Community
            </h3>
          </div> */}

          {/* <div className="text-center m-4 flex justify-center items-center">
            <p className="font-[poppins] max-sm:text-[.8rem] xl:w-[80%]">
              Be part of our community where influential clients, shaping global
              trade, unite under one roof. Regardless of their diversity, they
              share a common thread: reliance on TheOceann’s solutions essential
              tools in their daily operations.
            </p>
          </div> */}

          <div className="flex flex-col gap-[3rem] max-sm:gap-[1.5rem] bg-white flex-wrap items-center justify-center">
            <section>
              <div className="px-4 text-center">
                <div className="grid  grid-cols-1 sm:grid-cols-2 md:grid-cols-4 gap-12 max-sm:gap-4 ">
                  {/* <div class="bg-white flex flex-col justify-center items-center text-[#183165]  ">
                    <h2 class="text-[2.5rem] max-sm:text-[1.4rem] font-semibold">
                      <NumberAnimation value={100} duration={3000} fontSize="1.5em" />
                    </h2>
                    <p
                      className="text-[1rem] 
                    max-sm:text-[.7rem] text-center"
                    >
                      Vendor
                    </p>
                  </div> */}
                  {numbers.map((data) => (
                    <div class="flex flex-col justify-center items-center gap-0">
                      <span class="flex flex-row gap-[4rem] justify-center items-center text-[2.5rem] max-sm:text-[1.4rem] font-semibold">
                        <span className="flex justify-center items-center text-[2.2rem] lg:text-[2.5rem]">
                          {data.number}
                          {data.text}
                        </span>
                      </span>
                      <div className="flex ">
                        <p className="md-text-[1rem] font-semibold max-sm:text-[.7rem] text-center text-[#717172]">
                          {data.line}
                        </p>
                      </div>
                    </div>
                  ))}

                  {/* <div class=" flex flex-col justify-center items-center">
                    <h2 class="xl-text-[4rem] max-sm:text-[1.4rem] font-bold">
                      <span className="text-[4rem]"><NumberAnimation value={2} duration={1000} fontSize="1.5em" />M$+</span>
                    </h2>
                    <p
                      className="text-[1rem] 
                    max-sm:text-[.7rem] text-center"
                    >
                      Email Volume
                    </p>
                  </div>
                  <div class="bg-white flex flex-col justify-center items-center text-[#183165]  ">
                    <h2 class="text-[2.5rem] max-sm:text-[1.4rem] font-semibold">
                      <span><NumberAnimation value={157} duration={300} fontSize="1.5em" />+</span>
                    </h2>
                    <p
                      className="text-[1rem] 
                    max-sm:text-[.7rem] text-center"
                    >
                      Tonnage managed
                    </p>
                  </div> */}
                </div>
              </div>
            </section>

            <div className="text-center justify-center flex flex-col gap-1 bg-white pt-2 pb-6">
              <p className="text-[#183165] max-sm:px-[1rem] text-center  max-sm:text-[15px]">
                We measure our success by yours.
              </p>
              <motion.button
                onClick={() => navigator("/knowledge-Hub/events")}
                whileHover={{ scale: 1.04, backgroundColor: "#003e78" }}
                className="max-sm:p-2 max-sm:mt-3 mt-4 px-8 py-4 rounded-[2rem] text-xs sm:text-sm md:text-md text-white bg-[#ec9107]"
              >
                Explore Client Success Stories
              </motion.button>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
