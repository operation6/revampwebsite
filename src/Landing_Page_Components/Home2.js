import React from "react";
import { useNavigate } from "react-router-dom";

import { Community } from "./Community";
import MapSection from "./MapSection";
import { MartimeMasters } from "./MartimeMasters";
import Navherosection from "./Navherosection";
import Oceannzerocard from "../Componants/OceannZero-Components/Oceannzerocard";
import MailSection from "./oceannMailSection";
import ProfitableJourney from "./ProfitableJourney";
import TradingPlateform from "./TradingPlateform";
import VoyagePlanning from "../Componants/VoyagePlanning";
import HeroSection from "../Componants/heroSection";
import SliderCard from "./SliderCard";
import HomeOceann from "./HomeOceann";
import {
  ScrollAnimation,
  animationVariants,
} from "../Componants/motion/scrollanimation";
import Partners from "./Partners";

import { Menu } from "antd";
import { Link } from "react-router-dom"; // Adjust based on your routing setup
import MegaMenu from "../Componants/MegaMenu";

const { SubMenu } = Menu;

const Home2 = () => {
  const navigator = useNavigate();

  return (
    <>
      <div>
        <MegaMenu />

        <div className="font-serift">
          {/* <Navherosection /> */}
          <SliderCard />

          {/* Partner section */}
          <div className="mx-auto mb-[2rem] sm:mb-[3rem]">
            <h1 className="text-center text-lg sm:text-lg md:text-xl font-semibold mb-3 md:mb-4">
              Trusted collaborators
            </h1>
            <Partners />
          </div>
          {/* Partner section end */}

          <div className="w-[80%] mx-auto mb-[2rem] sm:mb-[3rem]">
            <HomeOceann />
          </div>
          {/* <section className="flex mx-[3rem]  justify-center">
            <main className="w-[100%]">
              <div className="flex justify-center">
                <div>
                  <h3 className="text-[#003E78] capitalize font-semibold text-[2rem]">
                    Oceann mail
                  </h3>
                  <hr className="w-[50%] h-1 bg-[#F39C12] rounded-sm" />
                </div>
              </div>
              <p
                className="text-center  text-[1.5rem] 
          my-3 font-semibold max-md:text-[1rem]"
              >
                Discover the evolution of maritime email solutions with
                OceannMail
              </p>
              <div className="flex  gap-[1rem] max-md:flex-col-reverse max-md:items-center justify-center">
                <div className="w-[50%] max-md:flex flex-col  max-md:w-[95%] max-sm:w-full max-sm:text-center mt-[3rem] max-sm:text-[.8rem] max-lg:mt-0">
                  <p className="font-[poppins] max-md:text-center">
                    Introducing OceannMail, the future of streamlined maritime
                    communications. Experience a revolution in maritime email
                    solutions, prioritizing speed and efficiency. Our technology
                    addresses existing platform inefficiencies to ensure your
                    emails are not just read but understood. Benefits include 2x
                    faster processing, augmented voyage efficiency, maximizing
                    voyagers per user, automated email sorting, and as we auto suggest ships for cargos for ships. Say goodbye to email
                    clutter and embrace a sea change in maritime communications
                    with OceannMail.
                  </p>
                  <button onClick={() => navigator("/solution/oceann-mail")} className="text-center max-sm:text-[.8rem] mt-[1.9rem] max-lg:mt-[1rem]    text-white text- px-[2rem] py-[.8rem] rounded-[30px] bg-[#F39C12] mx-auto">
                    LEARN MORE
                  </button>
                </div>
                <img
                  className="w-[50%] h-[80%] max-sm:w-full max-md:w-[95%]"
                  src={home_img_oceann_mail}
                  alt=""
                />
              </div>
            </main>
          </section> */}

          <ScrollAnimation>
            <div className="w-[80%] mx-auto mb-[2rem] sm:mb-[3rem]">
              <h1 className="text-center text-lg sm:text-lg md:text-xl font-semibold mb-3 md:mb-4">
                Oceann Voyage Manager
              </h1>
              <MartimeMasters />
            </div>
          </ScrollAnimation>
          <ScrollAnimation>
            <div className="w-[80%] mx-auto mb-[2rem] sm:mb-[3rem]">
              <h1 className="text-center text-lg sm:text-lg md:text-xl font-semibold mb-3 md:mb-4">
                Why Oceann Zero?
              </h1>
              {/* <p className=" text-center text-sm lg:text-lg sm:text-md md:text-md mb-4 xl:mb-12">
              Integrated Freight management platform
            </p> */}

              <Oceannzerocard />
            </div>
          </ScrollAnimation>
          <ScrollAnimation>
            <div className="w-[80%] mx-auto mb-[2rem] sm:mb-[3rem]">
              <h1 className="text-center text-lg sm:text-lg md:text-xl font-semibold mb-3 md:mb-4">
                What Oceann Mail Offers?
              </h1>
              <MailSection />
            </div>
          </ScrollAnimation>

          <ScrollAnimation className="flex justify-center items-center">
            <div className="w-[80%] mx-auto my-[3rem] sm:my-[5rem]">
              <TradingPlateform />
            </div>
          </ScrollAnimation>
          <ScrollAnimation>
            <div className="w-[90%] mx-auto my-[3rem] sm:my-[5rem] border-y-[1px] border-zinc-200">
              <div className="my-[1rem] sm:my-[3rem]">
                <Community />
              </div>
            </div>
          </ScrollAnimation>
          <ScrollAnimation>
            <div className="w-[90%] mx-auto my-[3rem] sm:my-[5rem]">
              <MapSection />
            </div>
          </ScrollAnimation>
          {/* <ScrollAnimation>   <VoyagePlanning /> </ScrollAnimation> */}
          <ScrollAnimation>
            <div className="w-[80%] mx-auto my-[3rem] sm:my-[5rem]">
              <ProfitableJourney />
            </div>
          </ScrollAnimation>
          {/* <HeroSection /> */}
        </div>
      </div>
    </>
  );
};

export default Home2;
