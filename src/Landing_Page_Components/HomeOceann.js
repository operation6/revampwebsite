import { motion } from "framer-motion";
import React, { useState } from "react";
import { InView } from "react-intersection-observer";
import { useNavigate } from "react-router-dom";
import "../App.css";
import { Images } from "../assests";
import OceanMailSlider from "./OceanMailSlider";
import OceanVMSlider from "./OceanVMSlider";
import OceanZeroSlider from "./OceanZeroSlider";
const OceannMail = Images + "oceannMail.png";
const OceannVM = Images + "oceannVM.png";
const vmLogo = Images + "oceanvmLogo.png";
const mailLogo = Images + "oceanmailLogo.png";
const zeroLogo = Images + "oceannzeroLogo.png";
const OceannZero = Images + "oceamnZero.png";
const mailChat = Images + "mail_chat.png";

const HomeOceann = () => {
  const Navigate = useNavigate();

  const [visibleSection, setVisibleSection] = useState(null);
  const handleIntersectionChange = (inView, entry) => {
    if (inView) {
      setVisibleSection(entry.target.id);
    }
  };

  const mainProduct = [
    {
      heading: "Oceann Voyage Manager",
      img: OceannVM,
      content:
        // "OceannVM, the Integrated Freight Management Platform designed to streamline and enhance your freight operations, no matter where in the world you're sailing.",
        "Step into the future of freight management with OceannVM, an AI-powered platform crafted to revolutionize maritime logistics. Designed to simplify complex operations, OceannVM empowers businesses with intelligent voyage optimization, seamless workflows, and unparalleled operational efficiency. Harness the power of real-time analytics and actionable insights to make smarter, faster decisions while reducing costs and maximizing profitability. Built for global connectivity, OceannVM ensures you stay in control, no matter where your journey takes you.",
      logo: vmLogo,
      path: "/solution/oceann-vm",
    },

    {
      heading: "Oceann Mail",
      img: mailChat,
      content:
        `Stay connected like never before with AI-powered email solutions designed for the maritime industry. Oceann Mail ensures fast, secure, and intelligent communication—even in low-bandwidth conditions.
Enjoy features like optimized voyage efficiency, intelligent email sorting, and automated cargo order management, empowering your operations with speed and precision`,
      logo: mailLogo,
      path: "/solution/oceann-mail",
    },

    {
      heading: "Oceann Zero",
      img: OceannZero,
      content:
        "Achieve your green goals with Oceann Zero, the ultimate solution for maritime decarbonization. Powered by AI, it optimizes fuel efficiency, ensures compliance with global regulations, and provides actionable insights to reduce emissions—all without compromising performance",
      logo: zeroLogo,
      path: "/product/oceann-zero",
    },
  ];

  return (
    <>
      <div className="flex flex-col lg:flex-row ourProducts">
        <InView
          as="div"
          className="flex flex-1 lg:h-[40vh] mb-[1rem] md:mb-[3rem] gap-0 lg:ml-6 top-[18%] lg:sticky xm:hidden sm:w-[100%]"
          onChange={handleIntersectionChange}
        >
          <div className="flex flex-col mb-2 md:mb-16">
            <p className="tracking-[6px] lg:tracking-[12px] pl-2 text-[14px]">
              SERVICES
            </p>
            <h2 className="xl:text-[4rem] lg:text-[4.8rem] font-bold text-[2.8rem] leading-[30px] lg:leading-[80px] flex gap-[8px] lg:gap-[0px] flex-col">
              <div>Our</div>
              <div>Products</div>

            </h2>

            {visibleSection === "Oceann Voyage Manager" && (
              <img
                style={{
                  width: "154px",
                  marginTop: "8px",
                }}
                className="vmlogo"
                src={vmLogo}
                alt="Oceann VM Logo"
              />
            )}
            {visibleSection === "Oceann Mail" && (
              <img
                style={{
                  width: "154px",
                  marginTop: "8px",
                }}
                className="maillogo"
                src={mailLogo}
                alt="Oceann Mail Logo"
              />
            )}
            {visibleSection === "Oceann Zero" && (
              <img
                style={{
                  width: "154px",
                  marginTop: "8px",
                }}
                className="zerologo"
                src={zeroLogo}
                alt="Oceann Zero Logo"
              />
            )}
          </div>
        </InView>

        <div
          className="flex flex-1 flex-col gap-[25px] md:gap-[72px] md-ml-16 mr-0 md:mr-8 mt-2 md:mt-4"
          style={{ alignItems: "end" }}
        >
          {mainProduct.map((data, index) =>
            data?.heading === "Oceann Zero" ? (
              <div id="scrollSectionId">
                <h2 className="text-[32px] font-black mb-2">{data.heading}</h2>
                <div
                  className="w-full sm:w-[510px] our-prods-slider"
                // style={{ width: "510px" }}
                >
                  <InView
                    key={data.heading}
                    id={data.heading}
                    as="div"
                    className="flex flex-col gap-[8px]"
                    onChange={handleIntersectionChange}
                  >
                    <OceanZeroSlider data={data} />
                  </InView>
                </div>
              </div>
            ) : data?.heading === "Oceann Voyage Manager" ? (
              <div id="scrollSectionId">
                <h2 className="text-[32px] font-black mb-2">{data.heading}</h2>
                <div
                  className="w-full sm:w-[510px] our-prods-slider"
                // style={{ width: "510px" }}
                >
                  <InView
                    key={data.heading}
                    id={data.heading}
                    as="div"
                    className="flex flex-col gap-[8px]"
                    onChange={handleIntersectionChange}
                  >
                    <OceanVMSlider data={data} />
                  </InView>
                </div>
              </div>
            ) : data?.heading === "Oceann Mail" ? (
              <div id="scrollSectionId">
                <h2 className="text-[32px] font-black mb-2">{data.heading}</h2>
                <div
                  className="w-full sm:w-[510px] our-prods-slider"
                // style={{ width: "510px" }}
                >
                  <InView
                    key={data.heading}
                    id={data.heading}
                    as="div"
                    className="flex flex-col gap-[8px]"
                    onChange={handleIntersectionChange}
                  >
                    <OceanMailSlider data={data} />
                  </InView>
                </div>
                {/* <img src={data.img} alt="" style={{ borderRadius: "0.7rem" }} />
                <p className="font-100 mt-2">{data.content}</p>
                <InView
                  key={data.heading}
                  id={data.heading}
                  as="div"
                  className="flex flex-col gap-[8px]"
                  onChange={handleIntersectionChange}
                >
                  <div>
                    {" "}
                    <motion.button
                      style={{}}
                      onMouseOver={(e) => (e.target.style.color = "#ffffff")}
                      onMouseOut={(e) => (e.target.style.color = "#003E78")}
                      onClick={() => Navigate(data.path)}
                      whileHover={{ scale: 1.04, backgroundColor: "#003e78" }}
                      className="text-[#003E78] bg-white border-[#003E78] border-[2px] px-[24px] mt-4 py-[8px] rounded-[42px]"
                    >
                      Learn More
                    </motion.button>
                  </div>
                </InView> */}
              </div>
            ) : (
              <div id="scrollSectionId" className="flex flex-col gap-[8px]">
                <h2 className="text-[32px] font-black">{data.heading}</h2>
                <img
                  src={data.img}
                  alt={data?.heading}
                  style={{ borderRadius: "0.7rem" }}
                />
                <p className="font-100 mt-2">{data.content}</p>
                <InView
                  key={data.heading}
                  id={data.heading}
                  as="div"
                  className="flex flex-col gap-[8px]"
                  onChange={handleIntersectionChange}
                >
                  <div>
                    {" "}
                    <motion.button
                      style={{}}
                      onMouseOver={(e) => (e.target.style.color = "#ffffff")}
                      onMouseOut={(e) => (e.target.style.color = "#003E78")}
                      onClick={() => Navigate(data.path)}
                      whileHover={{ scale: 1.04, backgroundColor: "#003e78" }}
                      className="text-[#003E78] bg-white border-[#003E78] border-[2px] px-[24px] mt-4 py-[8px] rounded-[42px]"
                    >
                      Learn More
                    </motion.button>
                  </div>
                </InView>
              </div>
            )
          )}
        </div>
      </div>
    </>
  );
};

export default HomeOceann;
