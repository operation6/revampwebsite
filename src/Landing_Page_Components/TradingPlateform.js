import React from "react";
// import backgroundImage2 from "../Componants/Assets/image/inclusiveImage.png";
import Animatedword from "../Componants/motion/animateword";
import Animatedwordchar from "../Componants/motion/animatewordchar";
import {
  ScrollAnimation,
  animationVariants,
} from "../Componants/motion/scrollanimation";
import { motion } from "framer-motion";
import { useNavigate } from "react-router-dom";
import { Images } from "../assests";
let backgroundImage2 = Images + 'inclusiveImage.png'
const TradingPlateform = () => {
  // const divStyle = {
  //   backgroundImage: `url(${backgroundImage2})`,
  //   backgroundSize: "cover",
  //   backgroundPosition: "center",

  //   display: "flex",
  //   justifyContent: "center",

  // };
  const navigator = useNavigate();
  return (
    <>
      {/* <div className="p-0 flex justify-center items-center text-center my-[1.5rem] mx-[3rem] max-sm:mx-[1rem] bg-cover bg-center  max-md:ml-[3rem] max-md:mr-[3rem] max-sm:ml-[1rem] max-sm:mr-[1rem] relative w-[90%]  "  > */}

      {/*         
        <div className="h-[100vh] flex justify-start relative">
        <div
          loop
          autoPlay
          muted
          className="absolute w-full h-full object-cover "
          style={{
            minHeight: "100vh",
            minWidth: "100%",
            width: "100%",
            height: "auto",
          }}
        >
           <div className="flex justify-center items-center"><img src={backgroundImage2} alt="abcimg" className="w-[82%] rounded-[2rem]"/></div>
        </div>
        <motion.div className="text-left text-[#FFF] text-2xl z-10 mt-[3.8rem] w-[100%] ml-[8%]">
          <div className="mt-[13rem] z-50 ">
            <p className="font-regular text-[12px] lg:text-[1.2rem] mb-6">
              <Animatedword text="Your all in one shipping solution" />
              <div className="w-[23.5%] h-[2px] bg-white rounded-md"></div>
            </p>

            <h2 className="text-[1.4rem]">
              <Animatedwordchar text="Commodities Trader" />
            </h2>
            
          </div>
        </motion.div>
      </div> */}
      {/* <p className=" absolute top-0 left-0 right-0 text-[2.6rem] max-sm:text-[0.8rem] max-md:text-[1rem] max-lg:text-[1.5rem]   max-sm:mt-[0.8rem] mt-[2.3rem] m-[1.2rem] text-center text-white">An Inclusive Trading Platform<span className="text-[#F39C12]"> {" "} catering To all stakeholders</span></p> */}
      {/* </div> */}

      <ScrollAnimation>
        <div className="relative h-[90vh]">
          <img
            src={backgroundImage2}
            alt="Parceling"
            className="w-full h-full object-cover object-center absolute top-0 left-0 z-0"
          />

          <div className="relative z-10 flex flex-col items-center justify-center h-full text-center  ">
            <h1 className="text-md sm:text-md md:text-[3.5rem] md:leading-[4rem] w-[80%] font-semibold text-white my-4">
              Make your shipping journey more easier
            </h1>

            <p className="text-xs sm:text-md w-[60%] text-white">
              Unlock the features of theoceann to make your shipping journey
              more smoother. It is your all in one shipping solution. Let’s make
              your shipping more easy
            </p>

            <motion.button
              onClick={() => navigator("/demo")}
              whileHover={{ scale: 1.04, backgroundColor: "#003e78" }}
              className="max-sm:p-2 max-sm:px-5 max-sm:mt-3 mt-4 px-6 py-2 rounded-[2rem] text-xs sm:text-sm md:text-md text-white bg-[#ec9107]"
            >
              Get started
            </motion.button>
          </div>
        </div>
      </ScrollAnimation>
    </>
  );
};

export default TradingPlateform;
