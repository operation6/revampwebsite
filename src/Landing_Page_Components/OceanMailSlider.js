import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import mailChat1 from "../Componants/Assets/oceann-mail-slider/1.png";
import mailChat2 from "../Componants/Assets/oceann-mail-slider/2.png";
import mailChat3 from "../Componants/Assets/oceann-mail-slider/3.png";
import mailChat4 from "../Componants/Assets/oceann-mail-slider/4.png";
import { Carousel } from "antd";
import { motion } from "framer-motion";

const OceanMailSlider = ({ data }) => {
  const Navigate = useNavigate();

  const contentStyle = {
    height: "160px",
    color: "#fff",
    lineHeight: "160px",
    textAlign: "center",
    background: "#364d79",
  };

  return (
    <div>
      <Carousel
        autoplay
        dotPosition="bottom"
        // arrows
        autoplaySpeed={3000}
        dots={{
          className: "custom-dots",
          activeClassName: "active-dot",
        }}
      >
        <div>
          <img
            src={mailChat1}

            alt={data?.heading}
            style={{ borderRadius: "0.7rem" }}
          />
          {/* <p className="font-100 mt-2">{data.content}</p> */}
          {/* <p className="font-100 mt-3">
          Experience a revolution in maritime email solutions, prioritizing
          speed and efficiency. Our technology addresses existing platform
          inefficiencies to ensure your emails are not just read but understood.
          Benefits include 2x faster processing, augmented voyage efficiency,
          maximizing voyagers per user, automated email sorting, and innovative
          tonnage and cargo order automation.
        </p> */}
          {/* <div>
          <motion.button
            style={{}}
            onMouseOver={(e) => (e.target.style.color = "#ffffff")}
            onMouseOut={(e) => (e.target.style.color = "#003E78")}
            onClick={() => Navigate(data.path)}
            whileHover={{
              // scale: 1.04,
              backgroundColor: "#003e78",
            }}
            className="text-[#003E78] bg-white border-[#003E78] border-[2px] px-[24px] mt-4 py-[8px] rounded-[42px]"
          >
            Learn More
          </motion.button>
        </div> */}
        </div>
        <div>
          <img
            src={mailChat2}

            alt={data?.heading}
            style={{ borderRadius: "0.7rem" }}
          />
          {/* <p className="font-100 mt-2">{data.content}</p> */}
          {/* <p className="font-100 mt-2">
          Experience a revolution in maritime email solutions, prioritizing
          speed and efficiency. Our technology addresses existing platform
          inefficiencies to ensure your emails are not just read but understood.
          Benefits include 2x faster processing, augmented voyage efficiency,
          maximizing voyagers per user, automated email sorting, and innovative
          tonnage and cargo order automation.
        </p> */}
          {/* <div>
          <motion.button
            style={{}}
            onMouseOver={(e) => (e.target.style.color = "#ffffff")}
            onMouseOut={(e) => (e.target.style.color = "#003E78")}
            onClick={() => Navigate(data.path)}
            whileHover={{
              // scale: 1.04,
              backgroundColor: "#003e78",
            }}
            className="text-[#003E78] bg-white border-[#003E78] border-[2px] px-[24px] mt-4 py-[8px] rounded-[42px]"
          >
            Learn More
          </motion.button>
        </div> */}
        </div>
        <div>
          <img
            src={mailChat3}

            alt={data?.heading}
            style={{ borderRadius: "0.7rem", }}
          />
          {/* <p className="font-100 mt-2">{data.content}</p> */}
          {/* <p className="font-100 mt-2">
          Experience a revolution in maritime email solutions, prioritizing
          speed and efficiency. Our technology addresses existing platform
          inefficiencies to ensure your emails are not just read but understood.
          Benefits include 2x faster processing, augmented voyage efficiency,
          maximizing voyagers per user, automated email sorting, and innovative
          tonnage and cargo order automation.
        </p> */}
          {/* <div>
          <motion.button
            style={{}}
            onMouseOver={(e) => (e.target.style.color = "#ffffff")}
            onMouseOut={(e) => (e.target.style.color = "#003E78")}
            onClick={() => Navigate(data.path)}
            whileHover={{
              // scale: 1.04,
              backgroundColor: "#003e78",
            }}
            className="text-[#003E78] bg-white border-[#003E78] border-[2px] px-[24px] mt-4 py-[8px] rounded-[42px]"
          >
            Learn More
          </motion.button>
        </div> */}
        </div>
        <div>
          <img
            src={'https://s3.ap-southeast-1.amazonaws.com/image.theoceann.com/website/assets/desktop-aaplication.png'}
            alt={data?.heading}
            style={{ borderRadius: "0.7rem", }}
          />
          {/* <p className="font-100 mt-2">{data.content}</p> */}
          {/* <p className="font-100 mt-2">
          Experience a revolution in maritime email solutions, prioritizing
          speed and efficiency. Our technology addresses existing platform
          inefficiencies to ensure your emails are not just read but understood.
          Benefits include 2x faster processing, augmented voyage efficiency,
          maximizing voyagers per user, automated email sorting, and innovative
          tonnage and cargo order automation.
        </p> */}
          {/* <div>
          <motion.button
            style={{}}
            onMouseOver={(e) => (e.target.style.color = "#ffffff")}
            onMouseOut={(e) => (e.target.style.color = "#003E78")}
            onClick={() => Navigate(data.path)}
            whileHover={{
              // scale: 1.04,
              backgroundColor: "#003e78",
            }}
            className="text-[#003E78] bg-white border-[#003E78] border-[2px] px-[24px] mt-4 py-[8px] rounded-[42px]"
          >
            Learn More
          </motion.button>
        </div> */}
        </div>
      </Carousel>

      <p className="font-100 mt-2">
        Stay connected like never before with AI-powered email solutions designed for the maritime industry. Oceann Mail ensures fast, secure, and intelligent communication—even in low-bandwidth conditions.Enjoy features like optimized voyage efficiency, intelligent email sorting, and automated cargo order management, empowering your operations with speed and precision
      </p>
      <div>
        <motion.button
          style={{}}
          onMouseOver={(e) => (e.target.style.color = "#ffffff")}
          onMouseOut={(e) => (e.target.style.color = "#003E78")}
          onClick={() => Navigate(data.path)}
          whileHover={{
            // scale: 1.04,
            backgroundColor: "#003e78",
          }}
          className="text-[#003E78] bg-white border-[#003E78] border-[2px] px-[24px] mt-4 py-[8px] rounded-[42px]"
        >
          Learn More
        </motion.button>
      </div>
    </div>
  );
};

export default OceanMailSlider;
