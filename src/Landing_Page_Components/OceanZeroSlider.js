import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import mailChat1 from "../Componants/Assets/oceann-zero-slider/1.png";
import mailChat2 from "../Componants/Assets/oceann-zero-slider/2.png";
import mailChat3 from "../Componants/Assets/oceann-zero-slider/3.png";
import mailChat4 from "../Componants/Assets/oceann-zero-slider/4.png";
import { Carousel } from "antd";
import { motion } from "framer-motion";

const OceanZeroSlider = ({ data }) => {
  const Navigate = useNavigate();

  const contentStyle = {
    height: "160px",
    color: "#fff",
    lineHeight: "160px",
    textAlign: "center",
    background: "#364d79",
  };

  return (
    <div>
      <Carousel
        autoplay
        dotPosition="bottom"
        // arrows
        autoplaySpeed={3000}
        dots={{
          className: "custom-dots",
          activeClassName: "active-dot",
        }}
      >
        <div>
          <img
            src={mailChat1}
            alt={data?.heading}
            style={{ borderRadius: "0.7rem" }}
          />
        </div>
        <div>
          <img
            src={mailChat2}
            alt={data?.heading}
            style={{ borderRadius: "0.7rem" }}
          />
        </div>
        <div>
          <img
            src={mailChat3}
            alt={data?.heading}
            style={{ borderRadius: "0.7rem", }}
          />
        </div>
        <div>
          <img
            src={mailChat4}
            alt={data?.heading}
            style={{ borderRadius: "0.7rem" }}
          />
        </div>
      </Carousel>

      <p className="font-100 mt-2">
        Achieve your green goals with Oceann Zero, the ultimate solution for maritime decarbonization. Powered by AI, it optimizes fuel efficiency, ensures compliance with global regulations, and provides actionable insights to reduce emissions—all without compromising performance.
      </p>
      <div>
        <motion.button
          style={{}}
          onMouseOver={(e) => (e.target.style.color = "#ffffff")}
          onMouseOut={(e) => (e.target.style.color = "#003E78")}
          onClick={() => Navigate(data.path)}
          whileHover={{
            // scale: 1.04,
            backgroundColor: "#003e78",
          }}
          className="text-[#003E78] bg-white border-[#003E78] border-[2px] px-[24px] mt-4 py-[8px] rounded-[42px]"
        >
          Learn More
        </motion.button>
      </div>
    </div>
  );
};

export default OceanZeroSlider;
